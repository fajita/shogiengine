package com.yeele.game.shogi.engine;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import com.yeele.util.ArrayListS;


public class Stand implements Subject{
	
	protected Shogi shogi;
	protected int order;
	ArrayListS<Koma> [] stack = new ArrayListS [Koma.NUM_KIND + 1];
	ArrayListS<Observer> observers;
	
	public Stand(Shogi shogi, int order){
		
		this.shogi = shogi;
		this.order = order;
		this.observers = new ArrayListS<Observer>();
		for(int i=Koma.FU; i <= Koma.OU; i++){
			stack[i] = new ArrayListS<Koma>();
		}
		
	}
	
	public void reset(){
		for(int kindId : Koma.BASIC_KIND_IDS){
			this.stack[kindId].clear();
		}
		this.observers.clear();
	}
	
	public int add(Koma k){
		int ret = Shogi.ERROR;
		if(Koma.isValid(k.kindId)){
			try{
				this.stack[Koma.getUnpromotedPiece(k.kindId)].add(k);
				this.register(k);
				ret = Shogi.SUCCESS;
			}catch (Exception e){
				e.printStackTrace();
			}
		}
			
		return ret;
	}
	
	public Koma get(int kindId){
		Koma k = null;
		if(Koma.isValid(kindId)){
			kindId = Koma.getUnpromotedPiece(kindId);
			int sz = this.stack[kindId].size();
			if(sz > 0){
				k = this.stack[kindId].get(sz - 1);
			}
		}
		return k;
	}
	
	public Koma fetch(int kindId){
		Koma k = null;
		if(Koma.isValid(kindId)){
			kindId = Koma.getUnpromotedPiece(kindId);
			int sz = this.stack[kindId].size();
			if(sz > 0){
				k = this.stack[kindId].get(sz - 1);
				this.stack[kindId].remove(sz - 1);
				this.unregister(k);
			}
		}
		return k;
	}
	
	
	public ArrayListS<Move> getAllHands(int order){
		ArrayListS<Move> moves = new ArrayListS<Move> ();
		for(Koma koma : this.shogi.getKomaListByOrder(order)){
			if(koma.isOnStand()){
				moves.addIfNot(koma.getMoves());
			}
		}
		return moves;
	}
	
	public ArrayListS<Move> getPrecedingMovesFor(Masu masu2backup){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		ArrayListS<Koma> komas = null;
		boolean [] alreadyAdded = new boolean[Koma.NUM_KIND+1];
		
		for(int kindId : Koma.BASIC_KIND_IDS){
			komas = this.shogi.getKomaList(kindId);
			for(Koma koma : komas){
				if(koma.getOrder() == order){
					if(koma.isOnBoard()){
						
					}else if(koma.isOnStand()){
						// only add one time if same koma kindid
						if(alreadyAdded[kindId] == false){
							moves.addAll(koma.getPrecedingMovesFor(masu2backup));
							alreadyAdded[kindId] = true;
						}
					}
				}
			}
		}
		return moves;
	}
	
	// preceding moves that the given kindId koma can made.
	public ArrayListS<Move> getPrecedingMovesFor(Masu masu2backup, int kindId){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		ArrayListS<Koma> komas = null;
		
		Koma koma = this.get(kindId);
		if(koma != null){
			moves.addAll(koma.getPrecedingMovesFor(masu2backup));
		}
		return moves;
	}
	
	public ArrayListS<Move> getMovesCanGoTo(int order, int square){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		Koma koma = null;
		boolean [] alreadyAdded = new boolean[Koma.NUM_KIND+1];
		Masu masu = this.shogi.board.getMasu(square);
		for(int kindId : Koma.BASIC_KIND_IDS){
			koma = this.get(kindId);
			if(koma != null){
				if(alreadyAdded[kindId] == false){
					moves.addAll(koma.getMovesCanGoTo(masu));
					alreadyAdded[kindId] = true;
				}
			}
		}
		return moves;
	}
	
	public int getOrder(){
		return this.order;
	}
	public int getCount(){
		int count = 0;
		for(int i=Koma.FU; i <= Koma.OU; i++){
			count += stack[i].size();
		}
		return count;
	}
	
	public int getCount(int kindId){
		return stack[Koma.getUnpromotedPiece(kindId)].size();
	}
	
	public int getCountOtherThanFu(){
		int count = 0;
		for(int i=Koma.KY; i <= Koma.OU; i++){
			count += stack[i].size();
		}
		return count;
	}
	
	public int getSquare(){
		assert(this.order != Shogi.ORDER_UNKNOWN);
		return (this.order == Shogi.BLACK) ? STAND_BLACK : STAND_WHITE; 
	}
	
	public ArrayListS<Koma> getStack(int kindId){
		return this.stack[kindId];
	}
	
	public String toString(){
		return this.toStringImpl(Shogi.LANGUAGE_JP);
	}
	
	public String toStringEN(){
		return this.toStringImpl(Shogi.LANGUAGE_EN);
	}
	
	public String toStringTrimed(){
		StringBuffer buff = new StringBuffer();
		for(int i=Koma.FU; i <= Koma.OU; i++){
			buff.append(Koma.getKomaNameTrimed(i));
			buff.append(String.valueOf(this.stack[i].size()));
		}
		return buff.toString();
	}
	
	protected String toStringImpl(int languageID){
		
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		for(int i=Koma.FU; i <= Koma.OU; i++){
			String komaStr = "";
			if(languageID == Shogi.LANGUAGE_EN){
				komaStr = Koma.getKomaName(i);
			}else if(languageID == Shogi.LANGUAGE_JP){
				komaStr = Koma.getKomaNameJP(i);
			}
			ps.printf("%s=%1d ", komaStr, this.stack[i].size());
		}
		str += buff.toString();
		return str;
	}
	
	/*****************************************
	 * subject interface
	 ******************************************/
	public int register(Observer obs){
		int ret = Shogi.SUCCESS;
		try{
			if(!this.observers.contains(obs))
				this.observers.add(obs);
		}catch(Exception o){
			ret = Shogi.ERROR;
		}
		return ret;
	}
	
	public int unregister(Observer obs){
		int ret = Shogi.SUCCESS;
		try{
			this.observers.remove(obs);
		}catch(Exception o){
			ret = Shogi.ERROR;
		}
		return ret;
	}
	
	public void notify(Observer obs) {
		obs.update();
	}
	
	public void notifyObservers() {
		for(int i=0; i<this.observers.size(); i++){
			this.observers.get(i).update();
		}
	}
	

	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	// there are used to represents square of Stand.
	public static final int STAND_BLACK = Shogi.BLACK;
	public static final int STAND_WHITE = Shogi.WHITE;
	public static final int STAND_UNKNOWN = Shogi.ORDER_UNKNOWN;
	
	public static int getStandPosition(int order){
		int ret = STAND_UNKNOWN;
		if(order == Shogi.BLACK) ret = STAND_BLACK;
		else if(order == Shogi.WHITE) ret = STAND_WHITE;
		return ret;
	}
	
	public static boolean isStandValid(int square){
		boolean ret = false;
		if(square == Stand.STAND_BLACK) ret = true;
		if(square == Stand.STAND_WHITE) ret = true;
		return ret;
	}
	
	public static int whichStand(int square){
		int ret = Shogi.ORDER_UNKNOWN;
		if(square == Stand.STAND_BLACK) ret = Shogi.BLACK;
		if(square == Stand.STAND_WHITE) ret = Shogi.WHITE;
		return ret;
	}

		
}