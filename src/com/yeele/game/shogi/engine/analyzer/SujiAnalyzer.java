package com.yeele.game.shogi.engine.analyzer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.stratagy.suji.AimDoubleOuSuji;
import com.yeele.game.shogi.engine.stratagy.suji.AimDoubleSuji;
import com.yeele.game.shogi.engine.stratagy.suji.BuildBaseCampSuji;
import com.yeele.game.shogi.engine.stratagy.suji.CloseCompanionKaPathSuji;
import com.yeele.game.shogi.engine.stratagy.suji.CloseOpponentKaPathSuji;
import com.yeele.game.shogi.engine.stratagy.suji.GetAwayIndirectSuji;
import com.yeele.game.shogi.engine.stratagy.suji.GetAwaySuji;
import com.yeele.game.shogi.engine.stratagy.suji.GetCloseToFloatSuji;
import com.yeele.game.shogi.engine.stratagy.suji.IncreaseIndirectSuji;
import com.yeele.game.shogi.engine.stratagy.suji.LikelyTakeInNextSuji;
import com.yeele.game.shogi.engine.stratagy.suji.MoveToBackedupSuji;
import com.yeele.game.shogi.engine.stratagy.suji.OpenCompanionKaPathSuji;
import com.yeele.game.shogi.engine.stratagy.suji.OuToSafeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PlaceOnSafeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PowerupDirectSuji;
import com.yeele.game.shogi.engine.stratagy.suji.SacrificeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.Suji;
import com.yeele.game.shogi.engine.stratagy.suji.TakeOpponentFreeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.TakeOpponentSuji;
import com.yeele.game.shogi.engine.stratagy.suji.ToSafeSuji;
import com.yeele.util.ArrayListS;

public class SujiAnalyzer{
	
	protected Shogi shogi;
	public HashMap<Integer, Suji> sujiIdMap = null;
	
	public SujiAnalyzer(Shogi shogi, HashMap<Integer, Suji> sujiIdMap){
		this.shogi = shogi;
		this.sujiIdMap = sujiIdMap;
	}
	
	public HashMap<Integer, Boolean> analyze(Move move){
		
		HashMap<Integer, Boolean> result = new HashMap<Integer, Boolean>();
		if(move!=null){
			for(Map.Entry<Integer, Suji> entry : this.sujiIdMap.entrySet()){
				int sujiId = entry.getKey();
				Suji suji = entry.getValue();
				result.put(sujiId, suji.examine(move));
			}
		}
		return result;
	}
	
	public String toString(HashMap<Integer, Boolean> result){
		String str = "";
		
		ByteArrayOutputStream buff;
		PrintStream ps;
		buff = new ByteArrayOutputStream();
		ps = new PrintStream(buff);
		
		for(Map.Entry<Integer, Boolean> entry : result.entrySet()){
			if(entry.getValue() == true){
				buff.reset();
				ps.printf("%s\n", this.sujiIdMap.get(entry.getKey()).toString());
				str += buff.toString();
			}
		}
		
		return str;
	}
	
}



