package com.yeele.game.shogi.engine.komas;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.KomaScope;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;


public class Fu extends Koma{
	
	
	public Fu(Shogi shogi, int order){
		
		super(
			   shogi
			 , order
			 , Koma.FU
  		     , new KomaScope(1, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			 , new KomaScope(1, 1, 1, 1, 1, 1, 0, 0, 0, 0)
		);
		
		
	}
	
	/*
	 * Fu msut be promoted on the edge no matter what.
	 * if the koma has the constraint, override this method.
	 */
	public boolean mustPromoteOn(Masu ms){
		return this.mustPromoteOn(ms.square);
	}
	
	public boolean mustPromoteOn(int sq) {
		boolean ret = false;
		int rank = Board.getRank(sq);
		if(this.order == Shogi.BLACK &&
		   rank == 1 &&
		   !this.isPromoted() 
		){
			ret = true;
		}else if(this.order == Shogi.WHITE &&
				 rank == 9 &&
				 !this.isPromoted() 
		){
			ret = true;
		}
		return ret;
	}
	
	protected void updateCapableOfKomaOnStand(Move lastMove){
		
		if(lastMove == null){
			// nullが渡されたら駒の情報を新規更新
			this.clearReference();
			boolean [] isFuOnFile = new boolean[Board.NUM_FILES + 1];
			for(Koma fu : this.shogi.getKomaList(true, false, this.order, Koma.FU, false)){
				int file = Board.getFile(fu.getSquare());
				isFuOnFile[file] = true;
			}
			
			for(Masu placableMasu : this.shogi.board.getPlacable()){
				if(isFuOnFile[Board.getFile(placableMasu.square)]==false){ // it's not nifu
					if(this.mustPromoteOn(placableMasu)==false){
						this.addMovable(placableMasu);
						this.reachable.addIfNot(placableMasu);
					}
				}
			}
		}else{
			Masu ms = null;
			ms = this.shogi.board.getMasu(lastMove.from);
			if(ms!=null){
				if(this.mustPromoteOn(ms)==false && 
				   this.shogi.board.existOnFile(this.order, Koma.FU, Board.getFile(lastMove.from))==false){
					this.addMovable(ms);
					this.reachable.addIfNot(ms);
					ms.register(this);
				}
			}
			
			ms = this.shogi.board.getMasu(lastMove.to);
			// lastMoveが駒がない升に動く場合。（captureする駒があるってことは以前そこに駒があったってこと。
			// つまり、this　駒は以前からその駒へは動けなかったってこと
			if(ms!=null && lastMove.komaToCapture == Koma.UK){
				ms.unregister(this);
				this.removeMovable(ms);
				this.reachable.remove(ms);
			}
		}
		
		
	}
	
	
	
	public ArrayListS<Move> getMovesCanGoTo(Masu aimedMasu){
		ArrayListS<Move> move_list = new ArrayListS<Move>();
		boolean canBePromotedOnAimedMasu = this.canBePromotedOn(aimedMasu);
		for(Move m : this.moves){
			if(m.to == aimedMasu.square){
				//　既に生成されたMoveを使うことで、ObjectのIDで比較するcontainsを成り立たせる。
				// 同時にメモリの節約にも少し効果がある。
				// 既に、成と歩成のMoveが生成されいる。　FU, HI, KAに関しては成れるところでは
				// なったほうがいいに決まっているので、その挙動だけoverrideする。
				if(canBePromotedOnAimedMasu){
					if(m.komaToMove == Koma.getPromotedPiece(this.kindId)){
						move_list.addIfNot(m);
					}
				}else{
					move_list.addIfNot(m);
				}
			}
		}
		return move_list;
	}
	
	public int getHandsToReach(int dstSquare){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Fu.getHandsToReach(this.order, this.isPromoted(), this.masu.square, dstSquare);
		}
		return hands; 
	}
	
	public int getHandsToReach(int from, int to){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Fu.getHandsToReach(this.order, this.isPromoted(), from, to);
		}
		return hands;
	}
	
	public static int getHandsToReach(int order, boolean isPromoted, int from, int to){
		int hands = -1;
		if(from == to){
			hands = 0;
		}else{
			if(isPromoted == false){
				int file = Board.getFile(from);
				int dstFile = Board.getFile(to);
				if(file == dstFile){
					hands = Math.abs(from - to);
				}
			}else if(isPromoted == true){
				hands = Ki.getHandsToReach(order, from, to);
			}
		}
		
		return hands; 
	}
	
	
	
	public static ArrayListS<Integer> getSquaresWhereYouNeedToBeFor(int aimedSquare, int order, boolean isPromoted){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		int sq;
		if(isPromoted == false){
			if(order == Shogi.BLACK){
				addIfSquareIsValid(sqs, aimedSquare + (  1));
			}else if(order == Shogi.WHITE){
				addIfSquareIsValid(sqs, aimedSquare + ( -1));
			}
		}else if(isPromoted){
			sqs = Ki.getSquaresWhereYouNeedToBeFor(aimedSquare, order, false);
		}
		
		return sqs;
	}
	
	protected void addMove(Masu ms){
		Move mv = null;
		
		if(this.isOnBoard()){
			// for FU it should always be promoted if it can.
			if (this.canBePromotedOn(ms)) {
				mv = new Move(this.order, this.masu.square, ms.square,
						Koma.getPromotedPiece(this.kindId), Koma.FLIP_YES,
						Koma.UK);
				this.moves.addIfNot(mv);
			}else{
				mv = new Move(this.order, this.masu.square, ms.square, this.kindId,
						Koma.FLIP_NO, Koma.UK);
				this.moves.addIfNot(mv);
			}
			
		}else if(this.isOnStand()){
			mv = new Move(this.order, this.stand.getSquare(), ms.square, this.kindId,
					Koma.FLIP_NO, Koma.UK);
			this.moves.addIfNot(mv);
		}
	}
	
}

