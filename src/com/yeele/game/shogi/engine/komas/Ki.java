package com.yeele.game.shogi.engine.komas;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.KomaScope;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;


public class Ki extends Koma{
	
	
	public Ki(Shogi shogi, int order){
		
		super(
			   shogi
			 , order
			 , Koma.KI
			 , new KomaScope(1, 1, 1, 1, 1, 1, 0, 0, 0, 0)
			 , new KomaScope(1, 1, 1, 1, 1, 1, 0, 0, 0, 0)
		);
		
		
	}
	
	public boolean amIPromotable(){
		return false;
	}
	
	public int getHandsToReach(int dstSquare){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Ki.getHandsToReach(this.order, this.masu.square, dstSquare);
		}
		return hands;
	}
	
	public int getHandsToReach(int from, int to){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Ki.getHandsToReach(this.order, from, to);
		}
		return hands;
	}
	
	public static int getHandsToReach(int order, int from, int to){
		int hands = -1;
		if(from == to){
			hands = 0;
		}else{
			int file = Board.getFile(from);
			int dstFile = Board.getFile(to);
			int diffFile = Math.abs(file-dstFile);
			int rank = Board.getRank(from);
			int dstRank = Board.getRank(to);
			int diffRank = Math.abs(rank-dstRank);
			if(Koma.isTowardForward(order, from, to)){
				hands = Math.max(diffFile, diffRank);
			}else{
				hands = diffFile + diffRank;
			}
		}
		return hands; 
	}
	
	public Move getMoveToGetClose(int dstSquare){
		Move move = null;
		int min = Board.MAX_DISTANCE + 1;
		
		if(this.getHandsToReach(dstSquare) != -1){
			for(Move m : this.moves){
				int hands = Ki.getHandsToReach(this.order, m.to, dstSquare);
				if(hands < min){
					// EXT. 
					// 現状、同じ距離のMoveは最後にヒットしたものを採用しているが、
					// 条件のよって動かしかたを帰れると粋
					min = hands;
					move = m;
				}
			}
		}
		return move;
	}
	
	public static ArrayListS<Integer> getSquaresWhereYouNeedToBeFor(int aimedSquare, int order, boolean isPromoted){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		if(isPromoted == false){
			if(order == Shogi.BLACK){
				addIfSquareIsValid(sqs, aimedSquare + (  1));
				addIfSquareIsValid(sqs, aimedSquare + ( 11));
				addIfSquareIsValid(sqs, aimedSquare + ( -9));
				addIfSquareIsValid(sqs, aimedSquare + ( 10));
				addIfSquareIsValid(sqs, aimedSquare + (-10));
				addIfSquareIsValid(sqs, aimedSquare + (-11));
			}else if(order == Shogi.WHITE){
				addIfSquareIsValid(sqs, aimedSquare + ( -1));
				addIfSquareIsValid(sqs, aimedSquare + (-11));
				addIfSquareIsValid(sqs, aimedSquare + (  9));
				addIfSquareIsValid(sqs, aimedSquare + (-10));
				addIfSquareIsValid(sqs, aimedSquare + ( 10));
				addIfSquareIsValid(sqs, aimedSquare + ( 11));
			}
		}else if(isPromoted){
			
		}
		
		return sqs;
	}
}

