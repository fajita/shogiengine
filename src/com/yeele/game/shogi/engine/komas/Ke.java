package com.yeele.game.shogi.engine.komas;

import java.io.*;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.KomaScope;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;


public class Ke extends Koma{
	
	
	public Ke(Shogi shogi, int order){
		
		super(
			   shogi
			 , order
			 , Koma.KE
			 , new KomaScope(0, 0, 0, 0, 0, 0, 0, 0, 1, 1)
			 , new KomaScope(1, 1, 1, 1, 1, 1, 0, 0, 0, 0)
		);
		
		
	}
	
	public boolean mustPromoteOn(Masu ms){
		return this.mustPromoteOn(ms.square);
	}
	
	public boolean mustPromoteOn(int sq) {
		boolean ret = false;
		int rank = Board.getRank(sq);
		if(this.order == Shogi.BLACK &&
		   ( ( rank == 1) || (rank == 2) ) &&
		   !this.isPromoted() 
		){
			ret = true;
		}else if(this.order == Shogi.WHITE &&
				 ( ( rank == 9) || (rank == 8) ) &&
				 !this.isPromoted() 
		){
			ret = true;
		}
		return ret;
	}
	
	public int getHandsToReach(int dstSquare){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Ke.getHandsToReach(this.order, this.isPromoted(), this.masu.square, dstSquare);
		}
		return hands; 
	}
	
	public int getHandsToReach(int from, int to){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Ke.getHandsToReach(this.order, this.isPromoted(), from, to);
		}
		return hands;
	}
	
	public static int getHandsToReach(int order, boolean isPromoted, int from, int to){
		int hands = -1;
		if(from == to){
			hands = 0;
		}else{
			if(isPromoted == false){
				int file = Board.getFile(from);
				int dstFile = Board.getFile(to);
				int diffFile = Math.abs(file-dstFile);
				int rank = Board.getRank(from);
				int dstRank = Board.getRank(to);
				int diffRank = Math.abs(rank-dstRank);
				
				if(diffRank % 2 == 0){
					int jumps = diffRank / 2;
					if(jumps == 1){
						if(diffFile == 1) hands = 1;
					}else if(jumps == 2){
						if(diffFile == 0 || diffFile == 2) hands = 2;
					}else if(jumps == 3){
						if(diffFile == 1 || diffFile == 3) hands = 3;
					}else if(jumps == 4){
						if(diffFile == 0 || diffFile == 2 || diffFile == 4) hands = 4;
					}
				}
			}else if(isPromoted == true){
				hands = Ki.getHandsToReach(order, from, to);
			}
		}
		return hands; 
	}
	
	public static ArrayListS<Integer> getSquaresWhereYouNeedToBeFor(int aimedSquare, int order, boolean isPromoted){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		if(isPromoted == false){
			if(order == Shogi.BLACK){
				addIfSquareIsValid(sqs, aimedSquare + ( 12));
				addIfSquareIsValid(sqs, aimedSquare + ( -8));
			}else if(order == Shogi.WHITE){
				addIfSquareIsValid(sqs, aimedSquare + (-12));
				addIfSquareIsValid(sqs, aimedSquare + (  8));
			}
		}else if(isPromoted){
			sqs = Ki.getSquaresWhereYouNeedToBeFor(aimedSquare, order, false);
		}
		
		return sqs;
	}
}

