package com.yeele.game.shogi.engine.komas;


import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.KomaScope;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;


public class Ka extends Koma{
	
	
	public Ka(Shogi shogi, int order){
		
		super(
			   shogi
			 , order
			 , Koma.KA
			 , new KomaScope(0, 0, 0, 0, 9, 9, 9, 9, 0, 0)
			, new KomaScope(1, 1, 1, 1, 9, 9, 9, 9, 0, 0)
		);
		
		this.throughNumber = 1;
	}
	
	public Koma getKomaBehindWall(Koma wallKoma){
		
		assert(this.walls.contains(wallKoma));
		
		Koma behindKoma = null;
		
		
		int file_wallKoma = Board.getFile(wallKoma.masu.square);
		int rank_wallKoma = Board.getRank(wallKoma.masu.square);
		int file = Board.getFile(this.masu.square);
		int rank = Board.getRank(this.masu.square);
		int i, td, dd, rd, ld;

		if(this.order == Shogi.BLACK){
			td = -1; dd = 1; rd = -1; ld = 1;
		}else if(this.order == Shogi.WHITE){
			td = 1; dd = -1; rd = 1; ld = -1;
		}else{
			return null;
		}
		
		boolean watchOut = false;
		Koma k;
		
		for(watchOut = false, i=1; i <= scope.tr; i++){
			k = this.shogi.board.getKoma(file + (i*(rd)), rank + (i*(td)));
			if(k!=null){
				if(watchOut==true){
					return k;
				}else{
					if(k==wallKoma){
						watchOut = true;
					}
				}
				
			}
		}
		for(watchOut = false, i=1; i <= scope.tl; i++){
			k = this.shogi.board.getKoma(file + (i*(ld)), rank + (i*(td)));
			if(k!=null){
				if(watchOut==true){
					return k;
				}else{
					if(k==wallKoma){
						watchOut = true;
					}
				}
				
			}
		}
		
		
		for(watchOut = false, i=1; i <= scope.dr; i++){
			k = this.shogi.board.getKoma(file + (i*(rd)), rank + (i*(dd)));
			if(k!=null){
				if(watchOut==true){
					return k;
				}else{
					if(k==wallKoma){
						watchOut = true;
					}
				}
				
			}
		}
		for(watchOut = false, i=1; i <= scope.dl; i++){
			k = this.shogi.board.getKoma(file + (i*(ld)), rank + (i*(dd)));
			if(k!=null){
				if(watchOut==true){
					return k;
				}else{
					if(k==wallKoma){
						watchOut = true;
					}
				}
				
			}
		}
		

		return behindKoma;
	}
	
	public Koma getWallKomaOnTheDirectionOf(int square) {
		Koma koma = null;
		if(Board.isSquareValid(square)){
			if(this.isOnBoard()){
				int direction = Board.getDirection(this.order, this.masu.square, square);
				int i, td, dd, rd, ld;
				int file = Board.getFile(this.masu.square);
				int rank = Board.getRank(this.masu.square);
				if(this.order == Shogi.BLACK){
					td = -1; dd = 1; rd = -1; ld = 1;
				}else if(this.order == Shogi.WHITE){
					td = 1; dd = -1; rd = 1; ld = -1;
				}else{
					return null;
				}
				Masu ms = null;
				if( direction == (Board.DIRECTION_TOP & Board.DIRECTION_RIGHT)) {
					for(i=1; i <= scope.tr; i++){
						ms = this.shogi.board.getMasu(file + (i*(rd)), rank + (i*(td)));
						if(ms==null) break;
						if(ms.isKoma()){
							koma = ms.koma;
							break;
						}
					}
				}else if( direction == (Board.DIRECTION_TOP & Board.DIRECTION_LEFT)) {
					for(i=1; i <= scope.tl; i++){
						ms = this.shogi.board.getMasu(file + (i*(ld)), rank + (i*(td)));
						if(ms==null) break;
						if(ms.isKoma()){
							koma = ms.koma;
							break;
						}
					}
				}else if( direction == (Board.DIRECTION_DOWN & Board.DIRECTION_RIGHT)){
					for(i=1; i <= scope.dr; i++){
						ms = this.shogi.board.getMasu(file + (i*(rd)), rank + (i*(dd)));
						if(ms==null) break;
						if(ms.isKoma()){
							koma = ms.koma;
							break;
						}
					}
				}else if( direction == (Board.DIRECTION_DOWN & Board.DIRECTION_LEFT)){
					for(i=1; i <= scope.dl; i++){
						ms = this.shogi.board.getMasu(file + (i*(ld)), rank + (i*(dd)));
						if(ms==null) break;
						if(ms.isKoma()){
							koma = ms.koma;
							break;
						}
					}
				}
			}
		}
		
		return koma;
	}
	
	public ArrayListS<Move> getMovesCanGoTo(Masu aimedMasu){
		ArrayListS<Move> move_list = new ArrayListS<Move>();
		boolean canBePromotedOnAimedMasu = this.canBePromotedOn(aimedMasu);
		for(Move m : this.moves){
			if(m.to == aimedMasu.square){
				//　既に生成されたMoveを使うことで、ObjectのIDで比較するcontainsを成り立たせる。
				// 同時にメモリの節約にも少し効果がある。
				// 既に、成と歩成のMoveが生成されいる。　FU, HI, KAに関しては成れるところでは
				// なったほうがいいに決まっているので、その挙動だけoverrideする。
				if(canBePromotedOnAimedMasu){
					if(m.komaToMove == Koma.getPromotedPiece(this.kindId)){
						move_list.addIfNot(m);
					}
				}else{
					move_list.addIfNot(m);
				}
			}
		}
		return move_list;
	}
	
	public int getHandsToReach(int dstSquare){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Ka.getHandsToReach(this.order, this.isPromoted(), this.masu.square, dstSquare);
		}
		return hands; 
	}
	
	public int getHandsToReach(int from, int to){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Ka.getHandsToReach(this.order, this.isPromoted(), from, to);
		}
		return hands;
	}
	
	public static int getHandsToReach(int order, boolean isPromoted, int from, int to){
		int hands = -1;
		if(from == to){
			hands = 0;
		}else{
			if(isOnSameLine(from, to)){
				hands = 1;
			}else{
				int oddOrEven = (Board.getFile(from) + Board.getRank(from)) % 2;
				int oddOrEven_dst = (Board.getFile(to) + Board.getRank(to)) % 2;
				
				if(isPromoted == false){
					if(oddOrEven == oddOrEven_dst){
						hands = 2;
					}
				}else if(isPromoted == true){
					if(oddOrEven == oddOrEven_dst){
						hands = 2;
					}else{
						hands = 3;
						// if match following condition, it can go in 2
						if(isOnSameLine(from, to+1)) hands = 2;
						else if(isOnSameLine(from, to-1)) hands = 2;
						else if(isOnSameLine(from, to+10)) hands = 2;
						else if(isOnSameLine(from, to-10)) hands = 2;
					}
				}
			}
		}
		return hands; 
	}
	
	
	// is the give square on the direct KA path?
	public static boolean isOnSameLine(int from, int to){
		boolean ret = false;
		int diff = Math.abs(from - to);
		int oddOrEven = (Board.getFile(from) + Board.getRank(from)) % 2;
		int oddOrEven_dst = (Board.getFile(to) + Board.getRank(to)) % 2;
		
		if(Board.isSquareValid(to)){
			if(diff % 11 == 0 || diff % 9 == 0) 
				if(oddOrEven == oddOrEven_dst)
					ret = true;
		}
		return ret;
	}
	
	public static ArrayListS<Integer> getSquaresWhereYouNeedToBeFor(int aimedSquare, int order, boolean isPromoted){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		int [] inc = {9, 11, -9, -11};
		int sq;
		for(int i=0; i<inc.length; i++){
			for(sq = aimedSquare+inc[i]; Board.isSquareValid(sq); sq+=inc[i]){ // 端にあたるまで
				sqs.add(sq);
			}
		}
		
		if(isPromoted){
			addIfSquareIsValid(sqs, aimedSquare + (  1));
			addIfSquareIsValid(sqs, aimedSquare + ( 10));
			addIfSquareIsValid(sqs, aimedSquare + (-10));
			addIfSquareIsValid(sqs, aimedSquare + ( -1));
		}
		
		return sqs;
	}
	
	protected void addMove(Masu ms){
		Move mv = null;
		
		if(this.isOnBoard()){
			// for KA should always promote if it can.
			if (this.canBePromotedOn(ms)) {
				mv = new Move(this.order, this.masu.square, ms.square,
						Koma.getPromotedPiece(this.kindId), Koma.FLIP_YES,
						Koma.UK);
				this.moves.addIfNot(mv);
			}else{
				mv = new Move(this.order, this.masu.square, ms.square, this.kindId,
						Koma.FLIP_NO, Koma.UK);
				this.moves.addIfNot(mv);
			}
		}else if(this.isOnStand()){
			mv = new Move(this.order, this.stand.getSquare(), ms.square, this.kindId,
					Koma.FLIP_NO, Koma.UK);
			this.moves.addIfNot(mv);
		}
	}
}

