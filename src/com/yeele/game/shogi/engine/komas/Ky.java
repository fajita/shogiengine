package com.yeele.game.shogi.engine.komas;


import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.KomaScope;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;


public class Ky extends Koma{
	
	
	public Ky(Shogi shogi, int order){
		
		super(
			   shogi
			 , order
			 , Koma.KY
			 , new KomaScope(9, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			, new KomaScope(1, 1, 1, 1, 1, 1, 0, 0, 0, 0)
		);
		
		this.throughNumber = 1;
	}

	public void flip() {
		
		//before flip
		if (Koma.isPromoted(this.kindId)) { // 不成りになる
			this.throughNumber = 1;
		} else { //成る
			// 成ると、金になるから、通し駒ではなくなる
			this.throughNumber = 0;
		}
		super.flip();
	}
	
	public boolean mustPromoteOn(Masu ms){
		return this.mustPromoteOn(ms.square);
	}
	
	public boolean mustPromoteOn(int sq) {
		boolean ret = false;
		int rank = Board.getRank(sq);
		if(this.order == Shogi.BLACK &&
		   rank == 1 &&
		   !this.isPromoted() 
		){
			ret = true;
		}else if(this.order == Shogi.WHITE &&
				 rank == 9 &&
				 !this.isPromoted() 
		){
			ret = true;
		}
		return ret;
	}
	
	public Koma getKomaBehindWall(Koma wallKoma){
		
		assert(this.walls.contains(wallKoma));
		
		Koma behindKoma = null;
		
		
		int file_wallKoma = Board.getFile(wallKoma.masu.square);
		
		int file = Board.getFile(this.masu.square);
		int rank = Board.getRank(this.masu.square);
		int i, td, dd, rd, ld;

		if(this.order == Shogi.BLACK){
			td = -1; dd = 1; rd = -1; ld = 1;
		}else if(this.order == Shogi.WHITE){
			td = 1; dd = -1; rd = 1; ld = -1;
		}else{
			return null;
		}
		
		boolean watchOut = false;
		Koma k;
		if( file_wallKoma == file){
			// serach file
			for(watchOut = false, i=1; i <= scope.t; i++){
				k = this.shogi.board.getKoma(file, rank + (i*(td)));
				if(k!=null){
					if(watchOut==true){
						return k;
					}else{
						if(k==wallKoma){
							watchOut = true;
						}
					}
					
				}
			}
		}

		return behindKoma;
	}
	
	public Koma getWallKomaOnTheDirectionOf(int square) {
		Koma koma = null;
		if(Board.isSquareValid(square)){
			if(this.isOnBoard()){
				if(this.isUnpromoted()){
					int direction = Board.getDirection(this.order, this.masu.square, square);
					int i, td, dd, rd, ld;
					int file = Board.getFile(this.masu.square);
					int rank = Board.getRank(this.masu.square);
					if(this.order == Shogi.BLACK){
						td = -1; dd = 1; rd = -1; ld = 1;
					}else if(this.order == Shogi.WHITE){
						td = 1; dd = -1; rd = 1; ld = -1;
					}else{
						return null;
					}
					Masu ms = null;
					if( direction == (Board.DIRECTION_TOP)) {
						for(i=1; i <= scope.t; i++){
							ms = this.shogi.board.getMasu(file, rank + (i*(td)));
							if(ms==null) break;
							if(ms.isKoma()){
								koma = ms.koma;
								break;
							}
						}
					}
				}
			}
		}
		
		return koma;
	}
	
	public int getHandsToReach(int dstSquare){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Ky.getHandsToReach(this.order, this.isPromoted(), this.masu.square, dstSquare);
		}
		return hands; 
	}
	
	public int getHandsToReach(int from, int to){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Ky.getHandsToReach(this.order, this.isPromoted(), from, to);
		}
		return hands;
	}
	
	public static int getHandsToReach(int order, boolean isPromoted, int from, int to){
		int hands = -1;
		if(from == to){
			hands = 0;
		}else{
			if(isPromoted == false){
				int file = Board.getFile(from);
				int dstFile = Board.getFile(to);
				if(file == dstFile){
					hands = 1;
				}
			}else if(isPromoted == true){
				hands = Ki.getHandsToReach(order, from, to);
			}
		}
		return hands; 
	}
	
	public static ArrayListS<Integer> getSquaresWhereYouNeedToBeFor(int aimedSquare, int order, boolean isPromoted){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		if(isPromoted == false){
			if(order == Shogi.BLACK){
				for(int sq = aimedSquare+1; Board.isSquareValid(sq); sq++){ // 端にあたるまで
					sqs.add(sq);
				}
			}else if(order == Shogi.WHITE){
				for(int sq = aimedSquare-1; Board.isSquareValid(sq); sq--){ // 端にあたるまで
					sqs.add(sq);
				}
			}
		}else if(isPromoted){
			sqs = Ki.getSquaresWhereYouNeedToBeFor(aimedSquare, order, false);
		}
		
		return sqs;
	}
	
}

