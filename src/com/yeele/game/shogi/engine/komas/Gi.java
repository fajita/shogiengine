package com.yeele.game.shogi.engine.komas;


import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.KomaScope;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;


public class Gi extends Koma{
	
	
	public Gi(Shogi shogi, int order){
		
		super(
			   shogi
			 , order
			 , Koma.GI
			 , new KomaScope(1, 0, 0, 0, 1, 1, 1, 1, 0, 0)
			 , new KomaScope(1, 1, 1, 1, 1, 1, 0, 0, 0, 0)
		);
		
		
	}
	
	public int getHandsToReach(int dstSquare){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Gi.getHandsToReach(this.order, this.isPromoted(), this.masu.square, dstSquare);
		}
		return hands; 
	}
	
	public int getHandsToReach(int from, int to){
		int hands = -1;
		if(this.isOnBoard()){
			hands = Gi.getHandsToReach(this.order, this.isPromoted(), from, to);
		}
		return hands;
	}
	
	public static int getHandsToReach(int order, boolean isPromoted, int from, int to){
		int hands = -1;
		if(from == to){
			hands = 0;
		}else{
			if(isPromoted == false){
				int file = Board.getFile(from);
				int dstFile = Board.getFile(to);
				int diffFile = Math.abs(file-dstFile);
				int rank = Board.getRank(from);
				int dstRank = Board.getRank(to);
				int diffRank = Math.abs(rank-dstRank);
				
				if(diffFile == diffRank){ // 斜めに進む
					hands = diffFile;
				}else{
					if(Koma.isTowardBackward(order, from, to)){
						// 斜め or/and 直下がりの組み合わせ
						if(diffFile == 0){ // 直下がりのみ
							if(diffRank % 2 == 0){
								hands = diffRank;
							}else if(diffRank % 2 == 1){
								hands = (diffRank/2) + 3;
							}
						}else{ // 斜めプラス直下がりの組み合わせ
							if(diffFile > diffRank){ // このパターンは、斜めと横に動く
								int diffsum = diffFile + diffRank;
								if(diffsum % 2 == 1){ // odds
									hands = Math.max(diffFile, diffRank) + 1;
								}else if(diffsum % 2 == 0){ // even
									hands = Math.max(diffFile, diffRank);
								}
							}else if(diffRank > diffFile){ // このパターンは、斜めと真下
								int diffsum = diffFile + diffRank;
								if(diffsum % 2 == 1){ // odds
									hands = Math.max(diffFile, diffRank) + 2;
								}else if(diffsum % 2 == 0){ // even
									hands = Math.max(diffFile, diffRank);
								}
							}else{
								// これはない。diffFile == diffRankの条件が上のほうにあります。
							}
						}
					}else{
						// 斜め or/and 直上がりの組み合わせ
						if(diffFile == 0){ // 直上がりのみ
							hands = diffRank;
						}else{ // 斜めプラス直の組み合わせ
							int diffsum = diffFile + diffRank;
							if(diffsum % 2 == 1){ // odds
								hands = Math.max(diffFile, diffRank) + 1;
							}else if(diffsum % 2 == 0){ // even
								hands = Math.max(diffFile, diffRank);
							}
						}
					}
				}
			}else if(isPromoted == true){
				hands = Ki.getHandsToReach(order, from, to);
			}
		}
		return hands; 
	}
	
	public static ArrayListS<Integer> getSquaresWhereYouNeedToBeFor(int aimedSquare, int order, boolean isPromoted){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		if(isPromoted == false){
			if(order == Shogi.BLACK){
				addIfSquareIsValid(sqs, aimedSquare + (  1));
				addIfSquareIsValid(sqs, aimedSquare + ( 11));
				addIfSquareIsValid(sqs, aimedSquare + ( -9));
				addIfSquareIsValid(sqs, aimedSquare + (  9));
				addIfSquareIsValid(sqs, aimedSquare + (-11));
			}else if(order == Shogi.WHITE){
				addIfSquareIsValid(sqs, aimedSquare + ( -1));
				addIfSquareIsValid(sqs, aimedSquare + (-11));
				addIfSquareIsValid(sqs, aimedSquare + (  9));
				addIfSquareIsValid(sqs, aimedSquare + ( -9));
				addIfSquareIsValid(sqs, aimedSquare + ( 11));
			}
		}else if(isPromoted){
			sqs = Ki.getSquaresWhereYouNeedToBeFor(aimedSquare, order, false);
		}
		
		return sqs;
	}
}

