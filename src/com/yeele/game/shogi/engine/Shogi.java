package com.yeele.game.shogi.engine;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Iterator;

import com.yeele.game.shogi.engine.analyzer.SujiAnalyzer;
import com.yeele.game.shogi.engine.kifu.CSAKifu;
import com.yeele.game.shogi.engine.kifu.KifuIF;
import com.yeele.game.shogi.engine.kifu.KitayamaCSAKifu;
import com.yeele.game.shogi.engine.komas.Fu;
import com.yeele.game.shogi.engine.komas.Gi;
import com.yeele.game.shogi.engine.komas.Hi;
import com.yeele.game.shogi.engine.komas.Ka;
import com.yeele.game.shogi.engine.komas.Ke;
import com.yeele.game.shogi.engine.komas.Ki;
import com.yeele.game.shogi.engine.komas.Ky;
import com.yeele.game.shogi.engine.komas.Ou;
import com.yeele.game.shogi.engine.player.Player;
import com.yeele.game.shogi.exceptions.ShogiException;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class Shogi{
	
	// Mandatory:
	// 2 players
	// board, komas, stand
	// kifu
	
	// Optional:
	// arm-rest
	// place( tatami room )
	// staff: narrator, kifu
	
	public CSAKifu kifu;
	public Board board;
	public Stand [] stand = new Stand[2];
	public KomaBox box;
	public ArrayListS<Koma> ou = new ArrayListS<Koma>();
	public ArrayListS<Koma> hi = new ArrayListS<Koma>();
	public ArrayListS<Koma> ka = new ArrayListS<Koma>();
	public ArrayListS<Koma> ki = new ArrayListS<Koma>();
	public ArrayListS<Koma> gi = new ArrayListS<Koma>();
	public ArrayListS<Koma> ke = new ArrayListS<Koma>();
	public ArrayListS<Koma> ky = new ArrayListS<Koma>();
	public ArrayListS<Koma> fu = new ArrayListS<Koma>();
	
	public Player [] player;
	
	public Shogi() throws ShogiException{
		
		String basepath = System.getProperty("user.dir");
		File hirateFile = new File(basepath, "/kifu/" + HIRATE_KIFU_FILE_NAME);
		String fullpath = hirateFile.getPath();
		
		try{
			FileInputStream kifuStream = new FileInputStream(fullpath);
			if(this.init(kifuStream) == Shogi.ERROR){
				throw new ShogiException();
			}
		}catch(FileNotFoundException e){
			Logger.error("Error: " + e.getMessage());
			throw new ShogiException();
		}catch(IOException e){
			Logger.error("Error: " + e.getMessage());
			throw new ShogiException();
		}catch(SecurityException e){
			Logger.error("Error: " + e.getMessage());
			throw new ShogiException();
		}
	}
	
	public Shogi(InputStream kifuStream) throws ShogiException{
		if(kifuStream == null){
			throw new ShogiException();
		}else{
			if(this.init(kifuStream) == Shogi.ERROR){
				throw new ShogiException();
			}
		}
	}
	
	public void setKifu(InputStream kifuStream){
		this.init(kifuStream);
	}
	private int init(InputStream kifuStream){
		int ret = Shogi.SUCCESS;
		this.kifu = new KitayamaCSAKifu();
		this.board = new Board(this);
		this.stand[BLACK] = new Stand(this, BLACK);
		this.stand[WHITE] = new Stand(this, WHITE);
		this.box = new KomaBox(this);
		
		this.player = new Player[2];
		this.initKomas();
		
		try{
			this.kifu.read(kifuStream);
			kifuStream.close();
		}catch(FileNotFoundException e){
			Logger.error("Error: " + e.getMessage());
			return Shogi.ERROR;
		}catch(IOException e){
			Logger.error("Error: " + e.getMessage());
			return Shogi.ERROR;
		}catch(Exception e){
			return Shogi.ERROR;
		}
		
		this.setInitialBoard(this.kifu);
		this.updateAll();
		this.jump(this.kifu.getLastHand()); // play to current hand.
		
		return ret;
	}
	
	public void resetBoard(){
		this.board.reset();
		this.stand[Shogi.BLACK].reset();
		this.stand[Shogi.WHITE].reset();
		this.box.reset();
		
		this.putAllKomaInBox();
	}
	
	public void putAllKomaInBox(){
		this.box.reset();
		
		for(Koma koma : this.ou){ koma.bePlaced(this.box); }
		for(Koma koma : this.hi){ koma.bePlaced(this.box); }
		for(Koma koma : this.ka){ koma.bePlaced(this.box); }
		for(Koma koma : this.ki){ koma.bePlaced(this.box); }
		for(Koma koma : this.gi){ koma.bePlaced(this.box); }
		for(Koma koma : this.ke){ koma.bePlaced(this.box); }
		for(Koma koma : this.ky){ koma.bePlaced(this.box); }
		for(Koma koma : this.fu){ koma.bePlaced(this.box); }
	
	}
	
	public void initKomas(){
		// 全ての駒を生成する
		Koma koma = null;
		int [] orders = {Shogi.BLACK, Shogi.WHITE};
		// 先手後手に一枚づつだから1
		for(int i=0; i<1; i++){ 
			for(int order : orders){
				koma = new Ou(this, order);
				this.ou.add(koma);
			}
		}
		
		for(int i=0; i<1; i++){
			for(int order : orders){
				koma = new Hi(this, order);
				this.hi.add(koma);
			}
		}
		
		for(int i=0; i<1; i++){
			for(int order : orders){
				koma = new Ka(this, order);
				this.ka.add(koma);
			}
		}
		
		for(int i=0; i<2; i++){
			for(int order : orders){
				koma = new Ki(this, order);
				this.ki.add(koma);
			}
		}
		
		for(int i=0; i<2; i++){
			for(int order : orders){
				koma = new Gi(this, order);
				this.gi.add(koma);
			}
		}
		
		for(int i=0; i<2; i++){
			for(int order : orders){
				koma = new Ke(this, order);
				this.ke.add(koma);
			}
		}
		
		for(int i=0; i<2; i++){
			for(int order : orders){
				koma = new Ky(this, order);
				this.ky.add(koma);
			}
		}
		
		for(int i=0; i<9; i++){
			for(int order : orders){
				koma = new Fu(this, order);
				this.fu.add(koma);
			}
		}
	}
	
	public void setInitialBoard(CSAKifu kifu){
		this.resetBoard();
		
		for(Koma modelKoma : kifu.getAllKomas()){
			Koma koma = this.box.fetch(modelKoma.getKindId());
			if(koma != null){
				if(modelKoma.isOnBoard()){
					koma.setOrder(modelKoma.getOrder());
					koma.flip(modelKoma.getKindId());
					koma.bePlaced(this.board.getMasu(modelKoma.getSquare()));
				}else if(modelKoma.isOnStand()){
					koma.setOrder(modelKoma.getOrder());
					koma.flip(modelKoma.getKindId());
					koma.bePlaced(this.stand[modelKoma.stand.getOrder()]);
				}else if(modelKoma.isInBox()){
					// do nothing
				}else{
					// do nothing
				}
			}
		}
		
		kifu.setCurrentHand(0);
	}
	
	
	
	// compensate flip, komaToCapture
	// return true if the move is valid, return false if invalid.
	public boolean compensate(Move move){
		
		if(!Shogi.isOrderValid(move.order)) return false;
		
		if(move.isPassMove()) return true;
		
		if(!Koma.isValid(move.komaToMove)) return false;
		if(!Board.isMatrixValid(move.from)) return false;
		if(!Board.isMatrixValid(move.to)) return false;
		
		boolean fromIsSquareValid = Board.isSquareValid(move.from); 
		boolean toIsSquareValid = Board.isSquareValid(move.to);
		if(fromIsSquareValid){
			if(this.board.matrix[move.from].koma == null) return false;
		}
		
		// data says this will promote, but pieceToMove is not corresponding.
		if((move.flip == Koma.FLIP_YES)&& (!Koma.isPromoted(move.komaToMove)))
			return false;
		
		// auto detect the promotion.
		if(move.flip == Koma.FLIP_UNKNOWN ){
			
			if(fromIsSquareValid){
				if(Koma.isPromoted(move.komaToMove) &&
				  !Koma.isPromoted(this.board.matrix[move.from].koma.kindId)){
					move.flip = Koma.FLIP_YES;
				}else{
					move.flip = Koma.FLIP_NO;
				}
			}
			
		}
		
		if(move.komaToCapture == Koma.UK){
			if(fromIsSquareValid && toIsSquareValid) {
				// capture the piece if exist.
				Masu masuTo = this.board.matrix[move.to];
				Masu masuFrom = this.board.matrix[move.from];
				if( (masuTo.koma != null ) ){
					//   piece exists. then, is the piece opponent?
					if(masuTo.koma.order == masuFrom.koma.order) return false;
						
					// fill if pieceToCapture not filled.
					move.komaToCapture = masuTo.koma.kindId;
				}
			}
		}else{
			if(toIsSquareValid) {
				// capture the piece if exist.
				Masu masuTo = this.board.matrix[move.to];
				if(move.komaToCapture != masuTo.koma.kindId) return false;
			}
		}
		
		
		return true;
	}
	
	public int move(Move move, boolean record, boolean back){
		
		int ret = Shogi.SUCCESS;
		if(this.compensate(move)==false){
			ret = Shogi.ERROR; 
			return ret;
		}
		
		if(move.isPassMove() == false){
			// pieceToMove is on the board.
			if(Board.isSquareValid(move.from)){
				
				if(move.komaToCapture != Koma.UK){
					this.capture(this.board.matrix[move.to], ORDER_UNKNOWN);
				}
							
				// move
				if(Board.isSquareValid(move.to)){
					Koma komaToMove = this.board.matrix[move.from].fetch();
					assert(komaToMove != null);
					komaToMove.bePlaced(this.board.matrix[move.to]);
					if(move.flip == Koma.FLIP_YES) komaToMove.flip();
				}else if(Stand.isStandValid(move.to)){
					this.capture(this.board.matrix[move.from], move.to);
				}
				
			}else if(Stand.isStandValid(move.from)){
				// will be placed from koma stand.
				
				Koma pieceToAdd = this.stand[move.order].fetch(move.komaToMove);
				
				if(pieceToAdd == null){
					ret = ERROR;
					Logger.error("There's no %s in %s koma stand!", Koma.getKomaName(move.komaToMove), Shogi.getOrderName(move.order));
				}else{
					// reverse the direction.
					if(back){
						pieceToAdd.setOrder(Shogi.getOtherOrder(pieceToAdd.order));
					}
					
					// move
					if(Koma.isPromoted(move.komaToMove)) pieceToAdd.flip();
					pieceToAdd.bePlaced(this.board.matrix[move.to]);
					
				}
			}
		}
		
		if(record){
			this.kifu.add(move);
		}
		
		if(move.isPassMove() == false){
			this.update(move);
			
			// judge check-mate?
			Koma ou = this.getOu(Shogi.getOtherOrder(move.order));
			if(ou == null){
				ret = CHECKMATED;
			}else if(ou.isCheckmated()){
				ret = CHECKMATED;
			}else if(move.komaToCapture == Koma.OU){
				ret = CHECKMATED;
			}
		}
		
		return ret;
	}
	
	public int back(){
		int ret = Shogi.SUCCESS;
		
		Move mv = this.kifu.getCurrentMove();
		if(mv == null){
			Logger.error("can not back since previous move is null\n");
			ret = ERROR;
		}else{
			// STEPS:
			// 1. move "to" to "from"
			// 2. place the captured koma back if exists.
			
			// 1.
			ret = this.move(new Move(mv.order, mv.to, mv.from, mv.komaToMove, mv.flip, Koma.UK), false, true);
			// 2.
			if(mv.komaToCapture != Koma.UK){
				this.move( new Move( mv.order // reverse of order is done in move()
						           , Stand.getStandPosition(mv.order) // to = Stand position
						           , mv.to 
						           , mv.komaToCapture
						           // all koma in stand are non-promoted komas, so in case of back()
						           // here, get back the status of koma when it was captured
						           , Koma.isPromoted(mv.komaToCapture) ? Koma.FLIP_YES : Koma.FLIP_NO
						           , Koma.UK)
				         , false
				         , true);
			}
			// just decrement current pointer of the kifu. do not delete this Move in case we support forward function.
			this.kifu.decrementCurrentHand();
			
		}
		
		return ret;
	}
	
	public int forward(){
		int ret = Shogi.SUCCESS;
		
		Move mv = this.kifu.getMoveByHand(this.kifu.getCurrentHand() + 1);
		if(mv == null){
			Logger.warn("It's most current.\n");
			ret = Shogi.ERROR;
		}else{
			ret = this.move(mv, false, false);
			this.kifu.incrementCurrentHand();
		}
		return ret;
	}
	
	public void jump(int handWhereUWannaJumpTo){
		this.autoPlay(handWhereUWannaJumpTo, 0);
	}
	
	public void autoPlay(int handWhereUWannaPlayTil, int seconds){
		// TODO: make this thread so that it can idle.
		int currentHand = this.kifu.getCurrentHand();
		if(currentHand == handWhereUWannaPlayTil){
			return;
		}else if(currentHand < handWhereUWannaPlayTil){
			// play forward
			
			while(this.kifu.getCurrentHand() < handWhereUWannaPlayTil){
				if(this.forward() != Shogi.SUCCESS) break;
				// wait for the given seconds
			}
			
		}else if(currentHand > handWhereUWannaPlayTil){
			// play backward
			
			while(this.kifu.getCurrentHand() >= handWhereUWannaPlayTil){
				this.back();
				// wait for the given seconds
			}
		}
	}
	
	
	public void updateAll(){
		this.board.setPlacable();
		this.board.updateAllKomaOnBoard();
		// komas on stand are automatically updated/notified in update(Move move) func.
		this.updateNotifiedKoma(null);
	}
	
	
	public void update(Move move){
		
		// update board, masu, koma
		// notifyした駒すべてを配列に保持する。ArrayListSより高速な為。
		Masu ms;
		ms = this.board.getMasu(move.from);
		if(ms!=null) ms.notifyObservers();
		ms = this.board.getMasu(move.to);
		if(ms!=null) ms.notifyObservers();
		
		// komas on stand also should be updated.
		if(this.stand[Shogi.BLACK] != null) this.stand[Shogi.BLACK].notifyObservers();
		if(this.stand[Shogi.WHITE] != null) this.stand[Shogi.WHITE].notifyObservers();
		
		
		// make sure, the one which has just moved also get notified.
		// this is for when back() is called.
		Koma koma;
		koma = this.board.getKoma(move.from);
		if(koma!=null) koma.update();
		koma = this.board.getKoma(move.to);
		if(koma!=null) koma.update();
		
		// updateing Order DOES matter. 
		// think before when you change this code here.
		
		// 1. update placable
		this.board.updatePlacable(move);
		// 2. update koma
		// pass the move which moved in the last, this includes back move.
		this.updateNotifiedKoma(move);
		// 3. else
		// 3-1. add the masu where the last moved koma sits on
		// and since the given move could be a back move, just ask kifu
		// to get the last(current) move. then find out the masu where it sits.
		Masu masuWhereLastMovedKomaSits = null;
		Move lastMove = this.kifu.getCurrentMove();
		if(lastMove!=null){
			masuWhereLastMovedKomaSits = this.board.getMasu(lastMove.to);
		}
		if(masuWhereLastMovedKomaSits != null){
			this.board.getInterfered().addIfNot(masuWhereLastMovedKomaSits);
		}
		
		
		// update status of the battle.
		for(Player ply : this.player){
			if(ply != null){
				if(ply.getType() == Player.PLAYER_COM){
					ply.update();
				}
			}
		}
	}
	
	/*
	 * update koma's information.
	 */
	public void updateNotifiedKoma(Move lastMove){
		//ArrayListS<Koma> komasToUpdate = new ArrayListS<Koma>(); 
		Koma [] arrayOfUpdatedKoma = new Koma[Koma.TOTAL_PIECES];
		int idx = 0;
		int i = 0;
		for(int kindId : Koma.BASIC_KIND_IDS){
			for(Koma koma : this.getKomaList(kindId)){
				if(koma.getIsNotified()){
					koma.beforeUpdate();
					arrayOfUpdatedKoma[idx++] = koma;
					//komasToUpdate.add(koma);
				}
			}
		}
		
		//for(Koma koma : komasToUpdate){
		//	koma.updateStatus(lastMove);
		//}
		for(i=0; i<arrayOfUpdatedKoma.length; i++){
			if(arrayOfUpdatedKoma[i] != null){
				arrayOfUpdatedKoma[i].updateStatus(lastMove);
			}else{
				break;
			}
		}
		
		// movable for all komas are now updated.
		this.board.deleteOutdatedInterfered();
		
		//for(Koma koma : komasToUpdate){
		//	koma.afterUpdate();
		//}
		for(i=0; i<arrayOfUpdatedKoma.length; i++){
			if(arrayOfUpdatedKoma[i] != null){
				arrayOfUpdatedKoma[i].afterUpdate();
			}else{
				break;
			}
		}
	}
	
	/*
	 * capture koma on the give masu and put it on the corresponding stand!!
	 * if whichStand is not specified, suppose that captured koma will be 
	 * placed on the opponent stand.
	 */
	private int capture(Masu masu, int whichStand){
		int ret = Shogi.SUCCESS;
		
		if(masu == null){
			ret = Shogi.ERROR;
		}else{
			Koma koma = masu.fetch();
			if(koma == null){
				ret = Shogi.NOTING_TO_CAPTURE;
			}else{
				if(whichStand == ORDER_UNKNOWN){
					assert(koma.order == Shogi.BLACK || koma.order == Shogi.WHITE);
					whichStand = Shogi.getOtherOrder(koma.order);
				}
				koma.bePlaced(this.stand[whichStand]);
			}
		}
		return ret;
	}
	
	
	public int isValidMove(int order, Move mv){
		// 駒の動き不整合は生成するときに、確認しているので、この関数はあまり呼ばれなくてよい。
		// 人間が指したい手の判定くらいには使える。
		int ret = isPhysicallyValidMove(order, mv);
		if(ret == TRUE) ret = isLegalMove(order, mv);
		return ret;
	}
	
	public int isPhysicallyValidMove(int order, Move mv){
		
		int ret = Shogi.TRUE;
		if(mv == null){
			Logger.warn("Give mv is null!!\n");
			return INVALID_ARGUMENT;
		}else if( !(order == BLACK || order == WHITE)){
			Logger.warn("order is invalid!!\n");
			return INVALID_ARGUMENT;
		}else if(!Koma.isValid(mv.komaToMove)){
			Logger.warn("invalid koma to Move, %s!!\n", Koma.getKomaName(mv.komaToMove));
			return INVALID_ARGUMENT;
		}
		
		if(mv.order == Shogi.ORDER_UNKNOWN){
			Logger.warn("Can not identified in which turn this hand is made!!");
			return ORDER_IS_UNKNOWN;
		}
		
		
		assert(Board.isSquareValid(mv.to));
		
		if(Board.isSquareValid(mv.from)){
			// 1. if there's from. is the piece your side?
			if(order != this.board.matrix[mv.from].koma.order ){
				Logger.warn("mv is invalid since order is not matched!\n");
				ret = NOT_YOUR_KOMA;
				return ret;
			}
			
			// komaToMove corersponds with the one you trying to move?
			if(Koma.getUnpromotedPiece(mv.komaToMove) != 
			   Koma.getUnpromotedPiece(this.board.matrix[mv.from].koma.getKindId()) ){
				Logger.warn("koma on the masu and komaToMove doesn't match!\n");
				ret = ERROR;
				return ret;
			}
			
			// destination has to be empty!
			if(this.board.matrix[mv.to].koma != null){
				if(this.board.matrix[mv.to].koma.order == order){
					ret = CANNOT_CAPTURE_YOUR_KOMA;
					return ret;
				}
			}
			
		}else if(Stand.isStandValid(mv.from)){
			// 2. if you taking piece from stand, is there the piece on the stand?
			if( this.stand[order].get(mv.komaToMove) == null ){
				Logger.warn("%s is not in the stand.\n", Koma.getKomaName(mv.komaToMove));
				ret = NOT_ON_STAND;
			}else if( this.board.matrix[mv.to].koma != null ){
				ret = CANNOT_PLACE_ON_OCCPIED_MASU;
			}
		}else{
			ret = INVALID_ARGUMENT;
		}
		
		return ret;
	}
	
	public int isLegalMove(int order, Move mv){
		int ret = TRUE;
		Koma ou = null;
		
		int opponent = Shogi.getOtherOrder(order);
		int companion = order;
		
		ou = this.getOu(order);
		boolean isFromSquare = Board.isSquareValid(mv.from);
		boolean isToSquare = Board.isSquareValid(mv.to);
			
		if(isFromSquare){
			// 1. check if the destnation is in the scope of movable or placable 
			if(this.board.matrix[mv.from].koma.movable.contains(this.board.matrix[mv.to]) == false){
				ret = OUT_OF_MOVEMENT_SCOPE;
				return ret;
			}
			
			
			// 2. check if the necessity of promotion.
			if(this.board.matrix[mv.from].koma.mustPromoteOn(this.board.matrix[mv.to])){
				if(! Koma.isPromoted(mv.komaToMove)){
					ret = MUST_PROMOTE;
					return ret;
				}
			}
			
			
		}else if(Stand.isStandValid(mv.from)){
			// 1. check if the destnation is in the scope of movable or placable
			// since the koma is on stand, regard the board.placable
			if(this.board.placable.contains(this.board.matrix[mv.to]) == false){
				ret = OUT_OF_MOVEMENT_SCOPE;
				return ret;
			}
			
			// 2. check if the necessity of promotion. (e.g, in case fu, ke )
			Koma km = this.stand[mv.order].get(mv.komaToMove);
			if(km.mustPromoteOn(this.board.matrix[mv.to])){
				if(! Koma.isPromoted(mv.komaToMove)){
					ret = MUST_PROMOTE;
					return ret;
				}
			}
			
			// 3. make sure it's no prohibited hand due to double piece on a file.
			if(Koma.isDuplicationProhibited(mv.komaToMove)){
				// check the file 
				int file = Board.getFile(mv.to);
				int base_square = (file*10) + 1;
				for(int i = 0; i < 9; i++){
					if(this.board.matrix[ base_square+i ].koma != null){
						if(this.board.matrix[ base_square+i ].koma.order == mv.order &&
						   this.board.matrix[ base_square+i ].koma.kindId == mv.komaToMove){
							ret = DUPLICATION_ON_A_FILE;
							return ret;
						}
					}
				}
			}
		}
		
		// TODO: ouがなかったら,,,
		// 3. It's illegal if the move cause to loose.
		if(ou!=null){
			if(ou.isDirectlyReferredByOpponent()){
				// equivalant to isChecked(), but I use the function name as it is.
				// if my Ou is being threaten, I have to do something about it.
				
				if(isToSquare){
					if(mv.komaToMove == Koma.OU){
						// you have to move to safe place!!
						if(this.board.matrix[mv.to].isDirectlyReferredBy(opponent)){
							// Ou's destination is not safe.
							ret = MOVE_CAUSES_A_LOSS;
							return ret;
						}
						
						// in case, thoughable is directly threating OU.
						// make sure that escaping square is thoughable square.
						for(Koma sniper : ou.masu.directRefered){
							if(sniper.order == opponent){
								if(this.board.matrix[mv.to].indirectRefered.contains(sniper)){
									// sorry, you just move to throuable masu.
									// nothing is protecting ou and sniper can go through.
									ret = MOVE_CAUSES_A_LOSS;
									return ret;
								}
							}
						}
						
					}else{
						
						for(Koma threat : ou.masu.directRefered){
							if(threat.order == opponent){
								if(threat.masu == this.board.matrix[mv.to]){
									// - hand that takes the threatening koma
									// capture the threat
									ret = TRUE;
									return ret;
								}
								
								if(threat.movable.contains(this.board.matrix[mv.to])){
									// - hand that puts wall between threatening koma and the king.
									// move to be a wall
									ret = TRUE;
									return ret;
								}
							}
						}
						ret = MOVE_CAUSES_A_LOSS;
						return ret;
						
					}

				}else if(Stand.isStandValid(mv.to)){
					// this is possible in back(), but suppose it's legal.
				}else{
					// assert!
				}
			}
			
			if(ou.isIndirectlyReferredByOpponent()){
				if(isFromSquare){
					for(Koma sniper : ou.masu.indirectRefered){
						if(sniper.order == opponent){
							// foresee one hand. didnt work since it's update reference(list) and
							// list are hevily used in loop in this method, so below should NOT be used.
							/*
							this.move(mv, true, false);
							if(ou.isDirectlyReferredByOpponent()){
								ret = MOVE_CAUSES_A_LOSS;
							}
							this.back();
							if(ret == MOVE_CAUSES_A_LOSS) return ret;
							*/
							// am I in between ou and the indirect
							// am I moving to different direction
							// if both yes, this move causes a loss.
							
							// mv.from this koma has directRefered by the attacking?
							if(this.board.matrix[mv.from].directRefered.contains(sniper)){
								// mv.to where this move's destination is not Masu of 
								// the attacking koma scope, it means this move is getting 
								// off from the line from attacking koma to OU.
								if(sniper.movable.contains(this.board.matrix[mv.to]) ||
								   sniper.throughable.contains(this.board.matrix[mv.to]) ){
									// after mv, still wall-koma is on the way
								}else{
									ret = MOVE_CAUSES_A_LOSS;
								}
							}

						}
					}
				}
			}
		}
		
		return ret;
	}
	
	
	public ArrayListS<Move> getAllHands(int order){
		ArrayListS<Move> ret = new ArrayListS<Move> ();
		
		ret.addAll(this.board.getAllHands(order));
		ret.addAll(this.stand[order].getAllHands(order));
		
		// debug
		Logger.debug("Legal hands\n");
		for(Iterator<Move> i = ret.iterator(); i.hasNext(); ){
			Logger.debug("%s\n", i.next());
		}
		return ret;
	}
	
	public ArrayListS<Koma> getKomaList(){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(int kindId : Koma.BASIC_KIND_IDS){
			komas.addAll(this.getKomaList(kindId));
		}
		return komas;
	}
	
	public ArrayListS<Koma> getKomaListByOrder(int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(int kindId : Koma.BASIC_KIND_IDS){
			komas.addAll(this.getKomaList(kindId, order));
		}
		return komas;
	}
	
	public ArrayListS<Koma> getKomaList(int kindId){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		int umpromotedKindId = Koma.getUnpromotedPiece(kindId);
		if(umpromotedKindId == Koma.FU) komas = this.fu;
		else if(umpromotedKindId == Koma.KY) komas = this.ky;
		else if(umpromotedKindId == Koma.KE) komas = this.ke;
		else if(umpromotedKindId == Koma.GI) komas = this.gi;
		else if(umpromotedKindId == Koma.KI) komas = this.ki;
		else if(umpromotedKindId == Koma.KA) komas = this.ka;
		else if(umpromotedKindId == Koma.HI) komas = this.hi;
		else if(umpromotedKindId == Koma.OU) komas = this.ou;
		return komas;
	}
	
	public ArrayListS<Koma> getKomaList(int kindId, int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		ArrayListS<Koma> komas_order = new ArrayListS<Koma>();
		int umpromotedKindId = Koma.getUnpromotedPiece(kindId);
		if(umpromotedKindId == Koma.FU) komas = this.fu;
		else if(umpromotedKindId == Koma.KY) komas = this.ky;
		else if(umpromotedKindId == Koma.KE) komas = this.ke;
		else if(umpromotedKindId == Koma.GI) komas = this.gi;
		else if(umpromotedKindId == Koma.KI) komas = this.ki;
		else if(umpromotedKindId == Koma.KA) komas = this.ka;
		else if(umpromotedKindId == Koma.HI) komas = this.hi;
		else if(umpromotedKindId == Koma.OU) komas = this.ou;
		for(Koma koma : komas){
			if(koma.getOrder() == order){
				komas_order.add(koma);
			}
		}
		return komas_order;
	}
	
	/*
	includeOnBoard 盤にある駒で、
	includeOnStand　駒台にのっている駒も
	order こっちの手番の
	kindId この駒
	bothOkay trueなら成歩成構わず
	 */
	public ArrayListS<Koma> getKomaList(boolean includeOnBoard, boolean includeOnStand, int order, int kindId, boolean bothOkay){
		
		ArrayListS<Koma> komas = new ArrayListS<Koma>(); 
		for(Koma koma : this.getKomaList(Koma.getUnpromotedPiece(kindId))){
			if((includeOnBoard == true && koma.isOnBoard() == true) ||
			  (includeOnStand == true && koma.isOnStand() == true)){
				if(koma.getOrder() == order){
					if(bothOkay == true){
						if(Koma.getUnpromotedPiece(koma.getKindId()) == Koma.getUnpromotedPiece(kindId)){
							komas.add(koma);
						}
					}else{
						if(koma.getKindId() == kindId){
							komas.add(koma);
						}
					}
				}
			}
		}
		return komas;
	}
	
	public ArrayListS<Koma> getKomaList(boolean includeOnBoard, boolean includeOnStand, int order){
		
		ArrayListS<Koma> komas = new ArrayListS<Koma>(); 
		for(int kindId : Koma.BASIC_KIND_IDS){
			for(Koma koma : this.getKomaList(Koma.getUnpromotedPiece(kindId))){
				if((includeOnBoard == true && koma.isOnBoard() == true) ||
				  (includeOnStand == true && koma.isOnStand() == true)){
					if(koma.getOrder() == order){
						komas.add(koma);
					}
				}
			}
		}
		return komas;
	}
	
	public Koma getOu(int order){
		for(Koma ou : this.ou){
			if(ou.getOrder() == order) return ou;
		}
		return null;
	}
	
	public ArrayListS<Move> getPrecedingMovesFor(Masu masu2backup, int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();

		moves.addAll(this.board.getPrecedingMovesFor(masu2backup, order));
		moves.addAll(this.stand[order].getPrecedingMovesFor(masu2backup));
		
		return moves;
	}
	
	public ArrayListS<Move> getMovesCanGoTo(int order, int square){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		moves.addAll(this.board.getMovesCanGoTo(order, square));
		moves.addAll(this.stand[order].getMovesCanGoTo(order, square));
		return moves;
	}
	
	public int setPlayer(Player p){
		int ret = Shogi.SUCCESS;
		if(p.getOrder() == Shogi.BLACK){
			p.setShogi(this);
			this.player[BLACK] = p;
		}else if(p.getOrder() == Shogi.WHITE){
			p.setShogi(this);
			this.player[WHITE] = p;
		}else{
			Logger.warn("black or white not yet decided!!\n");
		}
		
		return ret;
	}
	
	public Player getPlayer(String name){
		for(Player ply : this.player){
			if(ply.getName() == name){
				return ply;
			}
		}
		return null;
	}
	
	public Player getPlayer(int order){
		for(Player ply : this.player){
			if(ply.getOrder() == order){
				return ply;
			}
		}
		return null;
	}
	
	public String toString(){
		
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		ps.printf("%s\n%s\nLast:%s\nNextOrder:%s\n%s\n\n%s\n%s "
				, (this.getPlayer(Shogi.BLACK) != null) ? 
						this.player[Shogi.BLACK].getName() + ": " 
						+ Evaluator.getStageDescription(this.player[Shogi.BLACK].getStage()) :
							":"
				, (this.getPlayer(Shogi.WHITE) != null) ? 
						this.player[Shogi.WHITE].getName() + ": " 
						+ Evaluator.getStageDescription(this.player[Shogi.WHITE].getStage()) :
							":"
		        , this.kifu.getCurrentMove()
		        , Shogi.getOrderNameCasual(this.kifu.getNextOrder())
				, this.stand[WHITE], this.board, this.stand[BLACK]);
		str += buff.toString();
		
		return str;
	}
	
	
	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	public final static String HIRATE_KIFU_FILE_NAME = "hirate.csa";
	
	public final static int SUCCESS = 0x00000000;
	public final static int ERROR   = 0x00000001;
	public final static int NOTING_TO_CAPTURE   = 0x00000002;
	public final static int INVALID_ARGUMENT   = 0x00000003;
	public final static int TRUE    = 0x00000004;
	public final static int FALSE   = 0x00000005;
	public final static int ORDER_IS_UNKNOWN = 0x00000006;
	public final static int NOT_YOUR_KOMA = 0x00000007;
	public final static int CANNOT_CAPTURE_YOUR_KOMA = 0x00000008;
	public final static int NOT_ON_STAND = 0x00000009;
	public final static int CANNOT_PLACE_ON_OCCPIED_MASU = 0x0000000a;
	public final static int MUST_PROMOTE = 0x0000000b;
	public final static int MOVE_CAUSES_A_LOSS = 0x0000000c;
	public final static int DUPLICATION_ON_A_FILE = 0x0000000d;
	public final static int OUT_OF_MOVEMENT_SCOPE = 0x0000000e;
	
	public final static int CHECKMATED = 0x00000100;
	public final static int BLACK_IS_CHECKMATED = 0x00000101;
	public final static int WHITE_IS_CHECKMATED = 0x00000102;
	
	
	public final static int LOOP_BREAK = 0x00001000;
	public final static int LOOP_CONTINUE = 0x00001001;
	
	public final static int BLACK = 0;
	public final static int WHITE   = 1;
	public final static int ORDER_UNKNOWN = 2;
	
	public static final int POSITION_UNKNOW = 100;
	
	public final static int [] ORDERS = {Shogi.BLACK, Shogi.WHITE};
	
	public static final int LANGUAGE_JP = 0;
	public static final int LANGUAGE_EN = 1;
	
	
	public static boolean MODE_PRODUCTION = false;
	public static boolean  MODE_DEBUG = false;
	
	
	public static boolean isOrderValid(int order){
		return (order == BLACK || order == WHITE) ? true : false;
	}
	
	public final static int getOtherOrder(int order){
		if(order == BLACK) return WHITE;
		else if(order == WHITE) return BLACK;
		else return ORDER_UNKNOWN;
	}
	
	
	public static String getOrderName(int order){
		
		String str = "";
		if     (order == Shogi.BLACK) str = "+";
		else if(order == Shogi.WHITE) str = "-";
		else if(order == Shogi.ORDER_UNKNOWN) str = "?";
		else str = "?????";
		return str;
	}
	
	public static int getOrder(String orderStr){
		int order = ORDER_UNKNOWN;
		if      (orderStr.equals("+")) order = BLACK;
		else if (orderStr.equals("-")) order = WHITE;
		return order;
	}
	
	public static String getOrderAbbreviation(int order){
		
		String str = "";
		if     (order == Shogi.BLACK) str = "B";
		else if(order == Shogi.WHITE) str = "W";
		else if(order == Shogi.ORDER_UNKNOWN) str = "?";
		else str = "?????";
		return str;
	}
	
	public static String getOrderNameCasual(int order){
		
		String str = "";
		if     (order == Shogi.BLACK) str = "BLACK";
		else if(order == Shogi.WHITE) str = "WHITE";
		else if(order == Shogi.ORDER_UNKNOWN) str = "UNKNOWN";
		else str = "?????";
		return str;
	}
	
}


