package com.yeele.game.shogi.engine.player;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.MoveEval;
import com.yeele.game.shogi.engine.MoveEvalRanking;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.TreeOfMoveEval;
import com.yeele.util.ArrayListS;
import com.yeele.util.TimeConcierge;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class PlayerCom3 extends Player{
	
	private int thinkDepth;
	private int narrawedDownMoves;
	
	
	// debuging purpose
	private int cnt_move = 0, cnt_evaluate = 0;
	private HashMap<String, Integer> sim = new HashMap<String, Integer>();
	
	/*
	 * depth is the depth of pondering.
	 * narrowed is the number of moves this player can narrow down as he/she ponders.
	 */
	
	public PlayerCom3(int order, String name, int depth, int narrowed){
		super(Player.PLAYER_COM, order, name);
		this.thinkDepth = depth;
		this.narrawedDownMoves = narrowed;
	}
	public PlayerCom3(int order, String name, int depth, int narrowed, int maxMemorySize){
		super(Player.PLAYER_COM, order, name, maxMemorySize);
		this.thinkDepth = depth;
		this.narrawedDownMoves = narrowed;
	}
	
	
	public Evaluation evaluate(){
		Evaluation eval = null;
		if(this.evaluator != null){
			eval = this.evaluator.evaluate(Evaluator.TYPE_STATIC_SHALLOW);
		}
		return eval;
	}
	
	public Evaluation evaluateBy(int type){
		Evaluation eval = null;
		if(this.evaluator != null){
			eval = this.evaluator.evaluate(type);
		}
		return eval;
	}
	
	public Move generate(){
		
		TimeConcierge concierge = new TimeConcierge();
		
		sim = new HashMap<String, Integer>();
		this.cnt_move = 0; this.cnt_evaluate = 0;
		
		Move mv = null;
		Logger.info("Com is generating next best move ...\n");
		
		if(!this.isShogiSet()) return null;
		
		// 1. save the status
		int remember_hand = this.shogi.kifu.getCurrentHand();
		
		// 2. make the move

		MoveEval bestEvalMove = this.ponder(this.thinkDepth, this.order);
		if(bestEvalMove != null){
			mv = bestEvalMove.move;
		}
		
		// bring the status back
		this.shogi.kifu.removeLaterThan(remember_hand);
		
		concierge.measure();
		
		sim.put("move() calls", this.cnt_move);
		sim.put("evaluate() calls", this.cnt_evaluate);
		Iterator iter = this.sim.entrySet().iterator();
		while( iter.hasNext() ){
			Map.Entry<String, Integer> entry;
			String key = "";
			Integer val = new Integer(0);
			entry = (Map.Entry<String, Integer>)iter.next();
			key = entry.getKey();
			val = entry.getValue();
			Logger.warn("%s : %d\n", key, val);
		}
		Logger.warn("Com took following time for generating a move\n%s\n", concierge);
		
		return mv;
	}
	
	private MoveEvalRanking narrowdown(int order, int evalType){
		int ret = Shogi.SUCCESS;
		MoveEvalRanking niceMoves = new MoveEvalRanking(this.narrawedDownMoves, MoveEvalRanking.ORDER_ASC, order);
		
		// what hands possibly can be made?
		// 1. attacking
		// 1-1. move your porn.
		// 1-2. 
		// 2. protecting
		
		ArrayListS<Move> allhands = this.shogi.getAllHands(order);
		for(Move m : allhands){
			ret = this.shogi.move(m, true, false);
			this.cnt_move++; // debugging
			Evaluation e = this.evaluate(evalType);
			this.cnt_evaluate++; // debugging
			//Logger.debug("%s\n%s\n", m, e);
			niceMoves.add(new MoveEval(m, e));
			this.shogi.back();
		}
		return niceMoves;
	}
	
	private MoveEval ponder(int depth, int order){
		MoveEval best = null;
		
		Evaluation currentEval = this.evaluateBy(Evaluator.TYPE_STATIC_SHALLOW);
		// the fruit on the root has evaluation of current situation.
		TreeOfMoveEval root = new TreeOfMoveEval(new MoveEval(new Move(order, 0, 0, Koma.UK), currentEval));
		this.growTree(depth, order, root);
		Logger.debug("%s\n", root.getTreeString());
		if(root.markNextBestMoves() == Shogi.SUCCESS){
			Logger.info("Yomi suji...\n");
			Logger.debug("%s\n", root.toStringConfirmedPath());
			Logger.debug("full path is \n%s\n", root.getTreeStringByDepth());
			best = root.getConfirmedFruitInBranches();
		}
		Logger.debug("%s\n", root.getTreeString());
		return best;
	}
	
	private int growTree(int depth, int order, TreeOfMoveEval tree){
		
		int ret = Shogi.SUCCESS;
		Logger.debug("growTree %s move (depth = %d)\n", Shogi.getOrderNameCasual(order), depth);

		int evalType = (depth == this.thinkDepth) ? Evaluator.TYPE_DYNAMIC : Evaluator.TYPE_STATIC_SHALLOW;
		
		MoveEvalRanking niceMoves = narrowdown(order, evalType);
		
		ArrayListS<MoveEval> moves = new ArrayListS<MoveEval>();
		
		moves = (ArrayListS)niceMoves;
		
		if(!tree.isFruitOfTrunkPassMove() && !tree.isBackbone() && tree.trunk.getFruit() != null){
			moves.add(new MoveEval(new Move(order, 0, 0, Koma.UK), tree.trunk.getFruit().eval));
		}
			
		for(MoveEval me : moves){
			if(! me.move.isPassMove()){
				ret = this.shogi.move(me.move, true, false);
			}
			this.cnt_move++; // debugging
			TreeOfMoveEval branch = new TreeOfMoveEval(me);
			tree.addBranch(branch);
			
			if(depth != 1){
				this.growTree(depth-1, Shogi.getOtherOrder(order), branch);	
			}
			
			if(! me.move.isPassMove()){
				this.shogi.back();
			}
			
		}
		
		return ret;
	}
	
	
	
	
}










