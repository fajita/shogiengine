package com.yeele.game.shogi.engine.player;


import java.io.*;
import java.util.HashMap;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.SceneHashMap;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.TreeOfMoveEval;
import com.yeele.game.shogi.engine.analyzer.SujiAnalyzer;
import com.yeele.game.shogi.engine.kifu.KitayamaCSAKifu;
import com.yeele.game.shogi.engine.stratagy.attacktactic.ShikenBisha;
import com.yeele.game.shogi.engine.stratagy.castle.KataMino;
import com.yeele.game.shogi.engine.stratagy.suji.BackupSuji;
import com.yeele.game.shogi.engine.stratagy.suji.CheckOuSuji;
import com.yeele.game.shogi.engine.stratagy.suji.EscapeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.HashiFuUkeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PlaceIfProfitableSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PutBetweenBasicSuji;
import com.yeele.game.shogi.engine.stratagy.suji.ReinforceSuji;
import com.yeele.game.shogi.engine.stratagy.suji.Suji;
import com.yeele.game.shogi.engine.stratagy.suji.TakeLineSuji;
import com.yeele.game.shogi.engine.stratagy.suji.TakeSuji;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public abstract class Player{
	
	
	protected Shogi shogi = null;
	protected String name;
	protected int type;
	protected int order = Shogi.ORDER_UNKNOWN;
	protected int superiority = Player.SUPERIORITY_NOT_AVAILABLE;
	protected int teritory = Board.TERITORY_UNKNOWN;
	protected Evaluator evaluator = null;
	protected SujiAnalyzer analyzer = null;
	protected HashMap<Integer, Suji> sujiIdMap = new HashMap<Integer, Suji>();
	protected Evaluation evalBeforePonder = null;
	
	// 局面記憶の為の変数
	
	protected SceneHashMap<String, TreeOfMoveEval> sceneMap;
	
	
	/*
	 * UNKONWN is set to order because order can be decided later
	 * , as they decide it by fliping komas.
	 */
	public Player(int type, int order, String name){
		this.type = type;
		this.name = name;
		this.order = order;
		this.sceneMap = new SceneHashMap<String, TreeOfMoveEval>(PLAYER_DEFAULT_MAX_SCENE_TO_MEMORIZE);
	}
	
	public Player(int type, int order, String name, int maxMemorableScenes){
		this.type = type;
		this.name = name;
		this.order = order;
		this.sceneMap = new SceneHashMap<String, TreeOfMoveEval>(maxMemorableScenes);
	}
	
	
	
	
	public void setShogi(Shogi shogi){
		this.shogi = shogi;
		this.evaluator = new Evaluator(this.shogi);
		this.setParameters(); // overriden method should be called here.
		
		if(shogi.kifu != null){
			shogi.kifu.setName(this.getOrder(), this.getName());
			if(this.getType() == Player.PLAYER_COM){
				if(this.getOrder() == Shogi.BLACK){
					shogi.kifu.setInfo(KitayamaCSAKifu.KW_PLAYER_TYPE_B, this.getClass().getName());
				}else if(this.getOrder() == Shogi.WHITE){
					shogi.kifu.setInfo(KitayamaCSAKifu.KW_PLAYER_TYPE_W, this.getClass().getName());
				}
				
			}
		}
	}
	
	public void setSuji(){
		
	}
	
	public void setParameters(){
		
		this.setSuji();
		// これでこのプレーヤーが知っているsujiのみ解析できることになる。
		this.analyzer = new SujiAnalyzer(this.shogi, this.sujiIdMap);
		
	}
	
	public void addSuji(Suji s){
		if(s!=null){
			this.sujiIdMap.put(s.getSujiId(), s);
		}
	}
	
	
	public void setOrder(int order){
		if(order == Shogi.ORDER_UNKNOWN){
			Logger.warn("You are setting %s ..., are you sure!?\n", Shogi.getOrderName(order));
		}
		this.order = order;
		this.updateTeritory();
	}
	
	public void setSuperiority(int superiority){
		this.superiority = superiority;
		this.updateTeritory();
	}
	
	public void updateTeritory(){
		if(this.superiority == SUPERIORITY_NOT_AVAILABLE){
			if(this.order == Shogi.BLACK){
				this.teritory = Board.TERITORY_RANK789;
			}else if(this.order == Shogi.WHITE){
				this.teritory = Board.TERITORY_RANK123;
			}
		}else{
			if(this.superiority == SUPERIORITY_STORONGER){
				// TODO: 先手後手の概念をwrapしないと成り立たない。
				// 何故ならtactic, castleあたりで先手後手はhardcodeしているから。
				boolean isKomaOchi = false;
				if(isKomaOchi){ // 駒落ちなら
					if(this.order == Shogi.BLACK){ // 駒落ちなら、基本先手持ち
						// その場合通常の後手陣が自陣
						this.teritory = Board.TERITORY_RANK123; 
					}
				}
			}else if(this.superiority == SUPERIORITY_WEEKER){
				boolean isOpponentKomaOchi = false;
				if(isOpponentKomaOchi){ // 相手が駒落ちなら
					if(this.order == Shogi.WHITE){ // 駒落ちなら、下手は後手番
						// この場合、後手にも関わらず通常の先手陣が自陣
						this.teritory = Board.TERITORY_RANK789; 
					}
				}
			}
		}
		
		// update Masu teritoryFor member.
		if(this.isShogiSet()){
			this.shogi.board.updateTeritory(this);
		}
	}
	
	public Shogi getShogi(){
		return this.shogi;
	}
	public int getOrder(){
		return this.order;
	}
	public int getSuperiority(){
		return this.superiority;
	}
	public int getTeritory(){
		return this.teritory;
	}
	public int getOpponetTeritory(){
		return Board.getOppositeTeritory(this.teritory);
	}
	public int getType(){
		return this.type;
	}
	public String getName(){
		return this.name;
	}
	public Suji getSuji(int sujiId){
		return this.sujiIdMap.get(sujiId);
	}
	public void setEvalBeforePonder(Evaluation e){
		this.evalBeforePonder = e;
	}
	public Evaluation getEvalBeforePonder(){
		return this.evalBeforePonder;
	}
	
	
	public boolean isShogiSet(){
		boolean ret = false;
		if(shogi instanceof Shogi) ret = true;
		else Logger.warn("shogi is not yet set!\n");
		return ret;
	}
	
	public Evaluation evaluate(int evaluationType){
		if(this.evaluator != null){
			return this.evaluator.evaluate(evaluationType);
		}
		return null;
	}
	
	public int getStage(){
		if(this.evaluator != null){
			return this.evaluator.getStage();
		}
		return Evaluator.STAGE_UNKNOWN;
	}
	
	public void update(){
		this.updateStage();
		// update stratagy
		this.updateStratagy();
	}
	
	public void updateStage(){
		if(this.evaluator != null){
			this.evaluator.updateStage();
		}
	}
	
	public void updateStratagy(){
		Logger.error("Override!!!!!! in sub class\n");
	}
	
	public HashMap<Integer, Boolean> analyze(Move move){
		if(this.analyzer != null){
			return this.analyzer.analyze(move);
		}
		return null;
	}
	
	public String toString(HashMap<Integer, Boolean> result){
		if(this.analyzer != null){
			return this.analyzer.toString(result);
		}
		return null;
	}
	
	
	abstract public Move generate();
	
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("BW:%s, Name:%s, Type:%s", Shogi.getOrderName(this.order), this.name, Player.getType(this.type));
		str += buff.toString();
		return str;
	}

	
	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	
	public static final int PLAYER_HUMAN = 1;
	public static final int PLAYER_COM   = 2;
	
	public static final int PLAYER_DEFAULT_MAX_SCENE_TO_MEMORIZE   = 10000; // 一万局保持できる。
	
	public static final int SUPERIORITY_NOT_AVAILABLE = -1;
	public static final int SUPERIORITY_WEEKER = 0;    // 下手
	public static final int SUPERIORITY_STORONGER = 1; // 上手
	
	public static String getType(int type){
		String str = "";
		if(type == PLAYER_HUMAN) str = "HUMAN";
		else if(type == PLAYER_COM) str = "COM";
		else str = "N/A";
		return str;
	}
}

