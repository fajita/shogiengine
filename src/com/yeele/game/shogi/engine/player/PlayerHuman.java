package com.yeele.game.shogi.engine.player;

import java.io.*;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;

import static com.yeele.game.shogi.engine.util.Logger.Logger;

public class PlayerHuman extends Player{
	
	
	public PlayerHuman(int order, String name){
		super(Player.PLAYER_HUMAN, order, name);
	}
	
	

	public Move generate(){
		Move mv = null;
		
		InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        Logger.info("eg.+ 33 34 FU\n");
        Logger.info("generate your move: " + (this.order == Shogi.BLACK?"+":"-") + " ");
        boolean out = false;
        int from = 0, to = 0;
        String komaname = "";
        
        while(true){
        	try{
            	String str = br.readLine();
            	String [] strs = str.split("\\s");
            	if(strs.length == 3){
            		/////////////////////////////////////////////////////////
            		if(strs[0].equals("Q")){
            			out = true; break; // this will terminate the game.
            		}////////////////////////////////////////////////////////
            		
            		from = Integer.parseInt(strs[0]);
            		to   = Integer.parseInt(strs[1]);
            		komaname = strs[2].trim().toUpperCase();
            		mv = new Move(this.order, from, to, Koma.getKomaKind(komaname), Koma.FLIP_UNKNOWN, Koma.UK);
            		if( this.shogi.isValidMove(this.order, mv) == Shogi.TRUE){
            			out = true;
            		}else{
            			Logger.error("invalid move!!\n");
            			Logger.info("generate your move: " + (this.order == Shogi.BLACK?"+":"-") + " ");
            			br = new BufferedReader(is);
            		}
            		
            	}else{
            		Logger.error("%d arguments is invalid!! \n", strs.length);
            	}
            }catch(IOException ioe){
            	Logger.error("invalud input!!\n");
            }catch(Exception e){
            	Logger.error("error while parsing!!\n");
            }
        	if(out) break;
        }
        
        return mv;
	}
	
	
}