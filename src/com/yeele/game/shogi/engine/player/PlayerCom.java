package com.yeele.game.shogi.engine.player;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.MoveEval;
import com.yeele.game.shogi.engine.MoveEvalRanking;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;
import com.yeele.util.TimeConcierge;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class PlayerCom extends Player{
	
	private int thinkDepth;
	private int thinkDepthFix;
	public static final int THINK_DEPTH_FOR_NEXT_MOVE = 3;
	public static final int NO_BEST_MOVES = 2;
	
	// debuging purpose
	private int cnt1 = 0, cnt2 = 0, cnt3 = 0;
	private HashMap<String, Integer> sim = new HashMap<String, Integer>();
	
	public PlayerCom(int depth){
		super(Player.PLAYER_COM, Shogi.ORDER_UNKNOWN, "COM");
		this.thinkDepth = depth;
		this.thinkDepthFix = THINK_DEPTH_FOR_NEXT_MOVE;
 		
	}
	
	public PlayerCom(int depth, int order){
		super(Player.PLAYER_COM, order, "COM");
		this.thinkDepth = depth;
		this.thinkDepthFix = THINK_DEPTH_FOR_NEXT_MOVE;
	}
	
	public PlayerCom(int depth, int bw, String name){
		super(Player.PLAYER_COM, bw, name);
		this.thinkDepth = depth;
		this.thinkDepthFix = THINK_DEPTH_FOR_NEXT_MOVE;
	}
	
	/*
	 * order can be decided later, as they decide it by shaking komas.
	 */
	public PlayerCom(String name){
		super(Player.PLAYER_COM, Shogi.ORDER_UNKNOWN, name);
	}
	

	public Evaluation evaluate(){
		Evaluation eval = null;
		if(this.evaluator != null){
			eval = this.evaluator.evaluate(Evaluator.TYPE_DYNAMIC);
		}
		return eval;
	}
	
	
	public Move generate(){
		
		TimeConcierge concierge = new TimeConcierge();
		sim = new HashMap<String, Integer>();
		this.cnt1 = 0; this.cnt2 = 0; this.cnt3 = 0;
		
		Move mv = null;
		Logger.info("Com is generating next best move ...\n");
		
		if(!this.isShogiSet()) return null;
		/*
		try{
			Thread.sleep(2000); //3000ミリ秒Sleepする
		}catch(InterruptedException e){}
		*/
		
		// 1. save the status
		int remember_hand = this.shogi.kifu.getCurrentHand();
		
		// 2. make the move
		MoveEvalRanking bestMoves = new MoveEvalRanking (NO_BEST_MOVES, MoveEvalRanking.ORDER_ASC, this.order);
		bestMoves.clear();
		this.ponder_wNarrowDownProtocol(this.thinkDepth, this.order, bestMoves);
		try{
			Logger.info("@@@@@@ ranking @@@@@\n%s\n", bestMoves);
			mv = bestMoves.get(0).move;
		}catch(IndexOutOfBoundsException e){
			Logger.error("bestMoves(%d) is out of available index!\n", 0);
			mv = null;
		}
		
		// bring the status back
		this.shogi.kifu.removeLaterThan(remember_hand);
		
		concierge.measure();
		
		sim.put("moves in ponder", this.cnt1);
		sim.put("moves in generating single best hand", this.cnt2);
		sim.put("evaluation times", this.cnt3);
		Iterator iter = this.sim.entrySet().iterator();
		while( iter.hasNext() ){
			Map.Entry<String, Integer> entry;
			String key = "";
			Integer val = new Integer(0);
			entry = (Map.Entry<String, Integer>)iter.next();
			key = entry.getKey();
			val = entry.getValue();
			Logger.warn("%s : %d\n", key, val);
		}
		Logger.warn("Com took following time for generating a move\n%s\n", concierge);
		
		return mv;
	}
	
	private void ponder_wNarrowDownProtocol(int depth, int order, MoveEvalRanking bestmoves){
		
		int ret = Shogi.SUCCESS;
		Logger.info("== ponder_wNarrowDownProtocol %s move (depth = %d)\n", Shogi.getOrderNameCasual(order), depth);
		Move rootMove = null;
		boolean evaluate = false;
		// for this player's hand, get multi best ones, but for opponent hand, just get the best.
		int numberBestMoves = (order == this.order) ? NO_BEST_MOVES : 1;
		MoveEvalRanking tmpBestMoves = new MoveEvalRanking (numberBestMoves, MoveEvalRanking.ORDER_ASC, order);
		// thinkDepthFix is fixed depth for pondering best moves for next move.
		this.ponderBestMove(this.thinkDepthFix, order, tmpBestMoves);
		
		try{
			int ponderRootHand;
			for(MoveEval me : tmpBestMoves){
				evaluate = false;
				
				ret = this.shogi.move(me.move, true, false); 
				this.cnt1++; // debugging
				if(ret == Shogi.CHECKMATED){
					evaluate = true;
				}
				
				if(depth == 1){
					evaluate = true;
				}
				
				if(evaluate){
					
					ponderRootHand = this.shogi.kifu.getCurrentHand() - (this.thinkDepth - depth);
					for(int i=ponderRootHand; i <= this.shogi.kifu.getLastHand(); i++){
						Move m = this.shogi.kifu.getMoveByHand(i);
						Logger.info("%02d%02d%2s ", m.from, m.to, Koma.getKomaName(m.komaToMove));
					}
					
					Evaluation e = this.evaluate();
					this.cnt3++; // debugging
					Logger.info("=>\n%s\n%s", e, this.shogi);
					rootMove = this.shogi.kifu.getMoveByHand(ponderRootHand);
					bestmoves.add(new MoveEval(rootMove, e));
					
				}else{
					this.ponder_wNarrowDownProtocol(depth-1, Shogi.getOtherOrder(order), bestmoves);
				}
				this.shogi.back();
			}
			
		}catch(IndexOutOfBoundsException e){
			Logger.error("ponderindex(%d) is out of available index!\n", 0);
		}

		return ;
	}
	
	/*
	 * simply generate that best hands for order given in MoveEvaList.order
	 * during this process, this method also, generating the opponent hand, 
	 * and for opponent hand, just evaluate all possible hands and take the best
	 * one.
	 */
	private void ponderBestMove(int depth, int order, MoveEvalRanking bestmoves){
		int ret = Shogi.SUCCESS;
		Logger.debug("== ponderBestMove %s move (depth = %d)\n", Shogi.getOrderNameCasual(order), depth);
		Move rootMove = null;
		int ponderRootHand;
		boolean evaluate = false;
		ArrayListS<Move> li = this.shogi.getAllHands(order);
		
		// if generating opponent hand.
		MoveEvalRanking bestOpponentMove;
		
		if(order == Shogi.getOtherOrder(bestmoves.order)){
			bestOpponentMove = new MoveEvalRanking(1, MoveEvalRanking.ORDER_ASC, order);
		}else{
			bestOpponentMove = new MoveEvalRanking(depth*2, MoveEvalRanking.ORDER_ASC, order);
		}
		
		for(Move m : li){
			ret = this.shogi.move(m, true, false);
			this.cnt2++; // debugging
			Evaluation e = this.evaluate();
			this.cnt3++; // debugging
			Logger.debug("move -> %s, Eval -> %s\n", m, e);
			bestOpponentMove.add(new MoveEval(m, e));
			this.shogi.back();
		}
		// put those best moves into li,
		// instead of all possible hands, you just narrow it down to a oppoenet best move.
		li.clear();
		for(MoveEval me : bestOpponentMove){
			li.add(me.move);
		}
		
		Logger.debug("evaluating ... %d moves\n", li.size());
		for(Move m : li){
			Logger.debug("--- evaluation ---->>>>>>>>>>>>>>>>>\n");
			evaluate = false;
			ret = this.shogi.move(m, true, false);
			this.cnt2++; // debugging
			
			Logger.debug("%s\n", this.shogi);
			if(ret == Shogi.CHECKMATED){
				evaluate = true;
			}
			
			if(depth == 1){
				evaluate = true;
			}
			
			if(evaluate){
				Evaluation e = this.evaluate();
				this.cnt3++; // debugging
				Logger.debug("move -> %s, Eval -> %s\n", m, e);
				ponderRootHand = this.shogi.kifu.getCurrentHand() - (this.thinkDepthFix - depth);
				rootMove = this.shogi.kifu.getMoveByHand(ponderRootHand);
				bestmoves.add(new MoveEval(rootMove, e));
			}else{
				this.ponderBestMove(depth - 1, Shogi.getOtherOrder(order), bestmoves);
			}
			this.shogi.back();
			Logger.debug("<<<<<<<<<<<<<<<<-------------------\n\n");
		}
		
	}
	
}










