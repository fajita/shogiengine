package com.yeele.game.shogi.engine.player;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.MoveEval;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.TreeOfMoveEval;
import com.yeele.game.shogi.engine.analyzer.SujiAnalyzer;
import com.yeele.game.shogi.engine.stratagy.attacktactic.AttachTactic;
import com.yeele.game.shogi.engine.stratagy.attacktactic.ShikenBisha;
import com.yeele.game.shogi.engine.stratagy.castle.Castle;
import com.yeele.game.shogi.engine.stratagy.castle.KataMino;
import com.yeele.game.shogi.engine.stratagy.castle.Tactic;
import com.yeele.game.shogi.engine.stratagy.suji.BackupSuji;
import com.yeele.game.shogi.engine.stratagy.suji.CheckOuSuji;
import com.yeele.game.shogi.engine.stratagy.suji.EscapeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.HashiFuUkeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.OuEscapeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PlaceIfProfitableSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PutBetweenBasicSuji;
import com.yeele.game.shogi.engine.stratagy.suji.ReinforceSuji;
import com.yeele.game.shogi.engine.stratagy.suji.Suji;
import com.yeele.game.shogi.engine.stratagy.suji.TakeLineSuji;
import com.yeele.game.shogi.engine.stratagy.suji.TakeOuThreatSuji;
import com.yeele.game.shogi.engine.stratagy.suji.TakeSuji;
import com.yeele.util.ArrayListS;
import com.yeele.util.TimeConcierge;

import static com.yeele.game.shogi.engine.util.Logger.Logger;



public class PlayerCom4 extends Player{
	
	private int thinkDepthMax;
	private HashMap<Integer, Integer> sujiPriorityDefaultMap;
	
	private ArrayListS<Koma> komasThatIDontWannaMove = new ArrayListS<Koma>(); 
	
	private Castle castle;
	private AttachTactic attackTactic;
	
	protected int numOfMovesPerRead;
	private int numOfMovesPerSuji;
	public static int MINUS_POINT_4_KOMA_DONT_WANNA_MOVE = 5000;
	
	private class DebugMembers{
		HashMap<String, Integer> counter = new HashMap<String, Integer>();
		
		public DebugMembers(){
			this.counter.put("read", 0);
			this.counter.put("narrowdown", 0);
			this.counter.put("memory", 0);
			this.counter.put("memory_hit", 0);
			this.counter.put("memory_carryover", 0);
			
		}
		
		public int increment(String key, int increment){
			this.counter.put(key, this.counter.get(key) + increment);
			return this.counter.get(key);
		}
		
		public String toString(){
			String str = "";
			ByteArrayOutputStream buff = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(buff);
			for(Map.Entry<String, Integer> entry : this.counter.entrySet()){
				ps.printf("%s:%d\n", entry.getKey(), entry.getValue());
			}
			str += buff.toString();
			return str;
		}
	}
	private DebugMembers bugmem;
	
	
	private class ValueComperator implements Comparator<Object>{
	    public int compare(Object obj1, Object obj2){
	        Map.Entry ent1 =(Map.Entry)obj1;
	        Map.Entry ent2 =(Map.Entry)obj2;
	        Integer val1 = (Integer) ent1.getValue();
	        Integer val2 = (Integer) ent2.getValue();
	        return val2.compareTo(val1);
	    }
	}
	private ValueComperator valueComparator = new ValueComperator();
	
	/*
	 * depth is the depth of pondering.
	 * narrowed is the number of moves this player can narrow down as he/she ponders.
	 */
	
	public PlayerCom4(int order, String name, int depth, int numberOfMovesPerRead){
		super(Player.PLAYER_COM, order, name);
		this.thinkDepthMax = depth;
		this.setNumOfMovesPerRead(numberOfMovesPerRead);
		
	}
	public PlayerCom4(int order, String name, int depth, int numberOfMovesPerRead, int maxMemorableScenes){
		super(Player.PLAYER_COM, order, name, maxMemorableScenes);
		this.thinkDepthMax = depth;
		this.setNumOfMovesPerRead(numberOfMovesPerRead);
	}

	public void setNumOfMovesPerRead(int numberOfMovesPerRead){
		this.numOfMovesPerRead = numberOfMovesPerRead;
		// determince private int numOfMovesPerSuji;
		int numOfSujis = this.sujiIdMap.size();
		int tmp = 1;
		if(numOfSujis > 0){
			tmp = this.numOfMovesPerRead / numOfSujis;
			if(tmp == 0) tmp = 1;
		}
		this.numOfMovesPerSuji = tmp;
	}
	
	public void setSuji(){
		this.addSuji(new HashiFuUkeSuji(this.shogi));
		this.addSuji(new KataMino(this.shogi));
		this.addSuji(new ShikenBisha(this.shogi));
		this.addSuji(new ReinforceSuji(this.shogi));
		this.addSuji(new TakeSuji(this.shogi));
		this.addSuji(new PutBetweenBasicSuji(this.shogi));
		this.addSuji(new CheckOuSuji(this.shogi));
		this.addSuji(new PlaceIfProfitableSuji(this.shogi));
		this.addSuji(new EscapeSuji(this.shogi));
		this.addSuji(new BackupSuji(this.shogi));
		this.addSuji(new TakeLineSuji(this.shogi));
		this.addSuji(new OuEscapeSuji(this.shogi));
		this.addSuji(new TakeOuThreatSuji(this.shogi));
		
		// setting static suji-score map
		this.sujiPriorityDefaultMap = new HashMap<Integer, Integer>();
		this.sujiPriorityDefaultMap.put(Suji.ID_HASHI_FU_UKE, 1);
		this.sujiPriorityDefaultMap.put(Suji.ID_CASTLE_KATA_MINO, 4);
		this.sujiPriorityDefaultMap.put(Suji.ID_ATTACK_SHIKEN_BISHA, 2);
		this.sujiPriorityDefaultMap.put(Suji.ID_REINFORCE, 1);
		this.sujiPriorityDefaultMap.put(Suji.ID_TAKE, 0);
		this.sujiPriorityDefaultMap.put(Suji.ID_PUTBETWEEN_BASIC, 1);
		this.sujiPriorityDefaultMap.put(Suji.ID_CHECK_OU, 3);
		this.sujiPriorityDefaultMap.put(Suji.ID_PLACE_IF_PROFITABLE, 1);
		this.sujiPriorityDefaultMap.put(Suji.ID_ESCAPE, 1);
		this.sujiPriorityDefaultMap.put(Suji.ID_BACKUP, 0);
		this.sujiPriorityDefaultMap.put(Suji.ID_TAKE_LINE, 0);
		this.sujiPriorityDefaultMap.put(Suji.ID_OU_ESCAPE, 5);
		this.sujiPriorityDefaultMap.put(Suji.ID_TAKE_OU_THREAT, 5);
	}
	

	public Evaluation evaluate(){
		return this.evaluate(Evaluator.TYPE_STATIC_SHALLOW);
	}
	
	public void updateStratagy(){
		
		int stage = this.evaluator.getStage();
		if(stage==Evaluator.STAGE_OPENNING_GAME){
			if(this.castle == null){
				this.castle = (Castle)this.sujiIdMap.get(Suji.ID_CASTLE_KATA_MINO);
			}
			if(this.attackTactic == null){
				this.attackTactic = (AttachTactic)this.sujiIdMap.get(Suji.ID_ATTACK_SHIKEN_BISHA);
			}
			
		}else if(stage==Evaluator.STAGE_MIDDLE_GAME){
			
		}else if(stage==Evaluator.STAGE_END_GAME){
			
			this.castle = null;
			this.attackTactic= null;
		}
		
		
	}
	
	public Move generate(){
		
		TimeConcierge concierge = new TimeConcierge();
		
		
		Move mv = null;
		Logger.info("Com is generating next best move ...\n");
		
		if(!this.isShogiSet()) return null;
		
		// 毎回 hashmapが満杯だとメモリパフォーマンスが悪いのでをrefresh
		//this.sceneMap.refresh();
		this.sceneMap.clear();
		
		MoveEval bestEvalMove = this.ponder(this.thinkDepthMax, this.order);
		if(bestEvalMove != null){
			mv = bestEvalMove.move;
		}
		
		
		concierge.measure();
		Logger.warn("Com took following time for generating a move\n%s\n", concierge);
		
		return mv;
	}
	
	public void narrowdown(int num, ArrayListS<Move> moves, HashMap<Integer, Integer> priorityMap){
		if(moves.size() <= num) return;

		// use hash table hashmap<Move, integer> then sort with vlaue.
		HashMap<Move, Integer> scoreMap = new HashMap<Move, Integer>();
		Logger.debug("moves b4 narrowdown to %d\n", num);
		for(Move m : moves){
			Logger.debug("%s\n", m);
			
			int score = this.evaluator.evaluate(m);
			// 筋別の得点を足す
			//　筋別の得点表は動的に作成したい。最初は静的でよい。
			//HashMap<Integer, Integer> sujiScoreMap = new HashMap<Integer, Integer>();
			
			// sujiスコアを掛けてしまえ！！kindIdではなくevaluator.Socreでscoreを計算している
			// ので足し算では、ことんど効果がない。
			score *= this.evaluator.getTotalSujiScore(m, priorityMap);
			
			// さらに、他のsujiとの相性が悪いものは減点する
			// e.g. 囲いの上の部分の歩は早めに付きたくない。etc
			Koma koma = this.shogi.board.getKoma(m.from);
			if(koma != null){
				if(this.komasThatIDontWannaMove.contains(koma)){
					score -= MINUS_POINT_4_KOMA_DONT_WANNA_MOVE;
				}
			}
			
			scoreMap.put(m, score);
		}
		
		// SORT 
		ArrayList<Map.Entry> entries = new ArrayList(scoreMap.entrySet());
		Collections.sort(entries, this.valueComparator);
		
		// DELETE OUT OF THE RANK
		int nth = 1;
		for(Map.Entry entry : entries){
			Logger.debug("%dth key=%s, val=%d\n", nth, entry.getKey(), entry.getValue());
			if(nth > num){
				moves.remove(entry.getKey());
			}
			nth++;
		}
		/*
		Iterator<Map.Entry<Move, Integer>> i = entries.iterator();  
        while ( i.hasNext() )  
        {  
        	Map.Entry<Move, Integer> entry = (Map.Entry<Move, Integer>)i.next();
        	Logger.debug("%dth key=%s, val=%d\n", nth, entry.getKey(), entry.getValue());
        	nth++;
        } 
        */
		Logger.debug("moves after narrowdown to %d\n", num);
		for(Move m : moves){
			Logger.debug("%s\n", m);
		}
	}
	
	private ArrayListS<Move> narrowdown(int order){
		int opponent = Shogi.getOtherOrder(order);
		ArrayListS<Move> moves = new ArrayListS<Move>();
		this.komasThatIDontWannaMove.clear(); // 動かしたくない駒を覚えておくための変数
		
		HashMap<Integer, Integer> sujiPriorityMap = new HashMap<Integer, Integer>(this.sujiPriorityDefaultMap);
		
		//状況解析
		// - 囲いの建設状況- 戦型の見直し、
		this.updateStratagy();
		
		// - 序盤、中盤、終盤 打ち方の基本
		ArrayListS<Suji> sujiList = new ArrayListS<Suji>();
		int stage = this.evaluator.getStage();
		
		if(stage==Evaluator.STAGE_OPENNING_GAME){
			if(order == this.getOrder()){
				if(this.castle != null){
					sujiList.addIfNot(this.getSuji(this.castle.getSujiId()));
				}
				
				if(this.attackTactic != null){
					sujiList.addIfNot(this.getSuji(this.attackTactic.getSujiId()));
				}
			}
			
		}else if(stage==Evaluator.STAGE_MIDDLE_GAME){
			
		}else if(stage==Evaluator.STAGE_END_GAME){
			sujiList.addIfNot(this.getSuji(Suji.ID_CHECK_OU));
		}
		
		// 最終手の手の種類に対応した、筋を追加
		Move lastMove = this.shogi.kifu.getLastMove();
		lastMove.setExtension(this.analyze(lastMove));
		
		if(lastMove.isSuji(Suji.ID_HASHI_FU_UKE)){
			sujiList.addIfNot(this.getSuji(Suji.ID_HASHI_FU_UKE));
		}
		if(lastMove.isSuji(Suji.ID_CHECK_OU)){
			sujiList.addIfNot(this.getSuji(Suji.ID_OU_ESCAPE));
			sujiList.addIfNot(this.getSuji(Suji.ID_TAKE_OU_THREAT));
		}
		if(lastMove.isSuji(Suji.ID_OU_ESCAPE) || lastMove.isSuji(Suji.ID_TAKE_OU_THREAT)){
			// 動的に優先度を高めている。これで王手は優先的に選ばれる可能性が高い
			sujiPriorityMap.put(Suji.ID_CHECK_OU, sujiPriorityMap.get(Suji.ID_CHECK_OU) + 3 );
			sujiList.addIfNot(this.getSuji(Suji.ID_CHECK_OU));
		}
		
		// 最終5手のおける、意味とそれに対応する筋の追加　// To be extended.
		
		// 盤局面の状態から、手をつくる筋を追加。　
		// 升の点数低いところを補充する筋 , 升の突破できそうなところに効きを足す筋　etc
		sujiList.addIfNot(this.getSuji(Suji.ID_REINFORCE));
		sujiList.addIfNot(this.getSuji(Suji.ID_TAKE));
		sujiList.addIfNot(this.getSuji(Suji.ID_PUTBETWEEN_BASIC));
		sujiList.addIfNot(this.getSuji(Suji.ID_PLACE_IF_PROFITABLE));
		sujiList.addIfNot(this.getSuji(Suji.ID_TAKELINE));
		
		ArrayListS<Move> generatedMoves = null;
		// ここまでに手を生成するsujiが決定しているので、それらを生成する。
		for(Suji activeSuji : sujiList){
			generatedMoves = activeSuji.generate(order);
			
			Logger.debug("%s generated\n", activeSuji.getName());
			narrowdown(this.numOfMovesPerSuji, generatedMoves, sujiPriorityMap);
			moves.addIfNot(generatedMoves);
			
			// 戦術なら、動かしたくない駒がでてくるので、記憶しておく
			if(activeSuji.isTactic()){
				Tactic activeTacticSuji = (Tactic)activeSuji;
				this.komasThatIDontWannaMove.addIfNot(activeTacticSuji.getKomaShouldStay(order));
			}
			
		}
		
		// 交戦升においての手を生成する
		ArrayListS<Move> placeIfProfitableSujiMoves = new ArrayListS<Move>();
		ArrayListS<Move> backupSujiMoves = new ArrayListS<Move>();
		ArrayListS<Move> escapeSujiMoves = new ArrayListS<Move>();
		// ArrayListS<Move> xxxxxSujiMoves = new ArrayListS<Move>();
		for(Masu masu : this.shogi.board.getInterfered()){
			placeIfProfitableSujiMoves.addIfNot(this.getSuji(Suji.ID_PLACE_IF_PROFITABLE).generateFrom(order, masu.koma));
			escapeSujiMoves.addIfNot(this.getSuji(Suji.ID_ESCAPE).generateBy(order, masu.koma));
			//xxxxxSujiMoves.addIfNot(this.getSuji(Suji.ID_XXXXXX).generateXX(order, XXXX));
			if( (masu.isKoma(order) && masu.koma.canBeTakenForFree()) || 
				(masu.isEmpty() && masu.canBePromotedForFreeBy(opponent)) ){
				backupSujiMoves.addIfNot(this.getSuji(Suji.ID_BACKUP).generateFor(order, masu));
			}
			
		}
		narrowdown(this.numOfMovesPerSuji, placeIfProfitableSujiMoves, sujiPriorityMap);
		moves.addIfNot(placeIfProfitableSujiMoves);
		narrowdown(this.numOfMovesPerSuji, escapeSujiMoves, sujiPriorityMap);
		moves.addIfNot(escapeSujiMoves);
		narrowdown(this.numOfMovesPerSuji, backupSujiMoves, sujiPriorityMap);
		moves.addIfNot(backupSujiMoves);
		//narrowdown(3, xxxxxSujiMoves);
		//moves.addIfNot(xxxxxSujiMoves);
				
		// workaround まだ、sujiがそんなにないので、適当に生成してやります。この処理はあとで消します
		if(moves.size() == 0){
			ArrayListS<Move> all = this.shogi.getAllHands(order);
			if(all.size() > 0) moves.addIfNot(all.get(0));
		}
		
		narrowdown(this.numOfMovesPerRead, moves, sujiPriorityMap); // 6 ^ 5 = 7776 hands to read at max.
		
		return moves;
		
	}
	
	private MoveEval ponder(int depth, int order){
		MoveEval best = null;
		this.bugmem = new DebugMembers();
		
		Evaluation currentEval = this.evaluate();
		//this.setEvalBeforePonder(this.evaluate(Evaluator.TYPE_DYNAMIC)); // used in reinforce suji.
		Move curMove = shogi.kifu.getCurrentMove();
		// the fruit on the root has evaluation of current situation.
		TreeOfMoveEval root = new TreeOfMoveEval();
		root.setFruit(new MoveEval(curMove, currentEval));
		// this move is already played, so it's confirmed.
		root.setConfirmed(true);
		// 1. save the status
		int remember_hand = this.shogi.kifu.getCurrentHand();
				
		this.growTree(depth, order, root);
		//Logger.debug("%s\n", root.getTreeString());
		if(root.markNextBestMoves() == Shogi.SUCCESS){
			Logger.info("%d moves has been read..\n", this.bugmem.increment("read", 1));
			Logger.info("Yomi suji...\n");
			Logger.info("%s\n", root.toStringConfirmedPath());
			Logger.debug("full path is \n%s\n", root);
			best = root.getConfirmedFruitInBranches();
			Logger.info("debugging infomation...\n%s\n", this.bugmem);
		}
		
		// bring the status back
		this.shogi.kifu.removeLaterThan(remember_hand);
				
		return best;
	}
	
	private int growTree(int depth, int order, TreeOfMoveEval tree){
		
		int ret = Shogi.SUCCESS;
		
		// treeにbranchesが既にある場合、又は
		// sceneMapにこの局面の記録があるか探して、あった場合
		boolean enableOptimazation = true;
		String hashKey = "";
		boolean isMemorized = false;
		if(enableOptimazation){
			hashKey = this.sceneMap.generateHashKeyString(this.shogi);
			isMemorized = this.sceneMap.containsKey(hashKey);
		}
		
		if(enableOptimazation && (tree.branches.size() > 0 || isMemorized)){
			this.bugmem.increment("memory", 1);
			if(isMemorized){
				this.bugmem.increment("memory_hit", 1);
				TreeOfMoveEval memorizedTree = this.sceneMap.get(hashKey);
				// 読みが深い方をtreeにくっ付ける。
				if(memorizedTree.getRelativeDepthToTip() > tree.getRelativeDepthToTip()){
					
					// 全てのbranchesを削除する。
					tree.branches.clear();
					// depthはaddBranch関数の中で自動的に調整される
					for(TreeOfMoveEval branch : memorizedTree.branches){
						tree.addBranch(branch);
					}
					// TBD. treeの一個前trunkに戻って、treeを削除してmemorizedTreeをaddする
					// 方法もあるかなー、とおもうんだけど、削除したオブジェクトを不意に参照してしまいそうで嫌だった。
				}else{
					this.bugmem.increment("memory_carryover", 1);
				}
			}else{
				this.bugmem.increment("memory_carryover", 1);
			}
			
			// TODO java.util.ConcurrentModificationException
			// どこかでtreeを書き換えてしまう。
			
			// tree自体がdepth分の情報をもっているなら何もしなくてよい。
			if( tree.getRelativeDepthToTip() < (depth - 1)){
				for(TreeOfMoveEval branch : tree.branches){
					MoveEval me = branch.getFruit();
					if(me == null) continue;
					Move move = me.move;
					ret = this.shogi.move(move, true, false);
					this.bugmem.increment("read", 1);
					Logger.debug("%s played at depth = %d!!\n", move, tree.getDepth()+1);
					
					if(ret == Shogi.CHECKMATED){
						// do nothing means stop searching.
					}else if(depth > 1){ // if depth is 1, this was the last to search.
						this.growTree(depth-1, Shogi.getOtherOrder(order), branch);	
					}
					
					this.shogi.back();
				}
			}
			
		}else{
			// TODO: refresh()のタイミングは要検討
			//this.sceneMap.refresh();
			this.bugmem.increment("narrowdown", 1);
			ArrayListS<Move> moves = narrowdown(order);
			
			if(tree.getDepth()+1==1){
				Logger.info("%d moves are generated at(depth = %d)\n", moves.size(), tree.getDepth()+1);
				for(Move m : moves){
					Logger.info("%s\n", m.toStringExtended());
				}
			}else{
				Logger.debug("%d moves are generated at(depth = %d)\n", moves.size(), tree.getDepth()+1);
				for(Move m : moves){
					Logger.debug("%s\n", m.toStringExtended());
				}
			}
			
			Logger.debug("----------------------------\n\n");
			for(Move move : moves){
				ret = this.shogi.move(move, true, false);
				Logger.debug("%s played at depth = %d!!\n", move, tree.getDepth()+1);
				this.bugmem.increment("read", 1);
				Evaluation eval = this.evaluate();
				TreeOfMoveEval branch = new TreeOfMoveEval(new MoveEval(move, eval));
				tree.addBranch(branch);
				
				if(ret == Shogi.CHECKMATED){
					// do nothing means stop searching.
				}else if(depth > 1){ // if depth is 1, this was the last to search.
					this.growTree(depth-1, Shogi.getOtherOrder(order), branch);	
				}
				
				this.shogi.back();
			}
		}
		
		// このtreeの登録、この局面にたいして。
		if(enableOptimazation){
			String hashKeyRegister = this.sceneMap.generateHashKeyString(this.shogi);
			this.sceneMap.put(hashKeyRegister, tree);
		}
		
		return ret;
	}
	
	
	
	
}










