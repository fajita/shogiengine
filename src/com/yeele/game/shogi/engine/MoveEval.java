package com.yeele.game.shogi.engine;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;



public class MoveEval{
	public Move move;
	public Evaluation eval;
	
	public MoveEval(){
		this.move = null;
		this.eval = null;
	}
	
	public MoveEval(Move mv, Evaluation ev){
		this.move = mv;
		this.eval = ev;
	}
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s\n%s", this.move, this.eval);
		str += buff.toString();
	
		return str;
	}
	
	public String toStringSimple(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s %s", this.move, this.eval.getStringScoreDiff(this.move.order));
		str += buff.toString();
	
		return str;
	}
}


