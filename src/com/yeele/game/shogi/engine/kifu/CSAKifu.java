package com.yeele.game.shogi.engine.kifu;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.KomaBox;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.Stand;
import com.yeele.game.shogi.engine.komas.Fu;
import com.yeele.game.shogi.engine.komas.Gi;
import com.yeele.game.shogi.engine.komas.Hi;
import com.yeele.game.shogi.engine.komas.Ka;
import com.yeele.game.shogi.engine.komas.Ke;
import com.yeele.game.shogi.engine.komas.Ki;
import com.yeele.game.shogi.engine.komas.Ky;
import com.yeele.game.shogi.engine.komas.Ou;
import com.yeele.game.shogi.engine.util.IntHolder;
import com.yeele.game.shogi.engine.util.Logger;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;

public class CSAKifu implements KifuIF{
	
	// mandatory
	// V2.2
	
	// optional
	// N+NAKAHARA
	// N-YONENAGA
	
	// $EVENT:(文字列)
	// $SITE:(文字列)
	// $START_TIME:YYYY/MM/DD HH:MM:SS(時刻は省略可)
	// $END_TIME:YYYY/MM/DD HH:MM:SS
	// $TIME_LIMIT:HH:MM+SS
	// $OPENING:(文字列)
	// Also additional keyword can be added arbitrary, starting with $
	// $　 
	
	// <keyword, value>
	protected HashMap<String, String> info = new HashMap<String, String>(); 
	protected ArrayListS<KifuEntry> entries = new ArrayListS<KifuEntry>();
	protected int currentHand;  // steps which has been done.
	                  // if the game is just started and player has not yet play a turn, 
	                  // currentHand is 0
	
	/*
	CSAKifu だけ無料公開できるように、このクラスで全て完結させたいが、
	現時点では、Shogiに依存させます。
	理由は、
	そのほうが速いから。
	実装の重複がない。
	現時点で、CSAKifuの用途はこのShogiでの使用
	だから、
	今後、このCSAをフリーソースとして公開したくなったら、Shogiに依存している部分だけ変更すればいい。
	
	現時点での依存は、
	クラス: Board, Stand, 
	static: Shogi.BLACK, Shogi.WHITE
	くらいである。
	*/
	private int initialOrder = Shogi.ORDER_UNKNOWN;
	private int initialBoardIndicateMethod = INITIAL_BOARD_NONE;
	protected Board board = new Board(null);
	protected Stand [] stand = new Stand[2];
	protected KomaBox box = new KomaBox(null);
	protected ArrayListS<Koma> ou = new ArrayListS<Koma>();
	protected ArrayListS<Koma> hi = new ArrayListS<Koma>();
	protected ArrayListS<Koma> ka = new ArrayListS<Koma>();
	protected ArrayListS<Koma> ki = new ArrayListS<Koma>();
	protected ArrayListS<Koma> gi = new ArrayListS<Koma>();
	protected ArrayListS<Koma> ke = new ArrayListS<Koma>();
	protected ArrayListS<Koma> ky = new ArrayListS<Koma>();
	protected ArrayListS<Koma> fu = new ArrayListS<Koma>();
	
	
	public CSAKifu(HashMap<String, String> info){
		this.info = info;
		this.init();
	}
	
	public CSAKifu(){
		this.info.put(KW_VERSION, "V2.2");
		this.init();
	}
	
	public CSAKifu(String version){
		this.info.put(KW_VERSION, version);
		this.init();
	}
	
	public void init(){
		this.stand[Shogi.BLACK] = new Stand(null, Shogi.BLACK);
		this.stand[Shogi.WHITE] = new Stand(null, Shogi.WHITE);
		this.entries.clear();
		currentHand = 0;
		this.initKomas();
		this.setKeywords();
		this.setSpecialWords();
	}
	
	
	private void resetInitialBoard(){
		this.board.reset();
		this.stand[Shogi.BLACK].reset();
		this.stand[Shogi.WHITE].reset();
		this.box.reset();
		
		this.putAllKomaInBox();
	}
	
	public void putAllKomaInBox(){
		this.box.reset();
		
		for(Koma koma : this.ou){ koma.bePlaced(this.box); }
		for(Koma koma : this.hi){ koma.bePlaced(this.box); }
		for(Koma koma : this.ka){ koma.bePlaced(this.box); }
		for(Koma koma : this.ki){ koma.bePlaced(this.box); }
		for(Koma koma : this.gi){ koma.bePlaced(this.box); }
		for(Koma koma : this.ke){ koma.bePlaced(this.box); }
		for(Koma koma : this.ky){ koma.bePlaced(this.box); }
		for(Koma koma : this.fu){ koma.bePlaced(this.box); }
	
	}
	
	public ArrayListS<Koma> getAllKomas(){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		komas.addAll(this.ou);
		komas.addAll(this.hi);
		komas.addAll(this.ka);
		komas.addAll(this.ki);
		komas.addAll(this.gi);
		komas.addAll(this.ke);
		komas.addAll(this.ky);
		komas.addAll(this.fu);
		return komas;
	}
	
	public void initKomas(){
		// 全ての駒を生成する
		Koma koma = null;
		int [] orders = {Shogi.BLACK, Shogi.WHITE};
		// 先手後手に一枚づつだから1
		for(int i=0; i<1; i++){ 
			for(int order : orders){
				koma = new Ou(null, order);
				this.ou.add(koma);
			}
		}
		
		for(int i=0; i<1; i++){
			for(int order : orders){
				koma = new Hi(null, order);
				this.hi.add(koma);
			}
		}
		
		for(int i=0; i<1; i++){
			for(int order : orders){
				koma = new Ka(null, order);
				this.ka.add(koma);
			}
		}
		
		for(int i=0; i<2; i++){
			for(int order : orders){
				koma = new Ki(null, order);
				this.ki.add(koma);
			}
		}
		
		for(int i=0; i<2; i++){
			for(int order : orders){
				koma = new Gi(null, order);
				this.gi.add(koma);
			}
		}
		
		for(int i=0; i<2; i++){
			for(int order : orders){
				koma = new Ke(null, order);
				this.ke.add(koma);
			}
		}
		
		for(int i=0; i<2; i++){
			for(int order : orders){
				koma = new Ky(null, order);
				this.ky.add(koma);
			}
		}
		
		for(int i=0; i<9; i++){
			for(int order : orders){
				koma = new Fu(null, order);
				this.fu.add(koma);
			}
		}
	}
	
	
	public void setName(int order, String name){
		if(order == Shogi.BLACK){
			this.info.put(KW_NAME_B, name);
		}else if(order == Shogi.BLACK){
			this.info.put(KW_NAME_W, name);
		}
	}
	
	/* 
	 * return null if key doesn't exist
	 * otherwise, return the string.
	 */
	public String getInfo(String key){
		String ret = null;
		if(this.info.containsKey(key)){
			ret = this.info.get(key);
		}
		return ret;
	}
	
	public void setInfo(String key, String value){
		this.info.put(key, value);
	}
	
	public int add(KifuEntry kifuentry){
		int ret = Shogi.SUCCESS;
		
		if(this.getCurrentHand() < this.entries.size()){
			this.removeLaterThan(this.getCurrentHand());
		}
		this.entries.add(kifuentry);
		this.incrementCurrentHand();
		return ret;
	}
	
	public int add(Move mv){
		int ret = Shogi.SUCCESS;
		
		if(this.getCurrentHand() < this.entries.size()){
			this.removeLaterThan(this.getCurrentHand());
		}
		this.entries.add(new KifuEntry(mv));
		this.incrementCurrentHand();
		return ret;
	}
	
	public int remove(int idx){
		int ret = Shogi.SUCCESS;
		try{
			this.entries.remove(idx);
		}catch(IndexOutOfBoundsException e){
			Logger.error("index(%d) is out of available index!\n", idx);
			ret = Shogi.ERROR;
		}
		return ret;
	}
	
	public int remove(Move mv){
		int ret = Shogi.SUCCESS;
		try{
			this.entries.remove(mv);
		}catch(IndexOutOfBoundsException e){
			Logger.error("remove(): IndexOutOfBoundsException!\n");
			ret = Shogi.ERROR;
		}
		return ret;
	}
	
	public KifuEntry getKifuEntryByHand(int hand){
		KifuEntry kifuentry = null;
		try{
			kifuentry = this.entries.get(CSAKifu.getIndex(hand));
		}catch(IndexOutOfBoundsException e){
			Logger.error("getKifuEntryByHand(hand=%d): IndexOutOfBoundsException!\n", hand);
			kifuentry = null;
		}
		return kifuentry;
	}
	
	public Move getMoveByHand(int hand){
		
		Move mv = null;
		try{
			KifuEntry kifuentry = this.entries.get(CSAKifu.getIndex(hand));
			mv = kifuentry.getMove();
		}catch(IndexOutOfBoundsException e){
			Logger.error("getMoveByHand(hand=%d): IndexOutOfBoundsException!\n", hand);
			mv = null;
		}
		return mv;
	}
	
	public Move getMoveByIndex(int index){
		Move mv = null;
		try{
			KifuEntry kifuentry = this.entries.get(index);
			mv = kifuentry.getMove();
		}catch(IndexOutOfBoundsException e){
			Logger.error("getMoveByIndex(hand=%d): IndexOutOfBoundsException!\n", index);
			mv = null;
		}
		return mv;
	}
	
	/***************************************************************************
	     currentHand related methods
	**************************************************************************/
	public int getCurrentHand(){
		return this.currentHand;
	}
	
	public int setCurrentHand(int hand){
		int ret = Shogi.SUCCESS;
		if(0 <= hand && hand <= this.getLastHand() ){
			this.currentHand = hand;
		}else{
			ret = Shogi.ERROR;
			Logger.error("setCurrentMove() out of range.\n");
		}
		return ret;
	}
	
	public void incrementCurrentHand(){
		this.currentHand++;
	}
	
	public int decrementCurrentHand(){
		int ret = Shogi.SUCCESS;
		if(this.currentHand <= 0){
			ret = Shogi.ERROR;
			Logger.error("decrementCurrentHand() cannot decrement further!\n");
		}else{
			this.currentHand--;
		}
		return ret;
	}
	
	
	
	public KifuEntry getLastKifuEntry(){
		int n = this.entries.size();
		return this.getKifuEntryByHand(n);
	}
	
	public Move getCurrentMove(){
		return this.getMoveByHand(this.currentHand);
	}
	
	public int getLastHand(){
		return this.entries.size();
	}
	
	public Move getLastMove(){
		int n = this.entries.size();
		return this.getMoveByHand(n);
	}
	
	public int removeLaterThan(int hand){
		int ret = Shogi.SUCCESS;
		
		if(this.entries.size() > hand){
			int cnt = 1;
			for(Iterator<KifuEntry> i = this.entries.iterator(); i.hasNext()==true; cnt++){
				KifuEntry kifuentry = i.next();
				
				if(kifuentry == null) break;
				if(cnt > hand){
					try{
						i.remove();
					}catch(IndexOutOfBoundsException e){
						Logger.error("removeLaterThan(): IndexOutOfBoundsException!\n");
						ret = Shogi.ERROR;
						break;
					}
				}
			}
			this.setCurrentHand(hand);
		}
		
		
		return ret;
	}
	
	public int getNextOrder(){
		int order = Shogi.ORDER_UNKNOWN;
		Move mv = this.getCurrentMove();
		if(mv != null){
			order = Shogi.getOtherOrder(mv.order);
		}
		return order;
	}
	
	public int getCurrentOrder(){
		return this.getOrder(this.getCurrentHand());
	}
	
	public int getOrder(int hand){
		int order = Shogi.ORDER_UNKNOWN;
		Move mv = getMoveByHand(hand);
		if(mv != null){
			order = mv.order;
		}
		return order;
	}
	
	public String toVerboseString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		for(String keyword : this.infoKeywordsSequenced){
			buff.reset();
			String key_value = this.info.get(keyword);
			if(key_value != null){
				ps.printf("%s:%s\n", keyword, key_value);
				str += buff.toString();
			}
		}
		
		// arbitrary keyword if there is.
		for(Map.Entry<String, String> entry : this.info.entrySet()){
			String keyword = entry.getKey();
			// もしinfoKeywordsSequencedに含まれていないkeywordであれば、追加表示する
			if(!this.infoKeywordsSequenced.contains(keyword)){
				buff.reset();
				String key_value = this.info.get(keyword);
				if(key_value != null){
					ps.printf("%s:%s\n", keyword, key_value);
					str += buff.toString();
				}
			}
		}
		
		buff.reset();
		ps.printf("%s\n", this.toString());
		str += buff.toString();
		return str;
	}
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		ps.printf("CurrentHand:%d\n"
				, this.currentHand
				);
		str += buff.toString();
		int cnt = 1;
		for(Iterator<KifuEntry> i = this.entries.iterator(); i.hasNext()==true; cnt++){
			buff.reset();
			KifuEntry kifuentry = i.next();
			ps.printf("%2d: %s\n", cnt, kifuentry);
			str += buff.toString();
		}	
		
		return str;
	}
	
	private void stateMachineFunction(IntHolder stateMachine, String line)
	throws Exception
	{
		
		// ステートマシーンが最後の状態になるか、
		// このlineが読み込まれたら終了する。
		while(stateMachine.value <= STATE_MACHINE_KIFU){
			if(stateMachine.value == STATE_MACHINE_VERSION){
				if(this.readVersion(stateMachine, line)) break;
			}else if(stateMachine.value == STATE_MACHINE_INFOMATION){
				if(this.readInformation(stateMachine, line)) break;
			}else if(stateMachine.value == STATE_MACHINE_INITIAL_BOARD){
				if(this.readInitialBoard(stateMachine, line)) break;
			}else if(stateMachine.value == STATE_MACHINE_TEBAN){
				if(this.readTeban(stateMachine, line)) break;  
			}else if(stateMachine.value == STATE_MACHINE_KIFU){
				if(this.readKifu(stateMachine, line)) break;  
			}else if(stateMachine.value == STATE_MACHINE_FOOT_COMMENT){
				// do nothing, more or less it would never reach here.  
			}
		}
		
	}
	
	// return true if line is read, false if not yet exmained(read)
	private boolean readVersion(IntHolder stateMachine, String line){
		boolean didLineRead = true;
	    Pattern p = Pattern.compile("^V(\\d+\\.\\d+)");
	    Matcher m = p.matcher(line);
	    if(m.find()){
	    	if(m.groupCount() == 1){
	    		String version = m.group(1);
		    	this.info.put(KW_VERSION, version);
		    }
	    }else{
	    	stateMachine.value++;
	    	didLineRead = false;
	    }
	    return didLineRead;
	}

	// return true if line is read, false if not yet exmained(read)
	private boolean readInformation(IntHolder stateMachine, String line){
		boolean didLineRead = true;
	    Pattern p;
	    Matcher m;
	    p = Pattern.compile("^\\$(\\w+):(.*)");
	    m = p.matcher(line);
	    if(m.find()){
	    	if(m.groupCount() == 2){
		    	String keyword = m.group(1);
		    	String value = m.group(2);
		    	this.info.put(keyword, value);
		    }
	    }else{
	    	// it may be name.
	    	p = Pattern.compile("^N(\\+|-)(.*)");
		    m = p.matcher(line);
		    if(m.find()){
		    	if(m.groupCount() == 2){
			    	String order = m.group(1);
			    	String name = m.group(2);
			    	if(order.equals(NAME_BLACK)){
			    		this.setName(Shogi.BLACK, name);
			    	}else if(order.equals(NAME_WHITE)){
			    		this.setName(Shogi.WHITE, name);
			    	}
			    }
		    }else{
		    	stateMachine.value++;
		    	didLineRead = false;
		    }
	    }
	    return didLineRead;
	}
	
	
	
	
	// return true if line is read, false if not yet exmained(read)
	private boolean readInitialBoard(IntHolder stateMachine, String line) throws Exception{
		boolean didLineRead = true;
	    Pattern p, p1, p2, p3;
	    Matcher m, m1, m2, m3;
	    p = Pattern.compile("^P(.*)");
	    p1 = Pattern.compile("^PI(.*)");
	    p2 = Pattern.compile("^P([1-9])(.*)");
	    p3 = Pattern.compile("^P(\\+|-)(.*)");
	    m = p.matcher(line);
	    if(m.find()){
	    	m1 = p1.matcher(line);
	    	m2 = p2.matcher(line);
	    	m3 = p3.matcher(line);
	    	if(m1.find()){
	    		if((this.initialBoardIndicateMethod & INITIAL_BOARD_EACH_RANK) == INITIAL_BOARD_EACH_RANK){
	    			throw new Exception();
	    		}
	    		
	    		this.resetInitialBoard();
	    		this.setHirate();
	    		if(m1.groupCount() == 1){
			    	String omittingKomas = m1.group(1);
			    	this.interpretOmittingKomas(omittingKomas);
			    }
	    		
	    		this.initialBoardIndicateMethod |= INITIAL_BOARD_HIRATE;
	    	}else if(m2.find()){
	    		if((this.initialBoardIndicateMethod & INITIAL_BOARD_HIRATE) == INITIAL_BOARD_HIRATE){
	    			throw new Exception();
	    		}
	    		if(m2.groupCount() == 2){
	    			String rank = m2.group(1);
			    	String rankKomas = m2.group(2);
			    	this.interpretOneRank(new Integer(rank), rankKomas);
			    }
	    		this.initialBoardIndicateMethod |= INITIAL_BOARD_EACH_RANK;
	    		
	    	}else if(m3.find()){
	    		if(m3.groupCount() == 2){
	    			String orderStr = m3.group(1);
	    			int order = Shogi.getOrder(orderStr);
			    	String komasStr = m3.group(2);
			    	this.interpretPlacingKomas(order, komasStr);
			    }
	    		this.initialBoardIndicateMethod |= INITIAL_BOARD_SEPARATE;
	    	}else{
	    		stateMachine.value++;
		    	didLineRead = false;
	    	}
	    	
	    }else{
	    	stateMachine.value++;
	    	didLineRead = false;
	    }
	    return didLineRead;
	}
	
	private boolean readTeban(IntHolder stateMachine, String line) {
		boolean didLineRead = true;
	    Pattern p;
	    Matcher m;
	    p = Pattern.compile("^(\\+|-)$");
	    m = p.matcher(line);
	    if(m.find()){
	    	if(m.groupCount() == 1){
    			String orderStr = m.group(1);
    			int order = Shogi.getOrder(orderStr);
    			this.initialOrder = order;
		    }   	
	    }else{
	    	stateMachine.value++;
	    	didLineRead = false;
	    }
	    return didLineRead;
	}
	
	private boolean readKifu(IntHolder stateMachine, String line) {
		boolean didLineRead = true;
	    Pattern p, p1, p2, p3;
	    Matcher m, m1, m2, m3;
	    p = Pattern.compile("^(\\+|-)(\\d{2})(\\d{2})(\\w{2})");
	    m = p.matcher(line);
	    p1 = Pattern.compile("^T(\\d+)");
	    m1 = p1.matcher(line);
	    if(m.find()){
	    	if(m.groupCount() == 4){
    			int order = Shogi.getOrder(m.group(1));
		    	int from = new Integer(m.group(2));
		    	int to = new Integer(m.group(3));
		    	int kindId = Koma.getKomaKind(m.group(4));
		    	this.add(new Move(order, from, to, kindId));
		    }   	
	    }else if(m1.find()){
	    	if(m1.groupCount() == 1){
    			String secondsStr = m1.group(1);
    			int seconds = new Integer(secondsStr);
    			KifuEntry kifuentry = this.getLastKifuEntry();
    			if(kifuentry != null){
    				kifuentry.setExpendedTime(seconds);
    			}
		    }   
	    }else{
	    	p2 = Pattern.compile("^%(.+)");
		    m2 = p2.matcher(line);
		    if(m2.find()){
	    		if(m2.groupCount() == 1){
	    			String special = m2.group(1);
	    			if(Arrays.asList(this.specialWords).contains(special)){
	    				this.add(new KifuEntry(special));
	    			}
			    }
	    	}else{
	    		stateMachine.value++;
		    	didLineRead = false;
	    	}
	    	
	    }
	    return didLineRead;
	}
	
	/*
	**********************************************************************
	               interface
	**********************************************************************
	*/
	@Override
	public void read(InputStream ins) {
		try {
			this.resetInitialBoard();
			
			this.initialOrder = Shogi.ORDER_UNKNOWN;
			this.initialBoardIndicateMethod = INITIAL_BOARD_NONE;
			IntHolder smachine = new IntHolder(STATE_MACHINE_VERSION);
			InputStreamReader insr = new InputStreamReader(ins);
			BufferedReader reader = new BufferedReader(insr);
  
			String line; 
			Pattern p = Pattern.compile("^'");
  
			while ((line = reader.readLine()) != null) {
				Matcher m = p.matcher(line);
				Logger.info("%s\n", line);
				if(m.find()){
					// this is comment line, so do nothing
				}else{
					this.stateMachineFunction(smachine, line);  
				}
			}  
			reader.close();
			insr.close();  //(9)読み込みストリームを閉じる
		      
		} catch(IOException e) {
			
		} catch(Exception e){
			Logger.error("Failed to read ...%s\n", e.toString());
		}
	}

	
	
	@Override
	public void write(OutputStream outs) {
		String str = "";
		
		OutputStreamWriter outsw = new OutputStreamWriter(outs);
		BufferedWriter writer = new BufferedWriter(outsw);
		
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		for(String keyword : this.infoKeywordsSequenced){
			if(this.info.containsKey(keyword)){
				if(keyword.equals(KW_VERSION)){
					ps.printf("%s%s\n", VERSION_PREFIX, this.info.get(keyword));
				}else if(keyword.equals(KW_NAME_B)){
					ps.printf("%s%s\n", NANE_PREFIX_BLACK, this.info.get(keyword));
				}else if(keyword.equals(KW_NAME_W)){
					ps.printf("%s%s\n", NANE_PREFIX_WHITE, this.info.get(keyword));
				}else{
					ps.printf("%s%s%s%s\n", KW_PREFIX, keyword, KW_SEPARATOR, this.info.get(keyword) );
				}
			}
		}
		// 開始局面
		String hirateStr = INITIAL_BOARD_HIRATE_KW;
		ps.printf("%s\n", hirateStr);
		
		// 先手後手
		String orderStr = "";
		if(this.initialOrder == Shogi.BLACK) orderStr = NAME_BLACK;
		else if(this.initialOrder == Shogi.WHITE) orderStr = NAME_WHITE;
		ps.printf("%s\n", orderStr);
		
		for(KifuEntry kifuentry : this.entries){
			if(kifuentry.getMove() != null){
				ps.printf("%s\n", kifuentry.getMove().toCSAString());
			}
			if(kifuentry.getExpendedTime() > 0){
				ps.printf("%s%d\n", EXPENDED_TIME_PREFIX, kifuentry.getExpendedTime());
			}
			if(!kifuentry.getSpecial().equals("")){
				ps.printf("%s%s\n", SP_PREFIX, kifuentry.getSpecial());
			}
		}
		
		str += buff.toString();
		try {
			writer.write(str);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
		}
		
	}
	
	
	
	/*
	**********************************************************************
	               defining initial board and stand
	**********************************************************************
	*/
	
	private void interpretOneRank(int rank, String rankKomas){
		
		for(int i=0, file = 1; i<rankKomas.length(); i=i+3, file++){
			String orderStr = rankKomas.substring(i, i+1);
			int order = Shogi.getOrder(orderStr);
			String kindIdStr = rankKomas.substring(i+1, i+3);
			int kindId = Koma.getKomaKind(kindIdStr);
			
			Masu masu = this.board.getMasu(file, rank);
			if(masu != null){
				this.box.setKomaOn(masu, kindId, order);
			}
		}
	}
	
	private void interpretOmittingKomas(String omittingKomas){
		for(int i=0; i<omittingKomas.length(); i=i+4){
			String square = omittingKomas.substring(i, i+2);
			String kindIdStr = omittingKomas.substring(i+2, i+4);
			
			Koma koma = this.board.getKoma(new Integer(square));
			if(koma != null){
				if(koma.getKindId() == Koma.getKomaKind(kindIdStr)){
					koma.bePlaced(this.box);
				}
			}
		}
	}
	
	private void interpretPlacingKomas(int order, String placingKomaStr){
		if(!Shogi.isOrderValid(order)) return;
		
		for(int i=0; i<placingKomaStr.length(); i=i+4){
			String squareStr = placingKomaStr.substring(i, i+2);
			int square = new Integer(squareStr);
			String kindIdStr = placingKomaStr.substring(i+2, i+4);
			int kindId = Koma.getKomaKind(kindIdStr);
			Masu masu = this.board.getMasu(square);
			if(masu != null){
				this.box.setKomaOn(masu, kindId, order);
			}else if(square == SQUARE_FOR_STAND){
				if(kindId != Koma.UK){
					this.box.setKomaOn(this.stand[order], kindId, order);
				}else{
					if(kindIdStr.equals(KOMA_ALL_REST)){
						this.box.setAllKomasOn(this.stand[order]);
					}
				}
			}
		}
	}
	
	
	private void setHirate(){
		
		this.box.setKomaOn(this.board.getMasu(59), Koma.OU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(51), Koma.OU, Shogi.WHITE);
		
		this.box.setKomaOn(this.board.getMasu(28), Koma.HI, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(82), Koma.HI, Shogi.WHITE);
		
		this.box.setKomaOn(this.board.getMasu(88), Koma.KA, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(22), Koma.KA, Shogi.WHITE);
		
		this.box.setKomaOn(this.board.getMasu(69), Koma.KI, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(41), Koma.KI, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(49), Koma.KI, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(61), Koma.KI, Shogi.WHITE);
		
		this.box.setKomaOn(this.board.getMasu(79), Koma.GI, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(31), Koma.GI, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(39), Koma.GI, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(71), Koma.GI, Shogi.WHITE);
		
		this.box.setKomaOn(this.board.getMasu(89), Koma.KE, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(21), Koma.KE, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(29), Koma.KE, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(81), Koma.KE, Shogi.WHITE);
		
		this.box.setKomaOn(this.board.getMasu(99), Koma.KY, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(11), Koma.KY, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(19), Koma.KY, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(91), Koma.KY, Shogi.WHITE);
		
		this.box.setKomaOn(this.board.getMasu(57), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(53), Koma.FU, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(67), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(43), Koma.FU, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(47), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(63), Koma.FU, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(77), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(33), Koma.FU, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(37), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(73), Koma.FU, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(87), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(23), Koma.FU, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(27), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(83), Koma.FU, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(97), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(13), Koma.FU, Shogi.WHITE);
		this.box.setKomaOn(this.board.getMasu(17), Koma.FU, Shogi.BLACK);
		this.box.setKomaOn(this.board.getMasu(93), Koma.FU, Shogi.WHITE);
		
		
	}
	
	
	
	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	static public int getIndex(int hand){
		return hand - 1;
	}

	/*
	**********************************************************************
	               static variavle
	**********************************************************************
	*/
	
	// state machine
	static public final int STATE_MACHINE_VERSION = 1;
	static public final int STATE_MACHINE_INFOMATION = 2;
	static public final int STATE_MACHINE_INITIAL_BOARD = 3;
	static public final int STATE_MACHINE_TEBAN = 4;
	static public final int STATE_MACHINE_KIFU = 5;
	static public final int STATE_MACHINE_FOOT_COMMENT = 6;
	
	
	// see http://www.computer-shogi.org/protocol/record_v22.html
	// INITIAL_BOARD_HIRATE and INITIAL_BOARD_EACH_RANK can not be mixed.
	static public final int INITIAL_BOARD_NONE = 0x0000;
	static public final int INITIAL_BOARD_HIRATE = 0x0001;
	static public final int INITIAL_BOARD_EACH_RANK = 0x0002;
	static public final int INITIAL_BOARD_SEPARATE = 0x0004;
	
	
	static public final String VERSION_PREFIX = "V";
	static public final String NANE_PREFIX = "N";
	static public final String NAME_BLACK = "+";
	static public final String NAME_WHITE = "-";
	
	static public final String NANE_PREFIX_BLACK = NANE_PREFIX + NAME_BLACK;
	static public final String NANE_PREFIX_WHITE = NANE_PREFIX + NAME_WHITE;
	
	static public final String KOMA_ALL_REST = "AL";
	static public final String SQUARE_FOR_STAND_STR = "00";
	static public final int SQUARE_FOR_STAND = 0;
	
	// optional
	// $EVENT:(文字列)
	// $SITE:(文字列)
	// $START_TIME:YYYY/MM/DD HH:MM:SS(時刻は省略可)
	// $END_TIME:YYYY/MM/DD HH:MM:SS
	// $TIME_LIMIT:HH:MM+SS
	// $OPENING:(文字列)
	static public final String KW_PREFIX = "$";
	static public final String KW_SEPARATOR = ":";
	
	static public final String KW_VERSION = "VERSION";
	static public final String KW_NAME_B = "NAME_B";
	static public final String KW_NAME_W = "NAME_W";
	
	static public final String KW_EVENT = "EVENT";
	static public final String KW_SITE = "SITE";
	static public final String KW_START_TIME = "START_TIME";
	static public final String KW_END_TIME = "END_TIME";
	static public final String KW_TIME_LIMIT = "TIME_LIMIT";
	static public final String KW_OPENING = "OPENING";
	
	// initialize keyword in order of displayed using toString()
	
	protected ArrayList<String> infoKeywordsSequenced = new ArrayList<String>();
	
	protected void setKeywords(){
		this.infoKeywordsSequenced.add(KW_VERSION);
		this.infoKeywordsSequenced.add(KW_NAME_B);
		this.infoKeywordsSequenced.add(KW_NAME_W);
		this.infoKeywordsSequenced.add(KW_EVENT);
		this.infoKeywordsSequenced.add(KW_SITE);
		this.infoKeywordsSequenced.add(KW_START_TIME);
		this.infoKeywordsSequenced.add(KW_END_TIME);
		this.infoKeywordsSequenced.add(KW_TIME_LIMIT);
		this.infoKeywordsSequenced.add(KW_OPENING);
	}

	
	static public final String INITIAL_BOARD_HIRATE_KW = "PI";
	static public final String EXPENDED_TIME_PREFIX = "T";
	
	/*
	%で始まる。
	%TORYO 投了
	%CHUDAN 中断
	%SENNICHITE 千日手
	%TIME_UP 手番側が時間切れで負け
	%ILLEGAL_MOVE 手番側の反則負け、反則の内容はコメントで記録する
	%+ILLEGAL_ACTION 先手(下手)の反則行為により、後手(上手)の勝ち
	%-ILLEGAL_ACTION 後手(上手)の反則行為により、先手(下手)の勝ち
	%JISHOGI 持将棋
	%KACHI (入玉で)勝ちの宣言
	%HIKIWAKE (入玉で)引き分けの宣言
	%MATTA 待った
	%TSUMI 詰み
	%FUZUMI 不詰
	%ERROR エラー
	※文字列は、空白を含まない。
	※%KACHI,%HIKIWAKE は、コンピュータ将棋選手権のルールに対応し、
	第3版で追加。
	※%+ILLEGAL_ACTION,%-ILLEGAL_ACTIONは、手番側の勝ちを表現できる。
	 */
	// SP is abbreviation for Special
	static public final String SP_PREFIX = "%";
	static public final String SP_TORYO = "TORYO";
	static public final String SP_CHUDAN = "CHUDAN";
	static public final String SP_SENNICHITE = "SENNICHITE";
	static public final String SP_TIME_UP = "TIME_UP";
	static public final String SP_ILLEGAL_MOVE = "ILLEGAL_MOVE";
	static public final String SP_ILLEGAL_ACTION_BY_BLACK = "+ILLEGAL_ACTION";
	static public final String SP_ILLEGAL_ACTION_BY_WHITE = "-ILLEGAL_ACTION";
	static public final String SP_JISHOGI = "JISHOGI";
	static public final String SP_KACHI = "KACHI";
	static public final String SP_HIKIWAKE = "HIKIWAKE";
	static public final String SP_MATTA = "MATTA";
	static public final String SP_TSUMI = "TSUMI";
	static public final String SP_FUZUMI = "FUZUMI";
	static public final String SP_ERROR = "ERROR";
	
	protected ArrayList<String> specialWords = new ArrayList<String>();
	protected void setSpecialWords(){
		this.specialWords.add(SP_TORYO);
		this.specialWords.add(SP_CHUDAN);
		this.specialWords.add(SP_SENNICHITE);
		this.specialWords.add(SP_TIME_UP);
		this.specialWords.add(SP_ILLEGAL_MOVE);
		this.specialWords.add(SP_ILLEGAL_ACTION_BY_BLACK);
		this.specialWords.add(SP_ILLEGAL_ACTION_BY_WHITE);
		this.specialWords.add(SP_JISHOGI);
		this.specialWords.add(SP_KACHI);
		this.specialWords.add(SP_HIKIWAKE);
		this.specialWords.add(SP_MATTA);
		this.specialWords.add(SP_TSUMI);
		this.specialWords.add(SP_FUZUMI);
		this.specialWords.add(SP_ERROR);
	}

}




