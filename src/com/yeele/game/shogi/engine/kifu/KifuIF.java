package com.yeele.game.shogi.engine.kifu;

import java.io.InputStream;
import java.io.OutputStream;


public interface KifuIF{
	abstract public void read(InputStream ins);
	abstract public void write(OutputStream outs);
}