package com.yeele.game.shogi.engine.kifu;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import com.yeele.game.shogi.engine.Move;



public class KifuEntry{
	
	
	protected Move move = null;
	protected int seconds = 0;
	protected String comment = "";
	protected String special = "";
	
	public KifuEntry(Move move){
		this.move = move;
	}
	public KifuEntry(Move move, int seconds){
		this.move = move;
		this.seconds = seconds;
	}
	public KifuEntry(Move move, int seconds, String comment){
		this.move = move;
		this.seconds = seconds;
		this.comment = comment;
	}
	public KifuEntry(String special){
		this.special = special;
	}
	
	
	public Move getMove(){
		return this.move;
	}
	public void setMove(Move move){
		this.move = move;
	}
	public int getExpendedTime(){
		return this.seconds;
	}
	public void setExpendedTime(int seconds){
		this.seconds = seconds;
	}
	public String getComment(){
		return this.comment;
	}
	public void getComment(String comment){
		this.comment = comment;
	}
	public String getSpecial(){
		return this.special;
	}
	public void getSpecial(String special){
		this.special = special;
	}
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s %s", this.move, this.special);
		str += buff.toString();
		return str;
	}
	
	public String toVerboseString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s %d %s\n%s", this.move , this.seconds, this.special, this.comment);
		str += buff.toString();
		return str;
	}
		
}
