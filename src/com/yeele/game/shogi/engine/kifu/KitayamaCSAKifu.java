package com.yeele.game.shogi.engine.kifu;

import static com.yeele.game.shogi.engine.util.Logger.Logger;

import java.util.HashMap;

import com.yeele.game.shogi.engine.Shogi;

public class KitayamaCSAKifu extends CSAKifu{
	
	/*
	 * extend CSA Kifu
	 * 
	 * added additional information keyword to identify 
	 * what type of player.
	 */
	
	public KitayamaCSAKifu(HashMap<String, String> info){
		super(info);
	}
	
	public KitayamaCSAKifu(){
		super();
	}
	
	public KitayamaCSAKifu(String version){
		super(version);
	}
	
	
	
	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	
	/*
	**********************************************************************
	               static variavle
	**********************************************************************
	*/
	
	static public final String KW_PLAYER_TYPE_B = "NTYPE+";
	static public final String KW_PLAYER_TYPE_W = "NTYPE-";
	
	// valid value for KW_PLAYER_TYPE are
	// (should) defined in com.yeele.game.shogi.engine.player
	
	protected void setKeywords(){
		super.setKeywords();
		this.infoKeywordsSequenced.add(KW_PLAYER_TYPE_B);
		this.infoKeywordsSequenced.add(KW_PLAYER_TYPE_W);
		
	}
	
	protected void setSpecialWords(){
		super.setSpecialWords();
	}

}




