package com.yeele.game.shogi.engine.util;

import java.util.Comparator;

import com.yeele.game.shogi.engine.Koma;


// Array factors are automatically sorted in ascendant order.
public abstract class KomaComparator implements Comparator<Koma> {  
    abstract public int compare(Koma a, Koma b);
}

