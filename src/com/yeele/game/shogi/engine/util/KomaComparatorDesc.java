package com.yeele.game.shogi.engine.util;

import com.yeele.game.shogi.engine.Koma;


// Array factors are automatically sorted in descendant order.
public class KomaComparatorDesc extends KomaComparator {  
    public int compare(Koma a, Koma b) {
    	return a.getKindId() - b.getKindId();
    }  
}