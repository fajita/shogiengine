package com.yeele.game.shogi.engine.util;

import java.io.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;


public class Logger{
	
	public static final int LEVEL_DEBUG = 0;
	public static final int LEVEL_INFO  = 1;
	public static final int LEVEL_WARN  = 2;
	public static final int LEVEL_ERROR = 3;
	
	public static final String LOG_DIR = "log";
	public static final String LOGGER_NAME = "SLogger";
	public static final String LOGFILE_SURFIX = ".log";
	public static final String LOGGER_FILENAME = LOGGER_NAME + LOGFILE_SURFIX;
	//public static final String LOGGING_PROPERTIES_FILE = "shogilogging.properties";
	
	protected static final String LOGGING_PROPERTIES_DATA2
    = ""
    + "handlers= java.util.logging.FileHandler, java.util.logging.ConsoleHandler\n"
    //+ "handlers= java.util.logging.ConsoleHandler\n"
    + "\n"
    + ".level=INFO\n"
    + "\n"
    + "java.util.logging.ConsoleHandler.level=INFO\n"
    + "java.util.logging.ConsoleHandler.formatter=com.yeele.game.shogi.engine.util.SimplestFormatter"
	+ "\n"
	+ "java.util.logging.FileHandler.level=FINEST\n"
	+ "java.util.logging.FileHandler.pattern = " + LOG_DIR + "/"+ LOGGER_NAME + ".%g" + LOGFILE_SURFIX + "\n"
	+ "java.util.logging.FileHandler.limit =   4097152\n" // 4M
	+ "java.util.logging.FileHandler.count = 100\n"      // 50M*100, so loop/5G
	+ "java.util.logging.FileHandler.encoding = utf-8\n"
	+ "java.util.logging.FileHandler.formatter = com.yeele.game.shogi.engine.util.SimplestFormatter\n"
	;
	
	protected static final String LOGGING_PROPERTIES_DATA
    = ""
    + "handlers= java.util.logging.ConsoleHandler\n"
    + "\n"
    + ".level=INFO\n"
    + "\n"
    + "java.util.logging.ConsoleHandler.level=INFO\n"
    //+ "java.util.logging.ConsoleHandler.formatter=com.yeele.game.shogi.engine.util.SimplestFormatter"
	;
	
	private java.util.logging.Logger logger = null;
	// if you compile this as a jar and don't want to output log, set false in 2nd argument!
	public static final Logger Logger = new Logger(LOGGER_NAME, false);
	//public static final Logger Logger = new Logger(LOGGER_NAME, true);
	
	/* static initializer */
	static{
		
		InputStream inStream = null;
        try {
        	String configstr = LOGGING_PROPERTIES_DATA2;
            inStream = new ByteArrayInputStream(configstr.getBytes("UTF-8"));
            
            try {
                LogManager.getLogManager().readConfiguration(inStream);
                Logger.info("LogManager configured following \n %s", configstr);
            } catch (IOException e) {
                Logger.info("LogManager caught an exception.:" + e.toString());
            }
            
        } catch (UnsupportedEncodingException e) {
            Logger.error("Enconding UTF-8 is not supported." + e.toString());
        } finally {
            try {
                if (inStream != null) inStream.close(); 
            } catch (IOException e) {
            	Logger.warn("Error occured during closing InputStream..." + e.toString());
            }
        }
		
	}
	
	
	
	
	
	public Logger(String logger_name, boolean surpress_log){
		this.logger = java.util.logging.Logger.getLogger(logger_name);
		// if you compile this as a jar, set true in surpress_log argument!
		this.logger.setUseParentHandlers(surpress_log);
		this.logger.setLevel(Level.OFF);
		
		try {
			
			
			// ダイナミックにログの設定をしたいので、以下はプログラマティックに設定する例として参考まで。
            // 出力ファイルを指定する
            FileHandler fh = new FileHandler(logger_name + LOGFILE_SURFIX);
            // 出力フォーマットを指定する
            fh.setFormatter(new java.util.logging.SimpleFormatter());
			logger.addHandler(fh);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
	}
	
	public void setLevel(int lvl){
		Level logging_lvl = Level.INFO;
		if(lvl == LEVEL_ERROR){
			logging_lvl = Level.SEVERE;
		}else if(lvl == LEVEL_WARN){
			logging_lvl = Level.WARNING;
		}else if(lvl == LEVEL_INFO){
			logging_lvl = Level.INFO;
		}else if(lvl == LEVEL_DEBUG){
			logging_lvl = Level.FINE;
		}
		
        this.logger.setLevel(logging_lvl);
	}
	
	public int getLevel(){
		Level logging_lvl = this.logger.getLevel();
		int lvl = LEVEL_INFO;
		
		if(logging_lvl == Level.SEVERE){
			lvl = LEVEL_ERROR;
		}else if(logging_lvl == Level.WARNING){
			lvl = LEVEL_WARN;
		}else if(logging_lvl == Level.INFO){
			lvl = LEVEL_INFO;
		}else if(logging_lvl == Level.CONFIG){
			lvl = LEVEL_INFO;
		}else if(logging_lvl == Level.FINE){
			lvl = LEVEL_DEBUG;
		}else if(logging_lvl == Level.FINER){
			lvl = LEVEL_DEBUG;
		}else if(logging_lvl == Level.FINEST){
			lvl = LEVEL_DEBUG;
		}
		
		return lvl;
	}
	
	public Level convLevel(int lvl){
		Level logging_lvl = Level.INFO;
		if(lvl == LEVEL_ERROR){
			logging_lvl = Level.SEVERE;
		}else if(lvl == LEVEL_WARN){
			logging_lvl = Level.WARNING;
		}else if(lvl == LEVEL_INFO){
			logging_lvl = Level.INFO;
		}else if(lvl == LEVEL_DEBUG){
			logging_lvl = Level.FINE;
		}
		return logging_lvl;
	}
	
	public void debug(String fmt, Object ... args){
		this.logger.log(Level.FINE, String.format(fmt, args));
	}
	
	public void info(String fmt, Object ... args){
		this.logger.log(Level.INFO, String.format(fmt, args));
	}
	
	public void warn(String fmt, Object ... args){
		this.logger.log(Level.WARNING, String.format(fmt, args));
	}
	
	public void error(String fmt, Object ... args){
		this.logger.log(Level.SEVERE, String.format(fmt, args));
	}
		
}