package com.yeele.game.shogi.engine.util;


import com.yeele.game.shogi.engine.Koma;



public class KomaComparatorAsc extends KomaComparator {  
    public int compare(Koma a, Koma b) {
    	return b.getKindId() - a.getKindId();
    }  
}

