package com.yeele.game.shogi.engine.stratagy.suji;



import static com.yeele.game.shogi.engine.util.Logger.Logger;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

public class PassSuji extends Suji{
	
	// 取れる駒がある場合は、取る筋
	public static String NAME = "PasuSuji";
	
	public PassSuji(Shogi shogi){
		super(shogi, Suji.ID_PASS, PassSuji.NAME);
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		moves.add(new Move(order, 0, 0, Koma.UK));
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		return moves;
	}
	
	protected boolean examine_impl(Move lastMove){
		//パスする手
		//　取れる駒がある状況で、その駒をとらずに、相手を攻めるでもない手をさすこと。
		// TBD: don't know how to judge.
		boolean condition1=false;
		 
		Masu fromMasu = this.shogi.board.getMasu(lastMove.from);
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);

				
		return (condition1);
	}

}



