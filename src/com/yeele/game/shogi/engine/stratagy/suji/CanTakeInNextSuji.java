package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class CanTakeInNextSuji extends Suji{
	
	public static String NAME = "CanTakeInNextSuji";
	
	public CanTakeInNextSuji(Shogi shogi){
		super(shogi, Suji.ID_CAN_TAKE_IN_NEXT, NAME);
	}
	

	protected boolean examine_impl(Move lastMove){
		//取れるの手
				// 取れそうの手
				// 且つ、　movableにいる相手のmovableがない。
				boolean condition1=false;
				
				Koma toKoma = this.shogi.board.getKoma(lastMove.to);
				int opponent = Shogi.getOtherOrder(lastMove.order);
				
				if(toKoma!=null){
					for(Masu masu : toKoma.movable){
						if(masu.koma!=null){
							if(masu.koma.getOrder()==opponent){
								if(masu.koma.movable.size()==0){
									condition1 = true;
									break;
								}
							}
						}
					}
				}
				return condition1;
	}
	
}
