package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class TakeOpponentFreeSuji extends Suji{
	
public static String NAME = "TakeOpponentFreeSuji";
	
	public TakeOpponentFreeSuji(Shogi shogi){
		super(shogi, Suji.ID_TAKE_OPPONENT_FREE, TakeOpponentFreeSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
		//タダ駒を取る手
		// komaToCaptureに駒がある　
		// 且つ　toのmasuのdirectに相手がいない
		
		Suji dependency = new TakeOpponentSuji(this.shogi);
		if(dependency.examine(lastMove)){
			Masu masu = this.shogi.board.getMasu(lastMove.to);
			if(masu!=null){
				if( masu.isDirectlyReferredBy(Shogi.getOtherOrder(lastMove.order))==false){
					return true;
				}
			}
		}
		
		return false;
	}
	

}


