package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class AimThroughSuji extends Suji{
	
	public static String NAME = "AimDoubleOuSuji";
	
	public AimThroughSuji(Shogi shogi){
		super(shogi, Suji.ID_AIM_THROUGH, NAME);
	}
	
	protected boolean examine_impl(Move lastMove){
		//素抜きを狙う手（取れない手）
		// toのmasuのdirectに相手がいるが、その相手directに利きがある駒のmasuに
		// directにthrouableな味方がいて、且そのthrouableな駒の
		// throuableには相手の駒がいる
		boolean condition1=false;
		boolean condition2=false;
		boolean condition3=false;
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(toMasu != null){
			for(Koma directKoma : toMasu.directRefered){
				if(directKoma.getOrder() == opponent){
					condition1 = true;
					
					for(Koma sunukiCandidate : directKoma.masu.directRefered){
						if(sunukiCandidate.getOrder() == lastMove.order){
							if(sunukiCandidate.isThroughable()){
								condition2 = true;
								
								Koma aimedKoma = sunukiCandidate.getKomaBehindWall(directKoma);
								if(aimedKoma != null && aimedKoma.getOrder() == opponent){
									condition3 = true;
								}
							}
						}
					}
				}
			}
		}
				
		return (condition3);
	}

	
}



