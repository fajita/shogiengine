package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class SacrificeSuji extends Suji{
	
public static String NAME = "SacrificeSuji";
	
	public SacrificeSuji(Shogi shogi){
		super(shogi, Suji.ID_SACRIFICE, SacrificeSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
	
		//駒を犠牲にする手
		// toのmasuのdirectに相手がいるが
		// 他の狙いがある TBD 素抜き、次の手で味方が奪い返す、王又は他の他を誘導、玉の守り崩し
		boolean condition1=false;
		 
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(toMasu != null){
			if(toMasu.isDirectlyReferredBy(opponent)){
				condition1 = true;
			}
		}
				
		return (condition1);
	}
	

}


