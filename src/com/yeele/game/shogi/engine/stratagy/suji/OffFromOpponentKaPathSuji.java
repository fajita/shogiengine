package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;

public class OffFromOpponentKaPathSuji extends Suji{
	
public static String NAME = "OffFromOpponentKaPath";
	
	public OffFromOpponentKaPathSuji(Shogi shogi){
		super(shogi, Suji.ID_OFF_FROM_OPPONENT_KA_PATH, NAME);
	}
	
	protected boolean examine_impl(Move lastMove){
		// 角道から離れる
		
		// TODO 
		// Masu.isReferedBy(Koma koma)を実装して
		// fromMasu.isReferedBy(ka)　角道にあるかを調べれるようにしたい。
		
		boolean condition1=false;
		Masu fromMasu = this.shogi.board.getMasu(lastMove.from);
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		
		if(fromMasu != null){
			boolean isFromMasuRefered = false;
			for(Koma koma : fromMasu.getDirectlyReferredBy(opponent)){
				if(Koma.getUnpromotedPiece(koma.getKindId())==Koma.KA){
					isFromMasuRefered = true;
				}
			}
			for(Koma koma : fromMasu.getIndirectlyReferredBy(opponent)){
				if(Koma.getUnpromotedPiece(koma.getKindId())==Koma.KA){
					isFromMasuRefered = true;
				}
			}
			if(isFromMasuRefered){
				if(toMasu != null){
					boolean isToMasuRefered = false;
					for(Koma koma : toMasu.getDirectlyReferredBy(opponent)){
						if(Koma.getUnpromotedPiece(koma.getKindId())==Koma.KA){
							isFromMasuRefered = true;
						}
					}
					for(Koma koma : toMasu.getIndirectlyReferredBy(opponent)){
						if(Koma.getUnpromotedPiece(koma.getKindId())==Koma.KA){
							isFromMasuRefered = true;
						}
					}
					if(isToMasuRefered == false){
						condition1 = true;
					}
				}
			}
		}
		
		return (condition1);
	}
	
	
}


