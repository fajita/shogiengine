package com.yeele.game.shogi.engine.stratagy.suji;



import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.player.Player;
import com.yeele.util.ArrayListS;

public class HashiFuUkeSuji extends Suji{
	
	public static String NAME = "HashiFuUkeSuji";
	
	public HashiFuUkeSuji(Shogi shogi){
		super(shogi, Suji.ID_HASHI_FU_UKE, HashiFuUkeSuji.NAME);
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		for(Koma fu : this.shogi.getKomaList(true, false, order, Koma.FU, false)){// 歩成の場合は無視
			int fu_file = Board.getFile(fu.masu.square);
			if( fu_file == 1 || fu_file == 9 ){
				for(Masu movableMasu : fu.movable){
					for(Koma direct : movableMasu.directRefered){
						if(direct.getOrder() == opponent && direct.getKindId() == Koma.FU){ 
							moves.addAll(fu.moves);
						}
					}
				}
			}
		}
		return moves;
	}

	
	protected boolean examine_impl(Move lastMove){
		
		int file = Board.getFile(lastMove.to);
		if(lastMove.komaToMove == Koma.FU && (file == 1 || file == 9)){
			return true;
		}
		return false;
	}
	
	
}



