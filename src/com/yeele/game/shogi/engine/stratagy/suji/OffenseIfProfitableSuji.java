package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class OffenseIfProfitableSuji extends Suji{
	
	// 取れる駒がある場合は、取る筋
	public static String NAME = "OffenseIfProfitableSuji";
	
	public OffenseIfProfitableSuji(Shogi shogi){
		super(shogi, Suji.ID_OFFENSE_IF_PROFITABLE, OffenseIfProfitableSuji.NAME);
	}
	
	// generate 'suji to offense if profitable' on the given masu.
	protected ArrayListS<Move> generateOn_impl(int order, Masu masu){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		if(masu.isKoma(order) == false){ // 味方の駒はいない。　空升か相手がいある升であることを保証
			for(Koma pointingKoma : masu.getDirectlyReferredBy(order)){
			
				// ただし、タダで。又は優勢地点で
				boolean isProfitable = pointingKoma.isProfitable(masu);
				// その駒自体が、強い駒暴れ馬を収める。aimedMasuにある駒を取らなければ、次のてで大切な駒を取られる。
				boolean isBetterTakeHimDown = pointingKoma.isBetterTakeHimDown(masu);
				
				if(
					// 空升に優勢な感じで成れるか
					(masu.isEmpty() &&
					 pointingKoma.canBePromotedOn(masu) &&
					 isProfitable) ||
					 
					// 相手をいろんな意味でとっちゃったほうがいい
					(masu.isKoma(opponent) &&
					(isProfitable || isBetterTakeHimDown))
				)
				{
					for(Move m : pointingKoma.getMovesCanGoTo(masu)){
						moves.addIfNot(m);
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		
		return moves;
	}
	
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		if(koma.isOnBoard()){
			for(Koma opponentKoma : koma.getReachableKoma(opponent)){
				for(Move m : this.generateOn(order, opponentKoma.masu)){
					moves.addIfNot(m);
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		return moves;
	}
	
	
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(Koma koma : this.shogi.getKomaList(true, false, order)){
			for(Move m : this.generateBy_impl(order, koma)){
				moves.addIfNot(m);
			}
		}
		return moves;
	}

	
	
}



