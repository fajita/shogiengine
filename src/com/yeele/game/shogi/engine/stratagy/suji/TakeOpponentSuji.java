package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;

public class TakeOpponentSuji extends Suji{
	
public static String NAME = "TakeOpponentSuji";
	
	public TakeOpponentSuji(Shogi shogi){
		super(shogi, Suji.ID_TAKE_OPPONENT, TakeOpponentSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){

		//相手の駒をとる手
		// komaToCaptureに駒がある
		
		if(lastMove.komaToCapture != Koma.UK) return true;
		return false;
	}

}


