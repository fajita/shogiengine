package com.yeele.game.shogi.engine.stratagy.suji;



import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

public class TakeLineSuji extends Suji{
	
	// 位を取る筋
	
	public static String NAME = "TakeLineSuji";
	
	protected ArrayListS<Integer> qualifiedKindIds;
	
	public TakeLineSuji(Shogi shogi){
		super(shogi, Suji.ID_TAKELINE, TakeLineSuji.NAME);
		this.qualifiedKindIds = new ArrayListS<Integer>();
		// 以下の駒しか、この筋を生成できない
		this.qualifiedKindIds.add(Koma.FU);
		this.qualifiedKindIds.add(Koma.GI);
	}
	
	private boolean isQualified(int kindId){
		boolean ret = false;
		for(int qualifiedKindId : this.qualifiedKindIds){
			if(kindId == qualifiedKindId){
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		if(this.isQualified(koma.getKindId())){
			for(Move m : koma.getMoves()){
				if(this.examine(m)){
					moves.addIfNot(m);
				}
			}
		}
		return moves;
	}
	
	protected ArrayListS<Move> generateOn_impl(int order, Masu masu){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		if(masu.isEmpty()){
			for(Koma companionKoma : masu.getDirectlyReferredBy(order)){
				for(Move m : this.generateBy_impl(order, companionKoma)){
					moves.addIfNot(m);
				}
			}
		}
		return moves;
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(int kindId : this.qualifiedKindIds){
			for(Koma koma : this.shogi.getKomaList(true, false, order, kindId, false)){
				moves.addIfNot(this.generateBy_impl(order, koma));
			}
		}
		return moves;
	}
	

	protected boolean examine_impl(Move lastMove){
		//位を取る手( 前に進む手　+ 取れれない手　）
		// 進行方法に進む(from.rank-to.rank)*order > 0  (NEW SPEC, black=1, while=-1にしよっかなー)
		// だめ、black 0, while 1 は配列indexに使われているもん。
		// それをするなら、FWD_DIR_BLACK = 1;FWD_DIR_WHITE = -1; ってゆうか、こうしました。下記参照
		// public static final int [] FILE_FOWARD   = { -1,  1 };  
		// 動かした駒が歩である
		// 中段に進んだときのみ(toのrankが4/5/6)
		boolean condition1=false;
		boolean condition2=false;
		boolean condition3=false;
		
		if(lastMove.countForwardSteps() > 0){
			condition1 = true;
		}
		
		if(this.isQualified(lastMove.komaToMove)){
			condition2 = true;
		}
		
		if(Board.isMiddleArea(lastMove.to)){
			condition3 = true;
		}
		
				
		return (condition1 && condition2 && condition3);
	}
}



