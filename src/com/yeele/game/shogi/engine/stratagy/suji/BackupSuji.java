package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;

public class BackupSuji extends Suji{
	
	// がむしゃらにバックアップする手を生成
	public static String NAME = "BackupSuji";
	
	public BackupSuji(Shogi shogi){
		super(shogi, Suji.ID_BACKUP, BackupSuji.NAME);
	}
	
	// generate suji by the given koma
	// komaがbackupできる手を生成
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		// TODO
		return moves;
	}
	
	// generate suji for the given masu
	// masuをbackupできる手を生成
	protected ArrayListS<Move> generateFor_impl(int order, Masu masu){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(Move m : shogi.getPrecedingMovesFor(masu, order)){
			// exclude if you are already backing it up.
			if(Board.isSquareValid(m.from)){
				Koma koma = this.shogi.board.getKoma(m.from);
				if(koma.getReachable().contains(masu)==false){
					moves.addIfNot(m);
				}
			}else{
				moves.addIfNot(m);
			}
		}
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		return moves;
	}
	
	// generate suji for the given koma
	// komaをbackupできる手を生成
	protected ArrayListS<Move> generateFor_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		if(koma.isOnBoard()){
			for(Move m : this.generateFor(order, koma.masu)){
				moves.addIfNot(m);
			}
		}
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		return moves;
	}
	
	
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(Masu masu : this.shogi.board.getAllMasu()){
			for(Move m : this.generateFor(order, masu)){
				moves.addIfNot(m);
			}
		}
		return moves;
	}
	
	protected boolean examine_impl(Move lastMove){
		// 駒に効きをつける手
		//　movableに味方がいる
		
		Koma koma = this.shogi.board.getKoma(lastMove.to);
		if(koma!=null){
			for(Masu ms : koma.movable){
				if( ms.koma != null && ms.koma.getOrder() == koma.getOrder()){
					return true;
				}
			}
		}
		return false;
	}
	

}



