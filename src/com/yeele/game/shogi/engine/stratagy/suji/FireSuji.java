package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class FireSuji extends Suji{
	
	/* ぶつけるとは、
	 * 相手の利きが自分の駒にかかるところに駒を動かすこと、または、動かしたコマではないが、
	 * 自分の駒を動かすことで、味方のの駒が相手の利きにさらされること。
	 */
	public static String NAME = "FireSuji";
	
	public FireSuji(Shogi shogi){
		super(shogi, Suji.ID_FIRE, FireSuji.NAME);
	}
	
	// generate fire suji on the given masu.
	protected ArrayListS<Move> generateOn_impl(int order, Masu masu){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		
		if(masu.isEmpty() && masu.isDirectlyReferedByBoth()){ // ぶつける行為は空ますに限る
			for(Koma pointingKoma : masu.getDirectlyReferredBy(order)){
				for(Move m : pointingKoma.getMovesCanGoTo(masu)){
					moves.addIfNot(m);
				}
			}
		}
			
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		
		return moves;
	}
	
	// generate fire suji by the given koma
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(Masu masu : koma.getMovable()){
			for(Move m : this.generateOn(order, masu)){
				moves.addIfNot(m);
			}
		}
		return moves;
	}
	
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(Koma koma : this.shogi.getKomaList(true, false, order)){
			for(Move m : this.generateBy_impl(order, koma)){
				moves.addIfNot(m);
			}
		}
		return moves;
	}

	
	protected boolean examine_impl(Move lastMove){
		//取ろうの手(ぶつける)
		// toのmasuのmovableに相手がいる
		boolean condition1=false;
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(toKoma!=null){
			for(Masu masu : toKoma.movable){
				if(masu.koma!=null){
					if(masu.koma.getOrder()==opponent){
						condition1 = true;
						break;
					}
				}
			}
		}
		return condition1;
	}
}



