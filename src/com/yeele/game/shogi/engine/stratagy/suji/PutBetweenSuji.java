package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class PutBetweenSuji extends Suji{
	
	// 取れる駒がある場合は、取る筋
	public static String NAME = "PutBetweenSuji";
	
	public PutBetweenSuji(Shogi shogi){
		super(shogi, Suji.ID_PUTBETWEEN, PutBetweenSuji.NAME);
	}
	public PutBetweenSuji(Shogi shogi, int sujiId, String name){
		super(shogi, sujiId, name);
	}
	
	// komaを間駒として使う手を生成
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		for(Move m: koma.getMoves()){
			for(Koma sniper : this.shogi.board.getMasu(m.to).getDirectlyReferredBy(opponent)){
				if(sniper.isThroughable()){
					// もしthe give komaが動ける範囲でsniperが通っていたら、
					// この駒は(間駒）壁になることができる。　
					// ただし、もう既に壁なら、新しい手を生成しないこととする。
					if(koma.getWallFor(opponent).contains(sniper)) break;
					
					for(Koma threatenKoma : sniper.getReachableKoma(order)){
						// これで味方が狙われていることが分かる
						// sniperとthreatenKomaの間にorderの駒を置きたい
						// 守る駒であるthreatenKomaより安い駒を使いたい // これは別suji
						//if(koma.getKindId() < Koma.getUnpromotedPiece(threatenKoma.getKindId())){
						//	
						//}
						moves.addIfNot(m);
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		return moves;
	}
	
	// sniperに対して間駒する手を生成
	// generate 'put between suji' against the given sniper
	protected ArrayListS<Move> generateAgainst_impl(int order, Koma sniper){
		
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		if(sniper.getOrder()==opponent){
			for(Koma threatenKoma : sniper.getReachableKoma(order)){
				// これで味方が狙われていることが分かる
				// sniperとthreatenKomaの間にorderの駒を置きたい
				// 守る駒であるthreatenKomaより安い駒を使いたい
				for(Masu masu : this.shogi.board.getEmptyMasuBetween(threatenKoma.masu, sniper.masu)){
					for(Move m : this.shogi.getMovesCanGoTo(order, masu.square)){
						// このsujiは基本形なので、全ての間駒を生成する。
						// 特に
						// 守る駒であるthreatenKomaより安い駒を使いたい場合は
						// このsujiを継承したsujiを別途作成する。
						//if(m.komaToMove < threatenKoma.getKindId()){
						//	
						//}
						moves.addIfNot(m);
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		return moves;
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		for(Koma sniper : this.shogi.getKomaList(true, false, opponent, Koma.HI, true)){
			for(Move m : this.generateAgainst(order, sniper)){
				moves.addIfNot(m);
			}
		}
		for(Koma sniper : this.shogi.getKomaList(true, false, opponent, Koma.KA, true)){
			for(Move m : this.generateAgainst(order, sniper)){
				moves.addIfNot(m);
			}
		}
		for(Koma sniper : this.shogi.getKomaList(true, false, opponent, Koma.KY, false)){
			for(Move m : this.generateAgainst(order, sniper)){
				moves.addIfNot(m);
			}
		}
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		return moves;
	}
	
	protected boolean examine_impl(Move lastMove){
		//間駒
		// fromのmasuのdirectに相手の貫通可能駒がない、　del
		// 且つ　toのmasuのdirectに相手の貫通可能駒がある
		// 相手の貫通可能駒のthroughableに味方の駒がいる 
		// (駒の価値は関係ない）
		// 相手の駒を取ったときには、間駒が目的ではない。 // add
		boolean condition1=false;
		 
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		Suji dependency = new TakeOpponentSuji(this.shogi);
		if(dependency.examine(lastMove)==false){
			if(toMasu != null && toKoma != null){
				for(Koma throuableKoma : toKoma.getWallFor()){
					if(throuableKoma.getOrder() == opponent){
						Koma behind = throuableKoma.getKomaBehindWall(toKoma);
						if(behind!=null){
							if(behind.getOrder() == lastMove.order) condition1 = true;
						}
					}
				}
			}
		}
				
		return (condition1);
	}
}



