package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class PlaceOnSafeSuji extends Suji{
	
public static String NAME = "PlaceOnSafeSuji";
	
	public PlaceOnSafeSuji(Shogi shogi){
		super(shogi, Suji.ID_PLACE_ON_SAFE, PlaceOnSafeSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
		//次順で取られない升に駒を打つ手　// TBD delete　cuz, 駒を打つ手と取られない手のコンボだから
		// toに相手のdirect利きがない
		
		Suji dependency1 = new PlaceSuji(this.shogi);
		Suji dependency2 = new ToSafeSuji(this.shogi);
		
		if(dependency1.examine(lastMove) && dependency2.examine(lastMove)){
			return true;
		}
		return false;
	}
	

}


