package com.yeele.game.shogi.engine.stratagy.suji;



import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.player.Player;
import com.yeele.util.ArrayListS;

public class TakeSuji extends Suji{
	
	// 取れる駒がある場合は、取る筋
	public static String NAME = "TakeSuji";
	
	public TakeSuji(Shogi shogi){
		super(shogi, Suji.ID_TAKE, TakeSuji.NAME);
	}
	
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		for(Move m : koma.getMoves()){
			if(this.shogi.board.getMasu(m.to).isKoma(opponent)){
				moves.addIfNot(m);
			}
		}
		return moves;
	}
	
	protected ArrayListS<Move> generateOn_impl(int order, Masu masu){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		if(masu.isKoma(opponent)){
			for(Koma companionKoma : masu.getDirectlyReferredBy(order)){
				for(Move m : this.generateBy_impl(order, companionKoma)){
					moves.addIfNot(m);
				}
			}
		}
		return moves;
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(Masu masu : this.shogi.board.getAllMasu()){
			for(Move m : this.generateOn(order, masu)){
				moves.addIfNot(m);
			}
		}
		return moves;
	}
	
	/*
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		ArrayListS<Move> tmpmoves = null;
		Masu masu = null;
		int curOrder = this.shogi.kifu.getCurrentOrder();
		int nextOrder = this.shogi.kifu.getNextOrder();
		int opponent = Shogi.getOtherOrder(order);
		// assume this is my turn.
		if(nextOrder == order){
			// ぶつかっているをboardに聞く仕組み。
			// いろんなところから何回も参照される情報を、updateStatusの際に用意してしまう. TODO
			for(int i=0; i< Board.MATRIX_SIZE; i++){
				masu = this.shogi.board.getMasu(Board.SQUARES[i]);
				if(masu.isKoma(opponent)){
					if(masu.koma.isDirectlyReferredBy(nextOrder)){ // which is my turn.;
						for(Koma attacker : masu.koma.getDirectlyReferredByOpponent()){
							tmpmoves = attacker.getMovesCanGoTo(masu);
							for(Move m : tmpmoves){
								moves.addIfNot(m);
							}
						}
					}
				}
			}
		}
		
		return moves;
	}
	*/
}



