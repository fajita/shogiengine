package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class CheckOuSuji extends Suji{
	
	// 王手
	public static String NAME = "CheckOuSuji";
	
	public CheckOuSuji(Shogi shogi){
		super(shogi, Suji.ID_CHECK_OU, CheckOuSuji.NAME);
	}
	
	protected boolean examine_impl(Move lastMove){
		//王手
		// (toのmasuのkomaの)movableに相手の玉がいる
		boolean condition1 = false;
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		Koma ou = this.shogi.getOu(opponent);
		if(ou!=null){
			if(ou.isDirectlyReferredByOpponent()){
				condition1 = true;
			}
		}
		return condition1;
	}
	
	// the give komaでできる王手
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		for(Koma ou : this.shogi.getKomaList(true, false, opponent, Koma.OU, false)){
			for(Move m : koma.getPrecedingMovesFor(ou.masu)){
				moves.addIfNot(m);
			}
			
			if(koma.isOnBoard()){ // 盤上にあるなら
				for(Masu masu : koma.getMovable()){ // その駒を動かすことで、素抜き王手をてらえるか？
					for(Koma sniper : koma.getOpenedSnipersByMovingTo(masu, order)){
						if(sniper.getThroughableKoma(opponent).contains(ou)){
							if(sniper.getDirection(koma)==sniper.getDirection(ou)){//素抜く方向が正しいか
								for(Move m2 : koma.getMovesCanGoTo(masu)){
									moves.addIfNot(m2);
								}
							}
						}
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		return moves;
	}
	
	
	protected ArrayListS<Move> generate_impl(int order){
		
		ArrayListS<Move> moves = new ArrayListS<Move>();
		boolean [] alreadyAdded = new boolean[Koma.NUM_KIND+1];
		
		for(int kindId : Koma.BASIC_KIND_IDS){
			for(Koma koma: this.shogi.getKomaList(kindId, order)){
				if(koma.isOnBoard() || alreadyAdded[kindId]==false){
					for(Move m : this.generateBy_impl(order, koma)){
						moves.addIfNot(m);
					}
					if(koma.isOnStand()){
						alreadyAdded[kindId] = true;
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		return moves;
	}
	
	
}



