package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;

public class LikelyTakeInNextSuji extends Suji{
	
public static String NAME = "LikelyTakeInNextSuji";
	
	public LikelyTakeInNextSuji(Shogi shogi){
		super(shogi, Suji.ID_LIKELY_TAKE_IN_NEXT, LikelyTakeInNextSuji.NAME);
	}
	
	
	
	protected boolean examine_impl(Move lastMove){
		//取れそうの手
		// 取ろうの手　
		// 且つ、　movableにいる相手のmasuのdirectに相手がいない
		boolean condition1=false;
		
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(toKoma!=null){
			for(Masu masu : toKoma.movable){
				if(masu.koma!=null){
					if(masu.koma.getOrder()==opponent){
						if(masu.koma.isDirectlyReferredByCompanion()==false){
							condition1 = true;
							break;
						}
					}
				}
			}
		}
		return condition1;
	}
	

}


