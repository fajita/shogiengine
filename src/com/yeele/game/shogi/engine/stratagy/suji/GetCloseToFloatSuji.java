package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;

public class GetCloseToFloatSuji extends Suji{
	
public static String NAME = "GetCloseToFloatSuji";
	
	public GetCloseToFloatSuji(Shogi shogi){
		super(shogi, Suji.ID_GET_CLOSE_TO_FLOAT, GetCloseToFloatSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
	
		//浮いている駒に援護に行く手（銀）
		// 援護したい駒に近づく
		
		// TBD
		
		boolean condition1=false;
		 
		Masu fromMasu = this.shogi.board.getMasu(lastMove.from);
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
					
		return (condition1);
	}

}


