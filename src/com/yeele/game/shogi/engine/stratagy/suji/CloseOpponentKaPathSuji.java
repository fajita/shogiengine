package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class CloseOpponentKaPathSuji extends Suji{
	public static String NAME = "CloseOpponentKaPathSuji";
	
	public CloseOpponentKaPathSuji(Shogi shogi){
		super(shogi, Suji.ID_CLOSE_OPPONENT_KA_PATH, NAME);
	}
	
	protected boolean examine_impl(Move lastMove){
		//相手の角道を塞ぐ手
		// toのmasuのdirectに相手の角がいる
		boolean condition1=false;
		 
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(toKoma != null){
			for(Koma throuableKoma : toKoma.getWallFor()){
				if(throuableKoma.getOrder() == opponent ){
					if(throuableKoma.getKindId() == Koma.KA ||
					   throuableKoma.getKindId() == Koma.UM ){
						condition1 = true;
					}
				}
			}
		}
				
		return (condition1);
	}
	

}


