package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;

public class GetAwaySuji extends Suji{
	
	public static String NAME = "GetAwaySuji";
	
	public GetAwaySuji(Shogi shogi){
		super(shogi, Suji.ID_GETAWAY, GetAwaySuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
		//相手の効きから逃げる手
		// fromのmasuのdirectに相手がいる
		// 且つ　toのmasuのdirectに相手いない
		// 又は　toのmasuのdirectに味方がいる（相手もいるけど）
		
		Masu fromMasu = this.shogi.board.getMasu(lastMove.from);
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		if(fromMasu != null && toMasu != null){
			if( fromMasu.isDirectlyReferredBy(opponent) ){
				if( toMasu.countDirectlyReferredBy(opponent) == 0 ||
					toMasu.countDirectlyReferredBy(lastMove.order) > 0){
					return true;
				}
			}
		}
		
		return false;
	}
	

}


