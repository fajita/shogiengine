package com.yeele.game.shogi.engine.stratagy.suji;



import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.player.Player;
import com.yeele.util.ArrayListS;

public class ReinforceSuji extends Suji{
	
	public static String NAME = "ReinforceSuji";
	
	public ReinforceSuji(Shogi shogi){
		super(shogi, Suji.ID_REINFORCE, ReinforceSuji.NAME);
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		ArrayListS<Move> tmpMoves = null;
		ArrayListS<Integer> sqs = null;
		int opponent = Shogi.getOtherOrder(order);
		
		Evaluation evalb4ponder = this.shogi.player[order].getEvalBeforePonder();
		if(evalb4ponder != null){
			// 一番、弱いところを補強する手
			sqs = evalb4ponder.findWeakestSquare(order);
			if(sqs.size() <= 3){ // 弱点が4升以上ある場合は、多過ぎ、、序盤で拮抗しているとみなせる
				for(Integer weekestSquare : sqs){
					tmpMoves = this.shogi.getPrecedingMovesFor(this.shogi.board.getMasu(weekestSquare), order);
					for(Move m : tmpMoves){
						moves.addIfNot(m);
					}
				}
			}
			
			// 一番、強いところを補強する手
			sqs = evalb4ponder.findStrongestSquare(order);
			if(sqs.size() <= 3){ // 弱点が4升以上ある場合は、多過ぎ、、序盤で拮抗しているとみなせる
				for(Integer strongestSquare : sqs){
					tmpMoves = this.shogi.getPrecedingMovesFor(this.shogi.board.getMasu(strongestSquare), order);
					for(Move m : tmpMoves){
						moves.addIfNot(m);
					}
				}
			}
		}
		
		return moves;
	}

	
	
}



