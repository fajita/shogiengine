package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

public class OuEscapeSuji extends Suji{
	
public static String NAME = "OuEscapeSuji";
	
	public OuEscapeSuji(Shogi shogi){
		super(shogi, Suji.ID_OU_ESCAPE, OuEscapeSuji.NAME);
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		int opponent = Shogi.getOtherOrder(order);
		ArrayListS<Move> moves = new ArrayListS<Move>();
		Koma ou = this.shogi.getOu(order);
		for(Masu ms : ou.getMovable()){
			if(ms.isDirectlyReferredBy(opponent)==false){
				moves.addIfNot(ou.getMovesCanGoTo(ms));
			}
		}
		return moves;
	}
	
	protected boolean examine_impl(Move lastMove){
		
		// TBD
		// 今の状態が相手からの参照がないこと。
		// かつ言って前の状態が狙われていること。（この1手前の状態を知る為にはやはり、previous 変数が必要か？
		return false;
	}
	
}


