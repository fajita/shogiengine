package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class EscapeSuji extends Suji{
	
	// 逃げる　筋
	// 狙われている駒を取るのは逃げる手としない。
	
	public static String NAME = "EscapeSuji";
	
	public EscapeSuji(Shogi shogi){
		super(shogi, Suji.ID_ESCAPE, EscapeSuji.NAME);
	}
	

	// the given komaが逃げる手を生成 
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		if(koma == null){
		}else if(koma.getOrder() != order){
		}else{
			// move which is moving to escape from being captured.
			if(koma.isOnBoard()){
				for(Koma threat : koma.getDirectlyReferredByOpponent()){
					for(Move m : koma.getMoves()){
						Masu masuToEscape = this.shogi.board.getMasu(m.to);
						if(masuToEscape.isDirectlyReferredBy(opponent)==false){
							if(masuToEscape.square != threat.getSquare()){
								// 完全に安全な場所に(ただし、threatを取るのは、この筋の
								// 逃げる意図と違うのではずす
								moves.addIfNot(m);
							}
						}
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		return moves;
	}
	
	// the given attackingKomaから逃げる手を生成 
	protected ArrayListS<Move> generateFrom_impl(int order, Koma attackingKoma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		// move which is moving to escape from being captured.
		if(attackingKoma.getOrder() == opponent){
			for(Koma catchableKoma : attackingKoma.getReachableKoma(order)){
				// catachableKomaが逃げる手を生成
				for(Move m : this.generateBy_impl(order, catchableKoma)){
					if(m.to != attackingKoma.getSquare()){
						// attackingKomaを取る手は、この筋の逃げる手ではない
						moves.addIfNot(m);
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		return moves;
	}
	
	
	
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(Koma koma : this.shogi.getKomaList(true, false, order)){
			for(Move m : this.generateBy_impl(order, koma)){
				moves.addIfNot(m);
			}
		}
		return moves;
	}
	
	protected boolean examine_impl(Move lastMove){
		
		// fromの地点は相手に狙われていて
		// toは相手の利きがないところ
		boolean condition1=false;
		boolean condition2=false;
		 
		Masu fromMasu = this.shogi.board.getMasu(lastMove.from);
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(fromMasu != null){
			if(fromMasu.isDirectlyReferredBy(opponent)){
				condition1 = true;
			}
			
		}
		if(toMasu != null){
			if(toMasu.isDirectlyReferredBy(opponent) == false){
				condition2 = true;
			}
		}
		
		return (condition1 && condition2);
	}
}



