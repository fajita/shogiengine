package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.util.Logger;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class PlaceIfProfitableSuji extends Suji{
	
	// 駒を打つ　筋
	public static String NAME = "PlaceIfProfitableSuji";
	
	public PlaceIfProfitableSuji(Shogi shogi){
		super(shogi, Suji.ID_PLACE_IF_PROFITABLE, PlaceIfProfitableSuji.NAME);
	}
	
	// the given komaを打つ手を生成
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		ArrayListS<Move> moves = new ArrayListS<Move>();
	
		if(koma.isOnStand()){
			if(koma.getOrder()==order){
				for(Move m : koma.getMoves()){
					//　まず取られなかった打ってよし、
					Masu masu = this.shogi.board.getMasu(m.to);
					if(koma.isProfitable(masu)){
						if(Shogi.MODE_DEBUG){
							int ret = this.shogi.isPhysicallyValidMove(order, m);
							if(ret != Shogi.TRUE){
								int a = 0;
							}
						}
						moves.addIfNot(m);
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		
		return moves;
	}
	
	// the given masuに打てる駒を生成
	protected ArrayListS<Move> generateOn_impl(int order, Masu masu){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		
		for(Move m : shogi.stand[order].getPrecedingMovesFor(masu)){
			Koma km = this.shogi.stand[order].get(m.komaToMove);
			Masu masuToPlace = this.shogi.board.getMasu(m.to);
			if(km.isProfitable(masuToPlace)){
				moves.addIfNot(m);
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		
		return moves;
	}
	
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		boolean [] alreadyAdded = new boolean[Koma.NUM_KIND+1];
		
		for(int kindId : Koma.BASIC_KIND_IDS){
			for(Koma koma: this.shogi.getKomaList(false, true, order, kindId, false)){
				if(koma.isOnStand() && alreadyAdded[kindId]==false){
					for(Move m : this.generateBy_impl(order, koma)){
						moves.addIfNot(m);
					}
				}
			}
		}
		return moves;
	}
	
	
}



