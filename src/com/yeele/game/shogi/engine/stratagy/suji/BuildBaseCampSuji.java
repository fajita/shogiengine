package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class BuildBaseCampSuji extends Suji{
	
	public static String NAME = "BuildBaseCampSuji";
	
	public BuildBaseCampSuji(Shogi shogi){
		super(shogi, Suji.ID_BUILD_BASECAMP, NAME);
	}
	

	protected boolean examine_impl(Move lastMove){
		//攻撃の拠点を作る手
		// toのmasuのdirect相手がいない
		// toのmasuは5段を含む相手側にある　TBD　王にどれだけ近いか？のほうが正しい条件？
		
		boolean condition1=false;
		boolean condition2=false;
		 
		Suji dependency = new ToSafeSuji(this.shogi);
		condition1 = dependency.examine(lastMove);
		
		if(condition1==true){
			if(Board.isOpponentSide(lastMove.order, lastMove.to)){
				condition2 = true;
			}
		}
		
		return (condition1 && condition2);
	}
	
}

