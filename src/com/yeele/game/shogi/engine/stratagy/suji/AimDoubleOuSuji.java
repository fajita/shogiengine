package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

public class AimDoubleOuSuji extends Suji{
	
	public static String NAME = "AimDoubleOuSuji";
	
	public AimDoubleOuSuji(Shogi shogi){
		super(shogi, Suji.ID_AIM_DOUBLE_OU, NAME);
	}
	
	protected boolean examine_impl(Move lastMove){
		//王手駒取り（王手+両取り）
		boolean condition1=false;
		boolean condition2=false;
		
		Suji dependency1 = new CheckOuSuji(this.shogi);
		Suji dependency2 = new AimDoubleSuji(this.shogi);
		
		condition1 = dependency1.examine(lastMove);
		condition2 = dependency2.examine(lastMove);
		return (condition1 && condition2);
	}

	protected ArrayListS<Move> generate_impl(int order){ 

		ArrayListS<Move> moves = new ArrayListS<Move>();
		// TODO: 盤上の駒でもダブル王手を検出できるように改良
		
		this.setSujiIdInMoves(moves);
		return moves;
	}
	
}


