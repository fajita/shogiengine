package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;

public class PowerupDirectSuji extends Suji{
	
public static String NAME = "PowerupDirectSuji";
	
	public PowerupDirectSuji(Shogi shogi){
		super(shogi, Suji.ID_POWERUP_DIRECT, PowerupDirectSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){

		//利きを貯める手
		// movableのmasuのdirectの味方の数が2以上の手
		// 以前より多くなっていることを証明したいが、、、
		boolean condition1=false;
		 
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		
		if(toKoma != null){
			for(Masu movableMasu : toKoma.movable){
				if(movableMasu.countDirectlyReferredBy(lastMove.order) >= 2){
					condition1 = true;
				}
			}
		}
				
		return (condition1);
	}
	

}


