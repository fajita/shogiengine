package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class OpenCompanionKaPathSuji extends Suji{
	
public static String NAME = "OpenCompanionKaPathSuji";
	
	public OpenCompanionKaPathSuji(Shogi shogi){
		super(shogi, Suji.ID_OPEN_COMPANION_KA_PATH, OpenCompanionKaPathSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
	
		//味方の角道を開ける手
		// fromのmasuのdirectに味方の角がいる
		// 且つ　toのmasuのdirectに味方の角がいない
		boolean condition1=false;
		boolean condition2=false;
		
		Masu fromMasu = this.shogi.board.getMasu(lastMove.from);
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(fromMasu != null){
			for(Koma directKoma : fromMasu.directRefered){
				if(directKoma.getOrder() == lastMove.order && directKoma.isThroughable()){
					if(directKoma.getKindId()==Koma.KA || directKoma.getKindId()==Koma.UM){
						condition1 = true;
					}
					if(toMasu.directRefered.contains(directKoma)==false){
						condition2 = true;
					}
				}
			}
		}
				
		return (condition1&&condition2);
	}
	
}


