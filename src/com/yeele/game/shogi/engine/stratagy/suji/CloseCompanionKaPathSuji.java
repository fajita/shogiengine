package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class CloseCompanionKaPathSuji extends Suji{
	
	public static String NAME = "CloseCompanionKaPathSuji";
	
	public CloseCompanionKaPathSuji(Shogi shogi){
		super(shogi, Suji.ID_CLOSE_COMPANION_KA_PATH, NAME);
	}
	
	protected boolean examine_impl(Move lastMove){
		//味方の角道を塞ぐ手
		// toのmasuのdirectに味方の角がいる
		// 多分、ほとんど使われない。相手の角道を塞ぐ手とかぶっているから
		boolean condition1=false;
		 
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(toKoma != null){
			for(Koma throuableKoma : toKoma.getWallFor()){
				if(throuableKoma.getOrder() == lastMove.order ){
					if(throuableKoma.getKindId() == Koma.KA ||
					   throuableKoma.getKindId() == Koma.UM ){
						condition1 = true;
					}
				}
			}
		}
				
		return (condition1);
	}

}


