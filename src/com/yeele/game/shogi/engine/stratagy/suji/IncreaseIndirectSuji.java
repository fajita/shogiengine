package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class IncreaseIndirectSuji extends Suji{
	
public static String NAME = "IncreaseIndirectSuji";
	
	public IncreaseIndirectSuji(Shogi shogi){
		super(shogi, Suji.ID_INCREASE_INDIRECT, IncreaseIndirectSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
		//間接の聞きを増やす手
		// その駒はthrouableな駒である
		// 且つ、throuableに相手の駒がいる
		// 且つ、fromのmasuにいた時もthrouableがいたならば、対象の駒価値が高くなっている
		// HOLD: I want throuable komas to hold more information.
		//       like, koma's which is 
		
		boolean condition1=false;
		 
		Masu fromMasu = this.shogi.board.getMasu(lastMove.from);
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		return (condition1);
	}

}


