package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;

public class AimDoubleSuji extends Suji{
	
	public static String NAME = "AimDoubleSuji";
	
	public AimDoubleSuji(Shogi shogi){
		super(shogi, Suji.ID_AIM_DOUBLE, NAME);
	}
	
	protected boolean examine_impl(Move lastMove){
		//両取りを狙う手
		// movableに二つ相手の駒がある
		// それぞれの駒には紐がついていない　since　この条件がないと沢山当てはまってしまー
		boolean condition1=false;
		
		Koma toKoma = this.shogi.board.getKoma(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		int numFreeOpponent=0;
		if(toKoma!=null){
			for(Masu masu : toKoma.movable){
				if(masu.koma!=null){
					if(masu.koma.getOrder()==opponent){
						if(masu.koma.movable.size()==0){
							numFreeOpponent++;
						}
					}
				}
			}
		}
		if(numFreeOpponent>=2){
			condition1 = true;
		}
		return condition1;
	}
	
	
}


