package com.yeele.game.shogi.engine.stratagy.suji;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.stratagy.attacktactic.ShikenBisha;
import com.yeele.game.shogi.engine.stratagy.castle.KataMino;

import static com.yeele.game.shogi.engine.util.Logger.Logger;

import com.yeele.util.ArrayListS;

/*
筋とは？
将棋でいう、手のお手本といったところ。
相手がさした手に対しての守り定石や、
相手がさした手に対する切り替えし　も筋という、
さらに、戦術をの一連の手を筋ともいう

本システムでは常套手段、正解手　を筋とする。
また、美濃囲いなどの典型的な囲いや四間飛車などの
攻撃型もひとしの筋として扱う

筋はクラスとして設計し、各プレーヤーの知識として
追加可能な設計とする。このことで、新定石が生まれたときの対応や
棋力の違いによる思考を実現できる。

将棋には格言がたくさんあるが、それらも一つの筋としてとらえることとする。

これまでの説明は、すこし抽象な表現をしてきたが、
以下に具体的に、本システム、筋として扱うものの箇条する

- MoveModelで表現された手を指されたときの、返し手
- 戦法を実施するときの手
- 囲いを作るときの手
- 格言に対応した手（格言をMoveModelにあてはめる)

*/
public class Suji{
	
	protected Shogi shogi;
	protected String name;
	protected int sujiId;
	
	public static final int ID_MOVE_TO_BACKEDUP 	= 1;	//駒を効きのある升へ移動する手 
	public static final int ID_TAKE_OPPONENT		= 2;	//相手の駒をとる手
	public static final int ID_TAKE_OPPONENT_FREE= 3;	//タダ駒を取る手
	public static final int ID_GETAWAY			= 4;	//相手の効きから逃げる手
	public static final int ID_GETAWAY_INDIRECT	= 5;	//間接の利きを逸らす手（逃げる手）
	public static final int ID_PUT_BETWEEN		= 6;	//間駒
	public static final int ID_TO_SAFE			= 9;	//次順で相手に取られない手 
	public static final int ID_LIKELY_TAKE_IN_NEXT=11;	//取れそうの手
	public static final int ID_CAN_TAKE_IN_NEXT	=12;	//取れるの手
	public static final int ID_PLACE_ON_SAFE		=13;	//次順で取られない升に駒を打つ手
	public static final int ID_AIM_DOUBLE		=14;	//両取り
	public static final int ID_SACRIFICE			=15;	//駒を犠牲にする手
	public static final int ID_INCREASE_INDIRECT =16;	//間接の聞きを増やす手
	public static final int ID_OFF_FROM_OPPONENT_KA_PATH =17;	//間接の効きを減らす手
	public static final int ID_BUILD_BASECAMP	=18;	//攻撃の拠点を作る手
	public static final int ID_POWERUP_DIRECT	=19;	//利きを貯める手
	public static final int ID_TAKE_LINE			=20;	//位を取る手( 前に進む手　+ 取れれない手　）
	public static final int ID_AIM_THROUGH		=22;	//素抜きを狙う手（取れない手）
	public static final int ID_AIM_DOUBLE_OU		=23;	//王手駒取り（王手+両取り）
	public static final int ID_CLOSE_OPPONENT_KA_PATH =24;	//相手の角道を塞ぐ手
	public static final int ID_CLOSE_COMPANION_KA_PATH =25;	//味方の角道を塞ぐ手
	public static final int ID_OPEN_COMPANION_KA_PATH =26;	//味方の角道を開ける手
	public static final int ID_OU_TO_SAFE		=27;	//玉が安全な場所に移動する手
	public static final int ID_GET_CLOSE_TO_FLOAT=29;	//浮いている駒に援護に行く手（銀）
	
	// Normal suji in 100's
	public static final int ID_HASHI_FU_UKE	= 100;
	public static final int ID_REINFORCE 		= 101; // 補強する。守りにも、攻めにも使える。盤上得点で弱いところを補強。強いところはさらに強く補強
	public static final int ID_TAKE 			= 102; // 	単純に駒を取る手
	public static final int ID_OFFENSE_IF_PROFITABLE = 103; // 升ベースで、その升に移動するのが得かどうか判断し、得であれば動く手を返す筋
	public static final int ID_FIRE			= 104; // ぶつける
	public static final int ID_CHECK_OU		= 105; // 王手
	
	
	// Normal defense
	public static final int ID_ESCAPE			= 201; // 逃げる
	public static final int ID_BACKUP			= 202; // 利きをつける
	public static final int ID_PUTBETWEEN		= 203; // 間駒
	public static final int ID_PUTBETWEEN_BASIC= 204; // 基本間駒（守る駒より安いこまで、バックアップのある位置へ）
	public static final int ID_OU_ESCAPE 		= 205; // 王が狙われているので逃げる手 
	public static final int ID_TAKE_OU_THREAT = 206; // 王手している相手を取る手
	
	// Normal nutural
	public static final int ID_PLACE			= 301; // 打つ
	public static final int ID_PLACE_IF_SAFE	= 302; // 打つとられない場所に
	public static final int ID_PLACE_IF_PROFITABLE = 303; // 勝算があれば打つ
	public static final int ID_PASS			= 304; // パス
	public static final int ID_TAKELINE			= 305; // 位をとる手
	
	
	// Castle suji in 500's
	public static final int ID_TACTIC_START		= 500;
	public static final int ID_CASTLE_KATA_MINO 	= 501;
	public static final int ID_CASTLE_HON_MINO 	= 502;
	public static final int ID_CASTLE_KIMURA_MINO = 503;
	public static final int ID_CASTLE_TAKA_MINO 	= 504;
	
	// Attack suji in 700's
	public static final int ID_ATTACK_SHIKEN_BISHA = 701;
	public static final int ID_TACTIC_END		= 800;
	
	public boolean isTactic(){
		return (this.sujiId > ID_TACTIC_START && this.sujiId < ID_TACTIC_END) ? true : false;
	}
	
	
	public Suji(Shogi shogi, int sujiId, String name){
		this.shogi = shogi;
		this.name = name;
		this.sujiId = sujiId;
	}
	
	/*
	 * generateはこの筋に対して、はてはまる手を生成するので、引数はとくにとらない。
	 * 筋によっては、戦型、や囲い、最終手の情報が必要だかそれは別途、受け取り、generateの
	 * signatureは引数なしで統一する。ただし、どちら側の手を生成するのかだけは引数で指定。
	 */
	/*
	 * generateの関数は様々な引数を取りうる。
	 * 例えば、
	 * 逃げる筋を考えたとき、
	 * 盤上に逃げる手は複数あるとしよう、
	 * 考えられる手の生成は
	 * komaを引数に与え、その駒が逃げれる手の生成
	 * masuを引数に与え、その升に逃げれるての生成
	 * komaを引数に与え、その駒から逃げられる手の生成
	 * masuを引数に与え、その升にいる駒から逃げられる手の生成
	 * 
	 * これらの関数は同じ引数を取る関数をつくりたいが、
	 * 当然名前を変更しなくてはならない。
	 * ただ、sujiではgenerateという関数で当初統一したい思っていたし、
	 * generateという言葉は手を生成するという意味では正しい名前だから残したい。
	 * そこで命名規則、というか命名のヒントを以下に記す
	 * generate(suji)助動詞　引数()という考え方。分かりにくいから下に説明
	 * 関数の始まりはgenerateではじまる。
	 * generateには実際には入れないが、sujiをいれる
	 * 例えば、
	 * komaを引数に与え、その駒が逃げれる手の生成
	 * komaを引数に与え、その駒から逃げられる手の生成
	 * の名前を付けてみよう。
	 * komaを引数に与え、その駒が逃げれる手の生成　は、
	 * generate 'escapable suji' for the given koma
	 * よって名前はgenerateFor(int, Koma);
	 * komaを引数に与え、その駒から逃げられる手の生成
	 * generate 'escapable suji' from the given koma
	 * よって名前はgenerateFrom(int, Koma);
	 * 
	 * これを統一することで命名規則、使う側に一貫性を持たす。
	 */
	
	/*
	 * as of 2012.6.4
	 * generageForMasusとかlistを取る関数もつくっているが、
	 * 俺はこれは要らないと思う。その方向で進める
	 */
	public void setSujiIdInMove(Move move){
		move.setSujiCorrespondance(this.sujiId, true);
	}
	public void setSujiIdInMoves(ArrayListS<Move> moves){
		for(Move m : moves){ this.setSujiIdInMove(m); }
	}
	
	private void isPhysicallyInvalid(int order, ArrayListS<Move> moves){
		for(Move mv : moves){
			if(this.shogi.isPhysicallyValidMove(order, mv) != Shogi.TRUE){
				Logger.error("%s is pysically invalid move!!\n", mv);
			}
		}
	}
	
	public ArrayListS<Move> generate(int order){ 

		ArrayListS<Move> moves = this.generate_impl(order);
		this.setSujiIdInMoves(moves);
		if(Shogi.MODE_DEBUG) this.isPhysicallyInvalid(order, moves);
		return moves;
	}
	protected ArrayListS<Move> generate_impl(int order){ 
		return new ArrayListS<Move>();
	}
	
	/* 様々なインターフェースはサポートする*/
	public ArrayListS<Move> generateBy(int order, Koma koma){ 
		ArrayListS<Move> moves = this.generateBy_impl(order, koma);
		this.setSujiIdInMoves(moves);
		if(Shogi.MODE_DEBUG) this.isPhysicallyInvalid(order, moves);
		return moves;
	}
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		return new ArrayListS<Move>();
	}
	
	public ArrayListS<Move> generateOn(int order, Masu masu){ 
		ArrayListS<Move> moves = this.generateOn_impl(order, masu);
		this.setSujiIdInMoves(moves);
		if(Shogi.MODE_DEBUG) this.isPhysicallyInvalid(order, moves);
		return moves;
	}
	protected ArrayListS<Move> generateOn_impl(int order, Masu masu){ 
		return new ArrayListS<Move>();
	}
	
	public ArrayListS<Move> generateFrom(int order, Koma koma){ 
		ArrayListS<Move> moves = this.generateFor_impl(order, koma);
		this.setSujiIdInMoves(moves);
		if(Shogi.MODE_DEBUG) this.isPhysicallyInvalid(order, moves);
		return moves;
	}
	protected ArrayListS<Move> generateFrom_impl(int order, Koma koma){ 
		return new ArrayListS<Move>();
	}
	
	public ArrayListS<Move> generateAgainst(int order, Koma koma){ 
		ArrayListS<Move> moves = this.generateAgainst_impl(order, koma);
		this.setSujiIdInMoves(moves);
		if(Shogi.MODE_DEBUG) this.isPhysicallyInvalid(order, moves);
		return moves;
	}
	protected ArrayListS<Move> generateAgainst_impl(int order, Koma koma){ 
		return new ArrayListS<Move>();
	}
	
	public ArrayListS<Move> generateFor(int order, Koma koma){ 
		ArrayListS<Move> moves = this.generateFor_impl(order, koma);
		this.setSujiIdInMoves(moves);
		if(Shogi.MODE_DEBUG) this.isPhysicallyInvalid(order, moves);
		return moves;
	}
	protected ArrayListS<Move> generateFor_impl(int order, Koma koma){ 
		return new ArrayListS<Move>();
	}
	
	public ArrayListS<Move> generateFor(int order, Masu masu){ 
		ArrayListS<Move> moves = this.generateFor_impl(order, masu);
		this.setSujiIdInMoves(moves);
		if(Shogi.MODE_DEBUG) this.isPhysicallyInvalid(order, moves);
		return moves;
	}
	protected ArrayListS<Move> generateFor_impl(int order, Masu masu){ 
		return new ArrayListS<Move>();
	}
	
	public boolean examine(Move lastMove){
		// examine whether if the give move matches to this suji or not.
		boolean ret = false;
		ret = this.examine_impl(lastMove);
		return ret;
	}
	
	protected boolean examine_impl(Move lastMove){
		return false;
	}
	
	
	public int getSujiId(){
		return this.sujiId;
	}
	
	public String getName(){
		return this.name;
	}
	
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		buff.reset();
		ps.printf("%s", this.name);
		str += buff.toString();

		return str;
	}
	
	public static String getNameBySujiId(int sujiId){
		String name = ""; 
		switch(sujiId){
		case ID_MOVE_TO_BACKEDUP: name = MoveToBackedupSuji.NAME; break;
		case ID_TAKE_OPPONENT: name=TakeOpponentSuji.NAME; break;
		case ID_TAKE_OPPONENT_FREE: name=TakeOpponentFreeSuji.NAME; break;
		case ID_GETAWAY: name=GetAwaySuji.NAME; break;
		case ID_GETAWAY_INDIRECT: name=GetAwayIndirectSuji.NAME; break;
		case ID_PUT_BETWEEN: name=PutBetweenSuji.NAME; break;
		case ID_TO_SAFE: name=ToSafeSuji.NAME; break;
		case ID_LIKELY_TAKE_IN_NEXT: name=LikelyTakeInNextSuji.NAME; break;
		case ID_CAN_TAKE_IN_NEXT: name=CanTakeInNextSuji.NAME; break;
		case ID_PLACE_ON_SAFE: name=PlaceOnSafeSuji.NAME; break;
		case ID_AIM_DOUBLE: name=AimDoubleOuSuji.NAME; break;
		case ID_SACRIFICE: name=SacrificeSuji.NAME; break;
		case ID_INCREASE_INDIRECT: name=IncreaseIndirectSuji.NAME; break;
		case ID_OFF_FROM_OPPONENT_KA_PATH: name=OffFromOpponentKaPathSuji.NAME; break;
		case ID_BUILD_BASECAMP: name=BuildBaseCampSuji.NAME; break;
		case ID_POWERUP_DIRECT: name=PowerupDirectSuji.NAME; break;
		case ID_TAKE_LINE: name=TakeLineSuji.NAME; break;
		case ID_AIM_THROUGH: name=AimThroughSuji.NAME; break;
		case ID_AIM_DOUBLE_OU: name=AimDoubleOuSuji.NAME; break;
		case ID_CLOSE_OPPONENT_KA_PATH: name=CloseOpponentKaPathSuji.NAME; break;
		case ID_CLOSE_COMPANION_KA_PATH: name=CloseCompanionKaPathSuji.NAME; break;
		case ID_OPEN_COMPANION_KA_PATH: name=OpenCompanionKaPathSuji.NAME; break;
		case ID_OU_TO_SAFE: name=OuToSafeSuji.NAME; break;
		case ID_GET_CLOSE_TO_FLOAT: name=GetCloseToFloatSuji.NAME; break;

		case ID_HASHI_FU_UKE: name=HashiFuUkeSuji.NAME; break;
		case ID_REINFORCE: name=ReinforceSuji.NAME; break;
		case ID_TAKE: name=TakeSuji.NAME; break;
		case ID_OFFENSE_IF_PROFITABLE: name=OffenseIfProfitableSuji.NAME; break;
		case ID_FIRE: name=FireSuji.NAME; break;
		case ID_CHECK_OU: name=CheckOuSuji.NAME; break;

		case ID_ESCAPE: name=EscapeSuji.NAME; break;
		case ID_BACKUP: name=BackupSuji.NAME; break;
		case ID_PUTBETWEEN: name=PutBetweenSuji.NAME; break;
		case ID_PUTBETWEEN_BASIC: name=PutBetweenBasicSuji.NAME; break;
		case ID_OU_ESCAPE: name=OuEscapeSuji.NAME; break;
		case ID_TAKE_OU_THREAT: name=TakeOuThreatSuji.NAME; break;

		case ID_PLACE: name=PlaceSuji.NAME; break;
		case ID_PLACE_IF_SAFE: name=PlaceIfSafeSuji.NAME; break;
		case ID_PLACE_IF_PROFITABLE: name=PlaceIfProfitableSuji.NAME; break;
		case ID_PASS: name=PassSuji.NAME; break;
		case ID_TAKELINE: name=TakeLineSuji.NAME; break;

		case ID_CASTLE_KATA_MINO: name=KataMino.NAME; break;
		//case ID_CASTLE_HON_MINO: name=xxxx.NAME; break;
		//case ID_CASTLE_KIMURA_MINO: name=xxxx.NAME; break;
		//case ID_CASTLE_TAKA_MINO: name=xxxx.NAME; break;
		case ID_ATTACK_SHIKEN_BISHA: name=ShikenBisha.NAME; break;
		default: break;
		}
		return name;
	}
}



