package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

public class TakeOuThreatSuji extends Suji{
	
	// 王手している相手を取る手
	
	public static String NAME = "TakeOuThreatSuji";
	
	
	public TakeOuThreatSuji(Shogi shogi){
		super(shogi, Suji.ID_TAKE_OU_THREAT, TakeOuThreatSuji.NAME);
	}
	
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		Koma ou = this.shogi.getOu(order);
		for(Koma threat : ou.getDirectlyReferredByOpponent()){
			moves.addIfNot(threat.getMovesThatCanTakeMe());
		}
		return moves;
	}
	
	
	
	
	protected boolean examine_impl(Move lastMove){
		// TODO
		// TBD: previous movableを保存しておけば、楽に検出できるが、メモリの使用量が増えるトレードオフ
		// arraylist<Masu> * N(0-20) * 40 pieces, --> no so much.
		boolean condition1=false;
			
		return (condition1);
	}
}



