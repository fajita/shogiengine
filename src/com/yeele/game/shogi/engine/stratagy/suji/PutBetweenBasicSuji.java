package com.yeele.game.shogi.engine.stratagy.suji;


import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class PutBetweenBasicSuji extends Suji{
	
	// 取れる駒がある場合は、取る筋
	public static String NAME = "PutBetweenBasicSuji";
	
	public PutBetweenBasicSuji(Shogi shogi){
		super(shogi, Suji.ID_PUTBETWEEN_BASIC, PutBetweenBasicSuji.NAME);
	}
	
	// komaを間駒として使う手を生成
	protected ArrayListS<Move> generateBy_impl(int order, Koma koma){
		
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		for(Move m: koma.getMoves()){
			Masu masu = this.shogi.board.getMasu(m.to);
			// ただで取れれない場所に置く。
			// toMasuには自分の参照はあるので、自分以外の味方の参照を確認するには
			// countが2以上であればよい。
			if(masu.countDirectlyReferredBy(order) >= 2){
				for(Koma sniper : masu.getDirectlyReferredBy(opponent)){
					if(sniper.isThroughable()){
						// もしthe give komaが動ける範囲でsniperが通っていたら、
						// この駒は(間駒）壁になることができる。　
						// ただし、もう既に壁なら、新しい手を生成しないこととする。
						if(koma.getWallFor(opponent).contains(sniper)) break;
						
						for(Koma threatenKoma : sniper.getReachableKoma(order)){
							// これで味方が狙われていることが分かる
							// sniperとthreatenKomaの間にorderの駒を置きたい
							// 守る駒であるthreatenKomaより安い駒を使いたい 
							if(koma.getKindId() < Koma.getUnpromotedPiece(threatenKoma.getKindId())){
								moves.addIfNot(m);
							}
						}
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		return moves;
	}
	
	// sniperに対して間駒する手を生成
	// generate 'put between suji' against the given sniper
	protected ArrayListS<Move> generateAgainst_impl(int order, Koma sniper){
		
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		if(sniper.getOrder()==opponent){
			for(Koma threatenKoma : sniper.getReachableKoma(order)){
				// これで味方が狙われていることが分かる
				// それで、この狙われている駒がタダで取られるなら、間駒しよう。
				if(threatenKoma.canBeTakenForFree()){
					// sniperとthreatenKomaの間にorderの駒を置きたい
					// 守る駒であるthreatenKomaより安い駒を使いたい
					for(Masu masu : this.shogi.board.getEmptyMasuBetween(threatenKoma.masu, sniper.masu)){
						// toMasuには自分の参照はあるので、自分以外の味方の参照を確認するには
						// countが2以上であればよい。
						if(masu.countDirectlyReferredBy(order) >= 2){
							for(Move m : this.shogi.getMovesCanGoTo(order, masu.square)){
								// このsujiは基本形なので、全ての間駒を生成する。
								// 特に
								// 守る駒であるthreatenKomaより安い駒を使いたい場合は
								// このsujiを継承したsujiを別途作成する。
								if(m.komaToMove < threatenKoma.getKindId()){
									moves.addIfNot(m);
								}
							}
						}
					}
				}
			}
		}
		
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		return moves;
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		
		ArrayListS<Move> moves = new ArrayListS<Move>();
		int opponent = Shogi.getOtherOrder(order);
		
		for(Koma sniper : this.shogi.getKomaList(true, false, opponent, Koma.HI, true)){
			for(Move m : this.generateAgainst(order, sniper)){
				moves.addIfNot(m);
			}
		}
		for(Koma sniper : this.shogi.getKomaList(true, false, opponent, Koma.KA, true)){
			for(Move m : this.generateAgainst(order, sniper)){
				moves.addIfNot(m);
			}
		}
		for(Koma sniper : this.shogi.getKomaList(true, false, opponent, Koma.KY, false)){
			for(Move m : this.generateAgainst(order, sniper)){
				moves.addIfNot(m);
			}
		}
		Logger.debug("%s generate ", NAME);
		for(Move m : moves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		return moves;
	}
	
	protected boolean examine_impl(Move lastMove){
		//間駒
		// TODO
		boolean condition1=false;
		return (condition1);
	}
}



