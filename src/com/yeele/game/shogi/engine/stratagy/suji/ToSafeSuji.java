package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class ToSafeSuji extends Suji{
	
public static String NAME = "ToSafeSuji";
	
	public ToSafeSuji(Shogi shogi){
		super(shogi, Suji.ID_TO_SAFE, ToSafeSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
	
		//取られない手
		// toのmasuのdirectに相手いない
		boolean condition1=false;
		 
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(toMasu!=null){
			if(toMasu.isDirectlyReferredBy(opponent)==false){
				condition1 = true;
			}
		}
		
		return condition1;
		
	}
	

}


