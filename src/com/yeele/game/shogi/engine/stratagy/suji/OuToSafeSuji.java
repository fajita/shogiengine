package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;

public class OuToSafeSuji extends Suji{
	
public static String NAME = "OuToSafeSuji";
	
	public OuToSafeSuji(Shogi shogi){
		super(shogi, Suji.ID_OU_TO_SAFE, OuToSafeSuji.NAME);
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		int opponent = Shogi.getOtherOrder(order);
		ArrayListS<Move> moves = new ArrayListS<Move>();
		Koma ou = this.shogi.getOu(order);
		for(Masu ms : ou.getMovable()){
			if(ms.isDirectlyReferredBy(opponent)==false){
				moves.addIfNot(ou.getMovesCanGoTo(ms));
			}
		}
		return moves;
	}
	
	protected boolean examine_impl(Move lastMove){
	
		//玉が安全な場所に移動する手
		// 玉が動く
		// 玉が安全な升に向かって進む
		boolean condition1=false;
		boolean condition2=false;
		
		Koma lastMovedKoma = this.shogi.board.getKoma(lastMove.to);
		
		if(lastMove.komaToMove == Koma.OU){
			condition1 = true;
		}
		
		if(lastMovedKoma.isDirectlyReferredByOpponent() == false){
			condition2 = true;
		}
		
		return (condition1 && condition2);
		
	}
	
}


