package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class GetAwayIndirectSuji extends Suji{
	
	public static String NAME = "GetAwayIndirectSuji";
	
	public GetAwayIndirectSuji(Shogi shogi){
		super(shogi, Suji.ID_GETAWAY_INDIRECT, GetAwayIndirectSuji.NAME);
	}
	
	protected boolean examine_impl(Move lastMove){
		//間接の利きを逸らす手（逃げる手）
		//　toのmasuのindirectに相手いない
		// 又は(将来、角道、飛車道、香車道を保持させるなら、toのmasuにそれらに、それがないこと）
		// fromのmasuのindirectに相手がいる
		
		Masu fromMasu = this.shogi.board.getMasu(lastMove.from);
		Masu toMasu = this.shogi.board.getMasu(lastMove.to);
		int opponent = Shogi.getOtherOrder(lastMove.order);
		
		if(fromMasu != null){
			if( fromMasu.isIndirectlyReferredBy(opponent)){
				if(toMasu != null){
					if( toMasu.countIndirectlyReferredBy(opponent) == 0){
						return true;
					}
				}
			}
		}
		return false;
	}
	

}


