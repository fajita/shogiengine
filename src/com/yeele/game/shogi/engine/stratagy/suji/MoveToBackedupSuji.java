package com.yeele.game.shogi.engine.stratagy.suji;

import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;


public class MoveToBackedupSuji extends Suji{
	
public static String NAME = "MoveToBackedupSuji";
	
	public MoveToBackedupSuji(Shogi shogi){
		super(shogi, Suji.ID_MOVE_TO_BACKEDUP, MoveToBackedupSuji.NAME);
	}
	
	
	protected boolean examine_impl(Move lastMove){
	
		//駒を効きのある升へ移動する手
		// toのmasuのdirectに味方がいる
		// fromのmasuのdirectに味方がいない(この要因は別の手とする） del
		// 狙われている時に動くと割高( or TBD　逃げる手との組合せ包括する仕組みみする） del
		//  fromのmasuのdirectに相手がいる del
		//  又はfromのmasuのindirectに相手がいる del
		
		Masu masu = this.shogi.board.getMasu(lastMove.to);
		if(masu!=null){
			if(masu.isDirectlyReferredBy(lastMove.order)) return true;
		}
		return false;
	}

}


