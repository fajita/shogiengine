package com.yeele.game.shogi.engine.stratagy.attacktactic;



import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.komas.Fu;
import com.yeele.game.shogi.engine.komas.Gi;
import com.yeele.game.shogi.engine.komas.Hi;
import com.yeele.game.shogi.engine.stratagy.castle.TacticBuilder;
import com.yeele.game.shogi.engine.stratagy.suji.Suji;
import com.yeele.util.ArrayListS;




public class ShikenBisha extends AttachTactic{
	/*
	 * 四間飛車
	 */
	public static String NAME = "ShikenBisha";
	
	public ShikenBisha(Shogi shogi){
		super(shogi, Suji.ID_ATTACK_SHIKEN_BISHA, ShikenBisha.NAME);
		
		Koma hi, gi, fu;
		for(int order : Shogi.ORDERS){
			hi = new Hi(null, order);
			hi.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 68)));
			
			gi = new Gi(null, order);
			gi.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 67)));
			
			fu = new Fu(null, order);
			fu.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 66)));
			
			this.addModel(hi);
			this.addModel(gi);
			this.addModel(fu);
			
			// this has to be called here at the end, concretely after models are set.
			this.setTacticBuilder(new TacticBuilder(this, order));
			this.setGurdian(order); // set gurdian for the give order
		}
		
	}
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		moves.addAll(this.getCandidatesForNextMove(order));
		return moves;
	}
}


