package com.yeele.game.shogi.engine.stratagy.castle;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;




public class TacticBuilder{
	
	// TacticBuilder 訳して　型製造機
	
	protected Tactic tactic;
	protected int order;
	
	protected HashMap<Koma, Koma> modelCastMap; // key is model, value is the cast(actual koma)
	
	private int totalRequiredHands;
	
	public TacticBuilder(Tactic tactic, int order){
		this.tactic= tactic;
		this.order = order; 
		this.modelCastMap = new HashMap<Koma, Koma>();
		
		this.totalRequiredHands = this.getHandsRest();
	}
	
	public int getOrder(){
		return this.order;
	}
	
	public void updateModel(){
		this.modelCastMap.clear();
		for(Koma model : this.tactic.getModels(order)){
			this.modelCastMap.put(model, null);
		}
	}
	
	public void clearCast(){
		for(Map.Entry<Koma, Koma> entry : this.modelCastMap.entrySet()){
			this.modelCastMap.put(entry.getKey(), null);
		}
	}
	// その将棋、局面において、その囲いの守備駒となる駒のリストを返す。
	//　守備駒の判断は、最終位置に近いもの
	public void setGurdian(Shogi shogi){
		
		int closestDist; 
		Koma closestKoma, model;
		
		this.updateModel();
		
		for(Map.Entry<Koma, Koma> entry : this.modelCastMap.entrySet()){
			model = entry.getKey();
			closestKoma = null;
			closestDist = Board.MAX_DISTANCE + 1;
			for(Koma koma : shogi.getKomaList(true, false, this.order, model.getKindId(), true) ){
				// 近い駒を、採用する
				int turns = koma.getHandsToReach(model.getSquare());
				if(turns != -1){
					if(turns < closestDist){
						if(this.modelCastMap.containsValue(koma)==false){//金二枚使う囲いで、かぶってないことを保障
							closestDist = turns;
							closestKoma = koma;
						}
					}
				}
			}
			if(closestKoma!=null){
				this.modelCastMap.put(model, closestKoma);
			}
		}

	}
	
	public HashMap<Koma, Koma> getModelCastMap(){
		return this.modelCastMap;
	}
		
	public ArrayListS<Move> getCandidatesForNextMove(){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		Koma model = null, gurdian = null;
		Move move = null;
		for(Map.Entry<Koma, Koma> entry : this.getModelCastMap().entrySet()){
			model = entry.getKey();
			gurdian = entry.getValue();
			// gurdian がnullになるのは、おそらく、棋譜を読み込んで途中から試合を再開させるとき。
			// TacticBuilderが途中の局面からmodel、gurdianをセットしようとすると、guridanがいない状態がでてくる。
			//　、また、平手からセットした場合、guridanが必ず付くが、そのguridanは途中で相手に取られてしまうこともある。
			// それでもgetModelCastMapは、その相手に渡ってしまったguridanを保持しているので、そのgetMoveToGetCloseは取り込まない
			// ように注意。 // 
			if(gurdian == null) continue;
			
			if(gurdian.getSquare() == model.getSquare()){
				// you are already set
			}else if(gurdian.getOrder() != model.getOrder()){
				// gurdian は試合途中で取られちゃってるね。
			}else{
				move = gurdian.getMoveToGetClose(model.masu.square);
			}
			if(move != null){
				moves.addIfNot(move);
			}
		}
		return moves;
	}
	
	public String toString(){
		String str = "Builder for ";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		buff.reset();
		ps.printf("%s", this.tactic.getName());
		str += buff.toString();

		return str;
	}
	
	public boolean isCompleted(){
		boolean ret = true;
		for(Map.Entry<Koma, Koma> entry : this.modelCastMap.entrySet()){
			Koma model  = entry.getKey();
			Koma gurdian = entry.getValue();
			if(gurdian != null){
				if(gurdian.getHandsToReach(model.masu.square) != 0){
					ret = false;
					break;
				}
			}
			
		}
		return ret;
	}
	
	public int getHandsRest(Koma model){
		int count = 0, hands;
		if(model != null){
			Koma cast = this.modelCastMap.get(model);
			if(cast != null){
				hands = cast.getHandsToReach(model.masu.square);
				if(hands == -1){
					count += hands;
				}
			}
		}
		return count;
	}
	
	public int getHandsRest(){
		int count = 0, hands;
		for(Map.Entry<Koma, Koma> entry : this.modelCastMap.entrySet()){
			Koma model  = entry.getKey();
			Koma gurdian = entry.getValue();
			if(gurdian != null){
				hands = gurdian.getHandsToReach(model.masu.square);
				if(hands == -1){
					count += hands;
				}
			}
		}
		return count;
	}
	
	public int getTotalRequiredHands(){
		return this.totalRequiredHands;
	}
	
	public int getProgress(){
		int rest = this.getHandsRest();
		float percentage = (float)(totalRequiredHands - rest) / (float)(totalRequiredHands);
		return (int)(percentage * 100);
	}
}




