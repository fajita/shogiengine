package com.yeele.game.shogi.engine.stratagy.castle;


import java.util.Map;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.komas.Fu;
import com.yeele.game.shogi.engine.komas.Gi;
import com.yeele.game.shogi.engine.komas.Ki;
import com.yeele.game.shogi.engine.komas.Ou;
import com.yeele.game.shogi.engine.stratagy.suji.Suji;
import com.yeele.util.ArrayListS;




public class KataMino extends Castle{
	
	public static String NAME = "KataMino";
	
	// TODO prepare both BLACK&WHITE builder , then decide which when generate(int).
	public KataMino(Shogi shogi){
		super(shogi, Suji.ID_CASTLE_KATA_MINO, KataMino.NAME);
		
		Koma ou, gi, ki, fu1, fu2, fu3, fu4;
		
		for(int order : Shogi.ORDERS){
			// setting up for BLACK
			ou = new Ou(null, order);
			ou.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 28)));
			
			gi = new Gi(null, order);
			gi.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 38)));
			
			ki = new Ki(null, order);
			ki.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 49)));
			
			fu1 = new Fu(null, order);
			fu1.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 16)));
			
			fu2 = new Fu(null, order);
			fu2.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 27)));
			
			fu3 = new Fu(null, order);
			fu3.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 37)));
			
			fu4 = new Fu(null, order);
			fu4.bePlaced(new Masu(Board.convFromBlackToGivenPerspective(order, 47)));
			
			this.addModel(ou);
			this.addModel(gi);
			this.addModel(ki);
			this.addModel(fu1);
			this.addModel(fu2);
			this.addModel(fu3);
			this.addModel(fu4);
			
			// this has to be called here at the end, concretely after models are set.
			this.setTacticBuilder(new TacticBuilder(this, order));
			this.setGurdian(order); // set gurdian for the give order
		}
	}
	
	
	protected ArrayListS<Move> generate_impl(int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		
		// 飛車を振っていなければ、まずそれをしないといけない。
		// この飛車を振る担当はattackTicktacなので、この筋では何もしない。
		boolean isHiOnTheWay = false; // 飛車が囲いの邪魔になっていないか
		ArrayListS<Koma> hishas = this.shogi.getKomaList(true, false, order, Koma.HI, true);
		for(Koma hi : hishas){
			if(Board.isLeftSide(hi.getOrder(), hi.masu.square) == false){ // まだ右側にいる
				// isXXXSideは5筋を含むので、あえて、leftのfalseを条件としている
				if(Board.getRankFromYourBottom(hi.getOrder(), hi.masu.square) == 2){
					isHiOnTheWay = true;
					break;
				}
			}
		}
		if(isHiOnTheWay == false){
			moves.addAll(this.getCandidatesForNextMove(order));
		}
		
		return moves;
	}
	
	public ArrayListS<Koma> getKomaShouldStay(int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		
		// 基本、囲いの定位置まで、動いている駒はこの位置から動かす必要なし。
		if(!this.builder[order].isCompleted()){
			for(Map.Entry<Koma, Koma> entry : this.builder[order].getModelCastMap().entrySet()){
				Koma model = entry.getKey();
				Koma cast = entry.getValue();
				if(this.builder[order].getHandsRest(model) == 0){
					komas.addIfNot(cast);
				}
			}
		}
		
		// 上級者は相手自分の駒位置から、動かなくていよい、駒を判定する。
		
		return komas;
	}
}























