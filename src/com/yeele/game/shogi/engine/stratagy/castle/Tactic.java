package com.yeele.game.shogi.engine.stratagy.castle;



import java.util.Map;

import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.stratagy.suji.Suji;
import com.yeele.util.ArrayListS;




public class Tactic extends Suji{
	
	protected ArrayListS<Koma> [] models = new ArrayListS[2];
	protected TacticBuilder [] builder = new TacticBuilder[2];
	
	public Tactic(Shogi shogi, int sujiId, String name){
		super(shogi, sujiId, name);
		for(int i=0; i<this.models.length; i++){
			this.models[i] = new ArrayListS<Koma>();
		}
	}
	
	
	public ArrayListS<Koma> getModels(int order){
		return this.models[order];
	}
	public TacticBuilder getTacticBuilder(int order){
		return this.builder[order];
	}
	
	public void addModel(Koma koma){
		int order = koma.getOrder();
		this.models[order].addIfNot(koma);
	}
	public void setTacticBuilder(TacticBuilder tb){
		int order = tb.getOrder();
		this.builder[order] = tb;
	}
	public void setGurdian(int order){
		this.builder[order].setGurdian(this.shogi);
	}
	public ArrayListS<Move> getCandidatesForNextMove(int order){
		return this.builder[order].getCandidatesForNextMove();
	}
	
	public ArrayListS<Koma> getKomaShouldStay(int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		return komas;
	}
	
}




