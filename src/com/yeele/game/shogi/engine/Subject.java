package com.yeele.game.shogi.engine;


public interface Subject{
	abstract public int register(Observer obs);
	abstract public int unregister(Observer obs);
	abstract public void notifyObservers();
	abstract public void notify(Observer obs);
}



