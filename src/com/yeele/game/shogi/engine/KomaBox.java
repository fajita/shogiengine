package com.yeele.game.shogi.engine;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import com.yeele.util.ArrayListS;


public class KomaBox{
	
	protected Shogi shogi;
	ArrayListS<Koma> komas = new ArrayListS();
	
	public KomaBox(Shogi shogi){
		
		this.shogi = shogi;
	}
	
	public void reset(){
		this.komas.clear();
	}
	
	public int add(Koma k){
		int ret = Shogi.ERROR;
		if(Koma.isValid(k.kindId)){
			try{
				this.komas.add(k);
				ret = Shogi.SUCCESS;
			}catch (Exception e){
				e.printStackTrace();
			}
		}
			
		return ret;
	}
	
	public Koma get(int kindId){
		if(Koma.isValid(kindId)){
			for(Koma koma : komas){
				if(
				  (koma.getKindId() == kindId) ||
				  (koma.getKindId() == Koma.getUnpromotedPiece(kindId))
				){
					return koma;
				}
			}
		}
		return null;
	}
	
	public Koma fetch(int kindId){
		Koma k = null;
		if(Koma.isValid(kindId)){
			for(Koma koma : komas){
				if(
				  (koma.getKindId() == kindId) ||
				  (koma.getKindId() == Koma.getUnpromotedPiece(kindId))
				){
					k = koma;
					break;
				}
			}
		}
		if(k!=null) this.komas.remove(k);
		
		return k;
	}
	
	public Koma fetch(){
		// どれでもいいので、駒を取る
		Koma k = null;
		if(this.komas.size() > 0){
			k = this.komas.get(0);
		}
		
		if(k!=null) this.komas.remove(k);
		
		return k;
	}
	
	public void setKomaOn(Masu masuTo, int kindId, int order){
		Koma koma = null;
		if( (koma = this.fetch(kindId)) != null){
			koma.setOrder(order);
			koma.flip(kindId);
			if(masuTo != null){
				koma.bePlaced(masuTo);
			}
		}
	}
	
	public void setKomaOn(Stand stand, int kindId, int order){
		Koma koma = null;
		if( (koma = this.fetch(kindId)) != null){
			koma.setOrder(order);
			koma.flip(kindId);
			if(stand != null){
				koma.bePlaced(stand);
			}
		}
	}
	
	public void setAllKomasOn(Stand stand){
		Koma k = null;
		while( (k = this.fetch()) != null){
			k.bePlaced(stand);
		}
	}
	
	public String toString(){
		
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		for(Koma koma : this.komas){
			buff.reset();
			ps.printf("%s ", Koma.getKomaNameJP(koma.getKindId()));
			str += buff.toString();
		}
		return str;
	}
	
	

	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	
		
}