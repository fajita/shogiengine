package com.yeele.game.shogi.engine.evaluationitem;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.MoveEval;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.TreeOfMoveEval;
import com.yeele.game.shogi.engine.stratagy.suji.BackupSuji;
import com.yeele.game.shogi.engine.stratagy.suji.EscapeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.FireSuji;
import com.yeele.game.shogi.engine.stratagy.suji.OffenseIfProfitableSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PassSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PlaceIfProfitableSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PlaceIfSafeSuji;
import com.yeele.game.shogi.engine.stratagy.suji.PutBetweenBasicSuji;
import com.yeele.game.shogi.engine.stratagy.suji.TakeSuji;
import com.yeele.game.shogi.engine.util.Logger;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class EvaluationItemDynamicBattleOnMasuWithSuji extends EvaluationItem{
	
	public static String NAME= "EvaluationItemDynamicBattleOnMasu";
	private int debug_read_counter;
	
	private FireSuji fireSuji;
	private OffenseIfProfitableSuji offenseIfProfitableSuji;
	private TakeSuji takeSuji; 
	private EscapeSuji escapeSuji;
	private BackupSuji backupSuji;
	private PutBetweenBasicSuji putBetweenBasicSuji;
	private PlaceIfSafeSuji placeIfSafeSuji;
	private PlaceIfProfitableSuji placeIfProfitableSuji;
	private PassSuji passSuji;
	
	// max_depth_for_encompass is dynamically set and its 
	// value is variable during the execution time.
	// but, every time before the encompass is called, it is 
	// initialized by a certain value.
	private int initial_max_depth_for_encompass;
	private int initial_max_hands_for_reading;
	private int max_depth_for_encompass;
	private int max_hands_for_reading;
	
	public EvaluationItemDynamicBattleOnMasuWithSuji(Evaluator evaluator){
		super(evaluator, EvaluationItemDynamicBattleOnMasuWithSuji.NAME);
		
		Shogi shogi = this.evaluator.getShogi();
		this.fireSuji = new FireSuji(shogi);
		this.offenseIfProfitableSuji = new OffenseIfProfitableSuji(shogi);
		this.takeSuji = new TakeSuji(shogi);
		this.escapeSuji = new EscapeSuji(shogi);
		this.backupSuji = new BackupSuji(shogi);
		this.putBetweenBasicSuji = new PutBetweenBasicSuji(shogi);
		this.placeIfSafeSuji = new PlaceIfSafeSuji(shogi);
		this.placeIfProfitableSuji= new PlaceIfProfitableSuji(shogi);
		this.passSuji = new PassSuji(shogi);
		
		this.initial_max_depth_for_encompass = 5;
		this.initial_max_hands_for_reading = 20000;
		this.max_depth_for_encompass = this.initial_max_depth_for_encompass;
		this.max_hands_for_reading = this.initial_max_hands_for_reading;
	}

	/*
	TODO:
	1. check if the backing up 1-3. is working. since especially, when 23to.
	white possiblemoves are too much.
	2. set a limit on moves in evaluating one masu, for examples up to 2000 moves.
	*/
	
	public void evaluate(Evaluation eval, Masu masu, Evaluation curEval) {
		int nextOrder = eval.nextOrder;
		int curOrder = Shogi.getOtherOrder(nextOrder);
		// if one of following condition is true, skip evaluation for this masu.
		if(masu.koma != null){
			if(masu.countDirectlyReferredBy(Shogi.getOtherOrder(masu.koma.getOrder())) == 0) return;
			if(Shogi.getOtherOrder(masu.koma.getOrder()) != nextOrder) return;
		}else{
			int numCompanies = masu.countDirectlyReferredBy(curOrder);
			int numOpponents = masu.countDirectlyReferredBy(nextOrder);
			if( numCompanies == 0 || numOpponents == 0) return;
		}
		
		TreeOfMoveEval tree;
		ArrayListS<Masu> target = new ArrayListS<Masu>();
		Shogi shogi = this.evaluator.getShogi();
		Move curMove = shogi.kifu.getCurrentMove();
		int [] currentResult = new int[2];
		currentResult[Shogi.BLACK] = curEval.score[Shogi.BLACK] - curEval.score[Shogi.WHITE];
		currentResult[Shogi.WHITE] = curEval.score[Shogi.WHITE] - curEval.score[Shogi.BLACK];
		int [] initiators;
		
		if(masu.koma == null){
			initiators = new int[2];
			initiators[0] = eval.nextOrder;
			initiators[1] = Shogi.getOtherOrder(eval.nextOrder);
		}else{
			initiators = new int [1];
			initiators[0] = Shogi.getOtherOrder(masu.koma.getOrder());
			assert(initiators[0] == nextOrder);
		}

		for(int initiator : initiators){
			int follower = Shogi.getOtherOrder(initiator);
			
			// getting all possible hands pattern
			tree = new TreeOfMoveEval();
			tree.setFruit(new MoveEval(curMove, curEval));
			// this move is already played, so it's confirmed.
			tree.setConfirmed(true);
			target.add(masu);
			
			// initialize reference variables
			this.debug_read_counter = 0;
			this.max_depth_for_encompass = this.initial_max_depth_for_encompass;
			this.max_hands_for_reading = this.initial_max_hands_for_reading;
			this.encompassBattles(initiator, target, tree, false);
			
			if(tree.markNextBestMoves() == Shogi.SUCCESS){
				
			}
			MoveEval best = null;
			best = tree.getTipConfirmedFruit();
			Logger.debug("best move is so, [%s]\n", (best==null)?"null":best.move);
			Logger.debug("%d hands has been read\n", this.debug_read_counter);
			Logger.debug("Analysis result of square %d\n", masu.square);
			Logger.debug("%s\n", tree.toStringConfirmedPath());
			Logger.debug("full path\n%s\n", tree);
			Logger.debug("eval looks like \n%s\n", eval);

			if(best != null){
				int result = best.eval.score[initiator] - best.eval.score[follower];
				if(result > currentResult[initiator]){
					int diff = result - currentResult[initiator];
					// TBD
					// As of 2012/5/5, all battles comes to this if statement will be 
					// added. but, in the future, think only to add a few of best/
					// a fwe of worst senario to add.
					// replay the confirm and add points to each masu.
					
					this.pointEachMasuAlognWithConfirmedMoves(tree, eval);
					Logger.info("%d hands has been read\n", this.debug_read_counter);
					Logger.info("Analysis result of square %d\n", masu.square);
					Logger.info("%s\n", tree.toStringConfirmedPath());
					Logger.info("full path\n%s\n", tree);
					Logger.info("eval looks like \n%s\n", eval);
				}
			}
		}
		
	}
	
	public void pointEachMasuAlognWithConfirmedMoves(TreeOfMoveEval tree, Evaluation eval){
		int stage = this.evaluator.getStage();
		TreeOfMoveEval iter = tree;
		Move move = null;
		int points=0;
		do{
			iter = iter.getBranchThatHasConfirmedFruit();
			if(iter != null){
				move = iter.getFruit().move;
				points = 0;
				if(move.komaToCapture != Koma.UK){
					eval.plusToBoard( move.order
					        , Evaluator.komaValOnBoard[stage][move.komaToCapture]
					        , move.to
					        , Evaluator.CASE_EACHMASU);
				}
				if(move.flip == Koma.FLIP_YES){
					points += Evaluator.promotionVal[stage][move.komaToMove];
				}
				eval.plusToBoard( move.order
				        , points 
				        , move.to
				        , Evaluator.CASE_EACHMASU);
				// remainedKoma for the last move is not added.
			}
		}while(iter != null);
		
		
		
	}
	
	
	
	/*
	 * this function encompass all attacking moves which could be happened in the masu,
	 * and also any other masu which possibly invoked by this masu.
	 * eval is taken order, so evaluation.board, or score are accumulated properly.
	 */
	private void encompassBattles(int order, ArrayListS<Masu> target, TreeOfMoveEval tree, boolean stopFlag){
		
		// 1-1. acquire all moves of given order which can moves to the give target masu.
		// 1-2. add move which is moving to escape from being captured.
		// 2. move
		// 3. evaluate 
		// 4-1. check if any new masu which can be a target is produced
		// 4-2. add movable masu of the last moved koma, but only the move which can retrieve
		//      opponent or which can be promoted.
		// 5. call encompassBattles for the next order recursively.
		// 6. back
		
		Shogi shogi = this.evaluator.getShogi();
		int opponent = Shogi.getOtherOrder(order);
		ArrayListS<Move> possibleMoves = new ArrayListS<Move>();
		ArrayListS<Move> escapeMoves = new ArrayListS<Move>();
		ArrayListS<Move> offenseIfProfitableMoves = new ArrayListS<Move>();
		ArrayListS<Move> takeMoves = new ArrayListS<Move>();
		ArrayListS<Move> fireMoves = new ArrayListS<Move>();
		ArrayListS<Move> backupMoves = new ArrayListS<Move>();
		ArrayListS<Move> placeIfSafeMoves = new ArrayListS<Move>();
		ArrayListS<Move> placeIfProfitableMoves = new ArrayListS<Move>();
		ArrayListS<Move> putBetweenBasicMoves = new ArrayListS<Move>();
		ArrayListS<Move> passMoves = new ArrayListS<Move>();
		
		ArrayListS<Koma> relatedSnipers = new ArrayListS<Koma>();
		
		boolean firstHand = (tree.isBackbone())? true : false;
		for(Masu masu : target){
			Logger.debug("TARGET:%d\n", masu.square);
			if(firstHand){
				for(Move m : this.fireSuji.generateOn(order, masu)){
					fireMoves.addIfNot(m);
				}
			}
			
			for(Move m : this.takeSuji.generateOn(order, masu)){
				takeMoves.addIfNot(m);
			}
			
			for(Move m : this.offenseIfProfitableSuji.generateOn(order, masu)){
				offenseIfProfitableMoves.addIfNot(m);
			}
			
			if(masu.isKoma(order)){
				for(Move m : this.escapeSuji.generateBy(order, masu.koma)){
					escapeMoves.addIfNot(m);
				}
			}
			
			// 味方の駒がいて、かつタダで取られる　か
			// 空で、かつタダで成られる
			// 場合はバックアップする。
			if( (masu.isKoma(order) && masu.koma.canBeTakenForFree()) || 
				(masu.isEmpty() && masu.canBePromotedForFreeBy(opponent)) ){
				for(Move m : this.backupSuji.generateFor(order, masu)){
					backupMoves.addIfNot(m);
				}
			}
					
			//for(Move m : this.placeIfSafeSuji.generateOn(order, masu)){
			//	possibleMoves.addIfNot(m);
			//	placeIfSafeMoves.addIfNot(m);
			//}
			
			for(Move m : this.placeIfProfitableSuji.generateOn(order, masu)){
				placeIfProfitableMoves.addIfNot(m);
			}
		}
		
		for(Masu masu : target){
			for(Koma koma : masu.getDirectlyReferredBy(opponent)){
				if(koma.isThroughable()){
					relatedSnipers.addIfNot(koma);
				}
			}
		}
		for(Koma sniper : relatedSnipers){
			for(Move m : this.putBetweenBasicSuji.generateAgainst(order, sniper)){
				putBetweenBasicMoves.addIfNot(m);
			}
		}
		
		ArrayListS<ArrayListS<Move>> listOflist = new ArrayListS<ArrayListS<Move>>();
		
		listOflist.add(fireMoves);
		listOflist.add(escapeMoves);
		listOflist.add(offenseIfProfitableMoves);
		listOflist.add(backupMoves);
		listOflist.add(placeIfSafeMoves);
		listOflist.add(placeIfProfitableMoves);
		listOflist.add(putBetweenBasicMoves);
		
		// 本当は全部読みたいけど、リソースに限りがあるので、
		//　筋で生成した手から、さらに一筋N個に絞り込み
		int count_moves = 0;
		int num_suji = listOflist.size();

		for(ArrayListS<Move> moves : listOflist){
			count_moves += moves.size();
		}
		
		// 全体の生成具合を見て、いとつの筋にたいして、それだけ絞るかを動的に決める
		int num_filter_moves = 3;
		if(count_moves > 12 ) num_filter_moves = 12 / num_suji; // これで
		if(num_suji == 0) num_suji = 1;
		
		for(ArrayListS<Move> moves : listOflist){
			this.narrowdown(num_filter_moves, moves);
			for(Move m : moves){
				possibleMoves.addIfNot(m);
			}
		}
		
		// takeMovesは基本possibeに入れない。
		if(possibleMoves.size() == 0){
			this.narrowdown(3, takeMoves);
			for(Move m : takeMoves){
				possibleMoves.addIfNot(m);
			}
		}
		
		if(!tree.isFruitPassMove() && 
		   !tree.isBackbone() 
		   ){
			for(Move m : this.passSuji.generate(order)){
				possibleMoves.addIfNot(m);
				passMoves.addIfNot(m);
			}
		}
		
		Evaluation eval;
		MoveEval moveeval;
		TreeOfMoveEval branch;
		int debug_possible_moves_count = possibleMoves.size();
		// 2, 3, 4, 5, 6
				
		for(Move possibleMove : possibleMoves){
			
			shogi.move(possibleMove, true, false);
			this.debug_read_counter++; // increment read counter.
			
			if(!possibleMove.isPassMove()){
				eval = this.evaluator.evaluate(Evaluator.TYPE_STATIC_SHALLOW);
			}else{
				eval = tree.getFruit().eval;
			}
			
			moveeval = new MoveEval(possibleMove, eval);
			branch = new TreeOfMoveEval(moveeval);
			tree.addBranch(branch);
			
			Logger.debug("---------------------------\n");
			Logger.debug("%s\n", shogi);
			Logger.debug("%s\n", shogi.kifu);
			Logger.debug("depth = %d\n", branch.getDepth());
			Logger.debug("max depth = %d\n", this.max_depth_for_encompass);
			Logger.debug("%d/%d \n", branch.getIndex()+1, debug_possible_moves_count);
			Logger.debug("%d hands has been read\n", this.debug_read_counter);
			Logger.debug("%s is played\n", possibleMove);
			Logger.debug("---------------------------\n");
			
			if(possibleMove.from == 28 && 
					possibleMove.to == 24 &&
					possibleMove.komaToMove == Koma.HI ){
				int debugging = 2;
				int a = debugging;
			}
			
			target.clear();
			target.addAll(shogi.board.getInterfered());
			
			// decrease target 
			//ArrayListS<Masu> outdated = new ArrayListS<Masu>();
			//outdated.clear();
			//for(Masu t : target){
			//}
			
			Logger.debug("target is updated\n");
			for(Masu t : target) Logger.debug("%s\n", t); // debugging
			Logger.debug("\n");
			
			// 6. call encompassBattles for the next order recursively.
			// even if after PASS moving, keeping tracking until it hits 
			// the max_depth_for_encompass, or where it gets no possibleMoves.
			// the idea that tracing only a few moves after PASS is not enough
			// because in many cases, the passing turns good in a few moves, but
			// coming worse at furhter moves.
			// if OU is captured. stock searching
			
			// usually, it's okay to finish here since it reached to max_depth,
			// however, if case, the last move was a move that could pre-moved to 
			// get a consequence in a few moves later.
			int extended_hands = 0;
			if(branch.getDepth() == this.initial_max_depth_for_encompass){
				// the eariler the condition is, the more priority that condition has
				// some moves belongs to multi-moves, for example. a move belongs to
				// both offenseIfProfitable and BackupMoves. here it means is that 
				// in that case, offenseIfProfitable will be taken since it's top of this
				// if conditions.
				if(offenseIfProfitableMoves.contains(possibleMove)){
					extended_hands = 1; 
				}else if(placeIfSafeMoves.contains(possibleMove)){
					extended_hands = 2; 
				}else if(placeIfProfitableMoves.contains(possibleMove)){
					extended_hands = 2; 
				}else if(passMoves.contains(possibleMove)){
					extended_hands = 1; 
				}else if(backupMoves.contains(possibleMove)){
					extended_hands = 3; 
				}
				this.max_depth_for_encompass += extended_hands;
			}
			
			// extends max_depth if necessary
			if(this.debug_read_counter > initial_max_hands_for_reading){
				// no condition for reading as of 2012/6/5
			}
			
			if(
				possibleMove.komaToCapture == Koma.OU ||
				branch.getDepth() >= max_depth_for_encompass ||
				stopFlag == true ||
				this.debug_read_counter > max_hands_for_reading){
				
				if(this.debug_read_counter > max_hands_for_reading){
					Logger.warn("more than %d hands are read and cut the further reading\n", this.debug_read_counter);
				}
				
			}else{
				// 指した手によっては次の手で評価をやめます。じゃないと切りがないし。
				// 例えば、逃げた手のあと、次の手だけよみます。 
				if(escapeMoves.contains(possibleMove)==true){
					encompassBattles(Shogi.getOtherOrder(order), target, branch, true);
				}else{
					encompassBattles(Shogi.getOtherOrder(order), target, branch, false);
				}
				
			}
			
			shogi.back();
			Logger.debug("---------------------------\n");
			Logger.debug("  baaaaaaaaack\n", possibleMove);
			Logger.debug("---------------------------\n");
			// このpossibleMoveによって、延長されたmax_depthなので、元に戻す。
			this.max_depth_for_encompass -= extended_hands;
		}
		
	}

	// narrowdown the given moves to the given num of moves.
	public void narrowdown(int num, ArrayListS<Move> moves){
		if(moves.size() <= num) return;

		// use hash table hashmap<Move, integer> then sort with vlaue.
		HashMap<Move, Integer> scoreMap = new HashMap<Move, Integer>();
		
		for(Move m : moves){
			scoreMap.put(m, this.evaluator.evaluate(m));
		}
		// SORT 
		ArrayList entries = new ArrayList(scoreMap.entrySet());
		Collections.sort(entries, new Comparator() {
		    public int compare(Object obj1, Object obj2){
		        Map.Entry ent1 =(Map.Entry)obj1;
		        Map.Entry ent2 =(Map.Entry)obj2;
		        Integer val1 = (Integer) ent1.getValue();
		        Integer val2 = (Integer) ent2.getValue();
		        return val1.compareTo(val2);
		    }
		});
		
		// DELETE OUT OF THE RANK
		int nth = 1;
		for(Map.Entry<Move, Integer> entry : scoreMap.entrySet()){
			Logger.debug("%dth key=%s, val=%d\n", nth, entry.getKey(), entry.getValue());
			if(nth > num){
				moves.remove(entry.getKey());
			}
			nth++;
		}
	}

	/**********************************************************************
	               static methods
	**********************************************************************/
	

}
