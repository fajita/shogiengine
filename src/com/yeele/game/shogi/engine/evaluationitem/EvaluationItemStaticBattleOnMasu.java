package com.yeele.game.shogi.engine.evaluationitem;



import java.util.Collections;

import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;


public class EvaluationItemStaticBattleOnMasu extends EvaluationItem{
	
	public static String NAME= "EvaluationItemStaticBattleOnMasu";
	
	public EvaluationItemStaticBattleOnMasu(Evaluator evaluator){
		super(evaluator, EvaluationItemStaticBattleOnMasu.NAME);
	}

	
	public void evaluate(Evaluation eval, Masu masu) {

		int stage = this.evaluator.getStage();
		ArrayListS<Koma> [] soldier = new ArrayListS[2];
		for(int i=0; i < soldier.length; i++){
			soldier[i] = new ArrayListS<Koma> ();
		}
		soldier[Shogi.BLACK].clear();
		soldier[Shogi.WHITE].clear();
		
		// filling soldier by order for easy calculation.
		for(Koma km : masu.directRefered){
			soldier[km.getOrder()].add(km);
			for(Koma sniper : km.getWallFor(km.getOrder())){
				if(masu.indirectRefered.contains(sniper)){
					soldier[km.getOrder()].add(sniper);
				}
			}
		}
		
		// sort soldier by koma value. smaller the koma value is 
		// the koma comes eariler in the list.
		Collections.sort(soldier[Shogi.BLACK], Koma.comparatorDesc);
		Collections.sort(soldier[Shogi.WHITE], Koma.comparatorDesc);
		
		int score = 0;
		int next_order = eval.nextOrder;
		int cur_order = Shogi.getOtherOrder(next_order);
		int bigger_sz = 0;
		int attackingKomaVal = 0;
		int preKomaVal = 0;
		Koma attackingKoma = null;
		Koma remainedKoma = null;
		Koma firstRemainedKoma = null;
		int [] total = new int[2];
		total[Shogi.BLACK] = 0;
		total[Shogi.WHITE] = 0;
		boolean brk = false;
		
		bigger_sz = (soldier[Shogi.BLACK].size() >  soldier[Shogi.WHITE].size()) ? soldier[Shogi.BLACK].size() : soldier[Shogi.WHITE].size(); 
		
		int [] initiatorOrder;
		if(masu.koma == null){
			initiatorOrder = new int[2];
			// next order has priority to evaluate, if it's better, next order
			// would do that move.
			initiatorOrder[0] = eval.nextOrder;
			initiatorOrder[1] = Shogi.getOtherOrder(eval.nextOrder);
		}else{
			// if there's koma, ONLY opponent can initiate.
			initiatorOrder = new int[1];
			initiatorOrder[0] = Shogi.getOtherOrder(masu.koma.getOrder());
			if(Shogi.getOtherOrder(masu.koma.getOrder()) == eval.nextOrder){
				firstRemainedKoma = masu.koma;
			}else{
				// next turn is not yours, suppose you cannot get the koma on the masu.
				// because probably, the koma on the masu will move in next hand.
				firstRemainedKoma = null;
			}
		}
		
		for(int initiator : initiatorOrder){
			int follower = Shogi.getOtherOrder(initiator);
			// initialize 
			brk = false;
			total[initiator] = 0;
			total[follower] = 0;
			remainedKoma = firstRemainedKoma;
			if(remainedKoma == null){
				preKomaVal = 0;
			}else{
				preKomaVal = Evaluator.komaValOnBoard[stage][remainedKoma.getKindId()];
			}
			attackingKomaVal = 0;
			attackingKoma = null;
			
			int [] bw = {initiator, follower};
			for(int j=0; j < bigger_sz; j++){

				for(int order : bw){
					try{
						attackingKoma = soldier[order].get(j);
						attackingKomaVal = Evaluator.komaValOnBoard[stage][attackingKoma.getKindId()];
						// if no exception occurs, that means order is attacking to this masu.
						// so, whatever the koma previously stays at this koma, will be taken by 
						// the order. so
						total[order] += preKomaVal;
						preKomaVal = attackingKomaVal;
						remainedKoma = attackingKoma;
					}catch(IndexOutOfBoundsException e){
						//Logger.debug("%s doen't have any more piece attacking %d!\n"
						//		, Shogi.getOrderNameCasual(order), masu.square);
						brk = true;
						break;
					}
				}
				if(brk) break;
			}
			
			// make sure to evaluate the remained koma.
			if(remainedKoma != null){
				// if it can promote, add additional.
				if(  remainedKoma.canBePromotedOn(masu)){
					total[remainedKoma.getOrder()] += Evaluator.promotionVal[stage][remainedKoma.getKindId()];
				}
				
				// opponent is not there, no value to be able to move to that masu.
				// Although, it's worth if you can promote there, so above process comes first.
				if(soldier[Shogi.getOtherOrder(remainedKoma.getOrder())].size() == 0){
					
				}else{
					total[remainedKoma.getOrder()] += Evaluator.remainedKomaValOnBattleMasu[stage][remainedKoma.getKindId()];
				}
				
			}
			
			// initiator - follower
			score = total[initiator] - total[follower];
			if(score > 0){
				// initiator is wining the battle on this masu.
				// if socre is 0, both have 0.
				
				eval.plusToBoard(initiator, score, masu.square, Evaluator.CASE_EACHMASU);
				eval.plusToBoard(follower ,     0, masu.square, Evaluator.CASE_EACHMASU);
				
				break;
			}
		}
	}


	
	
	/**********************************************************************
	               static methods
	**********************************************************************/
	

}



/*
// IDEA: 2012-01-10
// suppose battle is initiated by nextOrder.
// which could happen if opponent wishes so.
// if opponent is winning, it shall be accepted.
// if opponent is losing, he wouldn't fire the battle,
// so check if you initiate the battle to see how it turns out.
// if you are winning, it shall be accepted.
// if you are losing, supposed that oppenent must have been winning
// in the previous check.

// IDEA: 2012-01-13
// initiate the battle from the order which has bigger number of pieces
// refering to the battle masu.
// then 
// substruct the total value of pieces of followed order from the initiated order.
// e.g) if BLACK initiated, total[BLACK] - total[WHITE].
// remember to add the value of koma which is lastly on the battle masu.
// this value should be get from RemainedBattleMasuVal.
// then, if the result is positive, put that result number to initiated order and
// put zero, 0 to followed order. 
// if the result is negative, put that absolute number of the result number to 
// followed order, then put zero, 0 to initiated order.
// One more thing, if the remained koma can be promoted, add additinal point to it.

// IDEA: 2012-01-13
// if initiator is wining, take that case. 
// that's it make it simple. if no initoator is winning, make it draw.
 */
