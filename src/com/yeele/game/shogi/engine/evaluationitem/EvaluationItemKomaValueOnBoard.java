package com.yeele.game.shogi.engine.evaluationitem;



import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;


public class EvaluationItemKomaValueOnBoard extends EvaluationItem{
	
	public static String NAME= "EvaluationItemKomaValueOnBoard";
	
	public EvaluationItemKomaValueOnBoard(Evaluator evaluator){
		super(evaluator, EvaluationItemKomaValueOnBoard.NAME);
	}

	
	public void evaluate(Evaluation eval, Koma koma) {
		int points;
		int stage = this.evaluator.getStage();
		if( koma != null){
			points = Evaluator.komaValOnBoard[stage][koma.getKindId()];
			eval.plus(koma.getOrder(), points, Evaluator.CASE_ONBOARD);
		}
	}

	
	/**********************************************************************
	               static methods
	**********************************************************************/
	

}


