package com.yeele.game.shogi.engine.evaluationitem;


import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Masu;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.MoveEval;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.TreeOfMoveEval;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class EvaluationItemDynamicBattleOnMasu extends EvaluationItem{
	
	public static String NAME= "EvaluationItemDynamicBattleOnMasu";
	
	public int read_counter; 
	
	public EvaluationItemDynamicBattleOnMasu(Evaluator evaluator){
		super(evaluator, EvaluationItemDynamicBattleOnMasu.NAME);
	}

	/*
	TODO:
	1. check if the backing up 1-3. is working. since especially, when 23to.
	white possiblemoves are too much.
	2. set a limit on moves in evaluating one masu, for examples up to 2000 moves.
	*/
	
	public void evaluate(Evaluation eval, Masu masu, Evaluation curEval) {
		int nextOrder = eval.nextOrder;
		int curOrder = Shogi.getOtherOrder(nextOrder);
		// if one of following condition is true, skip evaluation for this masu.
		if(masu.koma != null){
			if(masu.countDirectlyReferredBy(Shogi.getOtherOrder(masu.koma.getOrder())) == 0) return;
			if(Shogi.getOtherOrder(masu.koma.getOrder()) != nextOrder) return;
		}else{
			int numCompanies = masu.countDirectlyReferredBy(curOrder);
			int numOpponents = masu.countDirectlyReferredBy(nextOrder);
			if( numCompanies == 0 || numOpponents == 0) return;
		}
		
		TreeOfMoveEval tree;
		ArrayListS<Masu> target = new ArrayListS<Masu>();
		Shogi shogi = this.evaluator.getShogi();
		Move curMove = shogi.kifu.getCurrentMove();
		int [] currentResult = new int[2];
		currentResult[Shogi.BLACK] = curEval.score[Shogi.BLACK] - curEval.score[Shogi.WHITE];
		currentResult[Shogi.WHITE] = curEval.score[Shogi.WHITE] - curEval.score[Shogi.BLACK];
		int [] initiators;
		
		if(masu.koma == null){
			initiators = new int[2];
			initiators[0] = eval.nextOrder;
			initiators[1] = Shogi.getOtherOrder(eval.nextOrder);
		}else{
			initiators = new int [1];
			initiators[0] = Shogi.getOtherOrder(masu.koma.getOrder());
			assert(initiators[0] == nextOrder);
		}

		for(int initiator : initiators){
			int follower = Shogi.getOtherOrder(initiator);
			
			// getting all possible hands pattern
			tree = new TreeOfMoveEval();
			tree.setFruit(new MoveEval(curMove, curEval));
			// this move is already played, so it's confirmed.
			tree.setConfirmed(true);
			target.addIfNot(masu);
			
			this.read_counter = 0;
			this.encompassBattles(initiator, target, tree, false);
			
			if(tree.markNextBestMoves() == Shogi.SUCCESS){
				
			}
			MoveEval best = null;
			best = tree.getTipConfirmedFruit();
			Logger.debug("best move is so, [%s]\n", (best==null)?"null":best.move);
			
			if(best != null){
				int result = best.eval.score[initiator] - best.eval.score[follower];
				if(result > currentResult[initiator]){
					int diff = result - currentResult[initiator];
					// TBD
					// As of 2012/5/5, all battles comes to this if statement will be 
					// added. but, in the future, think only to add a few of best/
					// a fwe of worst senario to add.
					// replay the confirm and add points to each masu.
					
					this.pointEachMasuAlognWithConfirmedMoves(tree, eval);
					Logger.info("%d hands has been read\n", this.read_counter);
					Logger.info("Analysis result of square %d\n", masu.square);
					Logger.info("%s\n", tree.toStringConfirmedPath());
					Logger.info("full path\n%s\n", tree);
					Logger.info("eval looks like \n%s\n", eval);
				}
			}
		}
		
	}
	
	public void pointEachMasuAlognWithConfirmedMoves(TreeOfMoveEval tree, Evaluation eval){
		int stage = this.evaluator.getStage();
		TreeOfMoveEval iter = tree;
		Move move = null;
		int points=0;
		do{
			iter = iter.getBranchThatHasConfirmedFruit();
			if(iter != null){
				move = iter.getFruit().move;
				points = 0;
				if(move.komaToCapture != Koma.UK){
					eval.plusToBoard( move.order
					        , Evaluator.komaValOnBoard[stage][move.komaToCapture]
					        , move.to
					        , Evaluator.CASE_EACHMASU);
				}
				if(move.flip == Koma.FLIP_YES){
					points += Evaluator.promotionVal[stage][move.komaToMove];
				}
				eval.plusToBoard( move.order
				        , points 
				        , move.to
				        , Evaluator.CASE_EACHMASU);
				// remainedKoma for the last move is not added.
			}
		}while(iter != null);
		
		
		
	}
	
	
	
	/*
	 * this function encompass all attacking moves which could be happened in the masu,
	 * and also any other masu which possibly invoked by this masu.
	 * eval is taken order, so evaluation.board, or score are accumulated properly.
	 */
	private void encompassBattles(int order, ArrayListS<Masu> target, TreeOfMoveEval tree, boolean stopFlag){
		
		// 1-1. acquire all moves of given order which can moves to the give target masu.
		// 1-2. add move which is moving to escape from being captured.
		// 2. move
		// 3. evaluate 
		// 4-1. check if any new masu which can be a target is produced
		// 4-2. add movable masu of the last moved koma, but only the move which can retrieve
		//      opponent or which can be promoted.
		// 5. call encompassBattles for the next order recursively.
		// 6. back
		
		Shogi shogi = this.evaluator.getShogi();
		int opponent = Shogi.getOtherOrder(order);
		ArrayListS<Move> possibleMoves = new ArrayListS<Move>();
		Move move = null;

		// 1-1. acquire all moves of given order which can moves to the give target masu.
		ArrayListS<Move> offenseMoves = new ArrayListS<Move>();
		ArrayListS<Move> tempMoves = null;
		for(Masu aimedMasu : target){
			for(Koma pointingKoma : aimedMasu.directRefered){
				if(pointingKoma.getOrder() == order){
					if(aimedMasu.isKoma(order) == false){ // 味方の駒はいない。　空升か相手がいある升であることを保証

						if(tree.isBackbone() == true){ // ただし、1手目は空升に動ける。
							tempMoves = pointingKoma.getMovesCanGoTo(aimedMasu);
							for(Move m : tempMoves){
								if(possibleMoves.addIfNot(m)){
									offenseMoves.addIfNot(m);
								}
							}
						}
						// ただし、タダで。又は優勢地点で
						boolean profitable = false;
						if( aimedMasu.countDirectlyReferredBy(opponent)<aimedMasu.countDirectlyReferredBy(order) ||
							aimedMasu.countDirectlyReferredBy(opponent)<
							aimedMasu.countDirectlyReferredBy(order)+aimedMasu.countIndirectlyReferredBy(order)	){
							profitable = true;
						}
						// その駒自体が、強い駒暴れ馬を収める。aimedMasuにある駒を取らなければ、次のてで大切な駒を取られる。
						boolean betterCutRoot = false; // 出るくいはうつ
						if( aimedMasu.isKoma(opponent)){
							int maxValue = 0;
							if(this.evaluator.getKomaValue(aimedMasu.koma) >= 
									this.evaluator.getKomaValueUnpromoted(pointingKoma)){
								// 自分より駒価値が高いなら、取る価値あり、
								betterCutRoot = true;
							}else{
								// そうでなくても、その駒に利きが利いている駒価値が高いなら、取ってしまおう。
								for(Koma toreruKoma : aimedMasu.koma.getReachableKoma(order)){
									int komaVal = this.evaluator.getKomaValue(toreruKoma);
									if(komaVal > maxValue) maxValue = komaVal;
								}
								if(maxValue > this.evaluator.getKomaValue(pointingKoma)){
									betterCutRoot = true;
								}
							}
						}
						
						if(aimedMasu.isKoma() == false){
							if(pointingKoma.canBePromotedOn(aimedMasu)){ // 成れるなら
								if(profitable){ 
									tempMoves = pointingKoma.getMovesCanGoTo(aimedMasu);
									for(Move m : tempMoves){
										if(possibleMoves.addIfNot(m)) {
											offenseMoves.addIfNot(m);
										}
									}
								}
							}
						}else if(aimedMasu.isKoma(opponent)){
							if(profitable || betterCutRoot){
								tempMoves = pointingKoma.getMovesCanGoTo(aimedMasu);
								for(Move m : tempMoves){
									if(possibleMoves.addIfNot(m)){
										offenseMoves.addIfNot(m);
									}
								}
							}
						}
					}
				}
			}
		}
		Logger.debug("1-1. acquire all moves of given order which can moves to the give target masu\n");
		for(Move m : offenseMoves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		
		// 1-2. add move which is moving to escape from being captured.
		ArrayListS<Move> escapeMoves = new ArrayListS<Move>();
		for(Masu aimedMasu : target){
			if(aimedMasu.isKoma(opponent)){ // ターゲットに相手の駒がいること
				// targettedMasu.koma is opponent
				for(Masu oppoMovableMasu : aimedMasu.koma.movable){
					if(oppoMovableMasu.isKoma(order)){
						// ターゲットの升にいる相手が動ける升に味方がいる場合
						// movableMasu.koma is companion
						for(Masu escapableMasu : oppoMovableMasu.koma.movable){
							if(escapableMasu.isDirectlyReferredBy(opponent)==false){ // その升が安全に逃げれるなら
								tempMoves = oppoMovableMasu.koma.getMovesCanGoTo(escapableMasu);
								for(Move m : tempMoves){
									if(possibleMoves.addIfNot(m)){
										escapeMoves.addIfNot(m);
									}
								}
							}
						}
					}
				}
			}
		}
		Logger.debug("1-2. add move which is moving to escape from being captured.\n");
		for(Move m : escapeMoves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");

		
		// 1-3. 狙われている升、または駒に利きをつける手, // 1-4. 駒を打つ手
		// 1-1で手が作られてない場合は、適当にtargetにきいをつける手もありかも TBD
		// TODO 駒を打つ場合の打ち方は、かなり多い。絞り込む条件を考えよ
		// そもそも、1-3自体をなくすしても、問題ないか？
		ArrayListS<Move> prepareMoves = new ArrayListS<Move>();
		for(Masu aimedMasu : target){
			// 利きの数が負けているなら、数をたす手 
			if(aimedMasu.countDirectlyReferredBy(opponent) > aimedMasu.countDirectlyReferredBy(order)){ 
				// そこに利きをつくる手を追加
				// shogi.board.getKomaCanBackup(aimedMasu, order)
				// orderの手でaimedMasuに利きをつけることができるmoveのlistを取得
				for(Move m : shogi.getPrecedingMovesFor(aimedMasu, order)){
					if(possibleMoves.addIfNot(m)){
						prepareMoves.addIfNot(m);
					}
				}
			}
		}
		Logger.debug("1-3. backup the threating masu\n");
		for(Move m : prepareMoves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		
				
		// 1-4. sniperに直接狙われていたら歩合する
		ArrayListS<Move> brokerMoves = new ArrayListS<Move>();
		for(Masu aimedMasu : target){
			if(aimedMasu.isKoma(order)){
				for(Koma threat : aimedMasu.koma.getDirectlyReferredByOpponent()){
					if(threat.isThroughable()){
						if(shogi.stand[order].getCount(Koma.FU)>0){
							Koma btw = shogi.stand[order].get(Koma.FU);
							for(Move m : btw.getMoves()){
								Masu ms = shogi.board.getMasu(m.to);
								if(threat.getMovable().contains(ms)){
									if(Board.getDistance(aimedMasu.square, m.to)==1){
										if(possibleMoves.addIfNot(m)){
											brokerMoves.addIfNot(m);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		Logger.debug("1-4. place fu for direct throuable scope.\n");
		for(Move m : brokerMoves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		// 1-5. 駒を攻撃ポイントに打つ
		ArrayListS<Move> dropMoves = new ArrayListS<Move>();
		for(Masu aimedMasu : target){
			// 利きの数が負けているなら、数をたす手 
			if(aimedMasu.countDirectlyReferredBy(order) > 0){ 
				// 少なくとも、誰かがバックアップしてくれること。（しかしタダ捨ても検索した場合はこの条件をはずす。
				for(Move m : shogi.stand[order].getPrecedingMovesFor(aimedMasu)){
					if(shogi.board.getMasu(m.to).isDirectlyReferredBy(opponent)==false){
						// 相手の利きがないところに打つこと。（これも上級者では気にしたくない。 TODO
						if(possibleMoves.addIfNot(m)){
							dropMoves.addIfNot(m);
						}
					}
					
				}
			}
		}
		Logger.debug("1-5. place koma for attack\n");
		for(Move m : dropMoves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		
		// 1-6. pass move
		ArrayListS<Move> passMoves = new ArrayListS<Move>();
		if(!tree.isFruitPassMove() && 
		   !tree.isBackbone() &&
		   possibleMoves.size() > 0 ){
			move = new Move(order, 0, 0, Koma.UK);
			if(possibleMoves.addIfNot(move)){
				passMoves.addIfNot(move);
			}
		}
		Logger.debug("1-6. pass move?\n");
		for(Move m : passMoves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
		
		Logger.debug("1. so, here is conclusion\n");
		for(Move m : possibleMoves) Logger.debug("%s, ", m); // debugging
		Logger.debug("\n");
				
		Evaluation eval;
		MoveEval moveeval;
		TreeOfMoveEval branch;
		ArrayListS<Koma> protectingKomas_before = new ArrayListS<Koma>();
		ArrayListS<Koma> protectingKomas_after = new ArrayListS<Koma>();
		ArrayListS<Masu> newlyAddedTarget = new ArrayListS<Masu>();
		ArrayListS<Masu> outdatedMasu = new ArrayListS<Masu>();
		ArrayListS<Koma> openedSnipers = new ArrayListS<Koma>();
		// 2, 3, 4, 5, 6
		Logger.debug("encompass battles generate %d possible %s moves at depth %d\n"
				, possibleMoves.size(), Shogi.getOrderName(order), tree.getDepth());
		
		
		for(Move possibleMove : possibleMoves){
			
			protectingKomas_before.clear();
			Koma movingKoma = shogi.board.getKoma(possibleMove.from);
			if(movingKoma!=null){
				protectingKomas_before = movingKoma.getProtectingKomas();
			}
			openedSnipers.clear();
			if(movingKoma!=null){
				Masu masuTo = shogi.board.getMasu(possibleMove.to);
				for(Koma sniper : movingKoma.getOpenedSnipersByMovingTo(masuTo, opponent)){
					openedSnipers.addIfNot(sniper);
				}
			}
			shogi.move(possibleMove, true, false);
			this.read_counter++; // increment read counter.
			
			Logger.debug("---------------------------\n");
			Logger.debug("%s\n", shogi.kifu);
			Logger.debug("depth = %d\n", tree.getDepth());
			Logger.debug("%d hands has been read\n", this.read_counter);
			Logger.debug("%s is played\n", possibleMove);
			Logger.debug("---------------------------\n");
			
			if(!possibleMove.isPassMove()){
				eval = this.evaluator.evaluate(Evaluator.TYPE_STATIC_SHALLOW);
			}else{
				eval = tree.trunk.getFruit().eval;
			}
			
			moveeval = new MoveEval(possibleMove, eval);
			branch = new TreeOfMoveEval(moveeval);
			tree.addBranch(branch);
			
			if(possibleMove.from == 41 && 
					possibleMove.to == 32 ){
				int debugging = 2;
			}
			
			Logger.debug("4. before updating target\n");
			for(Masu t : target) Logger.debug("%s\n", t); // debugging
			Logger.debug("\n");
			
			if(possibleMove.isPassMove() == false){
				// 4-0. delete target if the masu is no longer point of battle.
				outdatedMasu.clear();
				for(Masu targetMasu : target){
					if(targetMasu.isKoma()==false){
						if(targetMasu.countDirectlyReferredBy(order) > 0 &&
						   targetMasu.countDirectlyReferredBy(opponent) > 0){
							// ターゲット上に駒がない場合は、両者の参照があること！なければ削除
						}else{
							outdatedMasu.addIfNot(targetMasu);
						}
					}else{
						if(targetMasu.countDirectlyReferredBy(
								Shogi.getOtherOrder(targetMasu.koma.getOrder())
								) > 0){
							// ターゲット上にある駒に対して、敵の参照があればＯＫ。
						}else{
							// もし、次の手で敵に歩で叩かれるような升なら残す。
							boolean ableToPlaceFu = false; // 歩で叩くことができる TODO　歩以外の駒でも
							Koma fu = shogi.stand[order].get(Koma.FU);
							if(fu != null){
								if(targetMasu.isKoma(opponent)){ // 敵がいて
									for(Move m : fu.getPrecedingMovesFor(targetMasu)){
										ableToPlaceFu = true; //駒が置ける
									}
								}
							}
							fu = shogi.stand[opponent].get(Koma.FU);
							if(fu != null){
								if(targetMasu.isKoma(order)){ // 敵がいて
									for(Move m : fu.getPrecedingMovesFor(targetMasu)){
										ableToPlaceFu = true; //駒が置ける
									}
								}
							}
							if(ableToPlaceFu){
								
							}else{
								outdatedMasu.addIfNot(targetMasu);
							}
						}
					}
				}
				
				for(Masu outdate : outdatedMasu){
					if(target.contains(outdate)){
						target.remove(outdate);
					}
				}
				
				Logger.debug("4-0. delete target if the masu is no longer point of battle.\n");
				for(Masu t : target) Logger.debug("%s\n", t); // debugging
				Logger.debug("\n");
						
				// 4-1 check if any new masu which can be a target is produced
				ArrayListS<Masu> abandonMasu = new ArrayListS<Masu>();
				Koma movedKoma = shogi.board.getKoma(possibleMove.to);
				protectingKomas_after.clear();
				if(movedKoma != null) protectingKomas_after = movedKoma.getProtectingKomas();
				for(Koma protectedKoma : protectingKomas_before){
					if(! protectingKomas_after.contains(protectedKoma) ){
						Koma abandonedKoma = protectedKoma;
						
						boolean profitable = false; // profitable for opponent?
						if( 	abandonedKoma.countDirectlyReferredBy(opponent) >
								abandonedKoma.countDirectlyReferredBy(order)){
								profitable = true;
						}
						boolean isWorthToExchange = false;
						for(Koma opponentKoma : abandonedKoma.getDirectlyReferredByOpponent()){
							if(this.evaluator.getKomaValue(abandonedKoma) >= 
							this.evaluator.getKomaValueUnpromoted(opponentKoma)){ //　たとえば、成歩と銀を交換するのを成立させるため
								isWorthToExchange = true;
							}
						}
						if(isWorthToExchange || profitable){
							if(target.addIfNot(abandonedKoma.masu)){ // targetとして新しい升であれば
								newlyAddedTarget.addIfNot(abandonedKoma.masu);
								abandonMasu.addIfNot(abandonedKoma.masu);
							}
						}
					}
				}
				
				Logger.debug("4-1 check if any new masu which can be a target is produced\n");
				for(Masu t : abandonMasu) Logger.debug("%s\n", t); // debugging
				Logger.debug("\n");
				
				// 4-2 add movable masu of the last moved koma, but only the move which can retrieve
				//      opponent or which can be promoted.
				ArrayListS<Masu> lastMoveInspiredMasu = new ArrayListS<Masu>();
				Koma lastMovedKoma = shogi.board.getKoma(possibleMove.to);
				if(lastMovedKoma!=null){
					if(lastMovedKoma.masu.isDirectlyReferredBy(opponent)){
						if(target.addIfNot(lastMovedKoma.masu)){
							newlyAddedTarget.addIfNot(lastMovedKoma.masu);
							lastMoveInspiredMasu.addIfNot(lastMovedKoma.masu);
						}
					}
					
					for(Masu masuCanBeMoved : lastMovedKoma.movable){
						if(masuCanBeMoved.isKoma(order)==false){ // 味方はいない升
							boolean profitable = false;
							if( 	masuCanBeMoved.countDirectlyReferredBy(opponent) <
									masuCanBeMoved.countDirectlyReferredBy(order)+masuCanBeMoved.countIndirectlyReferredBy(order)){
									profitable = true;
							}
							boolean isWorthToExchange = false;
							if(masuCanBeMoved.isKoma(opponent) && 
									this.evaluator.getKomaValue(masuCanBeMoved.koma)>=this.evaluator.getKomaValueUnpromoted(lastMovedKoma)){
								isWorthToExchange = true;
							}
							boolean ableToPlaceFu = false; // 歩で叩くことができる TODO　歩以外の駒でも
							Koma fu = shogi.stand[order].get(Koma.FU); // orderは今動かした方(e.g + 2824HI
							if(fu != null){
								if(masuCanBeMoved.isKoma(opponent)){ // 味方がいて、敵の歩が置けたら、
									for(Move m : fu.getPrecedingMovesFor(masuCanBeMoved)){
										ableToPlaceFu = true; //駒が置ける
									}
								}
								
							}
							
							if( (isWorthToExchange) || // 取れる駒価値の方が高い
								(ableToPlaceFu) ||	
								(lastMovedKoma.canBePromotedOn(masuCanBeMoved) && (profitable) ) || // タダでなれる
							    (masuCanBeMoved.koma!=null && (masuCanBeMoved.koma.getOrder()==opponent) && (profitable)) // 取れる
							){
								if(target.addIfNot(masuCanBeMoved)){
									newlyAddedTarget.addIfNot(masuCanBeMoved);
									lastMoveInspiredMasu.addIfNot(lastMovedKoma.masu);
								}
							}
						}
					}
				}
				Logger.debug("4-2 add movable masu of the last moved koma, plus himself.\n");
				for(Masu t : lastMoveInspiredMasu) Logger.debug("%s\n", t); // debugging
				Logger.debug("\n");
						
				// 4-3. if the move opened sniper path and sniper has direct aim.
				ArrayListS<Masu> openedSniperdMasu = new ArrayListS<Masu>();
				for(Koma sniper : openedSnipers){
					for(Koma directKoma : sniper.getReachableKoma(order)){
						// directKoma.masu could be new target
						boolean profitable = false;
						if( 	directKoma.masu.countDirectlyReferredBy(opponent) <
								directKoma.masu.countDirectlyReferredBy(order)+directKoma.masu.countIndirectlyReferredBy(order)){
								profitable = true;
						}
						boolean isWorthToExchange = false;
						if(directKoma.masu.isKoma(order) && 
								this.evaluator.getKomaValue(directKoma.masu.koma)>=this.evaluator.getKomaValueUnpromoted(sniper)){
							isWorthToExchange = true;
						}
						if(profitable || isWorthToExchange){
							if(target.addIfNot(directKoma.masu)){
								newlyAddedTarget.addIfNot(directKoma.masu);
								openedSniperdMasu.addIfNot(directKoma.masu);
							}
						}
					}
				}
				
				Logger.debug("4-3. if the move opened sniper path and sniper has direct aim..\n");
				for(Masu t : openedSniperdMasu) Logger.debug("%s\n", t); // debugging
				Logger.debug("\n");
				
			}
			
					
			Logger.debug("4. total\n");
			for(Masu t : target) Logger.debug("%s\n", t); // debugging
			Logger.debug("\n");
			
			// 6. call encompassBattles for the next order recursively.
			// even if after PASS moving, keeping tracking until it hits 
			// the max_depth_for_encompass, or where it gets no possibleMoves.
			// the idea that tracing only a few moves after PASS is not enough
			// because in many cases, the passing turns good in a few moves, but
			// coming worse at furhter moves.
			// if OU is captured. stock searching
			int max_depth_for_encompass = 7;
			int max_hands_for_reading = 3000;
			if(
				possibleMove.komaToCapture == Koma.OU ||
				branch.getDepth() >= max_depth_for_encompass ||
				stopFlag == true ||
				this.read_counter > max_hands_for_reading){
				
				if(this.read_counter > max_hands_for_reading){
					Logger.warn("more than %d hands are read and cut the further reading\n", this.read_counter);
				}
				
			}else{
				// 指した手によっては次の手で評価をやめます。じゃないと切りがないし。
				// 例えば、逃げた手のあと、次の手だけよみます。 
				if(escapeMoves.contains(possibleMove)==true){
					encompassBattles(Shogi.getOtherOrder(order), target, branch, true);
				}else{
					encompassBattles(Shogi.getOtherOrder(order), target, branch, false);
				}
				
			}
			
			shogi.back();
			Logger.debug("---------------------------\n");
			Logger.debug("  baaaaaaaaack\n", possibleMove);
			Logger.debug("---------------------------\n");
			// backの対象となっている動きによって追加されたtargetなので、backする際にはそれらも元に戻す
			for(Masu newTarget : newlyAddedTarget){
				if(target.contains(newTarget)){
					target.remove(newTarget);
				}
			}
			for(Masu deletedMasu : outdatedMasu){
				target.addIfNot(deletedMasu);
			}
		}
		
	}


	/**********************************************************************
	               static methods
	**********************************************************************/
	

}
