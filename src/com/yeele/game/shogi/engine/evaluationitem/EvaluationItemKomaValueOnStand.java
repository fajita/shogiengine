package com.yeele.game.shogi.engine.evaluationitem;



import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Shogi;


public class EvaluationItemKomaValueOnStand extends EvaluationItem{
	
	public static String NAME= "EvaluationItemKomaValueOnStand";
	
	public EvaluationItemKomaValueOnStand(Evaluator evaluator){
		super(evaluator, EvaluationItemKomaValueOnStand.NAME);
	}

	
	public void evaluate(Evaluation eval) {
		int points;
		int stage = this.evaluator.getStage();
		Shogi shogi = this.evaluator.getShogi();
		int [] shogi_order = {Shogi.BLACK, Shogi.WHITE};
		
		for(int order : shogi_order){
			for(int i= Koma.FU; i <= Koma.OU; i++){
				
				points = (Evaluator.komaValOnStand[stage][i] * (shogi.stand[order].getStack(i).size()));
				eval.plus(order, points, Evaluator.CASE_ONSTAND);
				
			}
		}
	}

	
	/**********************************************************************
	               static methods
	**********************************************************************/
	

}


