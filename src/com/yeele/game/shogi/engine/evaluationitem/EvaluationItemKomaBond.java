package com.yeele.game.shogi.engine.evaluationitem;



import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Shogi;


public class EvaluationItemKomaBond extends EvaluationItem{
	
	public static String NAME= "EvaluationItemKomaBond";
	
	public EvaluationItemKomaBond(Evaluator evaluator){
		super(evaluator, EvaluationItemKomaBond.NAME);
	}

	
	public void evaluate(Evaluation eval, Koma koma) {
		int points = 0;
		int stage = this.evaluator.getStage();
		Shogi shogi = this.evaluator.getShogi();
		int opponent = Shogi.getOtherOrder(koma.getOrder());
		
		if(koma.countDirectlyReferred(opponent) > koma.countDirectlyReferred(koma.getOrder())){
			points = -1;
			eval.plus(koma.getOrder(), points, Evaluator.CASE_KOMA_BOND);
		}
		
		
		
	}

	
	/**********************************************************************
	               static methods
	**********************************************************************/
	

}


