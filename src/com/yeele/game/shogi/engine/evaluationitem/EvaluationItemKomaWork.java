package com.yeele.game.shogi.engine.evaluationitem;


import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Shogi;


public class EvaluationItemKomaWork extends EvaluationItem{
	
	public static String NAME= "EvaluationItemKomaWork";
	
	public EvaluationItemKomaWork(Evaluator evaluator){
		super(evaluator, EvaluationItemKomaWork.NAME);
	}

	
	public void evaluate(Evaluation eval, Koma koma) {

		if(koma == null) return;
		
		int stage = this.evaluator.getStage();
		Shogi shogi = this.evaluator.getShogi();
		
		if(koma.canBePromotedInNextHandForFree() ||
		   koma.canTakeOpponentInNextHandForFree()){
			eval.plus(koma.getOrder(), Evaluator.forceBackup[stage][koma.getKindId()], Evaluator.CASE_FORCE_BACKUP);
		}
		
	}


	
	
}

