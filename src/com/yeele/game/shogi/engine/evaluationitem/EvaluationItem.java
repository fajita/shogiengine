package com.yeele.game.shogi.engine.evaluationitem;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;



abstract public class EvaluationItem{
	
	protected Evaluator evaluator;
	protected String name;
	

	public EvaluationItem(Evaluator evaluator, String name){
		this.evaluator = evaluator;
		this.name = name;
	}
	
	public String toString(){
		ByteArrayOutputStream buff;
		PrintStream ps;
		buff = new ByteArrayOutputStream();
		ps = new PrintStream(buff);
		ps.printf("%s", this.name);
		return buff.toString();
	}


	
	/**********************************************************************
	               static methods
	**********************************************************************/

}



