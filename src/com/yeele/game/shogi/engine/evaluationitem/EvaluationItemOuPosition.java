package com.yeele.game.shogi.engine.evaluationitem;



import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.util.ArrayListS;


public class EvaluationItemOuPosition extends EvaluationItem{
	
	public static String NAME= "EvaluationItemOuPosition";
		
	public EvaluationItemOuPosition(Evaluator evaluator){
		super(evaluator, EvaluationItemOuPosition.NAME);
	}

	
	public void evaluate(Evaluation eval) {
		// brainstorming ..................
		// position of the ou
		// protection of the ou.
		// protection of threaten koma.
		// progress of staratagy.
		
		// basic pricipal
		// 1-1. koma should be backed up by at least one other koma.
		// 1-2. if koma can be backed up in 2 hands, it's okay.(浮いているこまがあっても、次の手でバックアプできればよし。）
		// 2. porn, lance and knight at the original position are exceptions
		//    for pricipal No1.
		//
		
		// conclusion
		// 王が次の手で王手されなければ、プラス
		// 通しの利きが利いていれば、マイナス
		int points;
		int stage = this.evaluator.getStage();
		Shogi shogi = this.evaluator.getShogi();
		ArrayListS<Koma> ous = shogi.getKomaList(Koma.OU);
		
		if(stage == Evaluator.STAGE_OPENNING_GAME){
			
		}else if(stage == Evaluator.STAGE_MIDDLE_GAME){
			
		}else if(stage == Evaluator.STAGE_END_GAME){
			
		}
		
		for(Koma ou : ous){
			// TBD. how to evaluate
			points = 0; 
			eval.plus(ou.getOrder(), points, Evaluator.CASE_OU_POSITION);
		}
		
	}

	
	/**********************************************************************
	               static methods
	**********************************************************************/
	

}


