package com.yeele.game.shogi.engine;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import com.yeele.game.shogi.engine.stratagy.suji.Suji;



public class Move{
	
	
	public int order;
	public int komaToMove; // always indicates the kind after the movement was made.
	
	// valid number for to and from are 11 to 99 and number represents black or white stand.
	public int from;
	public int to;
	public int flip;  // if flip at this move, yes 
	                  // the previous name was "promote"
	                  // whether it's promoted in the move. doesn't mean if it has been promoted or not.
	                  // name "flip" suits better in case of back() function.
	public int komaToCapture;
	
	public MoveExtension extension = new MoveExtension();
	// Move should be initialized with arguments!! Always explicitly declare!
	/*
	public Move(){
		this.order = Shogi.ORDER_UNKNOWN;
		this.komaToMove = Koma.UK;
		this.from = Shogi.POSITION_UNKNOW;
		this.to = Shogi.POSITION_UNKNOW;
		this.flip = Koma.FLIP_UNKNOWN;
		this.komaToCapture = Koma.UK;
	}
	*/
	
	public Move(int order, int from, int to, int komaToMove, int flip, int komaToCapture){
		this.order = order;
		this.komaToMove = komaToMove;
		this.from = from;
		this.to = to;
		this.flip = flip;
		this.komaToCapture = komaToCapture;
	}
	
	public Move(int order, int from, int to, int komaToMove, int flip){
		this.order = order;
		this.komaToMove = komaToMove;
		this.from = from;
		this.to = to;
		this.flip = flip;
		this.komaToCapture = Koma.UK;
	}
	
	public Move(int order, int from, int to, int komaToMove){
		this.order = order;
		this.komaToMove = komaToMove;
		this.from = from;
		this.to = to;
		this.flip = Koma.FLIP_UNKNOWN;
		this.komaToCapture = Koma.UK;
	}

	public boolean isPassMove(){
		if( this.komaToMove == Koma.UK) return true;
		else return false;
	}
	
	public boolean isSameAs(Move m){
		boolean ret = false;
		if(this.order == m.order &&
				this.from == m.from &&
				this.to == m.to &&
				this.komaToMove == m.komaToMove &&
				this.komaToCapture == m.komaToCapture) ret = true;
		return ret;
	}
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s%02d%3s(%02d)"
				, Shogi.getOrderName(this.order)
				, this.to
				, Koma.getKomaName(this.komaToMove)
				, this.from
		);
		str += buff.toString();
	
		return str;
	}
	
	
	public String toCSAString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s%02d%02d%2s"
				, Shogi.getOrderName(this.order)
				, this.from
				, this.to
				, Koma.getKomaName(this.komaToMove)
		);
		str += buff.toString();
	
		return str;
	}
	
	public String toVerboseString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s%02d%02d%3s \"%4s\"%4s"
				, Shogi.getOrderName(this.order)
				, this.from
				, this.to
				, Koma.getKomaName(this.komaToMove)
				, Koma.getFlipType(this.flip)
				, Koma.getKomaName(this.komaToCapture)
		);
		str += buff.toString();
	
		return str;
	}
	
	public String toStringExtended(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s", this.toString());
		
		if(this.extension!=null){
			for(Map.Entry<Integer, Boolean> entry : this.extension.sujiCorrespondanceMap.entrySet()){
				if(entry.getValue() == true){
					ps.printf("%s, ", Suji.getNameBySujiId(entry.getKey()));
				}
			}
		}
		
		str += buff.toString();
	
		return str;
	}
	
	/*
	 * return number of steps moved forward.
	 * e.g BLACK moving from 57 to 56 return 1
	 * e.g WHITE moving from 52 to 41 return -1
	 */
	public int countForwardSteps(){
		int steps = 0;
		int forwardDirection = (this.order == Shogi.BLACK) ? -1 : 1;
		int rankFrom = Board.getRank(this.from);
		int rankTo   = Board.getRank(this.to);
		
		steps = (rankTo - rankFrom) * forwardDirection;
		
		return steps;
	}
	
	/*
	Extension manipurator
	*/
	
	public void setExtension(HashMap<Integer, Boolean> map){
		this.extension.sujiCorrespondanceMap = map;
	}
	public boolean isSuji(int sujiId){
		if(this.extension!=null){
			if(this.extension.sujiCorrespondanceMap.containsKey(sujiId)){
				return this.extension.sujiCorrespondanceMap.get(sujiId);
			}
		}
		return false;
	}
	
	public void setSujiCorrespondance(int sujiId, boolean isCorresponded){
		if(this.extension!=null){
			this.extension.sujiCorrespondanceMap.put(sujiId, isCorresponded);
		}
	}
	
	
}

