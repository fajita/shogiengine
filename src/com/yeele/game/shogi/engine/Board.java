package com.yeele.game.shogi.engine;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Iterator;

import com.yeele.game.shogi.engine.player.Player;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;

public class Board {
	
	protected Shogi shogi;
	protected Masu [] matrix; 
	protected ArrayListS<Masu> placable;
	protected ArrayListS<Masu> interfered; //　駒がぶつかっている升
	protected ArrayListS<Masu> allMasu;
	protected ArrayListS<Masu> masu123;
	protected ArrayListS<Masu> masu456;
	protected ArrayListS<Masu> masu789;
	
	protected boolean isNotified;
	
	
	public Board(Shogi shogi){
		
		this.shogi = shogi;
		/*
		 * only 11 - 99 are used.
		 */
		this.matrix = new Masu[PADDING + MATRIX_SIZE];
		this.allMasu = new ArrayListS<Masu>();
		this.masu123 = new ArrayListS<Masu>();
		this.masu456 = new ArrayListS<Masu>();
		this.masu789 = new ArrayListS<Masu>();
		
		for(int i=0; i<MATRIX_SIZE; i++){
			this.matrix[Board.SQUARES[i]] = new Masu(Board.SQUARES[i]);
			this.allMasu.add(this.matrix[Board.SQUARES[i]]);
		}
		for(int i=0; i<MATRIX_SIZE_OF_TERITORY; i++){
			this.masu123.add(this.matrix[Board.SQUARES123[i]]);
			this.masu456.add(this.matrix[Board.SQUARES456[i]]);
			this.masu789.add(this.matrix[Board.SQUARES789[i]]);
		}
		
		this.placable = new ArrayListS<Masu> ();
		this.interfered = new ArrayListS<Masu>();
	}
	
	public void reset(){
		for(Masu ms : this.allMasu){
			ms.reset();
		}
		this.placable.clear();
		this.interfered.clear();
	}
	
	public ArrayListS<Masu> getAllMasu(){
		return this.allMasu;
	}
	
	public ArrayListS<Masu> getAllMasu123(){
		return this.masu123;
	}
	public ArrayListS<Masu> getAllMasu456(){
		return this.masu456;
	}
	public ArrayListS<Masu> getAllMasu789(){
		return this.masu789;
	}
	
	public void updateTeritory(Player ply){
		int teritory = ply.getTeritory();
		int plyOrder = ply.getOrder();
		ArrayListS<Masu> teritoriedMasus = null;
		if(teritory == TERITORY_RANK123){
			teritoriedMasus = this.getAllMasu123();
		}else if(teritory == TERITORY_RANK789){
			teritoriedMasus = this.getAllMasu789();
		}
		for(Masu ms : teritoriedMasus){
			ms.setTeriotyFor(plyOrder);
		}
	}
	
	
	public ArrayListS<Masu> getPlacable(){
		return this.placable;
	}
	public ArrayListS<Masu> getInterfered(){
		return this.interfered;
	}
	
	public final Masu getMasu(int square){
		Masu masu = null;
		try{
			masu = this.matrix[square];
		}catch(Exception e){
		
		}
		return masu;
	}
	
	public final Masu getMasu(int file, int rank){
		int square = file * 10 + rank;
		return getMasu(square);
	}
	
	public final Koma getKoma(int square){
		Masu masu = this.getMasu(square);
		if(masu == null) return null;
		else return masu.koma;
	}
	
	public final Koma getKoma(int file, int rank){
		Masu ms = this.getMasu(file, rank);
		if(ms != null) return ms.koma;
		else return null;
	}
	
	
	
	public ArrayListS<Koma> getKomaOnFile(int order, int file){
		ArrayListS<Koma> komas = new ArrayListS<Koma>(); 
		int sq = file*10+1;
		for(int i=sq; i<sq+9;i++){
			Koma k = this.getMasu(i).koma;
			if(k !=null){
				if(k.getOrder()==order){
					komas.add(k);
				}
			}
		}
		return komas;
	}
	
	// This is slow, use the one in Shogi.
	// outdated
	//public Koma getOu(int order){
	//	Koma km = null;
	//	for(int i=0; i < Board.MATRIX_SIZE; i++){
	//		km = this.getKoma(Board.SQUARES[i]);
	//		if(km != null){
	//			if(km.kindId == Koma.OU && km.order == order) break;
	//		}
	//	}
	//	return km;
	//}
	
	
	// how many opponent exist in area of the give order
	public int getCountInArea(int order, int teritory){
		int count = 0;
		
		ArrayListS<Masu> teritories = null;
		if(teritory == TERITORY_RANK123){
			teritories = this.masu123;
		}else if(teritory == TERITORY_RANK456){
			teritories = this.masu456;
		}else if(teritory == TERITORY_RANK789){
			teritories = this.masu789;
		}else{
			return count;
		}
		
		for(Masu ms : teritories){
			if(ms.isKoma(order)){
				count++;
			}
		}
		return count;
	}
	
	public ArrayListS<Move> getMovesCanGoTo(int order, int square){
		// return list of moves, that moves to the given square among the given order komas.]
		ArrayListS<Move> moves = new ArrayListS<Move>();

		if(Board.isSquareValid(square)){
			Masu masu= this.getMasu(square);
			for(Koma direct : masu.getDirectlyReferredBy(order)){
				for(Move move : direct.moves){
					if(move.to == square){
						moves.add(move);
					}
				}
			}
		}
		return moves;
	}
	
	public ArrayListS<Move> getAllHands(int order){
		ArrayListS<Move> moves = new ArrayListS<Move> ();

		for(Koma koma : this.shogi.getKomaListByOrder(order)){
			if(koma.isOnBoard()){
				moves.addIfNot(koma.getMoves());
			}
		}
		return moves;
	}
	
	
	public void updateAll(){
		this.updateAllKomaOnBoard();
		this.shogi.updateNotifiedKoma(null);
		this.setPlacable();
	}
	
	public void updateAllKomaOnBoard(){
		Koma km = null;
		for(int i=0; i < Board.MATRIX_SIZE; i++){
			km = this.getKoma(Board.SQUARES[i]);
			if(km != null){
				km.update();
			}
		}
	}
	
	public void setPlacable(){
		Masu ms = null;
		this.placable.clear();
		for(int i=0; i < Board.MATRIX_SIZE; i++){
			ms = this.getMasu(Board.SQUARES[i]);
			if(ms.koma == null){
				this.placable.add(ms);
			}
		}
	}

	public void updatePlacable(Move move){
		if(move == null) return ;
		Masu ms;
		ms = this.getMasu(move.to);
		this.placable.remove(ms);
		ms = this.getMasu(move.from);
		if(ms!=null) this.placable.add(ms);
	}
	
	// TODO: optimize
	public void deleteOutdatedInterfered(){
		ArrayListS<Masu> outdatedMasu = new ArrayListS<Masu>();
		for(Masu masu : this.interfered){
			if(masu.isEmpty()){
				if(masu.countDirectlyReferredBy(Shogi.BLACK) > 0 &&
				   masu.countDirectlyReferredBy(Shogi.WHITE) > 0){
					// ターゲット上に駒がない場合は、両者の参照があること！なければ削除
				}else{
					outdatedMasu.add(masu);
				}
			}else{
				if(masu.countDirectlyReferredBy(Shogi.getOtherOrder(masu.koma.getOrder())) > 0){
					// ターゲット上にある駒に対して、敵の参照があればＯＫ。
				}else{
					outdatedMasu.add(masu);
				}
			}
		}
		
		for(Masu outdate : outdatedMasu){
			if(this.interfered.contains(outdate)){
				this.interfered.remove(outdate);
			}
		}
		
		//Logger.debug("delete target if the masu is no longer point of battle.\n");
		//for(Masu t : this.interfered) Logger.debug("%s\n", t); // debugging
		//Logger.debug("\n");
	}
	
	public ArrayListS<Move> getPrecedingMovesFor(Masu masu2backup, int order){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		ArrayListS<Koma> komas = null;
		for(int kindId : Koma.BASIC_KIND_IDS){
			komas = this.shogi.getKomaList(kindId);
			for(Koma koma : komas){
				if(koma.getOrder() == order){
					if(koma.isOnBoard()){
						moves.addAll(koma.getPrecedingMovesFor(masu2backup));
					}else if(koma.isOnStand()){
						
					}
				}
			}
		}
		return moves;
	}
	
	/*
	startMasuとendMasuの間の連続の空升を返す。
	ただし、ひとつでの途中に駒がある場合は、無効とする
	 */
	public ArrayListS<Masu> getEmptyMasuBetween(Masu startMasu, Masu endMasu){
		ArrayListS<Masu> empties = new ArrayListS<Masu>();
		int dir = Board.getDirection(Shogi.BLACK, startMasu.square, endMasu.square);
		if (((dir & DIRECTION_45_DIAGONAL)==DIRECTION_45_DIAGONAL) ||
			((dir & DIRECTION_STRAIGHT)==DIRECTION_STRAIGHT)){
			
			int file = Board.getFile(startMasu.square);
			int rank = Board.getRank(startMasu.square);
			int end_file = Board.getFile(endMasu.square);
			int end_rank = Board.getRank(endMasu.square);
			Masu masu = null;
			do{
				masu = this.getMasu(file, rank);
				if(masu!=null){
					if(masu.isEmpty()){
						empties.addIfNot(masu);
					}
					if(file == end_file && rank == end_rank) break;
					if(((dir & DIRECTION_TOP)==DIRECTION_TOP) ) rank += (-1);
					else if(((dir & DIRECTION_DOWN)==DIRECTION_DOWN) ) rank += (1);
					else if(((dir & DIRECTION_RIGHT)==DIRECTION_RIGHT) ) file += (-1);
					else if(((dir & DIRECTION_LEFT)==DIRECTION_LEFT) ) file += (1);		
				}
			}while(masu != null);
			
		}
		return empties;
	}
	
	public String toString(){
		return this.toStringImpl(Shogi.LANGUAGE_JP, false);
	}
	
	public String toStringEN(){
		return this.toStringImpl(Shogi.LANGUAGE_EN, false);
	}
	
	public String toStringTrimed(){
		StringBuffer buff = new StringBuffer();
		Masu masu = null;
		int emptySeq = 0;
		for(int i=0; i < Board.MATRIX_SIZE; i++){
			masu = this.matrix[Board.SQUARES[i]];
			if(masu.isKoma()){
				if(emptySeq > 0){
					buff.append(String.valueOf(emptySeq));
					emptySeq = 0;
				}
				buff.append(Shogi.getOrderName(masu.koma.order));
				buff.append(Koma.getKomaNameTrimed(masu.koma.kindId));
			}else{
				emptySeq++;
			}
		}
		return buff.toString();
	}
	
	protected String toStringImpl(int languageID, boolean doDisplayNumber){
		
		Masu masu = null;
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		String [] suji = {"9", "8", "7", "6", "5", "4", "3", "2", "1"};
		String gapChar = " ";
		if(languageID == Shogi.LANGUAGE_EN){
			suji = new String [] {"9", "8", "7", "6", "5", "4", "3", "2", "1"};
			gapChar = "*";
		}else if(languageID == Shogi.LANGUAGE_JP){
			suji = new String [] {"九", "八", "七", "六", "五", "四", "三", "二", "一"};
			gapChar = "口";
		}
		
		if(doDisplayNumber){
			
			for(int i=0; i < 9; i++){
				ps.printf("%4s", "*" + suji[i]);
			}
			ps.printf("\n");
			ps.printf("  --------------------------------\n");
		}
		
		for(int i=0; i < Board.SQUARES.length; i++){
			masu = this.matrix[Board.SQUARES[i]];
			String komaStr = "";
			if(masu.isKoma()){
				if(languageID == Shogi.LANGUAGE_EN){
					komaStr = Koma.getKomaName(masu.koma.kindId);
				}else if(languageID == Shogi.LANGUAGE_JP){
					komaStr = Koma.getKomaNameJP(masu.koma.kindId);
				}
			}
			String order = masu.koma == null ? "*" : (masu.koma.order == Shogi.BLACK ? "+" : "-");
			String koma = masu.koma == null ? gapChar : komaStr; 
			ps.printf("%4s", order + koma);
			// check if this is 1st file.
			if(Board.SQUARES[i] - 19 <= 0){
				if(doDisplayNumber){
					ps.printf("|%4d", (Board.SQUARES[i]%10));
				}
				ps.printf("\n");
			}
		}
		return buff.toString();
	}
	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	public static final int PADDING = 19;
	public static final int MATRIX_SIZE = 81;
	public static final int MATRIX_SIZE_OF_TERITORY = 27; // 81 / 3 = 27
	public static final int MATRIX_START = 11;
	public static final int MATRIX_END = 99;
	// distance from a masu to a masu.
	// and its max would be from 11 to 99 and its 16.
	public static final int MAX_DISTANCE = 16; 
	public static final int NUM_FILES = 9;
	public static final int NUM_RANKS = 9;
	
	//                                            BLACK, WHITE
	public static final int [] FILE_FOWARD 		= { -1,  1 };
	public static final int [] FILE_BACKWARD 	= {  1, -1 };
	public static final int [] RANK_RIGHT		= { -1,  1 };
	public static final int [] RANK_LEFT		= {  1, -1 };
	
	public final static int [] SQUARES = {
		91,  81,  71,  61,  51,  41,  31,  21,  11,  
		92,  82,  72,  62,  52,  42,  32,  22,  12,  
		93,  83,  73,  63,  53,  43,  33,  23,  13,  
		94,  84,  74,  64,  54,  44,  34,  24,  14,
		95,  85,  75,  65,  55,  45,  35,  25,  15,
		96,  86,  76,  66,  56,  46,  36,  26,  16,
		97,  87,  77,  67,  57,  47,  37,  27,  17,
		98,  88,  78,  68,  58,  48,  38,  28,  18,
		99,  89,  79,  69,  59,  49,  39,  29,	19
    };
	
	public final static int [] SQUARES123 = {
		91,  81,  71,  61,  51,  41,  31,  21,  11,  
		92,  82,  72,  62,  52,  42,  32,  22,  12,  
		93,  83,  73,  63,  53,  43,  33,  23,  13  
    };
	public final static int [] SQUARES456 = {
		94,  84,  74,  64,  54,  44,  34,  24,  14,
		95,  85,  75,  65,  55,  45,  35,  25,  15,
		96,  86,  76,  66,  56,  46,  36,  26,  16
    };
	public final static int [] SQUARES789 = {
		97,  87,  77,  67,  57,  47,  37,  27,  17,
		98,  88,  78,  68,  58,  48,  38,  28,  18,
		99,  89,  79,  69,  59,  49,  39,  29,	19
    };
	
	public final static int TERITORY_UNKNOWN = 0;
	public final static int TERITORY_RANK123 = 123;
	public final static int TERITORY_RANK456 = 456;
	public final static int TERITORY_RANK789 = 789;
	
	
	public static boolean isValidFile(int file){
		boolean ret = true;
		if(file > 9) ret = false;
		if(file < 1) ret = false;
		return ret;
	}
	
	public static boolean isValidRank(int rank){
		boolean ret = true;
		if(rank > 9) ret = false;
		if(rank < 1) ret = false;
		return ret;
	}
	public static boolean isSquareValid(int square){
		return !(square%10==0 || square < 11 || square > 99);
	}
	/*
	public static boolean isSquareValid(int square){
		if(square > 99) return false;
		else if(square < 11) return false;
		else if(square == 10) return false;
		else if(square == 20) return false;
		else if(square == 30) return false;
		else if(square == 40) return false;
		else if(square == 50) return false;
		else if(square == 60) return false;
		else if(square == 70) return false;
		else if(square == 80) return false;
		else if(square == 90) return false;
		else return true;
	}
	public static boolean isSquareValid(int square){
		int file = getFile(square);
		int rank = getRank(square);
		if(isValidFile(file) && isValidRank(rank)) ret = true;
		return ret;
	}
	*/
	
	/*
	 * check if the given kindId of the given order exists on the given file.
	 * return true if exist, false if not.
	 */
	public boolean existOnFile(int order, int kind, int file){
		if(Board.isValidFile(file)){
			for(int sq=(file*10)+1; sq < (file+1)*10; sq++){
				Masu ms = this.matrix[sq];
				if(ms!=null){
					if(ms.koma!=null){
						if(ms.koma.getOrder()==order &&ms.koma.getKindId()==kind){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public final static boolean isMatrixValid(int square){
		boolean ret = false;
		if(isSquareValid(square) || Stand.isStandValid(square)) ret = true;
		return ret;
	}
	
	public final static int getRank(int square){
		return square % 10;
	}
	
	public final static int getFile(int square){
		return (int)(square / 10);
	}
	
	public final static int getOppositeTeritory(int teritory){
		if(teritory == TERITORY_RANK123){
			return TERITORY_RANK789;
		}else if(teritory == TERITORY_RANK456){
			return TERITORY_RANK456;
		}else if(teritory == TERITORY_RANK789){
			return TERITORY_RANK123;
		}else{
			return TERITORY_UNKNOWN;
		}
	}
	
	public final static boolean isOpponentArea(int order, int square){
		boolean ret = false;
		int rank = Board.getRank(square);
		if( order == Shogi.BLACK){
			if(rank == 3 || rank == 2 || rank == 1) ret = true;
		}else if( order == Shogi.WHITE ){
			if(rank == 7 || rank == 8 || rank == 9) ret = true;
		}
		return ret;
	}
	
	public final static boolean isMiddleArea(int square){
		boolean ret = false;
		int rank = Board.getRank(square);
		if(rank == 4 || rank == 5 || rank == 6) ret = true;
		return ret;
	}
	
	public final static boolean isOpponentSide(int order, int square){
		/* whehter the given square is on which side, devided 
		 * in half of the board.
		 */
		boolean ret = false;
		int rank = Board.getRank(square);
		if( order == Shogi.BLACK){
			if(rank == 5 || rank == 4 || rank == 3 || rank == 2 || rank == 1) ret = true;
		}else if( order == Shogi.WHITE ){
			if(rank == 5 || rank == 6 || rank == 7 || rank == 8 || rank == 9) ret = true;
		}
		return ret;
	}
	
	public final static boolean isRightSide(int order, int square){
		/* whehter the given square is on which side, devided 
		 * in half of the board as the 5th file as the border.
		 */
		boolean ret = false;
		int file = Board.getFile(square);
		if( order == Shogi.BLACK){
			if(file == 5 || file == 4 || file == 3 || file == 2 || file == 1) ret = true;
		}else if( order == Shogi.WHITE ){
			if(file == 5 || file == 6 || file == 7 || file == 8 || file == 9) ret = true;
		}
		return ret;
	}
	
	public static boolean isLeftSide(int order, int square){
		/* whehter the given square is on which side, devided 
		 * in half of the board as the 5th file as the border.
		 */
		boolean ret = false;
		int file = Board.getFile(square);
		if( order == Shogi.WHITE){
			if(file == 5 || file == 4 || file == 3 || file == 2 || file == 1) ret = true;
		}else if( order == Shogi.BLACK ){
			if(file == 5 || file == 6 || file == 7 || file == 8 || file == 9) ret = true;
		}
		return ret;
	}
	
	static public boolean isOnSameFile(int sq_a, int sq_b){
		int file_a = Board.getFile(sq_a);
		int file_b = Board.getFile(sq_b);
		return (file_a == file_b) ? true : false;
	}
	
	static public boolean isOnSameRank(int sq_a, int sq_b){
		int rank_a = Board.getRank(sq_a);
		int rank_b = Board.getRank(sq_b);
		return (rank_a == rank_b) ? true : false;
	}
	
	static public int getDistance(int from, int to){
		int distance = 0;
		int file_from = Board.getFile(from);
		int rank_from = Board.getRank(from);
		int file_to   = Board.getFile(to);
		int rank_to = Board.getRank(to);
		int file_diff = Math.abs(file_from - file_to);
		int rank_diff = Math.abs(rank_from - rank_to);
		if(file_diff == rank_diff){
			distance = file_diff; 
		}else{
			int diff = Math.abs(file_diff - rank_diff);
			distance = ((file_diff < rank_diff) ? file_diff : rank_diff) + diff;
		}
		return distance;
	}
	
	// from the view of sq_origin, 
	// which direction is the sq_dst located?
	static public int getDirection(int order, int sq_origin, int sq_dst){
		int direction = DIRECTION_NONE;
		int file_origin = Board.getFile(sq_origin);
		int rank_origin = Board.getRank(sq_origin);
		int file_dst   = Board.getFile(sq_dst);
		int rank_dst = Board.getRank(sq_dst);
		int file_diff = file_origin - file_dst;
		int rank_diff = rank_origin - rank_dst;
		if(order == Shogi.BLACK){
			if(file_diff > 0) direction |= DIRECTION_RIGHT;
			if(file_diff < 0) direction |= DIRECTION_LEFT;
			if(rank_diff > 0) direction |= DIRECTION_TOP;
			if(rank_diff < 0) direction |= DIRECTION_DOWN;
		}else if(order == Shogi.WHITE){
			if(file_diff > 0) direction |= DIRECTION_LEFT;
			if(file_diff < 0) direction |= DIRECTION_RIGHT;
			if(rank_diff > 0) direction |= DIRECTION_DOWN;
			if(rank_diff < 0) direction |= DIRECTION_TOP;
		}
		if(Math.abs(file_diff) == Math.abs(rank_diff)) direction |= DIRECTION_45_DIAGONAL;
		if( (file_diff == 0 && rank_diff != 0) || (file_diff != 0 && rank_diff == 0) )
			direction |= DIRECTION_STRAIGHT;
		return direction;
	}
	
	static public int getOppositeDirection(int direction){
		int oppositeDirection = DIRECTION_NONE;
		if( (direction & DIRECTION_TOP) == DIRECTION_TOP) oppositeDirection |= DIRECTION_DOWN;
		if( (direction & DIRECTION_DOWN) == DIRECTION_DOWN) oppositeDirection |= DIRECTION_TOP;
		if( (direction & DIRECTION_RIGHT) == DIRECTION_RIGHT) oppositeDirection |= DIRECTION_LEFT;
		if( (direction & DIRECTION_LEFT) == DIRECTION_LEFT) oppositeDirection |= DIRECTION_RIGHT;
		return oppositeDirection;
	}
	
	public static int DIRECTION_MASK = 0xFFFF;
	public static int DIRECTION_NONE = 0x0000;
	public static int DIRECTION_TOP = 0x0001;
	public static int DIRECTION_DOWN = 0x0002;
	public static int DIRECTION_RIGHT = 0x0004;
	public static int DIRECTION_LEFT = 0x0008;
	public static int DIRECTION_45_DIAGONAL = 0x0010; // helper to identify ka path
	public static int DIRECTION_STRAIGHT = 0x0020;    // helper to identify hi, ky path
	
	
	public static int getRankFromYourBottom(int order, int square){
		/* 
		return the number of rank, counting up from your side. not the regular rank.
		e.g. BLACK, 88
		the rank is 8 in general, but this function returns 2.
		 */
		int num = 0;
		int rank = Board.getRank(square);
		if(order == Shogi.BLACK) num = 10 - rank; 
		else num = rank;
		return num;
	}
	
	/*
	 * e.g.  
	 * get180RotatedSquare(33) returns 77
	 * get180RotatedSquare(45) returns 65
	 */
	public static int get180RotatedSquare(int square){
		int sq = -1;
		if(Board.isSquareValid(square)){
			int file = Board.getFile(square);
			int rank = Board.getRank(square);
			sq = ((10-file)*10) + (10-rank);
		}
		return sq;
	}
	
	/* 
	このクラスでは囲いの基本型を示すクラスである。
	先手後手それぞれにクラスをつくらなくていいように、orderによって駒の最終配置を自動的に変える
	仕組みをもうけているが、
	その為の便利関数。
	実装者は、このクラスで新しい囲いを作るとき、先手、黒を基準に升目を入力すれば
	いいだけ。
	 */
	public static int convFromBlackToGivenPerspective(int order, int square){
		int sq = square;
		if(order == Shogi.WHITE){
			sq = Board.get180RotatedSquare(square);
		}
		return sq;
	}

	
	
}


	