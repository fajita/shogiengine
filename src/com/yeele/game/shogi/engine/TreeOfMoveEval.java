package com.yeele.game.shogi.engine;

import java.io.*;

import com.yeele.game.shogi.engine.MoveEval;

import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class TreeOfMoveEval {
	
	// which is index 
    // root of tree, which is backbone has index of 0.
	protected int depth;
	
	// which is the depth of the most tip leaf.
	// if you want relative depth to tip, you can do (deepest - depth).
	protected int deepest; 
	
	protected int index; // index of myself as a branch.
	protected MoveEval fruit;
	protected boolean isExamined;
	protected boolean isConfirmed; 
	
	public TreeOfMoveEval trunk;
	public ArrayListS<TreeOfMoveEval> branches;
	
	public String membersToString(){
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		ps.printf("D=%2d %s %s %s idx=%d, examined=%b "
				, this.depth
				, this.fruit.move
				, this.isConfirmed ? "!" : "-"
				, this.fruit.eval.getStringScore()
				, this.index
				, this.isExamined);
		return buff.toString();
	}
	
	/*
	 * tree is basically a list of list.
	 * therefore, tree could represents something like this.
	 * 
	  
	  tree
	  berry = null
	  branch ------berry = moveeval, braches ---------- berry = moveeval, branches = null
	          |                                 
	          |----berry = moveeval, branches ------ berry = moveeval, branches = null
	          |                                  |--- berry = moveeval, branches = null
	          |                                  |--- berry = moveeval, branches = null
	          |----berry = movaeval, branches = null
	          
	 */
	
	public TreeOfMoveEval(){
		this.depth = 0;
		this.deepest = 0;
		this.fruit = null;
		this.trunk = null;
		this.branches = new ArrayListS<TreeOfMoveEval> ();
		this.isExamined = false;
		this.isConfirmed = false;
	}
	
	public TreeOfMoveEval(MoveEval a_fruit){
		this.depth = 0;
		this.deepest = 0;
		this.fruit = a_fruit;
		this.trunk = null;
		this.branches = new ArrayListS<TreeOfMoveEval> ();
		this.isExamined = false;
		this.isConfirmed = false;
	}
	
	public void addBranch(TreeOfMoveEval branch){
		branch.trunk = this;
		branch.updateDepth(this.depth + 1);
		
		int idx = this.branches.size();
		branch.index = idx;
		this.branches.add(branch);
	}
	
	
	/*
	branch一段回ごとに1づつインクリメントした数をdepthとしてセットしていく。
	最初はnew_depthで始まる。
	*/
	// TODO stack overflow!!!!?
	public void updateDepth(int new_depth){
		if(new_depth > 10){
			int debugging = 0;
		}
		this.depth = new_depth;
		this.deepest = (new_depth > this.deepest) ? new_depth : this.deepest; 
		
		if(this.hasBranch()){
			for(TreeOfMoveEval branch : this.branches){
				branch.updateDepth(this.depth + 1);
			}
		}else{
			if(this.isBranch()){
				trunk.updateDeepestBackward(this.deepest);
			}
		}
	}
	
	/*
	ある地点より上に繋がる全ての幹のdeepestをgive deepestで更新する。
	通常、最端のノードがこれを呼ぶ
	*/
	public void updateDeepestBackward(int deepest){
		if(deepest > this.deepest){
			this.deepest = deepest;
			if(this.isBranch()){
				this.trunk.updateDeepestBackward(deepest);
			}
		}
	}
	
	public boolean hasBranch(){
		return this.branches.size() > 0 ? true : false;
	}
	
	public boolean isBranch(){
		return !this.isBackbone();
	}
	
	/*
	 * if trunk is null, it means you are the root of this tree.
	 */
	public boolean isBackbone(){
		return this.trunk == null ? true : false;
	}
	
	public MoveEval getFruit(ArrayListS<Integer> indice){
		
		TreeOfMoveEval iter = this;
		MoveEval a_fruit = iter.fruit;
		
		try{
			for(Integer i : indice){
				iter = iter.branches.get(i);
				a_fruit = iter.fruit;
			}
		}catch(IndexOutOfBoundsException e){
			Logger.error("IndexOutOfBoundsException raised in getFruit()!\n");
			a_fruit = null;
		}
		return a_fruit;
	}
	
	public MoveEval getFruit(Integer index){
		
		TreeOfMoveEval iter = this;
		MoveEval a_fruit = iter.fruit;
		
		try{
			iter = iter.branches.get(index);
			a_fruit = iter.fruit;
		}catch(IndexOutOfBoundsException e){
			Logger.error("IndexOutOfBoundsException raised in getFruit()!\n");
			a_fruit = null;
		}
		return a_fruit;
	}
	
	public MoveEval getFruit(){
		return this.fruit;
	}
	
	public void setFruit(MoveEval a_fruit){
		this.fruit = a_fruit;
	}
	
	public void setConfirmed(boolean isConfirmed){
		this.isConfirmed = isConfirmed;
	}
	
	public int getDepth(){
		return this.depth;
	}
	public int getDeepest(){
		return this.deepest;
	}
	public int getRelativeDepthToTip(){
		return this.deepest - this.depth;
	}
	public int getIndex(){
		return this.index;
	}
	
	public void racearound(){
		
		for(TreeOfMoveEval branch : this.branches){
			branch.racearound();
		}
		
	}
	
	/*
	 * info has 
	 * starting from the root node, whether it has more branch or not.
	 */
	private String toString_raceAround(){
		ByteArrayOutputStream buff = new ByteArrayOutputStream();;
		PrintStream ps = new PrintStream(buff);
		String str = "";
		String margin = "";
		for(int i=0; i<this.depth; i++) margin += " ";
		
		ps.printf("%s%2d:%s:%s",
				  margin
				, this.depth
				, this.isConfirmed() ? "!" : " "
				, this.fruit.toStringSimple());
		str += buff.toString();
		
		if(!this.hasBranch()){
			str += "\n";
		}else{
			str += "\n";
			for(TreeOfMoveEval branch : this.branches){
				str += branch.toString_raceAround();
			}
		}
		
		return str;
	}
	
	public String toString(){
		return this.toString_raceAround();
	}
	
	
	
	//----------------------------------------------------------------------------------------
	//  Special method for specific Template <MoveEval>
	//----------------------------------------------------------------------------------------
	public boolean isFruitOfTrunkPassMove(){
		boolean ret = false;
		if(this.isBranch()){
			MoveEval me = this.trunk.getFruit();
			if(me!=null){
				if(me.move!=null){
					ret = me.move.isPassMove();
				}
			}
		}
		return ret;
	}
	
	public boolean isFruitPassMove(){
		boolean ret = false;
		
		MoveEval me = this.getFruit();
		if(me!=null){
			if(me.move!=null){
				ret = me.move.isPassMove();
			}
		}
		
		return ret;
	}
	
	public boolean doesPassMoveExistInBranches(){
		boolean ret = false;
		
		for(TreeOfMoveEval branch : this.branches){
			if(branch.getFruit().move.isPassMove()){
				ret = true;
				break;
			}
		}
		
		return ret;
	}
	
	public boolean isConfirmedInDirectBranches(){
		for(TreeOfMoveEval branch : this.branches){
			if(branch.isConfirmed()){
				return true;
			}
		}
		return false;
	}
	
	public int countConfirmedInDirectBranches(){
		int count = 0;
		for(TreeOfMoveEval branch : this.branches){
			if(branch.isConfirmed()){
				count++;
			}
		}
		return count;
	}
	
	public void  leaveTheBestConfirmedInDirectBranches(int order){
		TreeOfMoveEval bestRoot=null, bestTip=null;
		TreeOfMoveEval tipBranch = null;
		for(TreeOfMoveEval branch : this.branches){
			if(branch.isConfirmed()){
				tipBranch = branch.getTipConfirmedBranch();
				if(bestRoot == null){
					bestRoot = branch;
					bestTip = tipBranch;
					branch.isConfirmed = true; // tipBranchが良い得です。confirmをセットするのは根元のbranch
				}else{
					if(Evaluation.diffFor(order, tipBranch.getFruit().eval, bestTip.getFruit().eval) >= 0){
						bestRoot.isConfirmed = false;
						branch.isConfirmed = true;
						bestRoot = branch;
						bestTip = tipBranch;
					}else{
						branch.isConfirmed = false;
					}
				}
			}
		}
	}
	
	public void  setConfirmedToTheBestOneInDirectBranches(int order){
		TreeOfMoveEval best = null;
		for(TreeOfMoveEval branch : this.branches){
			if(best == null){
				branch.isConfirmed = true;
				best = branch;
			}else{
				if(Evaluation.diffFor(order, branch.getFruit().eval, best.getFruit().eval) >= 0){
					best.isConfirmed = false;
					branch.isConfirmed = true;
					best = branch;
				}else{
					branch.isConfirmed = false;
				}
			}
		}
	}
	
	
	public TreeOfMoveEval getTipConfirmedBranch(){
		
		if(this.isConfirmed() && !this.hasBranch()){
			return this;
		}else{
			for(TreeOfMoveEval branch : this.branches){
				if(branch.isConfirmed()){
					TreeOfMoveEval tip = branch.getTipConfirmedBranch();
					if(tip != null) return tip;
				}
			}
		}
		if(this.isConfirmed()) return this;
		else return null;
	}
	
	
	public MoveEval getTipConfirmedFruitUnderBranches(){
		// disregard the this.fruit. whether it's confirmed or not.
		MoveEval me = null;
		if(this.hasBranch()){
			for(TreeOfMoveEval branch : this.branches){
				me = branch.getTipConfirmedFruit();
				if(me != null) return me;
			}
		}else{
			 return this.getFruit();
		}
		return null;
	}
	
	public MoveEval getTipConfirmedFruit(){
		MoveEval me = null;
		if(this.isConfirmed()){
			 if(this.hasBranch()){
				 for(TreeOfMoveEval branch : this.branches){
						me = branch.getTipConfirmedFruit();
						if(me != null) return me;
					}
			 }else{
				 return this.getFruit();
			 }
		}
		return null;
	}
	
	public String toStringConfirmedPath(){
		String str = "";
		if(this.isConfirmed()){
			//Logger.info("depth=%d, move=%s\n", this.depth, this.fruit.move);
			str += this.membersToString() + "\n";
		}
		
		for(TreeOfMoveEval branch : this.branches){
			if(branch.isConfirmed()){
				str += branch.toStringConfirmedPath();
			}
		}
		return str;
	}
	
	public MoveEval getConfirmedFruitInBranches(){
		
		for(TreeOfMoveEval branch : this.branches){
			if(branch.isConfirmed()){
				return branch.getFruit();
			}
		}
		return null;
	}
	
	public TreeOfMoveEval getBranchThatHasConfirmedFruit(){
		
		for(TreeOfMoveEval branch : this.branches){
			if(branch.isConfirmed()){
				return branch;
			}
		}
		return null;
	}
	
	public boolean isConfirmed(){
		return this.isConfirmed;
	}
	
	public int markNextBestMoves(){
		int ret = Shogi.SUCCESS;
		
		Evaluation originalEval = this.getFruit().eval;
		this.markNextBestMoves_impl(originalEval);
		
		return ret;
	}

	private boolean markNextBestMoves_impl(Evaluation ori){
		
		
		int order = this.fruit.move.order;
		int branchOrder = Shogi.getOtherOrder(order);
		
		if(this.hasBranch()){
			for( TreeOfMoveEval branch : this.branches){
				if(branch.markNextBestMoves_impl(ori)) return true;
			}
			
			if(Shogi.MODE_DEBUG){
				if(this.getFruit().move.from == 28 &&
						this.getFruit().move.to == 24){
					int debugging = 1;
				}
				if(this.getFruit().move.from == 41 &&
						this.getFruit().move.to == 32){
					int debugging = 1;
				}
			}
			int count = this.countConfirmedInDirectBranches();
			if(count == 0){
				// 強制的に全部confirmedにしちゃう。んでからleave the best one
				for(TreeOfMoveEval brch : this.branches){
					brch.setConfirmed(true);
				}
				this.leaveTheBestConfirmedInDirectBranches(branchOrder);
			}else{
				if(count > 1){ //Is there multiple branches?
					this.leaveTheBestConfirmedInDirectBranches(branchOrder);
				}
			}
			
			if(this.isBackbone()){
				return true;
			}
			
			MoveEval me = this.getTipConfirmedFruitUnderBranches();
			if(me == null){
				Logger.debug("there's no confirmed fruit!!!\n");
			}else{
				if( Evaluation.diffFor(order, me.eval, ori) >= 0 ){
					this.isConfirmed = true;
				}else{
					this.isConfirmed = false;
				}
			}
		}else{
			// if it's better than original situation.
			if( Evaluation.diffFor(order, this.getFruit().eval, ori) >= 0 ){
				this.isConfirmed = true;
			}else{
				this.isConfirmed = false;
			}
		}
		
		this.isExamined = true;
		return false;
	}
	
	
	public int getAbsMaxDepth(){
		int max = this.depth;
		if(!this.hasBranch()) return max;
		
		for(TreeOfMoveEval branch : this.branches){
			max = Math.max(max, branch.getAbsMaxDepth());
		}
		return max;
	}
	
	public String getTreeString(){
		
		String str = "";
		for(TreeOfMoveEval branch : this.branches){
			str += branch.getTreeString();
		}
		str += this.membersToString() + "\n";
		return str;
	}
	
	public String getTreeStringAtDepth(int depth){
		
		String str = "";
		for(TreeOfMoveEval branch : this.branches){
			str += branch.getTreeStringAtDepth(depth);
		}
		
		if(this.depth == depth)
			str += this.membersToString() + "\n";
		return str;
	}
	
	public String getTreeStringByDepth(){
		
		String str = "";
		int max_depth = this.getAbsMaxDepth();
		for(int i=1; i<=max_depth; i++){
			for(TreeOfMoveEval branch : this.branches){
				str += branch.getTreeStringAtDepth(i);
			}
		}
		return str;
	}
	

	
	
	//----------------------------------------------------------------------------------------
	
}



