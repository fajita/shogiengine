package com.yeele.game.shogi.engine;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class Evaluation implements Cloneable{
	
	public int nextOrder;
	// evaluation is refer when computer is generating a next move.
	// Usually, the higher the evaluation score is, the more likey the 
	// computer pick the move.
	// The evaluation should contains only nutural factors. which means
	// if evaluation result is not leaning on BLACK perspective nor WHITE 
	// perspective.
	// I have to keep this matter in mind!!
	
	public int [] score;
	public int [][] board; // this board[?] represents a score on each masu on the board.
	// debugging purpose
	public HashMap<String, Integer> [] detail = new HashMap[2];
	
	public Evaluation(int order){
		this.nextOrder = order;
		this.score = new int[2];
		this.score[Shogi.BLACK] = 0;
		this.score[Shogi.WHITE] = 0;
		this.board = new int[2][Board.PADDING + Board.MATRIX_SIZE]; // it's 2 X 100.
		// debugging purpose
		this.detail[Shogi.BLACK] = new HashMap<String, Integer>();
		this.detail[Shogi.WHITE] = new HashMap<String, Integer>();
	}
	
	public Evaluation(int order, int black_score, int white_score){
		this.nextOrder = order;
		this.score = new int[2];
		this.score[Shogi.BLACK] = black_score;
		this.score[Shogi.WHITE] = white_score;
		this.board = new int[2][Board.PADDING + Board.MATRIX_SIZE]; // it's 2 X 100.
		// debugging purpose
		this.detail[Shogi.BLACK] = new HashMap<String, Integer>();
		this.detail[Shogi.WHITE] = new HashMap<String, Integer>();
	}
	
	
	public ArrayListS<Integer> findWeakestSquare(int order){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		// 同点でminのところがある場合は、最後にヒットしたsqが返される。 TBD このままでよいか、どうか。
		int opponent = Shogi.getOtherOrder(order);
		sqs.clear();
		// 最初、絶対minが更新されるような値セット。
		int diff, sq = 0, min = this.board[order][Board.SQUARES[0]] - this.board[opponent][Board.SQUARES[0]] + 1;
		for(int i=0; i < Board.SQUARES.length; i++){
			diff = this.board[order][Board.SQUARES[i]] - this.board[opponent][Board.SQUARES[i]];
			if(diff <= min){
				if(diff < min){
					sqs.clear();
					min = diff;
				}
				sqs.add(Board.SQUARES[i]);
			}
		}
		return sqs;
	}
	
	public ArrayListS<Integer> findStrongestSquare(int order){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		// 同点でminのところがある場合は、最後にヒットしたsqが返される。 TBD このままでよいか、どうか。
		int opponent = Shogi.getOtherOrder(order);
		sqs.clear();
		// 最初、絶対minが更新されるような値セット。
		int diff, sq = 0, max = this.board[order][Board.SQUARES[0]] - this.board[opponent][Board.SQUARES[0]] - 1;
		for(int i=0; i < Board.SQUARES.length; i++){
			diff = this.board[order][Board.SQUARES[i]] - this.board[opponent][Board.SQUARES[i]];
			if(diff >= max){
				if(diff > max){
					sqs.clear();
					max = diff;
				}
				sqs.add(Board.SQUARES[i]);
			}
		}
		return sqs;
	}
	
	
	public Evaluation clone(){
		Evaluation eval;
		try{
			eval = (Evaluation)super.clone();
			eval.score = this.score.clone();
			eval.board = new int[2][Board.PADDING + Board.MATRIX_SIZE];
			
			for(int i=0; i < this.board.length; i++){
				for(int j=0; j < this.board[i].length; j++){
					eval.board[i][j] = this.board[i][j];
				}
			}
			
			eval.detail = new HashMap[2];
			eval.detail[Shogi.BLACK] = new HashMap<String, Integer>();
			eval.detail[Shogi.WHITE] = new HashMap<String, Integer>();
			for(int i=0; i < this.detail.length; i++){
				Iterator imap = this.detail[i].entrySet().iterator();
				String key = "";
				Integer val = 0;
				while(imap.hasNext()){
					Map.Entry<String, Integer> entry;
					entry = (Map.Entry<String, Integer>)imap.next();
					key = entry.getKey();
					val = entry.getValue();
					eval.detail[i].put(key,  val);
				}
			}

		}catch (CloneNotSupportedException e){
			return null;
		}
		return eval;
	}
	
	
	public void plus(int order, int points){
		this.score[order] += points;
	}
	
	public void plus(int order, int points, String key){
		this.score[order] += points;
		Integer current = this.detail[order].get(key);
		if(current == null) current = 0;
		this.detail[order].put(key, current + points);
	}
	
	
	public void plus(int order, int points, int square){
		this.board[order][square] += points;
		this.score[order] += points;
	}
	
	public void plusToBoard(int order, int points, int square, String key){
		this.board[order][square] += points;
		this.plus(order, points, key);
	}
	
	
	/*
	 * create a new Evaluation has difference bwtween this and obj.
	 * basically this - obj.
	 */
	public Evaluation diff(Evaluation obj){
		
		Evaluation e = new Evaluation(this.nextOrder);
		String key = "";
		Integer val = new Integer(0), curVal = new Integer(0);
		int [] bw = {Shogi.BLACK, Shogi.WHITE};
		
		for(int order : bw){
			e.score[order] = this.score[order] - obj.score[order];
			for(int i=0; i < Board.SQUARES.length; i++){
				e.board[order][Board.SQUARES[i]] = this.board[order][Board.SQUARES[i]] - obj.board[order][Board.SQUARES[i]];
			}
			Iterator imap = obj.detail[order].entrySet().iterator();
			while(imap.hasNext()){
				Map.Entry<String, Integer> entry;
				entry = (Map.Entry<String, Integer>)imap.next();
				key = entry.getKey();
				val = entry.getValue();
				
				curVal = this.detail[order].get(key);
				if(curVal == null) curVal = 0;
				this.detail[order].put(key, curVal - val);
			}
		}
		return e;
	}
	
	/*
	 * this + obj
	 */
	public Evaluation add(Evaluation obj){

		String key = "";
		Integer val = new Integer(0), curVal = new Integer(0);
		int [] bw = {Shogi.BLACK, Shogi.WHITE};
		
		for(int order : bw){
			this.score[order] += obj.score[order];
			for(int i=0; i < Board.SQUARES.length; i++){
				this.board[order][Board.SQUARES[i]] += obj.board[order][Board.SQUARES[i]];
			}
			Iterator imap = obj.detail[order].entrySet().iterator();
			while(imap.hasNext() ){
				Map.Entry<String, Integer> entry;
				entry = (Map.Entry<String, Integer>)imap.next();
				key = entry.getKey();
				val = entry.getValue();
				
				curVal = this.detail[order].get(key);
				if(curVal == null) curVal = 0;
				this.detail[order].put(key, curVal + val);
			}
		}
		return this;
	}
	
	/*
	 * this + obj
	 */
	public Evaluation add(Evaluation obj, String key){

		int [] bw = {Shogi.BLACK, Shogi.WHITE};
		Integer points;
		for(int order : bw){
			this.score[order] += obj.score[order];
			points = this.detail[order].get(key);
			points = (points == null) ? 0 : points;
			this.detail[order].put(key, points + obj.score[order]);
			for(int i=0; i < Board.SQUARES.length; i++){
				this.board[order][Board.SQUARES[i]] += obj.board[order][Board.SQUARES[i]];
			}
		}
		return this;
	}
	
	
	/*
	 * this - obj
	 */
	public Evaluation subtract(Evaluation obj){
		
		String key = "";
		Integer val = new Integer(0), curVal = new Integer(0);
		int [] bw = {Shogi.BLACK, Shogi.WHITE};
		
		for(int order : bw){
			this.score[order] -= obj.score[order];
			for(int i=0; i < Board.SQUARES.length; i++){
				this.board[order][Board.SQUARES[i]] -= obj.board[order][Board.SQUARES[i]];
			}
			Iterator imap = obj.detail[order].entrySet().iterator();
			while(imap.hasNext() ){
				Map.Entry<String, Integer> entry;
				entry = (Map.Entry<String, Integer>)imap.next();
				key = entry.getKey();
				val = entry.getValue();
				
				curVal = this.detail[order].get(key);
				if(curVal == null) curVal = 0;
				this.detail[order].put(key, curVal - val);
			}
		}
		return this;
	}
	
	/*
	 * after is how much better compare to before for the given order.
	 */
	static public int diffFor(int order, Evaluation A, Evaluation B){
		int diffA = A.score[order] - A.score[Shogi.getOtherOrder(order)];
		int diffB = B.score[order] - B.score[Shogi.getOtherOrder(order)];
		int diff = diffA - diffB;
		// if diff > 0, it got better for the given order.
		// if diff == 0, it's same.
		// if ddff < 0, it got worsen for the given order.
		return diff;
	}
	
	public String getStringScoreDiff(int order){
		// from the view of the give order perspective
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s:%d"
				  , Shogi.getOrderName(order)
				  , this.score[order] - this.score[Shogi.getOtherOrder(order)]
				);
		return buff.toString();
	}
	
	public String getStringScore(){
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("%s:%7d %s:%7d"
				, Shogi.getOrderAbbreviation(Shogi.BLACK)
				, this.score[Shogi.BLACK]
	            , Shogi.getOrderAbbreviation(Shogi.WHITE) 
				, this.score[Shogi.WHITE]
				);
		return buff.toString();
	}
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		//debugging purpose
		int [] shogi_order = {Shogi.BLACK, Shogi.WHITE};
		for(int bw : shogi_order){
			if(! this.detail[bw].containsKey(Evaluator.CASE_ONBOARD)) this.detail[bw].put(Evaluator.CASE_ONBOARD, 0);
			if(! this.detail[bw].containsKey(Evaluator.CASE_ONSTAND)) this.detail[bw].put(Evaluator.CASE_ONSTAND, 0);
			if(! this.detail[bw].containsKey(Evaluator.CASE_BACKUP)) this.detail[bw].put(Evaluator.CASE_BACKUP, 0);
			if(! this.detail[bw].containsKey(Evaluator.CASE_OUDEFENSE)) this.detail[bw].put(Evaluator.CASE_OUDEFENSE, 0);
			if(! this.detail[bw].containsKey(Evaluator.CASE_OU_POSITION)) this.detail[bw].put(Evaluator.CASE_OU_POSITION, 0);
			if(! this.detail[bw].containsKey(Evaluator.CASE_OU_POSITION)) this.detail[bw].put(Evaluator.CASE_OU_POSITION, 0);
			if(! this.detail[bw].containsKey(Evaluator.CASE_KOMA_BOND)) this.detail[bw].put(Evaluator.CASE_KOMA_BOND, 0);
//			if(! this.detail[bw].containsKey(Evaluator.CASE_CHASEOFF)) this.detail[bw].put(Evaluator.CASE_CHASEOFF, 0);
//			if(! this.detail[bw].containsKey(Evaluator.CASE_TADA)) this.detail[bw].put(Evaluator.CASE_TADA, 0);
			if(! this.detail[bw].containsKey(Evaluator.CASE_EACHMASU)) this.detail[bw].put(Evaluator.CASE_EACHMASU, 0);
			if(! this.detail[bw].containsKey(Evaluator.CASE_FORCE_BACKUP)) this.detail[bw].put(Evaluator.CASE_FORCE_BACKUP, 0);
		}
		
		String detailstr = "";
		Iterator ib = this.detail[Shogi.BLACK].entrySet().iterator();
		Iterator iw = this.detail[Shogi.WHITE].entrySet().iterator();
		
		while(ib.hasNext() || iw.hasNext()){
			
			Map.Entry<String, Integer> entry;
			String keyB = "", keyW = "";
			Integer valB = new Integer(0), valW = new Integer(0);
			if(ib.hasNext()){
				entry = (Map.Entry<String, Integer>)ib.next();
				keyB = entry.getKey();
				valB = entry.getValue();
			}
			if(iw.hasNext()){
				entry = (Map.Entry<String, Integer>)iw.next();
				keyW = entry.getKey();
				valW = entry.getValue();
			}
			buff.reset();
			ps.printf("%11s:%8d %11s:%8d\n", keyB, valB, keyW, valW);
			detailstr += buff.toString(); 
		}
		
		buff.reset();
		ps.printf("%11s:%8d %11s:%8d\n%s"
				, "BLACK"
				, this.score[Shogi.BLACK]
	            , "WHITE" 
				, this.score[Shogi.WHITE]
				, detailstr);
		str += buff.toString();
		
		if(Logger.getLevel() <= Logger.LEVEL_INFO){
			int nextOrder, curOrder;
			if(this.nextOrder == Shogi.ORDER_UNKNOWN){
				curOrder = Shogi.BLACK;
				nextOrder = Shogi.WHITE;
			}else{
				curOrder = Shogi.getOtherOrder(this.nextOrder);
				nextOrder = this.nextOrder;
			}
			
			buff.reset();
			ps.printf("\nNext Order is %s\n", Shogi.getOrderNameCasual(nextOrder));
			str += buff.toString();
			for(int i=0; i < Board.SQUARES.length; i++){
				buff.reset();
				ps.printf("%4s",this.board[nextOrder][Board.SQUARES[i]] - this.board[curOrder][Board.SQUARES[i]]);
				str += buff.toString();
				// check if this is 1st file.
				if(Board.SQUARES[i] - 19 <= 0){
					str += "\n";
				}
			}
		}
		
		
		return str;
	}
	
	public int get(int order){
		assert(order == Shogi.BLACK || order == Shogi.WHITE);
		return this.score[order];
	}
}