package com.yeele.game.shogi.engine;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.Iterator;

import com.yeele.game.shogi.engine.util.KomaComparator;
import com.yeele.game.shogi.engine.util.KomaComparatorAsc;
import com.yeele.game.shogi.engine.util.KomaComparatorDesc;
import com.yeele.util.ArrayListS;

import static com.yeele.game.shogi.engine.util.Logger.Logger;

public class Koma implements Observer {

	protected Shogi shogi;
	protected int order;
	protected int opponent;
	protected int kindId;
	public ArrayListS<Masu> movable, reachable, throughable;
	protected ArrayListS<Koma> wallFor, walls;
	protected ArrayListS<Koma> protectingKoma_pre, protectingKoma_now;
	public ArrayListS<Move> moves;
	final protected KomaScope scope_nonpromoted, scope_promoted;
	protected KomaScope scope;
	public Masu masu;
	protected Stand stand;
	protected int td, dd, rd, ld; // top direction, down direction , , 

	protected int throughNumber; // how many times can it go through when
									// updatingCapable(movable, rechable,
									// throughable)
	protected int throughCounter; // how many times can it go through when
									// updatingCapable(movable, rechable,
									// throughable)
	private boolean isNotified;
	protected boolean isJustPlacedOnStand = false;
	
	// supplementary information
	protected boolean canBePromotedInNextHand = false;
	protected boolean canBePromotedInNextHandForFree = false;
	protected boolean canTakeOpponentInNextHandForFree = false;
	protected boolean canBeTakenForFree = false;
	

	public Koma(Shogi shogi, int order, int kindId, KomaScope nonpromoted,
			KomaScope promoted) {
		this.shogi = shogi;
		this.kindId = kindId;
		this.movable = new ArrayListS<Masu>();
		this.reachable = new ArrayListS<Masu>();
		this.throughable = new ArrayListS<Masu>();
		this.moves = new ArrayListS<Move>();
		this.wallFor = new ArrayListS<Koma>(); // if this koma work as a wall for
												// a koma, append it.
		this.walls = new ArrayListS<Koma>();
		this.protectingKoma_now = new ArrayListS<Koma>();
		this.protectingKoma_pre = new ArrayListS<Koma>();
		this.scope_nonpromoted = nonpromoted;
		this.scope_promoted = promoted;
		this.masu = null;
		this.stand = null;
		this.scope = Koma.isPromoted(this.kindId) ? scope_promoted
				: scope_nonpromoted;
		this.throughNumber = 0;
		this.throughCounter = 0;
		this.isNotified = false;
		this.setOrder(order);
	}

	public void flip() {
		if (Koma.isPromoted(this.kindId)) {
			this.kindId = Koma.getUnpromotedPiece(this.kindId);
			this.scope = this.scope_nonpromoted;
		} else {
			this.kindId = Koma.getPromotedPiece(this.kindId);
			this.scope = this.scope_promoted;
		}
	}
	
	public void flip(int kindId) {
		if ( Koma.isPromoted(this.kindId) &&
		    (kindId == Koma.getUnpromotedPiece(this.kindId) )
		){
			flip();
			
		} else if( kindId == Koma.getPromotedPiece(this.kindId) ){ 
			flip();
		}
	}
	
	public void unpromote(){
		if(this.isPromoted()){
			flip();
		}
	}
	
	public void promote(){
		if(this.isUnpromoted()){
			flip();
		}
	}

	public void setOrder(int order){
		this.order = order;
		this.opponent = Shogi.getOtherOrder(order);
		if (this.order == Shogi.BLACK) {
			this.td = -1;
			this.dd = 1;
			this.rd = -1;
			this.ld = 1;
		} else if (this.order == Shogi.WHITE) {
			this.td = 1;
			this.dd = -1;
			this.rd = 1;
			this.ld = -1;
		} else {
			this.td = 0;
			this.dd = 0;
			this.rd = 0;
			this.ld = 0;
		}
	}
	public int bePlaced(Stand std) {
		int ret = Shogi.SUCCESS;
		
		if (std == null) {
			ret = Shogi.ERROR;
			Logger.error("stand is null!!\n");
		}else{
			this.setOrder(std.order);
			// automatically, flip over to non-promote type.
			this.unpromote();

			this.stand = std;
			this.masu = null;
			this.clearReference();
			this.isJustPlacedOnStand = true;
			this.stand.add(this);
			
		}
		
		return ret;
	}

	public int bePlaced(Masu ms) {
		int ret = Shogi.SUCCESS;

		if (ms == null) {
			ret = Shogi.ERROR;
			Logger.error("Masu is null!!\n");
		} else if (ms.koma != null) {
			ret = Shogi.ERROR;
			Logger.error("Masu is not empty!!\n");
		} else {
			ms.koma = this;

			this.masu = ms;
			this.stand = null; // at this moment, it makes sense to set null to
								// stand.
		}
		return ret;
	}
	
	public int bePlaced(KomaBox box) {
		int ret = Shogi.SUCCESS;

		if (box == null) {
			ret = Shogi.ERROR;
			Logger.error("box is null!!\n");
		} else {
			this.setOrder(Shogi.ORDER_UNKNOWN);
			this.masu = null;
			this.stand = null; // at this moment, it makes sense to set null to
								// stand.
			
			box.add(this);
		}
		return ret;
	}
	
	/*
	 * *********************************************************************
	 * Observer Interface
	 * *********************************************************************
	 */
	public void update() {
		this.setIsNotified(true);
	}
	
	/*
	 * beforeUpdate()
	 * updateStatus()
	 * afterUpdate() are one set!
	 */
	public void beforeUpdate(){
		// this is called before updateStatus() is called.
		
	}
	public void updateStatus(Move lastMove) {
		// update koma status
		this.updateCapable(lastMove);
	}
	public void afterUpdate(){
		// this is called after updateStatus() is called.
		
		// update board.'interfered masu'
		this.updateInterfered();
		this.updateSupplementaryInfo();
		this.setIsNotified(false);
	}
	
	public void setIsNotified(boolean notified) {
		this.isNotified = notified;
	}

	public boolean getIsNotified() {
		return this.isNotified;
	}
	/*
	 * *********************************************************************
	 */
	private void savePreviousProtectingKoma(){
		this.protectingKoma_pre.clear();
		for(Koma k : this.protectingKoma_now){
			this.protectingKoma_pre.add(k);
		}
	}
	
	private void clearCapable() {
		// TODO:
		// unregister
		// if this is costy, think better solution, since all case except for
		// this is
		// throuable koma nor the koma which has just moved, subjects are remain
		// same.
		for (int i = 0; i < this.reachable.size(); i++) {
			Masu ms = this.reachable.get(i);
			ms.unregister(this);
			ms.directRefered.remove(this);
		}
		for (int i = 0; i < this.movable.size(); i++) {
			Masu ms = this.movable.get(i);
			ms.unregister(this);
		}
		for (int i = 0; i < this.throughable.size(); i++) {
			Masu ms = this.throughable.get(i);
			ms.unregister(this);
			ms.indirectRefered.remove(this);
		}
		
		// save protectingKoma_now to pre, then clear()
		this.savePreviousProtectingKoma();
		this.protectingKoma_now.clear();
		
		this.clearMovable();
		this.reachable.clear();
		this.throughable.clear();
	}
	
	public void clearWallReference() {
		this.wallFor.clear();
		this.walls.clear();
	}
	
	/*
	 * --------------------------------------------------------------------------------
	 * SPEC: this.movesへの追加、削除はかならず addMovable, removeMovable経由で行われること！！
	 * --------------------------------------------------------------------------------
	 */
	protected void clearMovable(){
		this.movable.clear();
		this.moves.clear();
	}
	
	
	protected void addMovable(Masu ms){
		if(this.movable.addIfNot(ms)==true){
			// movable.addIfNotがtrueってことは新しいmsってことでここに動けるmoveも
			// まだ登録されてないはず。this.moves.addIfNotは毎new で生成されるMoveの
			// 同一かどうかを判定するにはloopで各membersを比較する必要があり、それにはコストがかかる
			this.addMove(ms);
		}
	}
	
	// add move(s) that can move to the given masu.
	protected void addMove(Masu ms){
		Move mv = null;
		if(this.isOnBoard()){
			mv = new Move(this.order, this.masu.square, ms.square, this.kindId,
					Koma.FLIP_NO, Koma.UK);
			this.moves.addIfNot(mv);
			
			// if the koma is un promoted, and it could be promoted on the
			// destination, add that move.
			if (this.canBePromotedOn(ms)) {
				mv = new Move(this.order, this.masu.square, ms.square,
						Koma.getPromotedPiece(this.kindId), Koma.FLIP_YES,
						Koma.UK);
				this.moves.addIfNot(mv);
			}
		}else if(this.isOnStand()){
			mv = new Move(this.order, this.stand.getSquare(), ms.square, this.kindId,
					Koma.FLIP_NO, Koma.UK);
			this.moves.addIfNot(mv);
		}
	}
	
	protected void removeMovable(Masu ms){
		if(this.movable.remove(ms)==true){
			// movable.removeがtrueってことはmsがmovableに入っていて、それを削除できたってこと。
			// したがって、movesはmsをtoに含むmoveを保持していることになる。
			this.removeMove(ms);
		}
	}
	
	// msがdestinationになっているすべてのmoveをmovesから削除する
	private void removeMove(Masu ms){
		for(Iterator<Move> iter = this.moves.iterator(); iter.hasNext(); ){
			Move m = iter.next();
			if(m.to == ms.square){
				iter.remove();
			}
		}
	}
	
	/*--------------------------------------------------------------------------------
	 --------------------------------------------------------------------------------*/
	
	
	
	
	/*
	 * updateCapable update "movable" and "reachable"
	 */
	private int updateCapable(Masu ms) {
		int ret = Shogi.LOOP_CONTINUE;

		if (ms == null) {
			// null is set, if square was invalid.
			// likey, in the loop of KomaScope, it gets the wall. It means
			// square is dividable by 10,
			// such as 50.
			ret = Shogi.LOOP_BREAK;
		} else {

			if (this.throughCounter == 0) {
				this.reachable.add(ms);
				if (ms.isKoma(this.order)) {
					this.protectingKoma_now.addIfNot(ms.koma);
				}
				ms.directRefered.add(this);
				ms.register(this);
			}

			if (ms.koma == null) {
				// if it's been through at least a koma, add it to throughable,
				// if not, just adding it to movable.
				if (this.throughCounter > 0) {
					this.throughable.addIfNot(ms);
					ms.indirectRefered.add(this);
					ms.register(this);
				} else {
					this.addMovable(ms);
					ms.register(this);
				}
			} else {

				if (this.throughNumber > 0 && this.throughCounter == 0) {
					// ms.koma is wall for this.
					ms.koma.wallFor.add(this);
					this.walls.add(ms.koma);
					ms.register(this);
				}

				if (ms.koma.order == this.order) {
					// your team member
					if (this.throughCounter > 0
							&& !(this.throughCounter == this.throughNumber)) {
						this.throughable.add(ms);
						ms.indirectRefered.add(this);
						ms.register(this);
					}
				} else {
					if (this.throughCounter > 0) {
						this.throughable.add(ms);
						ms.register(this);
						ms.indirectRefered.add(this);
					} else {
						this.addMovable(ms);
						ms.register(this);
					}
				}

				if (++this.throughCounter > this.throughNumber) {
					ret = Shogi.LOOP_BREAK;
				}
			}
		}

		return ret;
	}
	public void clearReference(){
		this.clearCapable();
		this.clearWallReference();
	}
	
	public int updateCapable(Move lastMove) {
		int ret = Shogi.SUCCESS;

		// 2. reset the xxx-able list;
		if(this.isOnBoard()){
			this.clearReference();
			// 1. starting form the koma posi, update kiki using scope.
			this.updateCapableOfKomaOnBoard();
		}else if(this.isOnStand()){
			if(Shogi.MODE_DEBUG){
				if(this.kindId == Koma.KE){
					if(lastMove.komaToMove == Koma.FU &&
					lastMove.order == Shogi.BLACK &&
					lastMove.from == 67 && 
					lastMove.to == 66){
						int a = 0;
					}
				}
			}
			if(this.isJustPlacedOnStand){
				// pass null instaed of lsatMove to fullfill the placeable to this capable.
				this.updateCapableOfKomaOnStand(null);
				this.isJustPlacedOnStand = false;
			}else{
				this.updateCapableOfKomaOnStand(lastMove);
			}
		}
		
		return ret;
	}
	
	
	protected void updateCapableOfKomaOnStand(Move lastMove){
		
		if(lastMove == null){
			// nullが渡されたら駒の情報を新規更新
			this.clearReference(); // TODO: たぶん省ける
			for(Masu placableMasu : this.shogi.board.placable){
				if(this.mustPromoteOn(placableMasu)==false){
					this.addMovable(placableMasu);
					this.reachable.addIfNot(placableMasu);
					placableMasu.register(this);
				}
			}
		}else{
			Masu ms = null;
			ms = this.shogi.board.getMasu(lastMove.from);
			if(ms!=null){
				if(this.mustPromoteOn(ms)==false){
					this.addMovable(ms);
					this.reachable.addIfNot(ms);
					ms.register(this);
				}
			}
			ms = this.shogi.board.getMasu(lastMove.to);
			// lastMoveが駒がない升に動く場合。（captureする駒があるってことは以前そこに駒があったってこと。
			// つまり、this　駒は以前からその駒へは動けなかったってこと
			if(ms!=null && lastMove.komaToCapture == Koma.UK){
				ms.unregister(this);
				this.removeMovable(ms);
				this.reachable.remove(ms);
			}
		}
	}
	
	
	protected void updateCapableOfKomaOnBoard(){
		int file = Board.getFile(this.masu.square);
		int rank = Board.getRank(this.masu.square);
		int i;
		Masu ms = null;
		// 3. filling
		for (this.throughCounter = 0, i = 1; i <= scope.t; i++) {
			try{
				ms = this.shogi.board.matrix[(file*10) + (rank + (i * (this.td)))];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.d; i++) {
			try{
				ms = this.shogi.board.matrix[(file*10) + (rank + (i * (this.dd)))];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.r; i++) {
			try{
				ms = this.shogi.board.matrix[((file + (i * this.rd))*10) + rank];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.l; i++) {
			try{
				ms = this.shogi.board.matrix[((file + (i * this.ld))*10) + rank];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.tr; i++) {
			try{
				ms = this.shogi.board.matrix[((file + (i * (this.rd)))*10) + (rank + (i * (this.td)))];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.tl; i++) {
			try{
				ms = this.shogi.board.matrix[((file + (i * (this.ld)))*10) + (rank + (i * (this.td)))];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.dr; i++) {
			try{
				ms = this.shogi.board.matrix[((file + (i * (this.rd)))*10) + (rank + (i * (this.dd)))];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.dl; i++) {
			try{
				ms = this.shogi.board.matrix[((file + (i * (this.ld)))*10) + (rank + (i * (this.dd)))];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.ttr; i++) {
			try{
				ms = this.shogi.board.matrix[((file + (1 * (this.rd)))*10) + (rank + (2 * (this.td)))];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
		for (this.throughCounter = 0, i = 1; i <= scope.ttl; i++) {
			try{
				ms = this.shogi.board.matrix[((file + (1 * (this.ld)))*10) + (rank + (2 * (this.td)))];
			}catch(Exception e){
				ms = null;
			}
			if (this.updateCapable(ms) == Shogi.LOOP_BREAK) break;
		}
	}
	
	public boolean isMoreCrew(Masu aimedMasu){
		boolean ret = false;
		if(aimedMasu.directRefered.contains(this)){
			if( 	aimedMasu.countDirectlyReferredBy(this.opponent)+
					aimedMasu.countIndirectlyReferredBy(this.opponent) <
					aimedMasu.countDirectlyReferredBy(this.order)+
					aimedMasu.countIndirectlyReferredBy(this.order)){
				ret = true;
			}
		}
		return ret;
	}
	
	public boolean isWorthToExchange(Masu aimedMasu){
		boolean isWorthToExchange = false;
		if(aimedMasu.isKoma(this.opponent) && 
				Koma.getUnpromotedPiece(aimedMasu.koma.getKindId()) >= 
				Koma.getUnpromotedPiece(this.getKindId()) ){
			// Evaluatorに得点を聞く必要はない。kindIdは弱いもの順だから。
			isWorthToExchange = true;
		}
		return isWorthToExchange;
	}
	
	public boolean isBetterTakeHimDown(Masu aimedMasu){
		boolean ret = false;
		int max_kindId = Koma.UK;
		if(aimedMasu.isKoma(this.opponent)){
			for(Koma aimedCompanion : aimedMasu.koma.getReachableKoma(this.order)){
				if(aimedCompanion.getKindId() > max_kindId) max_kindId = aimedCompanion.getKindId();
			}
			// max_kindIdはaimedMasuにいる相手が次にとれる味方のなかで一番強い駒のkindIdをさしている
			// その駒が自分より価値が高い駒なら、その相手をとってしまおう
			if(max_kindId > this.kindId){
				ret = true;
			}
		}
		return ret;
	}
	
	public boolean isProfitable(Masu aimedMasu){
		boolean ret = false;
		if(this.isOnBoard()){
			ret = (this.isMoreCrew(aimedMasu) || this.isWorthToExchange(aimedMasu));
		}else if(this.isOnStand()){
			// この升に打ち込むことに利益はあるのか？
			if(this.isPlacable(aimedMasu)){ 
				if(aimedMasu.isDirectlyReferredBy(this.opponent)==false){// 取られる心配がないので、
					ret = true;
				}else{ 
					if(aimedMasu.getBattleResultInitiatedBy(this.opponent)+ this.kindId <= 0){
						//この駒を打ったとして、相手から仕掛けてきた（打った駒をすぐ取られた場合の、差を調べて、
						//マイナスだったら相手にとって不利。よって、打ち込むことに意味がある
						ret = true;
					}
				}
			}
		}
		return ret;
	}
	
	public boolean canMoveTo(Masu toMasu){
		return this.movable.contains(toMasu);
	}
	
	private void updateInterfered(){
		// isOnStand()の場合下の条件のはinterferedは作れないでしょ。
		if(this.isOnBoard()){
			for(Masu reachableMasu : this.reachable){
				// その升に敵がいる状態で、この駒がそこに行くことに得のあるか
				// そこに成れるか
				
				if((reachableMasu.isKoma(this.opponent) && this.isProfitable(reachableMasu) )|| 
				   this.canBePromotedOn(reachableMasu)
				) {
					this.shogi.board.interfered.addIfNot(reachableMasu);
				}
			}
		}
		
	}
	
	/* methods　that checks the masu */
	public ArrayListS<Koma> getDirectlyReferred() {
		return this.masu.directRefered;
	}

	public ArrayListS<Koma> getIndirectlyReferred() {
		if(this.isOnBoard()){
			return this.masu.indirectRefered;
		}
		return new ArrayListS<Koma>();
	}

	public ArrayListS<Koma> getIndirectlyReferred(int order) {
		if(this.isOnBoard()){
			return this.masu.getDirectlyReferredBy(order);
		}
		return new ArrayListS<Koma>();
	}
	
	public ArrayListS<Koma> getDirectlyReferredByOpponent() {
		if(this.isOnBoard()){
			return this.masu.getDirectlyReferredBy(this.opponent);
		}
		return new ArrayListS<Koma>();
	}

	public ArrayListS<Koma> getDirectlyReferredByCompanion() {
		if(this.isOnBoard()){
			return this.masu.getDirectlyReferredBy(this.order);
		}
		return new ArrayListS<Koma>();
	}

	public ArrayListS<Koma> getIndirectlyReferredByOpponent() {
		if(this.isOnBoard()){
			return this.masu.getIndirectlyReferredBy(this.opponent);
		}
		return new ArrayListS<Koma>();
	}

	public ArrayListS<Koma> getIndirectlyReferredByCompanion() {
		if(this.isOnBoard()){
			return this.masu.getIndirectlyReferredBy(this.order);
		}
		return new ArrayListS<Koma>();
	}

	public boolean isDirectlyReferredBy(Koma k) {
		boolean ret = false;
		if (this.isOnBoard()) {
			if(this.masu.getDirectlyReferredBy(k.getOrder()).contains(k)) ret = true;
		}
		return ret;
	}
	
	public boolean isDirectlyReferredBy(int order) {
		boolean ret = false;
		if (this.isOnBoard()) {
			ret = this.masu.isDirectlyReferredBy(order);
		}
		return ret;
	}
	
	public boolean isDirectlyReferredByOpponent() {
		boolean ret = false;
		if (this.isOnBoard()) {
			ret = this.masu.isDirectlyReferredBy(this.opponent);
		}
		return ret;
	}

	public boolean isDirectlyReferredByCompanion() {
		boolean ret = false;
		if (this.isOnBoard()) {
			ret = this.masu.isDirectlyReferredBy(this.order);
		}
		return ret;
	}

	public int countDirectlyReferred(int order) {
		int cnt = 0;
		if (this.isOnBoard()) {
			cnt = this.masu.countDirectlyReferredBy(order);
		}
		return cnt;
	}

	public int countDirectlyReferredOpponent() {
		int cnt = 0;
		if (this.isOnBoard()) {
			cnt = this.masu.countDirectlyReferredBy(this.opponent);
		}
		return cnt;
	}

	public int countDirectlyReferredCompanion() {
		int cnt = 0;
		if (this.isOnBoard()) {
			cnt = this.masu.countDirectlyReferredBy(this.order);
		}
		return cnt;
	}
	
	public int countDirectlyReferredBy(int order) {
		int cnt = 0;
		if (this.isOnBoard()) {
			cnt = this.masu.countDirectlyReferredBy(order);
		}
		return cnt;
	}

	public boolean isIndirectlyReferredBy(int order) {
		boolean ret = false;
		if (this.isOnBoard()) {
			ret = this.masu.isIndirectlyReferredBy(order);
		}
		return ret;
	}
	
	public boolean isIndirectlyReferredByOpponent() {
		boolean ret = false;
		if (this.isOnBoard()) {
			ret = this.masu.isIndirectlyReferredBy(this.opponent);
		}
		return ret;
	}

	public boolean isIndirectlyReferredByCompanion() {
		boolean ret = false;
		if (this.isOnBoard()) {
			ret = this.masu.isIndirectlyReferredBy(this.order);
		}
		return ret;
	}

	public int countIndirectlyReferred(int order) {
		int cnt = 0;
		if (this.isOnBoard()) {
			cnt = this.masu.countIndirectlyReferredBy(order);
		}
		return cnt;
	}

	/*
	 * whether this koma can be promoted at the given masu. if this koma is
	 * already promoted, return false.
	 */
	public final boolean canBePromotedOn(Masu theMasu) {

		boolean ret = false;
		if(this.isOnBoard()){
			if (this.amIPromotable() && this.isUnpromoted()) {
				/*
				if (Board.isOpponentArea(this.order, this.masu.square)) {
					// whereever, you are moving, you could promted as you wish.
					ret = true;
				} else {
					if (Board.isOpponentArea(this.order, theMasu.square)) {
						ret = true;
					}
				}
				*/
				// TODO: take the faster process above or below??
				if( this.masu.isOpponentTeritoryOf(this) ){
					ret = true;
				}else{
					if( theMasu.isOpponentTeritoryOf(this) ){
						ret = true;
					}
				}
			}
		}
		
		return ret;
	}
	
		/*
	 if in movable, whether you can be promoted, return yes.
	 TODO: これはupdateStatusのときに、よんじゃえばいい。
	 */
	/*
	 * この関数でいっきに補助的な情報をアップデートする。
	 * この関数は駒がupdateされるときのみ呼ばれるべき。
	 */
	private void updateSupplementaryInfo(){
		
		if(this.isOnBoard()){
			// following supplementary info is only related to komas on board.
			this.canBePromotedInNextHand = false;
			this.canBePromotedInNextHandForFree = false;
			this.canTakeOpponentInNextHandForFree = false;
			
			for(Masu ms : this.movable){
				if(this.canBePromotedOn(ms)){
					this.canBePromotedInNextHand = true;
					
					if(ms.isDirectlyReferredBy(this.opponent)==false){
						this.canBePromotedInNextHandForFree = true;
					}
				}
				
				if(ms.isKoma(this.opponent)){
					if(ms.isDirectlyReferredBy(this.opponent)==false){
						this.canTakeOpponentInNextHandForFree = true;
					}
				}
			}
			
			this.canBeTakenForFree = false;
			if(this.isOnBoard()){
				if( this.isDirectlyReferredByOpponent() && 
				   !this.isDirectlyReferredByCompanion() ){
					this.canBeTakenForFree = true;
				}
			}
		
		}
		
	}
	
	public boolean canBePromotedInNextHand(){
		return this.canBePromotedInNextHand;
	}
		
	public boolean canBePromotedInNextHandForFree(){
		return this.canBePromotedInNextHandForFree;
	}
	
	
	public boolean canTakeOpponentInNextHandForFree(){
		return this.canTakeOpponentInNextHandForFree;
	}
	
	
	public boolean canBeTakenForFree(){
		return this.canBeTakenForFree;
	}
	
	/*
	return true if the koma can be moved to the given masu.
	 */
	public boolean isPlacable(Masu ms){
		// TODO: 下の方法はダメだったんだっけ？
		// return this.movable.contains(ms);
		
		for(Move m : this.moves){
			if(m.to == ms.square) return true;
		}
		return false;
	}
	
	public boolean isCurrentMovedKoma(){
		Move move = this.shogi.kifu.getCurrentMove();
		if(this.isOnBoard()){
			if(move.to == this.masu.square){
				return true;
			}
		}
		return false;
	}
	
	
	/*
	 * within the movable place, search companions and put them in a list and
	 * return the list.
	 */
	public ArrayListS<Koma> getProtectingKomas() {
		return protectingKoma_now;
	}
	public ArrayListS<Koma> getPreviousProtectingKomas() {
		return protectingKoma_pre;
	}

	/*
	 * Whether this koma is mated. if this koma is ou and return true, it's so
	 * called, check mate.
	 */
	public boolean isCheckmated() {
		boolean ret = true;

		// am I on board?
		if (this.isOnBoard()==false) {
			ret = true;
			return ret;
		}

		// am I threaten?
		if (this.masu.isDirectlyReferredBy(this.opponent)==false) {
			// no, nobody is attacking.
			ret = false;
			return ret;
		}

		// can I escape?
		for (Masu ms : this.movable) {
			if (!ms.isDirectlyReferredBy(this.opponent)) {
				// mostly, it can escape,
				// but need to check a special case.
				// where a sniper is threating me, and where I escape is
				// still scope of the sniper. ( although, info says it's not
				// directRefered )
				for (Koma sniper : this.masu.directRefered) {
					if (sniper.order == this.opponent) {
						if (!ms.indirectRefered.contains(sniper)) {

							ret = false;
							return ret;
						}
					}
				}
			}
		}

		// can my companion take the threat? and still am I safe?
		for (Koma threat : this.getDirectlyReferredByOpponent()) {
			for (Koma guard : threat.getDirectlyReferredByOpponent()) {
				// guard is this companion and can take the threat.
				// 王手している敵がその1人なら、guardがそれを取って事なきを得る
				int referenceByOpponent = this.countDirectlyReferredBy(this.opponent);
				if(referenceByOpponent==1 ){
					// しかしながら、guardが素抜かれる可能性もあるのでそれを確認しないといけない
					if(this.isIndirectlyReferredByOpponent()){
						for(Koma sniper : guard.getOpenedSnipersByMovingTo(threat.masu, this.opponent)){
							if(sniper.throughable.contains(this)){
								//　guardにとって、このthreatを取ることはsniperにthisへの道を空けることになる。
								
							}else{ // そうでなければ、threatは取れる
								ret = false;
								return ret;
							}
						}
					}
				}else if(referenceByOpponent >= 2 ){
					// 二つ以上の駒に王手を掛けられているときは、
					// 自分で、片方の駒をとって、かつ動きがもう片方のthreatをかわす動きでないと詰む
					if(threat.getDirectlyReferredByOpponent().contains(this)){ // 私はthreat取れる
						if(threat.isDirectlyReferredByOpponent()==false){
							ret = false;
							return ret;
						}
					}
				}
			}
		}
		
		return ret;
	}

	public Koma getKomaBehindWall(Koma wallKoma) {
		return null;
	}
	
	public Koma getWallKomaOnTheDirectionOf(int square) {
		return null;
	}
	
	public final boolean isPromoted() {
		return Koma.isPromoted(this.kindId);
	}

	public final boolean isUnpromoted() {
		return Koma.isUnpromoted(this.kindId);
	}

	// override in KI, OU to return false
	public boolean amIPromotable() {
		return true;
	}

	public boolean isThroughable() {
		if (this.throughNumber > 0)
			return true;
		else
			return false;
	}

	public final boolean isOnBoard() {
		return (this.masu != null) ? true : false;
	}

	public final boolean isOnStand() {
		return (this.stand != null) ? true : false;
	}

	public boolean isInBox() {
		return (this.stand == null && this.masu == null) ? true : false;
	}
	public boolean isNotUsed() {
		return this.isInBox();
	}
	
	// for your color, are you on left side
	// border by the given file?( includes the file )
	// e.g. if you are BLACK, file = 5 is given,
	// if you are on file 2, return false;
	// if you are on file 9, return true;
	// if you are on file 5, return true;
	public boolean isOnLefter(int file){
		boolean ret = false;
		if(this.isOnBoard()){
			int myFile = Board.getFile(this.masu.square);
			if(this.order == Shogi.BLACK){
				if(myFile - file >= 0) ret = true;
			}else if(this.order == Shogi.WHITE){
				if(myFile - file < 0) ret = true;
			}
		}
		return ret;
	}

	/*
	fromの升から一手でtoにいけるならtrue
	 */
	public boolean canMoveInOneHand(Masu from, Masu to){
		boolean ret = false;
		//　升はどっちも空升でないといけないよ
		if(from.isKoma() == false && to.isKoma() == false){
			if(this.getHandsToReach(from.square, to.square)==1){
				ret = true;
			}
		}
		return ret;
	}
	
	/*
	 * Fu msut be promoted on the edge no matter what. if the koma has the
	 * constraint, override this method.
	 */
	public boolean mustPromoteOn(Masu ms) {
		boolean ret = false;
		return ret;
	}
	public boolean mustPromoteOn(int sq) {
		boolean ret = false;
		return ret;
	}

	/************************************************************************************
			getter and setter
	************************************************************************************/
	public int getOrder() {
		return this.order;
	}

	public int getKindId() {
		return this.kindId;
	}

	public Masu getMasu(){
		return this.masu;
	}
	
	public Stand getStand(){
		return this.stand;
	}
	
	public int getSquare() {
		int sq = -1;
		if (this.isOnBoard()) {
			sq = this.masu.square;
		} else if (this.isOnStand()) {
			sq = this.stand.getSquare();
		} 
		return sq;
	}

	public ArrayListS<Koma> getWallFor(){
		return this.wallFor;
	}
	
	public ArrayListS<Koma> getWallFor(int order){
		ArrayListS<Koma> snipers = new ArrayListS<Koma>();
		for(Koma sniper : this.wallFor){
			if(sniper.getOrder() == order){
				snipers.add(sniper);
			}
		}
		return snipers;
	}
	
	public ArrayListS<Koma> getWalls(){
		return this.walls;
	}
	
	public ArrayListS<Koma> getWalls(int order){
		ArrayListS<Koma> wallKomas = new ArrayListS<Koma>();
		for(Koma wallKoma : this.walls){
			if(wallKoma.getOrder() == order){
				wallKomas.add(wallKoma);
			}
		}
		return wallKomas;
	}
	
	public ArrayListS<Masu> getMovable(){
		return this.movable;
	}
	public ArrayListS<Move> getMoves(){
		return this.moves;
	}
	public ArrayListS<Masu> getReachable(){
		return this.reachable;
	}
	public ArrayListS<Masu> getThroughable(){
		return this.throughable;
	}
	public ArrayListS<Koma> getReachableKoma(){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(Masu reachbleMasu : this.reachable){
			if(reachbleMasu.isKoma()){
				komas.add(reachbleMasu.koma);
			}
		}
		return komas;
	}
	public ArrayListS<Koma> getReachableKoma(int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(Masu reachbleMasu : this.reachable){
			if(reachbleMasu.isKoma(order)){
				komas.add(reachbleMasu.koma);
			}
		}
		return komas;
	}
	public ArrayListS<Koma> getThroughableKoma(){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(Masu throuableMasu : this.throughable){
			if(throuableMasu.isKoma()){
				komas.add(throuableMasu.koma);
			}
		}
		return komas;
	}
	public ArrayListS<Koma> getThroughableKoma(int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(Masu throuableMasu : this.throughable){
			if(throuableMasu.isKoma(order)){
				komas.add(throuableMasu.koma);
			}
		}
		return komas;
	}
	
	public int getHandsToReach(int dstSquare) {
		// override it in subclass.
		// square is the destination square.
		// return -1, if it's impossible to reach
		// disregards all obstacles to the destination
		return -1;
	}

	public int getHandsToReach(int from, int to) {
		// override it in subclass.
		return -1;
	}

	
	public ArrayListS<Masu> getMasuWhereYouNeedToBeFor(Masu aimedMasu, boolean inSequence, boolean includeFirstOpponent) {
		ArrayListS<Masu> masus = new ArrayListS<Masu>();
		int i;
		int file = Board.getFile(aimedMasu.square);
		int rank = Board.getRank(aimedMasu.square);
		// 動ける方向と逆に動いた位置からなら、aimedMasuに動けると考える
		for (i = 1; i <= scope.t; i++) {
			if( this.addMasuWhereYouNeedToBe(file, rank + ((i*(-1)) * (this.td)), inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.d; i++) {
			if( this.addMasuWhereYouNeedToBe(file, rank + ((i*(-1)) * (this.dd)), inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.r; i++) {
			if( this.addMasuWhereYouNeedToBe(file + ((i*(-1)) * this.rd), rank, inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.l; i++) {
			if( this.addMasuWhereYouNeedToBe(file + ((i*(-1)) * this.ld), rank, inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.tr; i++) {
			if( this.addMasuWhereYouNeedToBe(file + ((i*(-1)) * this.rd), rank + ((i*(-1)) * (this.td)), inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.tl; i++) {
			if( this.addMasuWhereYouNeedToBe(file + ((i*(-1)) * this.ld), rank + ((i*(-1)) * (this.td)), inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.dr; i++) {
			if( this.addMasuWhereYouNeedToBe(file + ((i*(-1)) * this.rd), rank + ((i*(-1)) * (this.dd)), inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.dl; i++) {
			if( this.addMasuWhereYouNeedToBe(file + ((i*(-1)) * this.ld), rank + ((i*(-1)) * (this.dd)), inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.ttr; i++) {
			if( this.addMasuWhereYouNeedToBe(file + ( (1*(i*(-1))) * (this.rd)), rank + ((2*(i*(-1))) * (this.td)), inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		for (i = 1; i <= scope.ttl; i++) {
			if( this.addMasuWhereYouNeedToBe(file + ((1*(i*(-1))) * (this.ld)), rank + ((2*(i*(-1))) * (this.td)), inSequence, includeFirstOpponent, masus) == Shogi.LOOP_BREAK) break;
		}
		return masus;
	}
	
	private int addMasuWhereYouNeedToBe(int file, int rank, boolean inSequence, boolean includeFirstOpponent,ArrayListS<Masu> masus){
		int ret = Shogi.LOOP_CONTINUE;
		Masu ms = this.shogi.board.getMasu(file, rank);
		if(ms != null){
			if(inSequence == false){
				masus.add(ms);
			}else{
				if(ms.isKoma() == false){
					masus.add(ms);
				}else if(ms.isKoma(this.order)){
					// you can not either move nor place on that masu, so break, however,
					// if you are the koma you are hitting is yourself, ignore.
					if(ms.koma == this){
						// but you can't move this masu anyway
					}else{
						ret = Shogi.LOOP_BREAK;
					}
				}else if( ms.isKoma(this.opponent)){ // 相手がいる升
					if(includeFirstOpponent){
						masus.add(ms);
					}
					ret = Shogi.LOOP_BREAK;
				}
			}
		}else{
			ret = Shogi.LOOP_BREAK;
		}
		return ret;
	}
	
	public ArrayListS<Move> getMovesCanGoTo(Masu aimedMasu){
		ArrayListS<Move> move_list = new ArrayListS<Move>();
		for(Move m : this.moves){
			if(m.to == aimedMasu.square){
				//　既に生成されたMoveを使うことで、ObjectのIDで比較するcontainsを成り立たせる。
				// 同時にメモリの節約にも少し効果がある。
				// 既に、成と歩成のMoveが生成されいる。　FU, HI, KAに関しては成れるところでは
				// なったほうがいいに決まっているので、その挙動だけoverrideする。
				move_list.addIfNot(m);
			}
		}
		return move_list;
	}
	
	public ArrayListS<Move> getMovesThatCanTakeMe(){
		ArrayListS<Move> moves = new ArrayListS<Move>();
		for(Koma opponentThreat : this.getDirectlyReferredByOpponent()){
			moves.addIfNot(opponentThreat.getMovesCanGoTo(this.masu));
		}
		return moves;
	}
	
	/*
	aimedMasuに利きをつける手を生成する。
	しかし、amiedMasuに自分がいたら、何もしないで帰りなさい。
	 */
	public ArrayListS<Move> getPrecedingMovesFor(Masu aimedMasu) {
		ArrayListS<Move> move_list = new ArrayListS<Move>();
		if(this.isOnBoard()){
			// throuable can reach there by moving wall koma, add the move of wall.
			for(Koma sniper : aimedMasu.getIndirectlyReferredBy(order)){ // 味方のsniperで
				for(Koma wall : sniper.getWalls(order)){ // 味方が壁になっていて
					// 壁を取る動きができれば
					if(wall == this){
						if(sniper.getDirection(this)==sniper.getDirection(aimedMasu)){ // 目的の升と同じ方向にあるなら、壁をとることで、そこにsniperが到達できると言える。
							for(Move m : wall.getMoves()){
								Masu ms = this.shogi.board.getMasu(m.to);
								if(this.getOpenedSnipersByMovingTo(ms, order).contains(sniper)){
									move_list.addAll(this.getMovesCanGoTo(ms));
								}
							}
						}
					}
				}
			}
			// もしすでに、この駒がaimedMasuにmovableなら、検索する必要なし。それを確認
			if(this.movable.contains(aimedMasu)==false){
				//if(aimedMasu.koma != this){ // 自分を動かして、自分のいる所に利きをつけるのはなし。
				// TBD: 何故か上の条件は成立していなかった。参照の駒と実態の駒のidは違うのか？
				if(aimedMasu.square != this.getSquare()){
					for(Masu ms : getMasuWhereYouNeedToBeFor(aimedMasu, true, true)){
						if(ms.getDirectlyReferredBy(order).contains(this)){
							for(Move m : this.getMovesCanGoTo(ms)){
								move_list.addIfNot(m);
							}
							
						}
					}
				}else{
					int debugging_purpose = 0; 
				}
			}
		}else if(this.isOnStand()){
			if(aimedMasu.square != this.getSquare()){
				for(Masu ms : getMasuWhereYouNeedToBeFor(aimedMasu, true, false)){
					if(ms.isKoma() == false){ // just to make sure
						move_list.addAll(this.getMovesCanGoTo(ms));
					}
				}
			}
		}
		return move_list;
	}
	
	// genereate next move for coming to the give square.
	// if you can reach there in the next move, of course the
	// Move is returned.
	// otherwise, return the move, which leads you closer to the destination
	// give as dstSquare.
	public Move getMoveToGetClose(int dstSquare) {
		Move move = null;
		int min = Board.MAX_DISTANCE + 1; // set max value to min.
		if (this.getHandsToReach(dstSquare) != -1) {
			for (Move m : this.moves) {
				int hands = this.getHandsToReach(m.to, dstSquare);
				if (hands < min) {
					// EXT.
					// 現状、同じ距離のMoveは最後にヒットしたものを採用しているが、
					// 条件のよって動かしかたを帰れると粋
					min = hands;
					move = m;
				}
			}
		}
		return move;
	}

	/*
	if this koma is wall for a sniper.
	then return the snipers if this koma moving to the give Masu causes
	to open a path for snipers.
	 */
	public ArrayListS<Koma> getOpenedSnipersByMovingTo(Masu masuTo, int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(Koma sniper : this.getWallFor(order)){
			if(masuTo.getDirectlyReferredBy(order).contains(sniper) ||
			   masuTo.getIndirectlyReferredBy(order).contains(sniper)){
				// sniperのpath上に近づく手、or　遠のく手なので、openしない。
			}else{
				komas.addIfNot(sniper);
			}
		}
		return komas;
	}
	
	public int getDirection(Koma dstKoma){
		int direction = Board.DIRECTION_NONE;
		if(dstKoma.isOnBoard()){
			direction = this.getDirection(dstKoma.masu);
		}
		return direction;
	}
	
	public int getDirection(Masu dstMasu){
		int direction = Board.DIRECTION_NONE;
		if(this.isOnBoard() && dstMasu != null){
			direction = Board.getDirection(this.order, this.getSquare(), dstMasu.square);
		}
		return direction;
	}
	
	public String toString() {

		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);

		ps.printf("%s%s(%2d)"
				, Shogi.getOrderName(this.order)
				, Koma.getKomaName(this.kindId)
				, this.getSquare()
		);
		
		return buff.toString();
	}
	
	public String toVerboseString() {

		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		str += this.toString();
		str += ": ";
		str += "movable: ";
		for (Masu ms : this.movable) {

			buff.reset();
			ps.printf("[%02d] ", ms.square);
			str += buff.toString();
			// check if this is 1st file.
		}
		return str;
	}
	
	/*
	 * *********************************************************************
	 * static methods
	 * *********************************************************************
	 */
	public final static KomaComparator comparatorAsc = new KomaComparatorAsc();
	public final static KomaComparator comparatorDesc = new KomaComparatorDesc();

	public final static int UK = 0x0000; // UnKnown
	public final static int FU = 0x0001;
	public final static int KY = 0x0002;
	public final static int KE = 0x0003;
	public final static int GI = 0x0004;
	public final static int KI = 0x0005;
	public final static int KA = 0x0006;
	public final static int HI = 0x0007;
	public final static int OU = 0x0008;
	public final static int PROMOTE = 8;
	public final static int TO = FU + PROMOTE;
	public final static int NY = KY + PROMOTE;
	public final static int NK = KE + PROMOTE;
	public final static int NG = GI + PROMOTE;
	public final static int KN = KI + PROMOTE; // Kin Nari
	public final static int UM = KA + PROMOTE;
	public final static int RY = HI + PROMOTE;
	// ///////////////////// OU
	public final static int [] BASIC_KIND_IDS = {Koma.FU, Koma.KY, Koma.KE, Koma.GI, Koma.KI, Koma.KA, Koma.HI, Koma.OU};
	
	public final static int TOTAL_PIECES = 40; // number of koma kind.
	public final static int NUM_KIND = 8; // number of koma kind.

	public final static int FLIP_UNKNOWN = -1;
	public final static int FLIP_NO = 0x0000;
	public final static int FLIP_YES = 0x0001;
	
	

	public static String getFlipType(int promote) {
		String ret = "";
		if (promote == FLIP_UNKNOWN)
			ret = "UNKNOWN";
		else if (promote == FLIP_YES)
			ret = "Flip";
		else if (promote == FLIP_NO)
			ret = "----";
		return ret;
	}

	public static boolean isValid(int kind) {
		boolean ret = false;
		if (kind >= FU && kind <= RY) {
			if (kind != KN)
				ret = true;
		}
		return ret;
	}

	public static boolean isPromoted(int kindId) {
		// isValid time consumeing and it's not neccessary to judge if the kind
		// is valid or not.
		// for example, if the invalid kindId 24 is passed.
		// 24-PROMOTE = 18 and this returns true.
		// but, who cares. something must have gone wrong in the prior execution.
		return (kindId - PROMOTE > 0) ? true : false;
	}
	
	public static boolean isUnpromoted(int kindId) {
		// isValid time consumeing and it's not neccessary to judge if the kind
		// is valid or not.
		// for example, if the invalid kindId 24 is passed.
		// 24-PROMOTE = 18 and this returns true.
		// but, who cares. something must have gone wrong in the prior execution.
		return (kindId - PROMOTE > 0) ? false : true;
	}

	public static int getPromotedPiece(int kindId) {

		int ret = Koma.UK;
		if (kindId == Koma.FU)
			ret = Koma.TO;
		else if (kindId == Koma.KY)
			ret = Koma.NY;
		else if (kindId == Koma.KE)
			ret = Koma.NK;
		else if (kindId == Koma.GI)
			ret = Koma.NG;
		else if (kindId == Koma.KA)
			ret = Koma.UM;
		else if (kindId == Koma.HI)
			ret = Koma.RY;
		return ret;
	}

	public static int getUnpromotedPiece(int kindId) {

		int ret = Koma.UK;
		if (Koma.isPromoted(kindId)) {
			if (kindId == Koma.TO)
				ret = Koma.FU;
			else if (kindId == Koma.NY)
				ret = Koma.KY;
			else if (kindId == Koma.NK)
				ret = Koma.KE;
			else if (kindId == Koma.NG)
				ret = Koma.GI;
			else if (kindId == Koma.UM)
				ret = Koma.KA;
			else if (kindId == Koma.RY)
				ret = Koma.HI;
		} else {
			ret = kindId;
		}
		return ret;
	}

	public static String getKomaName(int kindId) {
		String str = "";
		if (kindId == Koma.FU)
			str = "FU";
		else if (kindId == Koma.KY)
			str = "KY";
		else if (kindId == Koma.KE)
			str = "KE";
		else if (kindId == Koma.GI)
			str = "GI";
		else if (kindId == Koma.KI)
			str = "KI";
		else if (kindId == Koma.KA)
			str = "KA";
		else if (kindId == Koma.HI)
			str = "HI";
		else if (kindId == Koma.OU)
			str = "OU";
		else if (kindId == Koma.TO)
			str = "TO";
		else if (kindId == Koma.NY)
			str = "NY";
		else if (kindId == Koma.NK)
			str = "NK";
		else if (kindId == Koma.NG)
			str = "NG";
		else if (kindId == Koma.UM)
			str = "UM";
		else if (kindId == Koma.RY)
			str = "RY";

		else if (kindId == Koma.UK)
			str = "UK";
		else
			str = "??";
		return str;
	}
	
	// expressing all in one character. used when generating hashcode.
	public static String getKomaNameTrimed(int kindId){
		String str = "";
		if (kindId == Koma.FU)  			str = "F";
		else if (kindId == Koma.KY)			str = "Y";
		else if (kindId == Koma.KE)			str = "E";
		else if (kindId == Koma.GI)			str = "G";
		else if (kindId == Koma.KI)			str = "I";
		else if (kindId == Koma.KA)			str = "A";
		else if (kindId == Koma.HI)			str = "H";
		else if (kindId == Koma.OU)			str = "O";
		else if (kindId == Koma.TO)			str = "T";
		else if (kindId == Koma.NY)			str = "Z"; // Z is next char of Y
		else if (kindId == Koma.NK)			str = "L"; // L is next char of K
		else if (kindId == Koma.NG)			str = "J"; // J is 3 chars forward from G(since H and I are already in use)
		else if (kindId == Koma.UM)			str = "U";
		else if (kindId == Koma.RY)			str = "R";
		else if (kindId == Koma.UK)			str = "X";
		else                     			str = "?";
		return str;
	}
	
	public static String getKomaNameJP(int kindId) {
		String str = "";
		if (kindId == Koma.FU)
			str = "歩";
		else if (kindId == Koma.KY)
			str = "香";
		else if (kindId == Koma.KE)
			str = "桂";
		else if (kindId == Koma.GI)
			str = "銀";
		else if (kindId == Koma.KI)
			str = "金";
		else if (kindId == Koma.KA)
			str = "角";
		else if (kindId == Koma.HI)
			str = "飛";
		else if (kindId == Koma.OU)
			str = "王";
		else if (kindId == Koma.TO)
			str = "と";
		else if (kindId == Koma.NY)
			str = "糸";
		else if (kindId == Koma.NK)
			str = "今";
		else if (kindId == Koma.NG)
			str = "全";
		else if (kindId == Koma.UM)
			str = "馬";
		else if (kindId == Koma.RY)
			str = "竜";
		else
			str = "??";
		return str;
	}

	public static int getKomaKind(String komaName) {

		int kind = Koma.UK;
		if (komaName.equals("FU"))
			kind = Koma.FU;
		else if (komaName.equals("KY"))
			kind = Koma.KY;
		else if (komaName.equals("KE"))
			kind = Koma.KE;
		else if (komaName.equals("GI"))
			kind = Koma.GI;
		else if (komaName.equals("KI"))
			kind = Koma.KI;
		else if (komaName.equals("KA"))
			kind = Koma.KA;
		else if (komaName.equals("HI"))
			kind = Koma.HI;
		else if (komaName.equals("OU"))
			kind = Koma.OU;
		else if (komaName.equals("TO"))
			kind = Koma.TO;
		else if (komaName.equals("NY"))
			kind = Koma.NY;
		else if (komaName.equals("NK"))
			kind = Koma.NK;
		else if (komaName.equals("NG"))
			kind = Koma.NG;
		else if (komaName.equals("UM"))
			kind = Koma.UM;
		else if (komaName.equals("RY"))
			kind = Koma.RY;
		else
			kind = Koma.UK;
		return kind;
	}

	/*
	 * ni-fu is prohibited.
	 */
	public static boolean isDuplicationProhibited(int kindId) {
		boolean ret = false;
		if (kindId == Koma.FU)
			ret = true;
		return ret;
	}

	public static boolean isTowardForward(int order, int from, int to) {
		boolean ret = false;
		if (order == Shogi.BLACK)
			ret = (Board.getRank(to) - Board.getRank(from)) < 0 ? true : false;
		else if (order == Shogi.WHITE)
			ret = (Board.getRank(to) - Board.getRank(from)) > 0 ? true : false;
		return ret;
	}

	public static boolean isTowardBackward(int order, int from, int to) {
		boolean ret = false;
		if (order == Shogi.BLACK)
			ret = (Board.getRank(to) - Board.getRank(from)) > 0 ? true : false;
		else if (order == Shogi.WHITE)
			ret = (Board.getRank(to) - Board.getRank(from)) < 0 ? true : false;
		return ret;
	}
	
	public static ArrayListS<Integer> getSquaresWhereYouNeedToBeFor(int aimedSquare, int order, boolean isPromoted){
		ArrayListS<Integer> sqs = new ArrayListS<Integer>();
		return sqs;
	}
	
	public static void addIfSquareIsValid(ArrayListS<Integer> sq_list, int square){
		if(Board.isSquareValid(square)){
			sq_list.add(square);
		}
	}
}
