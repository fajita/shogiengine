package com.yeele.game.shogi.engine;

public class KomaScope{

	
	public KomaScope(int t, int d, int r, int l
			          , int tr, int tl, int dr, int dl
			          , int ttr, int ttl){
		this.t = t; // top
		this.d = d; // down
		this.r = r; // right
		this.l = l; // left
		this.tr = tr; 
		this.tl = tl;  
		this.dr = dr;
		this.dl = dl;
		this.ttr = ttr;
		this.ttl = ttl;
	}
	
	public final int t;
	public final int d;
	public final int r;
	public final int l;
	public final int tr;
	public final int tl;
	public final int dr;
	public final int dl;
	public final int ttr;
	public final int ttl;
	
	
	/*
	 *  value +9 : can move infinitely to the direction
	 *  value +1 : can move 1 to the direction
	 *  value  0 : can not move to the direction
	 *  
	 */
	
	/*
	 *  below represents a shogiboard.
	 *  and now, * marked position is where 
	 *  the koma sits.
	 -----------------------------
	 |   |   |   |   |   |   |   |
	 -----------------------------
	 |   |ttl|   |ttr|   |   |   |
	 -----------------------------
	 |   | tl| t | tr|   |   |   |
	 -----------------------------
	 |   | l | * | r |   |   |   |
	 -----------------------------
	 |   | dl| d | dr|   |   |   |
	 -----------------------------
	 |   |   |   |   |   |   |   |
	 -----------------------------
	 |   |   |   |   |   |   |   |
	 -----------------------------
	 |   |   |   |   |   |   |   |
	 */
	
	/*
	 * ttl and ttr is reserved move for keima
	 */
}







