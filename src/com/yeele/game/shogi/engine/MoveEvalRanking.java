package com.yeele.game.shogi.engine;


import java.io.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import com.yeele.util.ArrayListS;



public class MoveEvalRanking extends ArrayListS<MoveEval>{
	
	public static final int ORDER_ASC = 1;
	public static final int ORDER_DESC = 2;
	
	// Array factors are automatically sorted in ascendant order.
	public abstract class EvalComparator implements Comparator<MoveEval> {  
		protected int order;
		
		public EvalComparator(int order){
			this.order = order;
		}
		
	    abstract public int compare(MoveEval a, MoveEval b);
	}
	
	public class EvalComparatorAsc extends EvalComparator {
		
		public EvalComparatorAsc(int order){
			super(order);
		}
		
	    public int compare(MoveEval a, MoveEval b) {
	    	
	    	
	    	int diffA = a.eval.score[this.order] - a.eval.score[Shogi.getOtherOrder(this.order)];
	    	int diffB = b.eval.score[this.order] - b.eval.score[Shogi.getOtherOrder(this.order)];
	    	return diffB - diffA;
	    	
	    	//return b.eval.score[a.move.order] - a.eval.score[a.move.order];
	    }  
	}
	
	// Array factors are automatically sorted in descendant order.
	public class EvalComparatorDesc extends EvalComparator {  
		public EvalComparatorDesc(int order){
			super(order);
		}
		
	    public int compare(MoveEval a, MoveEval b) {
	    	return a.eval.score[a.move.order] - b.eval.score[a.move.order];
	    }  
	}
	
	
	private int size;
	public int order;
	private EvalComparator comparator;
	
	/*
	 * MoveEvaList
	 * list of MoveEval
	 * and it will be sorted by ASC or DESC order.
	 * if ASC is set for sortType and BLACK is set for order,
	 * they will be sorted by best moves for BLACK.
	 */
	public MoveEvalRanking(int size, int sortType, int order){
		
		this.size = size;
		if(sortType == ORDER_ASC){
			this.comparator = new EvalComparatorAsc(order);
		}else if(sortType == ORDER_DESC){
			this.comparator = new EvalComparatorDesc(order);
		}else{
			assert(false);
		}
		this.order = order;
	}
	
	
	
	public boolean add(MoveEval me){

		super.add(me);
		Collections.sort(this, comparator);
		if( this.size() > this.size ){
			this.remove(this.size);	
		}
		
		return true;
	}
	
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		int rank = 1;
		for(Iterator<MoveEval> i = this.iterator(); i.hasNext()==true; rank++){
			buff.reset();
			MoveEval tmp = i.next();
			ps.printf("%2d: %s\n%s\n", rank, tmp.move, tmp.eval);
			str += buff.toString();
		}
		
		return str;
	}
}

