package com.yeele.game.shogi.engine;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;

import com.yeele.util.ArrayListS;


public class Masu implements Subject{
	
	public int square;
	public Koma koma;
	public ArrayListS<Koma> directRefered; // apend all koma refering to this masu regardless of it's order.
	public ArrayListS<Koma> indirectRefered; // like hi, ka, ky can refer you through a koma
	protected int teritoryFor = Shogi.ORDER_UNKNOWN;
	public ArrayListS<Observer> observers;
	
	public Masu(int square){
		this.square = square;
		this.koma = null;
		this.directRefered = new ArrayListS<Koma> ();
		this.indirectRefered = new ArrayListS<Koma> ();
		this.observers = new ArrayListS<Observer>();
	}
	
	public void setTeriotyFor(int order){
		this.teritoryFor = order;
	}
	public int getTeriotyFor(){
		return this.teritoryFor;
	}
	public boolean isTeritoryOf(Koma k){
		return (this.teritoryFor == k.order) ? true : false; 
	}
	public boolean isOpponentTeritoryOf(Koma k){
		return (this.teritoryFor == k.opponent) ? true : false; 
	}
	
	public void reset(){
		this.koma = null;
		this.directRefered.clear();
		this.indirectRefered.clear();
		this.observers.clear();
	}
	/*
	 * fetch the Koma on this Masu if exist.
	 */
	public Koma fetch(){
		Koma k = null;
		
		if(this.koma != null){
			k = this.koma;
			this.koma = null;
		}
		
		return k;
	}
	
	public void clearReference(){
		this.directRefered.clear();
		this.indirectRefered.clear();
	}
	
	/*
	 * whether if the designated order's koma exist or not
	 */
	public final boolean isKoma(int order){
		boolean ret = false;
		if(this.koma != null){
			if(this.koma.order == order) ret = true;
		}
		return ret;
	}
	
	public final boolean isKoma(){
		return (this.koma == null) ? false : true;
	}

	public boolean isEmpty(){
		return (this.koma == null) ? true : false;
	}
	
	public int getSquare(){
		return this.square;
	}
	
	public  ArrayListS<Koma> getDirectlyReferred(){
		return this.directRefered;
	}
	
	public  ArrayListS<Koma> getIndirectlyReferred(){
		return this.indirectRefered;
	}
	
	public  ArrayListS<Koma> getDirectlyReferredBy(int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(Koma koma : this.directRefered){
			if(koma.order == order){
				komas.add(koma);
			}
		}
		return komas;
	}
	
	public  ArrayListS<Koma> getIndirectlyReferredBy(int order){
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		for(Koma koma : this.indirectRefered){
			if(koma.order == order){
				komas.add(koma);
			}
		}
		return komas;
	}
	
	public boolean isDirectlyReferedByBoth(){
		return (this.isDirectlyReferredBy(Shogi.BLACK) &&
				this.isDirectlyReferredBy(Shogi.WHITE)) ? true : false;
	}
	
	public boolean isIndirectlyReferedByBoth(){
		return (this.isIndirectlyReferredBy(Shogi.BLACK) &&
				this.isIndirectlyReferredBy(Shogi.WHITE)) ? true : false;
	}
	
	public boolean isReferedByBoth(){
		return (this.isDirectlyReferedByBoth() ||
				this.isIndirectlyReferedByBoth()) ? true : false;
	}
	
	public boolean isDirectlyReferredBy(int order){
		boolean ret = false;
		for(Koma km : this.directRefered){
			if(km.order == order){
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	public boolean isIndirectlyReferredBy(int order){
		boolean ret = false;
		for(Koma km : this.indirectRefered){
			if(km.order == order){
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	public int countDirectlyReferredBy(int order){
		int cnt = 0;
		for(Koma km : this.directRefered){
			if(km.order == order){
				cnt++;
			}
		}
		return cnt;
	}
	
	public int countIndirectlyReferredBy(int order){
		int cnt = 0;
		for(Koma km : this.indirectRefered){
			if(km.order == order){
				cnt++;
			}
		}
		return cnt;
	}
	
	public boolean canBePromotedForFreeBy(int order){
		boolean ret = false;
		int oppositeOrder = Shogi.getOtherOrder(order);
		boolean canAnybodyPromote = false;
		for(Koma koma : this.getDirectlyReferredBy(order)){
			if(koma.canBePromotedOn(this)){
				canAnybodyPromote = true;
			}
		}
		if( !this.isDirectlyReferredBy(oppositeOrder) && canAnybodyPromote){
			ret = true;
		}
		return ret;
	}
	
	public int getBattleResultInitiatedBy(int order){
		return this.getBattleResultInitiatedBy(order, null, null);
	}
	
	public int getBattleResultInitiatedBy(int order, Evaluator evaluator){
		return this.getBattleResultInitiatedBy(order, evaluator, null);
	}
	
	public int getBattleResultInitiatedBy(Koma initiatingKoma){
		return this.getBattleResultInitiatedBy(initiatingKoma.getOrder(), null, initiatingKoma);
	}
	
	public int getBattleResultInitiatedBy(Koma initiatingKoma, Evaluator evaluator){
		return this.getBattleResultInitiatedBy(initiatingKoma.getOrder(), evaluator, initiatingKoma);
	}
	
	private int getBattleResultInitiatedBy(int order, Evaluator evaluator, Koma initiatingKoma){

		int opponent = Shogi.getOtherOrder(order);
		if(this.isKoma(order)){// 味方がいたら始められない
			return 0;
		}
		
		int score = 0;
		ArrayListS<Koma> [] soldier = new ArrayListS[2];
		int [] orders = {order, opponent};
		
		for(int i=0; i < soldier.length; i++){
			soldier[i] = new ArrayListS<Koma> ();
		}
		
		// filling soldier by order for easy calculation.
		for(Koma km : this.getDirectlyReferred()){
			soldier[km.getOrder()].add(km);
			for(Koma sniper : km.getWallFor(km.getOrder())){
				if(this.indirectRefered.contains(sniper)){
					soldier[km.getOrder()].add(sniper);
				}
			}
		}
		
		// 弱いもん順にならべる　
		Collections.sort(soldier[order], Koma.comparatorDesc);
		Collections.sort(soldier[opponent], Koma.comparatorDesc);
		
		if(initiatingKoma != null){
			// initiatingKomaを先頭にもっていゆく
			if(soldier[order].remove(initiatingKoma)){
				soldier[order].add(0, initiatingKoma);
			}
		}
		
		// initiated orderを基準に考える
		Koma remainedKoma = (this.isKoma(opponent) ? this.koma : null);
		Koma movingKoma = null;
		boolean finish = false;
		for(int i=0; i<soldier[order].size(); i++){
			for( int odr : orders){
				try{
					movingKoma = soldier[odr].get(i);
					if(remainedKoma != null){
						if(evaluator != null){
							score += (remainedKoma.getOrder()==opponent?
									evaluator.getValueOnStand(remainedKoma.getKindId()):
									evaluator.getValueOnStand(remainedKoma.getKindId()) * (-1)
									);
						}else{
							score += (remainedKoma.getOrder()==opponent?
									Koma.getUnpromotedPiece(remainedKoma.getKindId()):
									(Koma.getUnpromotedPiece(remainedKoma.getKindId())*-1));
						}
						
					}
					remainedKoma = movingKoma;
				}catch(IndexOutOfBoundsException e){
					finish = true;
				}
			}
			if(finish) break;
		}
		return score;
	}
	
	
	
	public String toSimpleString(){
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		ps.printf("[%02d]", this.square);
		return buff.toString();
	}
	
	public String toString(){
		
		String str = "";
		int komaKindId = Koma.UK;
		
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		if(this.koma != null){
			komaKindId = this.koma.kindId;
		}
		str += this.toSimpleString() + " ";
	
		str += "Direct: ";
		for(Koma km : this.directRefered){
			
			buff.reset();
			ps.printf("%s ", km);
			str += buff.toString();
			// check if this is 1st file.
		}
		str += "Indirect: ";
		for(Koma km : this.indirectRefered){
			
			buff.reset();
			ps.printf("%s ", km);
			str += buff.toString();
			// check if this is 1st file.
		}
		return str;
	}
	
	/*
	**********************************************************************
	               Subject Interface 
	**********************************************************************
	*/
	public int register(Observer obs){
		int ret = Shogi.SUCCESS;
		try{
			if(!this.observers.contains(obs))
				this.observers.add(obs);
		}catch(Exception o){
			ret = Shogi.ERROR;
		}
		return ret;
	}
	
	public int unregister(Observer obs){
		int ret = Shogi.SUCCESS;
		try{
			this.observers.remove(obs);
		}catch(Exception o){
			ret = Shogi.ERROR;
		}
		return ret;
	}
	
	public void notify(Observer obs) {
		obs.update();
	}
	
	public void notifyObservers() {
		for(int i=0; i<this.observers.size(); i++){
			this.observers.get(i).update();
		}
	}
	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	
	
}



