package com.yeele.game.shogi.engine;


import java.io.*;
import java.util.HashMap;
import java.util.Hashtable;

import com.yeele.util.Tree;


/*
 局面ごとに、TreeOfMoveEvalを保存したい。
 */

public class SceneHashMap<K, V> extends HashMap<K, V>{
	
	final private int maxSize;
	private int sizeToDelete;
	private int size;
	
	public SceneHashMap(int maxSize){
		super(maxSize*4/3); // 初期要領をmaxSizeの4/3に。
		this.maxSize = maxSize;

		this.sizeToDelete = 1;
		if(this.maxSize > 0){
			// 4分の1を一回で消す
			this.sizeToDelete = this.maxSize / 4; 
		}
		if(this.sizeToDelete < 1){
			this.sizeToDelete = 1;
		}
	}
	
	public V put(K key,
		      V value){
		V pre = null;
		// 既に登録されているtreeの方が大きい場合はputしない。
		if(this.containsKey(key)){
			TreeOfMoveEval cur  = (TreeOfMoveEval)this.get(key);
			TreeOfMoveEval tree = (TreeOfMoveEval)value;
			if(cur.getRelativeDepthToTip() > tree.getRelativeDepthToTip()){
				return null;
			}
		}
		
		this.size++;
		pre = super.put(key, value);
		return pre;
	}
	
	public void refresh(){
		if(this.overMaxSize()){
			this.deleteSome();
		}
	}
	
	
	public void deleteSome(){
		int i = this.sizeToDelete;
		for(K key : super.keySet()){
			super.remove(key);
			i--;
			this.size--;
			if(i <= 0) break;
		}
	}
	public boolean overMaxSize(){
		return (this.size > this.maxSize) ? true : false;
	}
	
	public Long generateHashKey(Shogi shogi){
		long hashKey = 0;
		
		for(Koma koma : shogi.getKomaList()){
			if(koma.isOnBoard()){
				hashKey += koma.getSquare() * koma.getKindId();
			}else if(koma.isOnStand()){
				int sq = koma.getSquare();
				int base_point = 0;
				if(sq == Stand.STAND_BLACK){
					base_point = SceneHashMap.STAND_BLACK; 
				}else if(sq == Stand.STAND_WHITE){
					base_point = SceneHashMap.STAND_WHITE;
				}
				hashKey += base_point * koma.getKindId() * koma.stand.getCount(koma.getKindId());
			}
		}
		
		if(shogi.kifu.getNextOrder() == Shogi.BLACK){
			hashKey *= NEXT_ORDER_IS_BLACK;
		}else if(shogi.kifu.getNextOrder() == Shogi.WHITE){
			hashKey *= NEXT_ORDER_IS_WHITE;
		}
		return hashKey;
	}
	
	public String generateHashKeyString(Shogi shogi){
		String hashKey = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		ps.printf("%s%s%s%s"
				, shogi.stand[Shogi.WHITE].toStringTrimed()
				, shogi.board.toStringTrimed()
				, shogi.stand[Shogi.BLACK].toStringTrimed()
				, (shogi.kifu.getNextOrder()==Shogi.BLACK)?Shogi.getOrderName(Shogi.BLACK):
					(shogi.kifu.getNextOrder()==Shogi.WHITE)?shogi.getOrderName(Shogi.WHITE):
						shogi.getOrderName(Shogi.ORDER_UNKNOWN)
						);
		hashKey += buff.toString();
		return hashKey;
	}
	
	public String generateHashKeyVerboseString(Shogi shogi){
		String hashKey = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		
		ps.printf("%s\n%s\n%s%s"
				, shogi.stand[Shogi.WHITE].toStringEN()
				, shogi.board.toStringEN()
				, shogi.stand[Shogi.BLACK].toStringEN()
				, (shogi.kifu.getNextOrder()==Shogi.BLACK)?Shogi.getOrderName(Shogi.BLACK):
					(shogi.kifu.getNextOrder()==Shogi.WHITE)?shogi.getOrderName(Shogi.WHITE):
						shogi.getOrderName(Shogi.ORDER_UNKNOWN)
						);
		hashKey += buff.toString();
		return hashKey;
	}
	
	public static final int NEXT_ORDER_IS_BLACK = 1; // なんとなく素数
	public static final int NEXT_ORDER_IS_WHITE= -1; // なんとなく素数
	
	public static final int STAND_BLACK = 3; // なんとなく素数
	public static final int STAND_WHITE = 7;
	
	
}


