package com.yeele.game.shogi.engine;


import java.util.HashMap;
import java.util.Map;

import com.yeele.game.shogi.engine.evaluationitem.EvaluationItemDynamicBattleOnMasu;
import com.yeele.game.shogi.engine.evaluationitem.EvaluationItemDynamicBattleOnMasuWithSuji;
import com.yeele.game.shogi.engine.evaluationitem.EvaluationItemKomaBond;
import com.yeele.game.shogi.engine.evaluationitem.EvaluationItemKomaValueOnBoard;
import com.yeele.game.shogi.engine.evaluationitem.EvaluationItemKomaValueOnStand;
import com.yeele.game.shogi.engine.evaluationitem.EvaluationItemKomaWork;
import com.yeele.game.shogi.engine.evaluationitem.EvaluationItemStaticBattleOnMasu;
import com.yeele.game.shogi.engine.player.Player;


public class Evaluator{
	
	private Shogi shogi;
	private int m_stage;
	
	// EvaluationItem
	EvaluationItemKomaValueOnBoard evaluationItemKomaValueOnBoard;
	EvaluationItemKomaValueOnStand evaluationItemKomaValueOnStand;
	EvaluationItemStaticBattleOnMasu evaluationItemStaticBattleOnMasu;
	EvaluationItemDynamicBattleOnMasu evaluationItemDynamicBattleOnMasu;
	
	EvaluationItemDynamicBattleOnMasuWithSuji evaluationItemDynamicBattleOnMasuWithSuji; 
	
	EvaluationItemKomaBond evaluationItemKomaBond;
	EvaluationItemKomaWork evaluationItemKomaWork;
	
	
	public Evaluator(Shogi shogi){
		this.shogi = shogi;
		this.m_stage = STAGE_OPENNING_GAME;
		
		/****** EvaluationItem ******/
		// 駒価値
		this.evaluationItemKomaValueOnBoard = new EvaluationItemKomaValueOnBoard(this);
		this.evaluationItemKomaValueOnStand = new EvaluationItemKomaValueOnStand(this);
		
		this.evaluationItemKomaWork = new EvaluationItemKomaWork(this);
		
		// 升価値
		this.evaluationItemStaticBattleOnMasu = new EvaluationItemStaticBattleOnMasu(this);
		this.evaluationItemDynamicBattleOnMasu = new EvaluationItemDynamicBattleOnMasu(this);
		
		this.evaluationItemDynamicBattleOnMasuWithSuji = new EvaluationItemDynamicBattleOnMasuWithSuji(this);
		
		//　駒の働き
		this.evaluationItemKomaBond = new EvaluationItemKomaBond(this);
	}
	
	public Evaluation evaluate(int type){
		
		int currentOrder = this.shogi.kifu.getCurrentOrder();
		int nextOrder = Shogi.getOtherOrder(currentOrder);
		Evaluation eval = new Evaluation(nextOrder);
		
		if(type == TYPE_STATIC_SHALLOW){
			evaluateStaticallyShallow(eval);
		}else if(type == TYPE_STATIC_DEEP){
			evaluateStaticallyDeep(eval);
		}else if(type == TYPE_DYNAMIC){
			evaluateDynamically(eval);
		}
		return eval;
	}
	
	public int evaluate(Move move){
		
		assert(this.shogi.compensate(move));
		int score = 0;
		score += (move.flip == Koma.FLIP_UNKNOWN) ? 0 : this.getPromotionValue(move.komaToMove);
		
		// 下の処理はその下のgetBattle...に含まれる。
		//score += this.getValueOnStand(move.komaToCapture);
		
		if(Board.isSquareValid(move.from)){
			Masu masuTo = this.shogi.board.getMasu(move.to);
			Koma komaToMove = this.shogi.board.getKoma(move.from);
			if(masuTo != null){
				score += masuTo.getBattleResultInitiatedBy(komaToMove, this);
			}
		}else if(Stand.isStandValid(move.from)){
			Masu masuTo = this.shogi.board.getMasu(move.to);
			if(masuTo != null){
				score += masuTo.getBattleResultInitiatedBy(Shogi.getOtherOrder(move.order), this);
				// 打ったら直ぐ取られる場合は、打つ駒価値を引く
				if(masuTo.isDirectlyReferredBy(Shogi.getOtherOrder(move.order))){
					score -= this.getValueOnStand(move.komaToMove);
				}
			}
		}
		
		return score;
	}
	
	public int getTotalSujiScore(Move move, HashMap<Integer, Integer> sujiScoreMap){
		int score = 0;
		if(move != null && move.extension != null){
			for(Map.Entry<Integer, Boolean> entry : move.extension.sujiCorrespondanceMap.entrySet()){
				Integer key_sujiId = entry.getKey();
				Boolean val_bool = entry.getValue();
				if(val_bool){
					if(sujiScoreMap.containsKey(key_sujiId)){
						score += sujiScoreMap.get(key_sujiId);
					}
				}
			}
		}
		return score;
	}
	
	public final int getStage(){
		return this.m_stage;
	}
	public Shogi getShogi(){
		return this.shogi;
	}
	
	public void updateStage(){
		this.m_stage = STAGE_OPENNING_GAME;
		
		if((this.shogi.stand[Shogi.BLACK].getCountOtherThanFu() >= 2) ||
		   (this.shogi.stand[Shogi.WHITE].getCountOtherThanFu() >= 2) ){
			this.m_stage = STAGE_MIDDLE_GAME;
		}
		
		Player ply1 = this.shogi.getPlayer(Shogi.BLACK);
		Player ply2 = this.shogi.getPlayer(Shogi.WHITE);
		if(ply1 != null){
			if(this.shogi.board.getCountInArea(ply1.getOrder(), ply1.getOpponetTeritory()) > 2){
				this.m_stage = STAGE_END_GAME;
			}
		}
		if(ply2 != null){
			if(this.shogi.board.getCountInArea(ply2.getOrder(), ply2.getOpponetTeritory()) > 2){
				this.m_stage = STAGE_END_GAME;
			}
		}
	}
	
	private void evaluateStaticallyShallow(Evaluation eval){
		
		for(int i=0; i < Board.MATRIX_SIZE; i++){
			Masu masu = this.shogi.board.getMasu(Board.SQUARES[i]);
			Koma koma = this.shogi.board.getKoma(Board.SQUARES[i]);
			
			//　盤上駒価値
			this.evaluationItemKomaValueOnBoard.evaluate(eval, koma);
			this.evaluationItemKomaWork.evaluate(eval, koma);
		}
		
		// 駒置場駒価値
		this.evaluationItemKomaValueOnStand.evaluate(eval);
		
	}
	
	private void evaluateStaticallyDeep(Evaluation eval){
		
		for(int i=0; i < Board.MATRIX_SIZE; i++){
			Masu masu = this.shogi.board.getMasu(Board.SQUARES[i]);
			Koma koma = this.shogi.board.getKoma(Board.SQUARES[i]);
			
			//　盤上駒価値
			this.evaluationItemKomaValueOnBoard.evaluate(eval, koma);
			// 静的升バトル評価
			this.evaluationItemStaticBattleOnMasu.evaluate(eval, masu);
		}
		
		// 駒置場駒価値
		this.evaluationItemKomaValueOnStand.evaluate(eval);
		
	}
	
	private void evaluateDynamically(Evaluation eval){
		
		Evaluation curEval = this.evaluate(Evaluator.TYPE_STATIC_SHALLOW);
		
		for(int i=0; i < Board.MATRIX_SIZE; i++){
			Koma koma = this.shogi.board.getKoma(Board.SQUARES[i]);
			//　盤上駒価値
			this.evaluationItemKomaValueOnBoard.evaluate(eval, koma);
		}
		// 駒置場駒価値
		this.evaluationItemKomaValueOnStand.evaluate(eval);

		
		// この動的評価を一旦、駒価値評価が終わった後に呼ばれないといけない。
		// TODO: ループの無駄使い。
		for(int i=0; i < Board.MATRIX_SIZE; i++){
			Masu masu = this.shogi.board.getMasu(Board.SQUARES[i]);
			// 動的升バトル評価
			this.evaluationItemDynamicBattleOnMasuWithSuji.evaluate(eval, masu, curEval);
		}
	}
	
	public int getKomaValue(Koma koma){
		int val = 0;
		if(koma.isOnBoard()){
			val = this.getValueOnBoard(koma.getKindId());
		}else if(koma.isOnStand()){
			val = this.getValueOnStand(koma.getKindId());
		}
		return val;
	}
	
	public int getKomaValueUnpromoted(Koma koma){
		int val = 0;
		if(koma.isOnBoard()){
			val = this.getValueOnBoard(Koma.getUnpromotedPiece(koma.getKindId()));
		}else if(koma.isOnStand()){
			val = this.getValueOnStand(Koma.getUnpromotedPiece(koma.getKindId()));
		}
		return val;
	}
	
	public int getValueOnBoard(int kindId){
		return Evaluator.komaValOnBoard[this.m_stage][kindId];
	}
	
	public int getValueOnStand(int kindId){
		return Evaluator.komaValOnStand[this.m_stage][kindId];
	}
	
	public int getPromotionValue(int kindId){
		return Evaluator.promotionVal[this.m_stage][Koma.getUnpromotedPiece(kindId)];
	}
	

	
	

	
	/*
	**********************************************************************
	               static methods
	**********************************************************************
	*/
	static public final int TYPE_STATIC_SHALLOW = 1;
	static public final int TYPE_STATIC_DEEP = 2;
	static public final int TYPE_DYNAMIC = 3;
	
	static public final String CASE_ONBOARD = "OnBoard";
	static public final String CASE_ONSTAND = "OnStand";
	static public final String CASE_BACKUP = "Backup";
	static public final String CASE_OU_POSITION = "OuPosition";
	static public final String CASE_OUDEFENSE = "OuDefense";
	static public final String CASE_KOMA_BOND = "KomaBond";
	static public final String CASE_OFFENSE = "Offense";
	static public final String CASE_CHASEOFF = "ChaseOff";
	static public final String CASE_TADA = "Tada";
	static public final String CASE_EACHMASU = "EachMasu";
	static public final String CASE_FORCE_BACKUP = "ForceBackup";
	
	// this STAGE_xxx is index of multi array contains values of
	// koma in each stage.
	public final static int STAGE_UNKNOWN = -1;
	public final static int STAGE_OPENNING_GAME = 0;
	public final static int STAGE_MIDDLE_GAME = 1;
	public final static int STAGE_END_GAME = 2;
	
	public static String getStageDescription(int stageId){
		String description = "Stage Unknown...";
		switch(stageId){
		case STAGE_OPENNING_GAME : description = "Openning Stage"; break;
		case STAGE_MIDDLE_GAME : description = "MIDDLE Stage"; break;
		case STAGE_END_GAME : description = "End Stage"; break;
		default: break;
		}
		return description;
	}
	
	public final static int [][] komaValOnBoard = {
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8,   9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU,  TO,  NY,  NK,  NG,  ,  UM,  RY  */
	{   0,  87, 235, 254, 371, 447, 571, 647,15000, 530, 482, 500, 489, 0, 832, 955 } // open
  , {   0,  87, 235, 254, 371, 447, 571, 647,15000, 530, 482, 500, 489, 0, 832, 955 } // mid
  , {   0,  87, 235, 254, 371, 447, 571, 647,15000, 530, 482, 500, 489, 0, 832, 955 } // end
	};  
	
	public final static int [][] komaValOnStand = {
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8    9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU   TO,  NY,  NK,  NG,   -  ,  UM,  RY  */
	{   0,  87, 235, 254, 371, 447, 571, 647,99999   , 87, 235, 254, 371, 447, 571, 647} // open
  , {   0,  87, 235, 254, 371, 447, 571, 647,99999   , 87, 235, 254, 371, 447, 571, 647} // mid
  , {   0, 174, 470, 254, 742, 447,1142,1294,99999   , 87, 235, 254, 371, 447, 571, 647} // end  // double
	}; 

	public final static int [][] promotionVal= { // difference from promoted koma - unpromoted koma.
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8    , 9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU  */
	{   0,   0,   0,   0,   0,   0,   0,   0,    0 // since these are not yet promoted
		
	, komaValOnBoard[0][Koma.TO] - komaValOnBoard[0][Koma.FU]
	, komaValOnBoard[0][Koma.NY] - komaValOnBoard[0][Koma.KY]
	, komaValOnBoard[0][Koma.NK] - komaValOnBoard[0][Koma.KE]
	, komaValOnBoard[0][Koma.NG] - komaValOnBoard[0][Koma.GI]
	, komaValOnBoard[0][Koma.KI] - komaValOnBoard[0][Koma.KI] // == 0
	, komaValOnBoard[0][Koma.UM] - komaValOnBoard[0][Koma.KA]
	, komaValOnBoard[0][Koma.RY] - komaValOnBoard[0][Koma.HI]
	, komaValOnBoard[0][Koma.OU] - komaValOnBoard[0][Koma.OU]   } // open
  , {   0,   0,   0,   0,   0,   0,   0,   0,    0 // since these are not yet promoted
	, komaValOnBoard[1][Koma.TO] - komaValOnBoard[1][Koma.FU]
	, komaValOnBoard[1][Koma.NY] - komaValOnBoard[1][Koma.KY]
	, komaValOnBoard[1][Koma.NK] - komaValOnBoard[1][Koma.KE]
	, komaValOnBoard[1][Koma.NG] - komaValOnBoard[1][Koma.GI]
	, komaValOnBoard[1][Koma.KI] - komaValOnBoard[1][Koma.KI] // == 0
	, komaValOnBoard[1][Koma.UM] - komaValOnBoard[1][Koma.KA]
	, komaValOnBoard[1][Koma.RY] - komaValOnBoard[1][Koma.HI]
	, komaValOnBoard[1][Koma.OU] - komaValOnBoard[1][Koma.OU]   } // mid
  , {   0,   0,   0,   0,   0,   0,   0,   0,    0 // since these are not yet promoted
	, komaValOnBoard[2][Koma.TO] - komaValOnBoard[2][Koma.FU]
	, komaValOnBoard[2][Koma.NY] - komaValOnBoard[2][Koma.KY]
	, komaValOnBoard[2][Koma.NK] - komaValOnBoard[2][Koma.KE]
	, komaValOnBoard[2][Koma.NG] - komaValOnBoard[2][Koma.GI]
	, komaValOnBoard[2][Koma.KI] - komaValOnBoard[2][Koma.KI] // == 0
	, komaValOnBoard[2][Koma.UM] - komaValOnBoard[2][Koma.KA]
	, komaValOnBoard[2][Koma.RY] - komaValOnBoard[2][Koma.HI]
	, komaValOnBoard[2][Koma.OU] - komaValOnBoard[2][Koma.OU]   } // end  // double
	}; 
	
	public final static int [][] forceBackup = {
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8,   9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU,  TO,  NY,  NK,  NG,  ,  UM,  RY  */
	{   0,   3,   2,   2,   1,   1,   1,   1,   -1,   1,   1,   1,   1, 0,   1,   1   } // open // because attacking is important too.
  , {   0,   3,   2,   2,   1,   1,   1,   1,   -1,   1,   1,   1,   1, 0,   1,   1   } // mid
  , {   0,   3,   2,   2,   1,   1,   1,   1,   -1,   1,   1,   1,   1, 0,   1,   1   } // end
	};
		
	public final static int [][] komaValBackup = {
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8,   9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU,  TO,  NY,  NK,  NG,  ,  UM,  RY  */
	{   0,   1,   1,   1,   1,   1,   1,   1,    1,   1,   1,   1,   1, 0,   5,   2   } // open // because attacking is important too.
  , {   0,   3,   3,   3,   3,   4,   3,   3,    2,   4,   4,   4,   4, 0,   5,   2   } // mid
  , {   0,   6,   6,   6,   6,   8,   6,   6,    4,   8,   8,   8,   8, 0,  10,   4   } // end
	};
	
	public final static int [][] komaValOuProtection = {
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8,   9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU,  TO,  NY,  NK,  NG,  ,  UM,  RY  */
	{   0,   8,   6,   3,  10,  12,   4,   4,    0,  15,  14,  13,  13, 0,  15,  14   } // open
  , {   0,   8,   6,   3,  10,  12,   4,   4,    0,  15,  14,  13,  13, 0,  15,  14   } // mid
  , {   0,   8,   6,   3,  10,  12,   4,   4,    0,  15,  14,  13,  13, 0,  15,  14   } // end
	};
	
	public final static int [][] komaValOffense = {
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8,   9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU,  TO,  NY,  NK,  NG,  ,  UM,  RY  */
	{   0,   3,   4,   5,   6,   7,   8,   9,   12,   8,   8,   8,   8, 0,  10,  11   } // open
  , {   0,   3,   4,   5,   6,   7,   8,   9,   12,   8,   8,   8,   8, 0,  10,  11   } // mid
  , {   0,   3,   4,   5,   6,   7,   8,   9,   12,   8,   8,   8,   8, 0,  10,  11   } // end
	};
	
	public final static int [][] komaValTada = {
	/*  0,   1,     2,     3,     4,     5,     6,     7,      8,     9,    10,    11,    12,13,    14,    15  */
	/* EM,  FU,    KY,    KE,    GI,    KI,    KA,    HI,     OU,    TO,    NY,    NK,    NG,  ,    UM,    RY  */
	{   0,  87,   235,   254,   371,   447,   571,   647,15000,    530,    482,   500,   489, 0,   832,   955 } // open
  , {   0,  87,   235,   254,   371,   447,   571,   647,15000,    530,    482,   500,   489, 0,   832,   955 } // mid
  , {   0,  87,   235,   254,   371,   447,   571,   647,15000,    530,    482,   500,   489, 0,   832,   955 } // end
	};
			
	
	public final static int [][] komaValChaseOff = {
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8,   9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU,  TO,  NY,  NK,  NG,  ,  UM,  RY  */
	{   0,   0,   0,   10,  5,   5,  50,  60,  100,  40,  40,  40,  40, 0,  60,  70   } // open
  , {   0,   0,   0,   10,  5,   5,  50,  60,  100,  40,  40,  40,  40, 0,  60,  70   } // mid
  , {   0,   0,   0,   10,  5,   5,  50,  60,  100,  40,  40,  40,  40, 0,  60,  70   } // end
	};
	
	
	public final static int [][] remainedKomaValOnBattleMasu = {
	/*  0,   1,   2,   3,   4,   5,   6,   7,    8,   9,  10,  11,  12,13,  14,  15  */
	/* EM,  FU,  KY,  KE,  GI,  KI,  KA,  HI,   OU,  TO,  NY,  NK,  NG,  ,  UM,  RY  */
	{   0,   1,   1,   1,   2,   2,   5,   6,    0,   3,   3,   3,   3, 0,   7,   8   } // open
  , {   0,   1,   1,   1,   2,   2,   5,   6,    0,   3,   3,   3,   3, 0,   7,   8   } // mid
  , {   0,   1,   1,   1,   2,   2,   5,   6,    0,   3,   3,   3,   3, 0,   7,   8   } // end
	};
	
			
	
}



