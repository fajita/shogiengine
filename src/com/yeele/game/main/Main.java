package com.yeele.game.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.player.Player;
import com.yeele.game.shogi.engine.player.PlayerCom4;
import com.yeele.game.shogi.engine.player.PlayerHuman;
import com.yeele.game.shogi.exceptions.ShogiException;
import com.yeele.util.TimeConcierge;

import static com.yeele.game.shogi.engine.util.Logger.Logger;


public class Main{
	
	public static void main(String [] argv){

		Logger.setLevel(Logger.LEVEL_DEBUG);
		//Logger.setLevel(Logger.LEVEL_INFO);
		//Logger.setLevel(Logger.LEVEL_WARN);
		//Logger.setLevel(Logger.LEVEL_ERROR);
		
		Shogi.MODE_DEBUG = false;
		
		if(false){
			//Test.test();
			//Test.test_map();
			//Test.test_only_one_turn();
			//Test.test_rotatedSquare();
			//Test.test_handsToReach();
			//Test.test_getPrecedingMovesFor2();
			//Test.test_getPrecedingMovesFor();
			//Test.test_ArrayListS();
			//Test.test_logger();
			//Test.test_suji_Fu_De_Fusage();
			//Test.test_case();
			Test.test_kifu();
			return;
		}
		
		TimeConcierge concierge = new TimeConcierge();
		
		Logger.info("---------------- SHOGI -----------------\n\n");
		
		Player player1 = new PlayerHuman(Shogi.ORDER_UNKNOWN, "mako");
		// can think 3 moves per a read and 8 depth makes 6561 moves at maximum.
		Player player2 = new PlayerCom4(Shogi.ORDER_UNKNOWN, "sachi", 6, 5); 
		//Player player2 = new PlayerHuman("sachi"); // can think 3 moves ahead.
		
		// Decide the order by koma-furi. etc
		player1.setOrder(Shogi.BLACK);
		player2.setOrder(Shogi.WHITE);
		
		Shogi shogi = null;
		try {
			shogi = new Shogi();
		} catch (ShogiException e) {
			Logger.error("failed to instanciate Shogi.....");
		}
		
		
		try{
			String basepath = System.getProperty("user.dir");
			//String kifuDirPath = basepath + "/kifu/";
			String kifuDirPath = new File(basepath, "/kifu/").getPath();
			
			//shogi = new Shogi(new FileInputStream(new File(kifuDirPath, "test1.csa").getPath()));
			//shogi = new Shogi(new FileInputStream(new File(kifuDirPath, "test1.csa").getPath()));
			//shogi = new Shogi(new FileInputStream(new File(kifuDirPath, "test3.csa").getPath()));
			//shogi = new Shogi(new FileInputStream(new File(kifuDirPath, "tumiso.csa").getPath()));
			shogi = new Shogi(new FileInputStream(new File(kifuDirPath, "last_move_takes_100000_read_takes69secs.csa").getPath()));
			
		}catch(FileNotFoundException e){
			Logger.error("Error: " + e.getMessage());
		}catch(IOException e){
			Logger.error("Error: " + e.getMessage());
		}catch(ShogiException e) {
			Logger.error("failed to instanciate Shogi.....");
		}
		
		
		shogi.setPlayer(player1);
		shogi.setPlayer(player2);
		
		
		
		Logger.info("%s\n", shogi);
		
		
		int ret;
		do{
			ret = menu(shogi);
		}while(ret == Shogi.SUCCESS);
		
		
		concierge.measure();
		Logger.info("concierge: \n%s\n", concierge);
	}
	
	public static int menu(Shogi shogi){
		int ret = Shogi.SUCCESS;
		Logger.warn(
		"-----------------------------------------------------------------------------\n" +
		"1. play one turn  " +
		"2. see masu at  " +
		"3. see board  " +
		"4. see kifu  " +
		"\n" +
		"5. back  " +
		"6. forward  " +
		"\n" +
		"7. evaluate (shallow) \n" +
		"8. evaluate (deep) \n" +
		"9. evaluate (dynamic) \n" +
		"10. save kifu \n" +
		"99. quit\n" +
		"-----------------------------------------------------------------------------\n" +
		"Enter: "
		);
		InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        int selected = getInputInt();
        Logger.warn("\n");
        
        switch(selected){
        case 1: ret = one_turn(shogi); break;
        case 2: see_masu_at(shogi); break;
        case 3: show_info(shogi); break;
        case 4: see_kifu(shogi); break;
        case 5: back(shogi); break;
        case 6: ret = forward(shogi); break;
        case 7: ret = evaluate(shogi, Evaluator.TYPE_STATIC_SHALLOW); break;
        case 8: ret = evaluate(shogi, Evaluator.TYPE_STATIC_DEEP); break;
        case 9: ret = evaluate(shogi, Evaluator.TYPE_DYNAMIC); break;
        case 10: ret = save_kifu(shogi); break;
        case 99: ret = Shogi.ERROR; break;
        default: break;
        }
		return ret;
	}
	
	public static int one_turn(Shogi shogi){
		int ret = Shogi.SUCCESS;
		/* black then white */
		for(Player player: shogi.player){
			
			Move mv = null;
			mv = player.generate();
			if(mv == null) return Shogi.ERROR;
			
			ret = shogi.move(mv, true, false);
			if(ret == Shogi.ERROR){
				// invalid move!!
				Logger.error("Error: Failed to generate a move!\n");
				ret = Shogi.ERROR;
				
			}else{
				Logger.warn("MOVE: %s\n", mv);
				Logger.warn("Board:\n%s\n", shogi.toString());
				Player com = shogi.getPlayer("sachi");
				Logger.warn("Evaluation:\n%s\n", com.evaluate(Evaluator.TYPE_STATIC_SHALLOW));
				Logger.warn("Move Analysis:\n%s\n", com.toString(com.analyze(mv)));
				if(ret == shogi.CHECKMATED){
					break;
				}
			}
		}
        return ret;
	}
	
	public static void pause(){
		InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        Logger.warn("hit something to go on ...");
        try{
        	String str = br.readLine();
        }catch(IOException ioe){
        	
        }
	}
	
	public static int getInputInt(){
		int ret = -1;
		InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        while(true){
        	try{
            	String str = br.readLine();
            	ret = Integer.parseInt(str);
            	break;
            }catch(Exception e){
            	Logger.error("\nEnter again: ");
            }
        }
        return ret;
	}
	
	public static String getInputString(){
		String str;
		InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        while(true){
        	try{
            	str = br.readLine();
            	break;
            }catch(Exception e){
            	Logger.error("\nEnter again: ");
            }
        }
        return str;
	}
	
	public static Move input_your_move(){
		
		Move mv = null;
		
		InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        Logger.warn("eg.+ 33 34 FU\n");
        Logger.warn("input your move: ");
        boolean out = false;
        int bw = -1, from = 0, to = 0;
        String komaname = "";
        
        while(true){
        	try{
            	String str = br.readLine();
            	String [] strs = str.split("\\s");
            	if(strs.length == 4){
            		/////////////////////////////////////////////////////////
            		if(strs[0].equals("Q")){
            			out = true; break; // this will terminate the game.
            		}////////////////////////////////////////////////////////
            		
            		if(strs[0].equals("+")) bw = Shogi.BLACK;
            		else if(strs[0].equals("-")) bw = Shogi.WHITE;
            		else throw new Exception(); 
            		from = Integer.parseInt(strs[1]);
            		to   = Integer.parseInt(strs[2]);
            		komaname = strs[3].trim().toUpperCase();
            		mv = new Move(bw, from, to, Koma.getKomaKind(komaname), Koma.FLIP_UNKNOWN, Koma.UK);
            		out = true;
            	}else{
            		Logger.error("%d arguments is invalid!! \n", strs.length);
            	}
            }catch(IOException ioe){
            	Logger.error("invalud input!!\n");
            }catch(Exception e){
            	Logger.error("error while parsing!!\n");
            }
        	if(out) break;
        }
        
        return mv;
	}
	
	public static void see_masu_at(Shogi shogi){
		Logger.warn("Enter square: ");
		int square = getInputInt();
		if(Board.isSquareValid(square)){
			Logger.warn("%s\n", shogi.board.getMasu(square).toString());
			if(shogi.board.getMasu(square).koma != null){
				Logger.warn("%s\n", shogi.board.getMasu(square).koma);
			}
			
		}else{
			Logger.error("%d is invalid for square on board.\n", square);
		}
	}
	
	public static void show_info(Shogi shogi){
		Logger.warn("%s\n", shogi);
	}
	
	public static void see_kifu(Shogi shogi){
		Logger.warn("%s\n", shogi.kifu);
	}
	
	public static void back(Shogi shogi){
		shogi.back();
		Logger.warn("Board:\n%s\n", shogi.toString());
	}
	
	public static int forward(Shogi shogi){
		int ret = Shogi.SUCCESS;
		ret = shogi.forward();
		Logger.warn("Board:\n%s\n", shogi.toString());
		return ret;
	}
	
	public static int evaluate(Shogi shogi, int type){
		int ret = Shogi.SUCCESS;
		Evaluator evaluator = new Evaluator(shogi);
		Evaluation e = evaluator.evaluate(type);
		Logger.warn("Evaluation:\n%s\n", e);
		return ret;
	}
	
	public static int save_kifu(Shogi shogi){
		int ret = Shogi.SUCCESS;
		Logger.warn("Enter file name: ");
		String filename = getInputString();
		
		String basepath = System.getProperty("user.dir");
		try{
			FileOutputStream fouts = new FileOutputStream(basepath + "/kifu/" + filename);
			shogi.kifu.write(fouts);
			fouts.close();
		}catch(FileNotFoundException e){
			Logger.error("Error: " + e.getMessage());
		}catch(IOException e){
			Logger.error("Error: " + e.getMessage());
		}
		return ret;
	}
	
}


/**************************************************************************************

TODO List:
______________________________________________________________________________________
Status			Created		Content
______________________________________________________________________________________

Done			2012/07/03	局面のハッシュテーブル実装（処理時間の高速化を図る）
Impossible		2012/06/xx	よく使われる関数のinline化(e.g. getMasu())
							final,staticにしたりやれることはやったが無理だった。というよりも
							インライン化されたかどうかの確認が困難だった。eclipse標準のコンパイラでなく他のコンパイラで
							オプション付きでコンパイルすれば確認できるらしいが、そこまで手を回せていない。
just came up	2012/07/04	Moveのメンバーをオブジェクトにしたろか？getMasuとかのコスト削減、もちkomaToMoveとかもKomaオブジェクトに。
				2012/07/04  Moveのextensionのsujiマップもplayerの筋への参照にしたろか？ちょっと相互参照で複雑か!?
Done 			2012/07/30	this.updateCapableOfKomaOnStand();を最適化、差分を埋める
New				2012/07/31	HashCodeを短く、さらにstring.hashCode採用
New				2012/07/31	optimize getCountOpponentInArea()
Done			2012/07/31	hold oppoenet in Koma instead of accessing each time with getOtherOrder(order)	
Bug				2012/08/01	どうも駒置きにある駒のmovesが狂って来る。lastMove.toを省くところがあやしいか？
Done			2012/08/02	movableとmoveは同時に更新したほうがよい。現状、movesが重複しているようだ。
							さらにmovableは綺麗に更新されているがmovesは追加されつづけているので悲惨にことに
							これはcriticalなバグだ。
Done			2012/08/03	canBePromotedInNextHandForFree 系の処理は全部loop of movalbeだから
							一緒にして高速化を計る
New 			2012/08/08	内部からのgetter setterの仕様は避け、直接変数を使う。なのでm_ メンバ変数を意味する
							m_VariableNameの法則で命名したほうがよいかも。
Improve			2012/08/10	王手されたとき、王手を受ける以外の手が生成されてしまっている。
Bug				2012/08/10  KEが成らずで29へ(refer. ke_should_promote.csa)
evaluating		2012/08/10	ShikenBishaの-3243GIがぜんぜん指せない。(narrowdownをチェックすること） 
done			2012/08/10	上のと関係あり、naroowdown(,,,) とevaluate(Move)を再思考しよう。
							得にevaluate(Move) Masu.getBattleResultInitialtedBY(order, evauator)
							のようにevaluatorを私て、実際の点数を返すようにしたほうがいいのでは。
***************************************************************************************/






