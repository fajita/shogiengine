package com.yeele.game.main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import com.yeele.game.shogi.engine.Board;
import com.yeele.game.shogi.engine.Evaluation;
import com.yeele.game.shogi.engine.Evaluator;
import com.yeele.game.shogi.engine.Koma;
import com.yeele.game.shogi.engine.Move;
import com.yeele.game.shogi.engine.MoveEval;
import com.yeele.game.shogi.engine.Shogi;
import com.yeele.game.shogi.engine.kifu.CSAKifu;
import com.yeele.game.shogi.engine.komas.Fu;
import com.yeele.game.shogi.engine.komas.Hi;
import com.yeele.game.shogi.engine.komas.Ke;
import com.yeele.game.shogi.engine.komas.Ky;
import com.yeele.game.shogi.engine.player.Player;
import com.yeele.game.shogi.engine.player.PlayerCom3;
import com.yeele.game.shogi.engine.player.PlayerCom4;
import com.yeele.game.shogi.engine.player.PlayerHuman;
import com.yeele.game.shogi.engine.util.Logger;
import com.yeele.game.shogi.exceptions.ShogiException;
import com.yeele.util.ArrayListS;
import com.yeele.util.TimeConcierge;
import com.yeele.util.Tree;

import static com.yeele.game.shogi.engine.util.Logger.Logger;

public class Test{
	
	public static void test_only_one_turn(){
		//Logger.setLevel(Logger.LEVEL_DEBUG);
		//Logger.setLevel(Logger.LEVEL_INFO);
		//Logger.setLevel(Logger.LEVEL_WARN);
		
		TimeConcierge concierge = new TimeConcierge();
		
		Logger.info("---------------- SHOGI TEST-----------------\n\n");
		
		Player player1 = new PlayerHuman(Shogi.ORDER_UNKNOWN, "mako");
		Player player2 = new PlayerCom4(Shogi.ORDER_UNKNOWN, "sachi", 8, 2); // 2move per read tiems 8 depth=256
		
		// Decide the order by koma-furi. etc
		player1.setOrder(Shogi.BLACK);
		player2.setOrder(Shogi.WHITE);
		
		Shogi shogi = null;
		try {
			shogi = new Shogi();
		} catch (ShogiException e) {
			Logger.error("failed to instanciate Shogi.....");
		}
		shogi.setPlayer(player1);
		shogi.setPlayer(player2);
		
		Logger.info("%s\n", shogi);
		
		for(Player player: shogi.player){
			
			Move mv = null;
			if(player.getType() == Player.PLAYER_HUMAN){
				mv = new Move(player.getOrder(), 27, 26, Koma.FU);
			}else if(player.getType() == Player.PLAYER_COM){
				mv = player.generate();
			}
			
			int ret = shogi.move(mv, true, false);
			if(ret == Shogi.ERROR){
				// invalid move!!
				Logger.error("Error: Failed to generate a move!\n");
				
			}else{
				Logger.warn("MOVE: %s\n", mv);
				Logger.warn("Board:\n%s\n", shogi.toString());
				Logger.warn("Evaluation:\n%s\n", shogi.getPlayer("sachi").evaluate(Evaluator.TYPE_STATIC_SHALLOW));
				Logger.warn("Move Analysis:\n%s\n", shogi.getPlayer("sachi").analyze(mv));
			}
		}
		
		concierge.measure();
		Logger.info("concierge: \n%s\n", concierge);
		
	}
	
	public static void test(){
		Tree<MoveEval> rootTree = new Tree<MoveEval>();
		
		Tree<MoveEval> branch = new Tree<MoveEval>(new MoveEval());
		rootTree.addBranch(branch);
		Tree<MoveEval> branch2_1 = new Tree<MoveEval>(new MoveEval());
		Tree<MoveEval> branch2_2 = new Tree<MoveEval>(new MoveEval());
		Tree<MoveEval> branch2_3 = new Tree<MoveEval>(new MoveEval());
		branch.addBranch(branch2_1);
		branch.addBranch(branch2_2);
		branch.addBranch(branch2_3);
		
		Tree<MoveEval> branch3_1 = new Tree<MoveEval>(new MoveEval());
		Tree<MoveEval> branch3_2 = new Tree<MoveEval>(new MoveEval());
		Tree<MoveEval> branch3_3 = new Tree<MoveEval>(new MoveEval());
		branch2_2.addBranch(branch3_1);
		branch2_2.addBranch(branch3_2);
		branch2_2.addBranch(branch3_3);
		
		branch = new Tree<MoveEval>(new MoveEval());
		rootTree.addBranch(branch);
		
		Logger.info("show tree structure\n");
		Logger.info("%s", rootTree);
	}
	
	public static void test_eval_clone(){
		Evaluation eval = new Evaluation(1, 777, 333);
		eval.board[0][21] += 20;
		eval.detail[0].put(Evaluator.CASE_BACKUP, 29);
		Evaluation copied = eval.clone();
		
		eval.board[0][22] += 3;
		copied.score[0] = 82;
		copied.board[0][23] = 4;
		eval.nextOrder = 3;
		eval.detail[0].put(Evaluator.CASE_EACHMASU, 7);
		Logger.info("how was it?");
		
	}
	
	public static void test_map(){
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "hello");
		map.put(2, "makoto");
		
		HashMap<Integer, String> map2 = new HashMap<Integer, String>();
		map2.put(1, "hello2");
		map2.put(4, "nike");
		
		Logger.info("map=%s\n", map);
		Logger.info("map2=%s\n", map2);
		map.putAll(map2);
		
		Logger.info("map=%s\n", map);
		
	}
	
	public static void test_rotatedSquare(){
		
		Logger.info("roated square for %d is %d\n", 33, Board.get180RotatedSquare(33));
		Logger.info("roated square for %d is %d\n", 45, Board.get180RotatedSquare(45));
		
	}
	
	private static void printHandsToRearch(Koma koma, int sq, int expected){
		int hands = koma.getHandsToReach(sq);
		Logger.info("%s(%d) takes %d (expected=%d) turns for (%d)\n"
				,koma.getKomaName(koma.getKindId())
				, koma.masu.square
				, hands
				, expected
				,sq 
		);
		//assert hands == expected : "error!!";
		if(hands != expected) Logger.error("Error!!\n");
		
		
	}
	public static void test_handsToReach(){
		
		Shogi shogi = null;
		try {
			shogi = new Shogi();
		} catch (ShogiException e) {
			Logger.error("failed to instanciate Shogi.....");
		}
		shogi.move(new Move(Shogi.BLACK, 27, 26, Koma.FU), true, false);
		
		Koma koma = null;
		// Check GI forward
		koma = shogi.board.getKoma(79);
		printHandsToRearch(koma, 66, 3);
		printHandsToRearch(koma, 28, 5);
		printHandsToRearch(koma, 38, 5);
		printHandsToRearch(koma, 69, 2);
		
		// check GI back
		koma = shogi.board.getKoma(39);
		shogi.move(new Move(Shogi.BLACK, 39, 46, Koma.GI), true, false);
		printHandsToRearch(koma, 19, 3);
		printHandsToRearch(koma, 58, 4);
		printHandsToRearch(koma, 59, 3);
		printHandsToRearch(koma, 87, 5);
		
		// Gi white
		koma = shogi.board.getKoma(31);
		printHandsToRearch(koma, 66, 5);
		printHandsToRearch(koma, 62, 3);
		printHandsToRearch(koma, 52, 3);
		
		// hi
		koma = shogi.board.getKoma(82);
		printHandsToRearch(koma, 37, 2);
		// ka
		koma = shogi.board.getKoma(88);
		printHandsToRearch(koma, 21, -1);
		printHandsToRearch(koma, 52, -1);
		printHandsToRearch(koma, 44, 1);
		
		koma.flip();
		printHandsToRearch(koma, 21, 2);
		printHandsToRearch(koma, 52, 3);
		
		// ki
		koma = shogi.board.getKoma(61);
		printHandsToRearch(koma, 21, 4);
		printHandsToRearch(koma, 52, 1);
		// ki back
		shogi.move(new Move(Shogi.WHITE, 61, 55, Koma.KI), true, false);
		printHandsToRearch(koma, 21, 7);
		printHandsToRearch(koma, 52, 3);
		
		// check KE
		koma = shogi.board.getKoma(81);
		printHandsToRearch(koma, 65, 2);
		printHandsToRearch(koma, 55, -1);
		// check KY
		koma = shogi.board.getKoma(19);
		printHandsToRearch(koma, 17, 1);
		printHandsToRearch(koma, 11, 1);
		printHandsToRearch(koma, 55, -1);
		// check OU
		koma = shogi.board.getKoma(59);
		printHandsToRearch(koma, 58, 1);
		printHandsToRearch(koma, 88, 3);
		printHandsToRearch(koma, 28, 3);
		
		shogi.move(new Move(Shogi.BLACK, 59, 56, Koma.OU), true, false);
		printHandsToRearch(koma, 88, 3);
		printHandsToRearch(koma, 28, 3);
		
	}
	
	public static void test_getSquaresWhereYouNeedToBeFor(ArrayListS<Integer> sqs){
		Logger.info("getSquaresWhereYouNeedToBeFor() = ");
		for(Integer i : sqs){
			Logger.info("%d, " ,i);
		}
		Logger.info("\n");
	}
	
	public static void test_getSquaresWhereYouNeedToBeFor(){
		/*
		// FU
		test_getSquaresWhereYouNeedToBeFor(Fu.getSquaresWhereYouNeedToBeFor(55, Shogi.BLACK, false));//56
		test_getSquaresWhereYouNeedToBeFor(Fu.getSquaresWhereYouNeedToBeFor(13, Shogi.WHITE, false));//12
		test_getSquaresWhereYouNeedToBeFor(Fu.getSquaresWhereYouNeedToBeFor(13, Shogi.BLACK, false));//14
		// KY
		test_getSquaresWhereYouNeedToBeFor(Ky.getSquaresWhereYouNeedToBeFor(55, Shogi.BLACK, false));//
		test_getSquaresWhereYouNeedToBeFor(Ky.getSquaresWhereYouNeedToBeFor(13, Shogi.WHITE, false));//
		
		// RY
		test_getSquaresWhereYouNeedToBeFor(Hi.getSquaresWhereYouNeedToBeFor(55, Shogi.BLACK, true));//
		test_getSquaresWhereYouNeedToBeFor(Hi.getSquaresWhereYouNeedToBeFor(13, Shogi.WHITE, true));//
		*/
		// KE
		test_getSquaresWhereYouNeedToBeFor(Ke.getSquaresWhereYouNeedToBeFor(55, Shogi.BLACK, false));//
		test_getSquaresWhereYouNeedToBeFor(Ke.getSquaresWhereYouNeedToBeFor(13, Shogi.WHITE, false));//
	}
	
	
	public static void print_getPrecedingMovesFor(ArrayListS<Move> moves){
		Logger.info("getPrecedingMovesFor() = \n");
		for(Move move : moves){
			Logger.info("%s\n" ,move);
		}
		Logger.info("\n");
	}
	
	public static void test_getPrecedingMovesFor(){
		TimeConcierge concierge = new TimeConcierge();
		Shogi shogi = null;
		try {
			shogi = new Shogi();
		} catch (ShogiException e) {
			Logger.error("failed to instanciate Shogi.....");
		}
		
		//print_getPrecedingMovesFor(shogi.getPrecedingMovesFor(shogi.board.getMasu(32), Shogi.WHITE));
		
		// 飛車先のくずしをテスト
		shogi.move(new Move(Shogi.BLACK, 27, 26, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 82, 42, Koma.HI), true, false);
		shogi.move(new Move(Shogi.BLACK, 26, 25, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 23, 24, Koma.FU), true, false);
		shogi.move(new Move(Shogi.BLACK, 25, 24, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 31, 32, Koma.GI), true, false);
		shogi.move(new Move(Shogi.BLACK, 24, 23, Koma.TO), true, false);
		shogi.move(new Move(Shogi.WHITE, 32, 23, Koma.GI), true, false);
		
		Logger.info("%s\n", shogi);
		print_getPrecedingMovesFor(shogi.getPrecedingMovesFor(shogi.board.getMasu(23), Shogi.WHITE));
		
		
		concierge.measure();
		Logger.info("concierge: \n%s\n", concierge);
	}
	
	public static void test_getPrecedingMovesFor2(){
		TimeConcierge concierge = new TimeConcierge();
		Shogi shogi = null;
		try {
			shogi = new Shogi();
		} catch (ShogiException e) {
			Logger.error("failed to instanciate Shogi.....");
		}
		//print_getPrecedingMovesFor(shogi.getPrecedingMovesFor(shogi.board.getMasu(32), Shogi.WHITE));
		
		// 飛車先のくずしをテスト
		shogi.move(new Move(Shogi.BLACK, 27, 26, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 82, 42, Koma.HI), true, false);
		shogi.move(new Move(Shogi.BLACK, 26, 25, Koma.FU), true, false);
		//shogi.move(new Move(Shogi.WHITE, 23, 24, Koma.FU), true, false);
		//shogi.move(new Move(Shogi.BLACK, 25, 24, Koma.FU), true, false);
		//shogi.move(new Move(Shogi.WHITE, 31, 32, Koma.GI), true, false);
		//shogi.move(new Move(Shogi.BLACK, 24, 23, Koma.TO), true, false);
		
		Player player4 = new PlayerCom4(Shogi.ORDER_UNKNOWN, "cpu", 5, 5);
		player4.setOrder(Shogi.WHITE);
		shogi.setPlayer(player4);
		Evaluation eval = player4.evaluate(Evaluator.TYPE_DYNAMIC); // used in reinforce suji.
		
		//print_getPrecedingMovesFor(shogi.getPrecedingMovesFor(shogi.board.getMasu(23), Shogi.WHITE));
		Logger.info("%s\n", eval);
		Logger.info("%s\n", shogi);
		concierge.measure();
		Logger.info("concierge: \n%s\n", concierge);
	}
	
	public static void test_ArrayListS(){
		
		ArrayListS<Koma> komas = new ArrayListS<Koma>();
		
		Koma k = new Koma(null, Shogi.BLACK, Koma.UK, null, null);
		Logger.info("%s\n", komas.addIfNot(k));
		Logger.info("%s\n", komas.addIfNot(k));
		Logger.info("test_ArrayListS finished.\n");
	}
	
	public static void test_logger(){
		
		Logger.setLevel(Logger.LEVEL_DEBUG);
		int lvl = Logger.getLevel();
		Logger.error("this message is error level.(value = %d)", Logger.LEVEL_ERROR);
		Logger.warn("this message is warn level.(value = %d)", Logger.LEVEL_WARN);
		Logger.info("this message is info level.(value = %d)", Logger.LEVEL_INFO);
		Logger.debug("this message is debug level.(value = %d)", Logger.LEVEL_DEBUG);
	}
	
	public static void test_suji_Fu_De_Fusage(){
		TimeConcierge concierge = new TimeConcierge();
		Shogi shogi = null;
		try {
			shogi = new Shogi();
		} catch (ShogiException e) {
			Logger.error("failed to instanciate Shogi.....");
		}
		
		
		// 飛車先のくずしをテスト
		shogi.move(new Move(Shogi.BLACK, 27, 26, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 82, 42, Koma.HI), true, false);
		shogi.move(new Move(Shogi.BLACK, 26, 25, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 41, 32, Koma.FU), true, false);
		shogi.move(new Move(Shogi.BLACK, 25, 24, Koma.FU), true, false);
		
		// 同歩、同飛車、歩ではじく！
		// この筋を実行すべき！
		
		Player player4 = new PlayerCom4(Shogi.ORDER_UNKNOWN, "cpu", 5, 3);
		player4.setOrder(Shogi.WHITE);
		shogi.setPlayer(player4);
		Evaluation eval = player4.evaluate(Evaluator.TYPE_DYNAMIC); // used in reinforce suji.
		
		//print_getPrecedingMovesFor(shogi.getPrecedingMovesFor(shogi.board.getMasu(23), Shogi.WHITE));
		Logger.info("%s\n", eval);
		Logger.info("%s\n", shogi);
		
		concierge.measure();
		Logger.info("concierge: \n%s\n", concierge);
	}
	
	public static void test_case(){
		
		TimeConcierge concierge = new TimeConcierge();
		
		Logger.info("---------------- SHOGI TEST-----------------\n\n");
		
		Player player1 = new PlayerHuman(Shogi.ORDER_UNKNOWN, "mako");
		Player player2 = new PlayerCom4(Shogi.ORDER_UNKNOWN, "sachi", 5, 4); // max 2^5=1024
		
		// Decide the order by koma-furi. etc
		player1.setOrder(Shogi.BLACK);
		player2.setOrder(Shogi.WHITE);
		
		Shogi shogi = null;
		try {
			shogi = new Shogi();
		} catch (ShogiException e) {
			Logger.error("failed to instanciate Shogi.....");
		}
		shogi.setPlayer(player1);
		shogi.setPlayer(player2);
		
		progress1(shogi);
		
		Logger.info("%s\n", shogi);
		
		int ret;
		do{
			ret = Main.menu(shogi);
		}while(ret == Shogi.SUCCESS);
		
		
		concierge.measure();
		Logger.info("concierge: \n%s\n", concierge);
		
	}
	
	public static void progress1(Shogi shogi){
		shogi.move(new Move(Shogi.BLACK, 27, 26, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 43, 44, Koma.FU), true, false);
		shogi.move(new Move(Shogi.BLACK, 26, 25, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 31, 32, Koma.GI), true, false);
		shogi.move(new Move(Shogi.BLACK, 25, 24, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 23, 24, Koma.FU), true, false);
		shogi.move(new Move(Shogi.BLACK, 28, 24, Koma.HI), true, false);
		shogi.move(new Move(Shogi.WHITE,  1, 23, Koma.FU), true, false);
		shogi.move(new Move(Shogi.BLACK, 24, 28, Koma.HI), true, false);
		shogi.move(new Move(Shogi.WHITE, 82, 42, Koma.HI), true, false);
		shogi.move(new Move(Shogi.BLACK, 77, 76, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 93, 94, Koma.FU), true, false);
		shogi.move(new Move(Shogi.BLACK, 79, 78, Koma.GI), true, false);
		shogi.move(new Move(Shogi.WHITE, 51, 62, Koma.OU), true, false);
		shogi.move(new Move(Shogi.BLACK, 78, 77, Koma.GI), true, false);
		shogi.move(new Move(Shogi.WHITE, 62, 72, Koma.OU), true, false);
		shogi.move(new Move(Shogi.BLACK, 39, 48, Koma.GI), true, false);
		shogi.move(new Move(Shogi.WHITE, 72, 82, Koma.OU), true, false);
		shogi.move(new Move(Shogi.BLACK, 37, 36, Koma.FU), true, false);
		shogi.move(new Move(Shogi.WHITE, 71, 72, Koma.GI), true, false);
		shogi.move(new Move(Shogi.BLACK, 48, 37, Koma.GI), true, false);
		shogi.move(new Move(Shogi.WHITE, 32, 43, Koma.GI), true, false);
		//shogi.move(new Move(Shogi.BLACK, 59, 58, Koma.OU), true, false);

	}
	
	public static void test_kifu(){
		
		Logger.info("testing kifu reader");
		CSAKifu csaReader = new CSAKifu();
		String basepath = System.getProperty("user.dir");
		try{
			//FileInputStream fins = new FileInputStream(basepath + "/kifu/sample.csa");
			FileInputStream fins = new FileInputStream(basepath + "/kifu/hirate.csa");
			csaReader.read(fins);
			fins.close();
		}catch(FileNotFoundException e){
			Logger.error("Error: " + e.getMessage());
		}catch(IOException e){
			Logger.error("Error: " + e.getMessage());
		}
		Logger.info("%s\n", csaReader.toVerboseString());
		
	}
	
}

