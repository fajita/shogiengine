package com.yeele.util;


import java.io.*;

import static com.yeele.game.shogi.engine.util.Logger.Logger;




public class Tree<T> {
	
	public int depth; // which is index 
	                   // root of tree, which is backbone has index of 0.
	public int index; // index of myself as a branch.
	public T fruit;

	
	public Tree<T> trunk;
	public ArrayListS<Tree<T>> branches;
	/*
	 * tree is basically a list of list.
	 * therefore, tree could represents something like this.
	 * 
	  
	  tree
	  berry = null
	  branch ------berry = moveeval, braches ---------- berry = moveeval, branches = null
	          |                                 
	          |----berry = moveeval, branches ------ berry = moveeval, branches = null
	          |                                  |--- berry = moveeval, branches = null
	          |                                  |--- berry = moveeval, branches = null
	          |----berry = movaeval, branches = null
	          
	 */
	
	public Tree(){
		this.depth = 0;
		this.fruit = null;
		this.trunk = null;
		this.branches = new ArrayListS<Tree<T>> ();
	}
	
	public Tree(T a_fruit){
		this.fruit = a_fruit;
		this.trunk = null;
		this.branches = new ArrayListS<Tree<T>> ();
	}
	
	public void addBranch(Tree<T> branch){
		branch.trunk = this;
		branch.depth = this.depth + 1;
		int idx = this.branches.size();
		branch.index = idx;
		this.branches.add(branch);
	}
	
	
	public boolean hasBranch(){
		return this.branches.size() > 0 ? true : false;
	}
	
	public boolean isBranch(){
		return !this.isBackbone();
	}
	
	/*
	 * if trunk is null, it means you are the root of this tree.
	 */
	public boolean isBackbone(){
		return this.trunk == null ? true : false;
	}
	
	public T getFruit(ArrayListS<Integer> indice){
		
		Tree<T> iter = this;
		T a_fruit = iter.fruit;
		
		try{
			for(Integer i : indice){
				iter = iter.branches.get(i);
				a_fruit = iter.fruit;
			}
		}catch(IndexOutOfBoundsException e){
			Logger.error("IndexOutOfBoundsException raised in getFruit()!\n");
			a_fruit = null;
		}
		return a_fruit;
	}
	
	public T getFruit(Integer index){
		
		Tree<T> iter = this;
		T a_fruit = iter.fruit;
		
		try{
			iter = iter.branches.get(index);
			a_fruit = iter.fruit;
		}catch(IndexOutOfBoundsException e){
			Logger.error("IndexOutOfBoundsException raised in getFruit()!\n");
			a_fruit = null;
		}
		return a_fruit;
	}
	
	public T getFruit(){
		return this.fruit;
	}
	
	public void setFruit(T a_fruit){
		this.fruit = a_fruit;
	}
	
	//----------------------------------------------------------------------------------------
	//  Special method for specific Template <MoveEval>
	//----------------------------------------------------------------------------------------
	/*
	 * only Tree<MoveEval> override this method 
	 */
	public boolean searchBestFruit(int order, ArrayListS<T> best, ArrayListS<Integer> indice, int startingDepth){
		return false;
	}
	/*
	 * only Tree<MoveEval> override this method 
	 */
	public int preindicateNextMoves(int nextOrder, ArrayListS<Integer> path){
		return -1;
	}
	
	//----------------------------------------------------------------------------------------
	
	
	
	public void racearound(){
		
		for(Tree<T> branch : this.branches){
			branch.racearound();
		}
		
	}
	
	/*
	 * info has 
	 * starting from the root node, whether it has more branch or not.
	 */
	private String toString_raceAround(ArrayListS<Boolean> info){
		ByteArrayOutputStream buff = new ByteArrayOutputStream();;
		PrintStream ps = new PrintStream(buff);
		String str = "";
		String margin = "";

		ps.printf(" --%2d", this.depth);
		str += buff.toString();
		if(!this.hasBranch()){
			str += "\n";
			margin = "";
			if(this.isBranch()){
				for(Boolean tf : info){
					if(tf == true){
						margin += "    ";
						margin += "|";
					}
				}
			}
			str += margin;
		}else{
			
			int counter = 0;
			int number = this.branches.size();
			for(Tree<T> branch : this.branches){
				counter++;
				if(counter < number){
					info.add(true);
				}else{
					info.add(false);
				}
				str += branch.toString_raceAround(info);
				
			}
		}
		int idx = info.size() - 1;
		if(idx >= 0) info.remove(idx);
		
		return str;
	}
	
	public String toString(){
		ArrayListS<Boolean> info = new ArrayListS<Boolean>(); 
		return this.toString_raceAround(info);
	}
}

