package com.yeele.util;


import java.io.*;
import java.util.ArrayList;




/*
 * S for Shogi
 * ArrayList for Shogi so, abbreviation is ArrayListS :D
 */

public class ArrayListS<T> extends ArrayList<T>{
	
	// return true if added.
	// return false if it wasn't added for any reason.
	public boolean addIfNot(T obj){
		if(this.contains(obj)==false){
			this.add(obj);
			return true;
		}
		return false;
	}
	
	// array list can be added just like adding an element
	public void addIfNot(ArrayListS<T> objList){
		for(T t : objList){
			this.addIfNot(t);
		}
	}
}

