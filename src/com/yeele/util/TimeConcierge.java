package com.yeele.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class TimeConcierge{
	

	public long last, now, diff, _last;
	
	public TimeConcierge(){

		this.now = System.currentTimeMillis();
		this.last = this.now;
	}
	
	public long measure(){
		this.now = System.currentTimeMillis();
		this.diff = this.now - this.last;
		this._last = this.last;
		this.last = this.now;
		return this.diff;
	}
	
	public String toString(){
		String str = "";
		ByteArrayOutputStream buff = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buff);
		ps.printf("Now : %15d ms\n"+
				  "Last: %15d ms\n"+
				  "Diff: %15d ms\n"+
				  "Diff: %15d  S\n", this.now, this._last, this.diff, this.diff/1000);
		str += buff.toString();
		return str;
	}
		
}


